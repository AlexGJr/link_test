#ifndef _GammaMeats_h
#define _GammaMeats_h


//Initial Param Saved
extern char IsInitialParam;
extern unsigned int CurMeasurement;  // acm, unused

extern ROM float GainValue[4];

extern struct stInsulationParameters *Parameters;
extern struct stDiagnosis *Diagnosis;
extern struct stInsulationParameters *TempParam;

extern char GammaStatus;
extern char FullStatus;

extern char MeasurementReloaded;

signed char StartMeasure(char Save,char GammaAvg);
char RunCalibration(float *Phase,float *Freq,float *Ampl,char NoBalans, char Side);
char GetGammaStatus(float Value,char Num,char OldStatus,char AllowAlarms);
void DoSingle(void);
void ClearInitParam(char Side);
char SaveCurrentMeasurement(void);
void OutRelay(char Status);
void OutGammaStatus(char GammaStatus,char SkipRelayOut);
void GetFullStatus(char GammaStatus,char Trend,char KT,char *Status,char *AlarmStatus,char Side);
char ReadSourcePhases(float *AmplA,float *AmplB,float *AmplC,float *ABPhaseShift,float *ACPhaseShift, char TrSide, float *Freq,char OnBalance);
void ReadAmlPhase(unsigned int *Ampl, float *Phase, float PshHV, float Freq, char Side);
// acm, 12-1-10, add these to so visible to Main(), needed to clear baseline
//�����  ���������� ������������ ������ 	Number of controlling the measurement
extern unsigned int SaveNum;
extern unsigned int DiagResult;

//void ReadAmlPhase(unsigned int *Ampl,float *Phase,char NoCalibr,float PshHV/*,float ChAmplDiff*/,float Freq);

extern void OutLED(char Status);  // acm, 2-2-2012 gets rid of a linker error
#endif
