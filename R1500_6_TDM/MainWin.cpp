#include "LCD.h"
#include "graph.h"
#include "picres.h"
#include "defs.h"
#include "StrConst.h"
#include "defKB.h"
#include "BoardAdd.h"
#include "Link.h"
#include "KeyBoard.h"
#include "SysUtil.h"

#include "TRSTR.h"
extern char C50;
unsigned int RunMainWin(int IsWait){

 int i;
 KEY ch=KB_NO;

 GET_TIME_INBUF();

 ClearLCD();
 SetFont(Font8x8);
 if(C50)
 {
   OutString(32,0,"C50 BHM");   
 }
 else
 {
#ifdef TDM
 OutString(32,0,DeviceStr);
#else
 OutString(40,0,DeviceStr);
#endif
 }

 SetFont(Font8x6);
 OutString(32,8,VersionStr);
 OutString(80,8,trVersionValue);  // acm, change 3-12 to make consistent with other modules scheme);

// OutPicture(1,1,LogoSM);

 if (IsWait) {
    for (i=0;i<1500;i++) {
        ScanMessages();
        DelayMks(1);
//        Delay(1);
        if (LCDConnected) ch=ReadKey();
        else ch=KB_NO;
        if (ch==KB_ESC) break;
        SetFont(Font8x8);
 if(C50)
 {
   OutString(32,0,"C50 BHM");   
 }
 else
 {
#ifdef TDM
        OutString(32,0,DeviceStr);
#else
        OutString(40,0,DeviceStr);
#endif
 }
        Redraw();
    }
 } else  Redraw();

 return ch;
}


