//---------------------------------------------------------------------------
#ifndef DoItH
#define DoItH
//---------------------------------------------------------------------------
// ����������� ������
#include "rtc.h"
#include "DateTime.h"
#include "Defs.h"
#include "MeasurementDefs.h"
#include "GammaMeasurement.h"
#include "Link.h"
#include "Setup.h"
#include "Error.h"
#include "Archive.h"
#include "LogFile.h"
#include "tgDelta.h"
#include "RAM.h"
#include "FLASH.h"
#include "Diagnosis.h"
#include "SysUtil.h"
#include "LogFile.h"
#include "Diagnosis.h"
#include "WatchDog.h"


#include "Graph.h"
#include "LCD.h"
#include "KeyBoard.h"

#include "TRSTR.h"  // acm, 3-12-10 defines trDeviceType

extern struct StDateTime DateTime;
extern _TDateTime TimeNextMeas;      //����� ���������� ���������� ������

extern unsigned int PausedFrom;
//��������� ���������� ���������
extern signed char MeasurementResult;

char MeasCountInSchedule=0;

void GetNextTime(_TDateTime CurDateTime);

extern void TestSetup(); // acm, do this so Understand resolves

// ���������� ����
void SetDate(long DateValue, char Set)
{
long ltmp;
//YYYYMMDD
GET_TIME_INBUF();
ltmp=DateValue;
DateTime.Year=(unsigned int)(ltmp/(long)10000);
ltmp-=((long)DateTime.Year*(long)10000);
DateTime.Year-=2000;
DateTime.Month=(char)(ltmp/(long)100);
ltmp-=(long)DateTime.Month*(long)100;
DateTime.Day=ltmp;
if (Set) {
   SET_TIME_FROMBUF();
   GetNextTime(DateTimeNowNoSec());
}
}

// ������ DATE YYYYMMDD;
/*
void GetDate(void)
{long l=0;
AddOutConst(&trCommandDATE);
AddOutConst(&trEmptyStr);
GET_TIME();
l=((long)DateTime.Year+(long)2000)*(long)10000;
l+=(long)DateTime.Month*(long)100;
l+=(long)DateTime.Day;
AddOutLong(l);
AddOutEnd();
}
*/


// ���������� �����

/* acm, 1-19-11 more closely match behavior to PDM - return input time wasn't getting returned
void SetTime(long Time)
{long l;
//HHMMSS
l=Time;
DateTime.Hour=(char)(l/(long)10000);
l-=(long)DateTime.Hour*(long)10000;
DateTime.Min=(char)(l/(long)100);
l-=(long)DateTime.Min*(long)100;
DateTime.Sec=l;
SET_TIME_FROMBUF();
GetNextTime(DateTimeNowNoSec());
}*/
void SetTime(long DateValue, long Time)
{long ltmp;

//YYYYMMDD
ltmp=DateValue;
DateTime.Year=(unsigned int)(ltmp/(long)10000);
ltmp-=((long)DateTime.Year*(long)10000);
DateTime.Year-=2000;
DateTime.Month=(char)(ltmp/(long)100);
ltmp-=(long)DateTime.Month*(long)100;
DateTime.Day=ltmp;
//HHMMSS
ltmp=Time;
DateTime.Hour=(char)(ltmp/(long)10000);
ltmp-=(long)DateTime.Hour*(long)10000;
DateTime.Min=(char)(ltmp/(long)100);
ltmp-=(long)DateTime.Min*(long)100;
DateTime.Sec=ltmp;

SET_TIME_FROMBUF();
GetNextTime(DateTimeNowNoSec()); // acm, do this here as orig done with BHM
//RecalcTime=1; // acm, not used here
}

// ������ TIME YYYYMMDD HHMMSS;
void GetTime(void)
{long l;
AddOutConst(trCommandTIME);
AddOutConst(trEmptyStr);
GET_TIME_INBUF();
l=((long)DateTime.Year+(long)2000)*(long)10000;
l+=(long)DateTime.Month*(long)100;
l+=(long)DateTime.Day;
AddOutDate(l);
l=(long)DateTime.Hour*(long)10000;
l+=(long)DateTime.Min*(long)100;
l+=(long)DateTime.Sec;
AddOutTime(l);
AddOutEnd();
}


// DEVICETYPE ?;
void GetDeviceType(void)
{
AddOutConst(trCommandDEVICETYPE);
AddOutConst(trEmptyStr);
AddOutConst(trDeviceType);
AddOutEnd();
}


// VER ?;
void GetVER(void)
{
AddOutConst(trCommandVER);
AddOutConst(trEmptyStr);
AddOutConst(trVersionValue);
AddOutEnd();
}


// ���������� Mode � �������� ��� Time
void SetTreadBegin(int Mode)
{
   Setup.SetupInfo.ScheduleType=Mode;
   MeasCountInSchedule=0;
}


//Set Arch On Day
void SetArchOnDay(char Param1)
{
Setup.SetupInfo.SaveDays=Param1;
}


// �������� Time
void SetTreadDelta(int Time) // HHMM
{ int h,m;

 h=(int)((float)Time/100.0);
 m=Time-(h*100);
 Setup.SetupInfo.dTime.Hour=h;
 Setup.SetupInfo.dTime.Min=m;
}

// �������� Time
void SetTreadAdd(int Time) // HHMM
{ int h,m;

 h=(int)((float)Time/100.0);
 m=Time-(h*100);
 Setup.SetupInfo.MeasurementsInfo[MeasCountInSchedule].Hour=h;
 Setup.SetupInfo.MeasurementsInfo[MeasCountInSchedule].Min=m;
 MeasCountInSchedule++;
 if (MeasCountInSchedule<MaxMeasurementsPerDay) {
    Setup.SetupInfo.MeasurementsInfo[MeasCountInSchedule].Hour=0;
    Setup.SetupInfo.MeasurementsInfo[MeasCountInSchedule].Min=0;
 }
}


void GetTread(void)
{ unsigned int l;
  int i;
AddOutConst(trCommandTREAD);
AddOutConst(trEmptyStr);
AddOutInt(Setup.SetupInfo.ScheduleType);
AddOutInt(Setup.SetupInfo.SaveDays);
l=Setup.SetupInfo.dTime.Hour*100+Setup.SetupInfo.dTime.Min;
AddOutShortTime(l);
for (i=0;i<MaxMeasurementsPerDay;i++) {
    l=Setup.SetupInfo.MeasurementsInfo[i].Hour*100+Setup.SetupInfo.MeasurementsInfo[i].Min;
//    if (!l) break;
//    AddOutInt(l);
    AddOutShortTime(l);
}
AddOutEnd();
}


// SINGLE
void SetStart(void)
{
//if (IsInitialParam)
 DoSingle();
}


//SHOW
void SetShow(char Param1,
             char Param2,
             char Param3,
             char Param4,
             char Param5,
             char Param6,
             char Param7,
             char Param8,
             char Param9,
             char Param10
             )
{

Setup.SetupInfo.OutTime=Param1;
if (Param2) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<0);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<0);
if (Param3) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<1);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<1);
if (Param4) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<2);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<2);
if (Param5) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<3);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<3);
if (Param6) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<4);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<4);
if (Param7) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<5);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<5);
if (Param8) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<6);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<6);
if (Param9) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<7);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<7);
if (Param10) Setup.SetupInfo.DisplayFlag|=(unsigned int)(1<<8);
else        Setup.SetupInfo.DisplayFlag&=~(unsigned int)(1<<8);
}


//SHOW ?
void GetShow(void)
{
AddOutConst(trCommandSHOW);
AddOutConst(trEmptyStr);
AddOutInt(Setup.SetupInfo.OutTime);
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0001));
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0002)>>1);
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0004)>>2);
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0008)>>3);
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0010)>>4);
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0020)>>5);
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0040)>>6);
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0080)>>7);
AddOutInt((Setup.SetupInfo.DisplayFlag&0x0100)>>8);
AddOutEnd();
}


// BUSY ?;
void GetBusy(void)
{
AddOutConst(trCommandBUSY);
AddOutConst(trEmptyStr);
AddOutInt(BusyFlag);
AddOutEnd();
}


// CLEAR;
void SetClear(void)
{
 ClearArchive();
 SaveLog(lcClear);
}


// CLEARALL;
void SetClearAll(void)
{
  ClearAllData();
}


// PAUSE
void PauseOn(void)
{
SetPause();
}

void SetPause(void)  // acm, 7-14-10 move back from Archive.cpp, didn't seem to belong there
{
  /* acm, change to PDM code (simpler)
  extern unsigned int PausedFrom;
  #ifdef __arm
  GET_TIME_INBUF();
  #else
  GET_TIME();
  #endif
  PausedFrom=(unsigned int)DateTime.Hour*(unsigned int)60+(unsigned int)DateTime.Min;
  if (!PausedFrom) PausedFrom=1;
  Error|=((unsigned long)1<<erPause);
  */
  PausedFrom=GetTic32();
  Error|=(1<<erPause);
}

// PAUSE ?
void GetPause(void)
{
/* acm, replace with PDM verison
extern unsigned int PausedFrom;
unsigned int Interval,tmp;

AddOutConst(trCommandPAUSE);
AddOutConst(trEmptyStr);

if (Error&((unsigned long)1<<erPause)) {
   GET_TIME_INBUF();
   tmp=(unsigned int)DateTime.Hour*(unsigned int)60+(unsigned int)DateTime.Min;
   if (tmp>=PausedFrom)
      tmp-=PausedFrom;
   else
      tmp=(MaxMinPerDay-PausedFrom)+tmp;
   tmp=MaxPauseMin-tmp;
   Interval=(tmp/60*100)+tmp%60;
   AddOutInt(Interval);
} else {
  AddOutInt(0);
}
AddOutEnd();
*/

extern _TDateTime PausedFrom;
unsigned int /*Interval,*/ tmp;

AddOutConst(trCommandPAUSE);
AddOutConst(trEmptyStr);

if (Error&(1<<erPause)) {
   tmp=GetTic32();
   tmp=MaxPauseMin-(tmp-PausedFrom)/32/60;
   AddOutLong(tmp/60);
   AddOutLong((tmp%60)/10);
   AddOutLong((tmp%60)%10);
} else {
  AddOutLong(0);
}
AddOutEnd();
}

// RESUME
void SetResume(void)
{
/* acm, replace with PDM code
  Setup.SetupInfo.Stopped=0;
  Error&=~((unsigned long)1<<erStop);
  PausedFrom=0;
  Error&=~((unsigned long)1<<erPause);
*/
extern _TDateTime PausedFrom;

  if(Setup.SetupInfo.Stopped){
    Setup.SetupInfo.Stopped=0;
    //ReSetSetup=1;
  }
  PausedFrom=0;
  Error&=~(1<<erPause);
  Error&=~((unsigned long)1<<erStop);
  AddOutConst(trCommandRESUME);
  AddOutEnd();
}

// STOP
void GetStop(void)
{
  AddOutConst(trCommandSTOP);
  AddOutConst(trEmptyStr);
  if (Setup.SetupInfo.Stopped) AddOutInt(1); else  AddOutInt(0);
  AddOutEnd();
}

// STOP
void SetStop(void)
{
  Setup.SetupInfo.Stopped=1;
  Error|=((unsigned long)1<<erStop);
  GetStop();
}

unsigned long DateToLong(int Year,int Month,int Day)
{
  return (unsigned long)Year*10000+Month*100+Day;
}


unsigned long TimeToLong(int Hour,int Min,int Sec)
{
  return (unsigned long)Hour*10000+Min*100+Sec;
}


// MONDATE;
void GetMeasurementOnDate(unsigned long Date,unsigned long Time)
{
  unsigned int i;
  int MNumber=0;
//  int Cur;
  unsigned long CurDate,CurTime;
//  unsigned int Page,Shift;
  extern struct stInsulationParameters *TempParam;
#ifndef __arm
  char Page=GetPage();
#endif

AddOutConst(trCommandMONDATE);
AddOutConst(trEmptyStr);

for (i=0;i<MeasurementsInArchive;i++) {
//ReadFromFLASh
    SetPage(TempPage);
    LoadData(TempParam,MeasurementNumToIndex(MeasurementsInArchive-i),TempPage);
//    LoadArchDate(ArchiveData,MeasurementNumToIndex(MeasurementsInArchive-i),TempPage);

    CurDate=(unsigned long)(2000+(unsigned long)(*TempParam).Year%100)*10000+(unsigned long)(*TempParam).Month*100+(unsigned long)(*TempParam).Day;
    CurTime=(unsigned long)(*TempParam).Hour*10000+(unsigned long)(*TempParam).Min*100;

//    CurDate=DateToLong(2000+(*ArchiveData).Year%100,(*ArchiveData).Month,(*ArchiveData).Day);
//    CurTime=TimeToLong((*ArchiveData).Hour,(*ArchiveData).Min,0);

    if ( (CurDate<Date)||
         ((CurDate==Date)&&(CurTime<=Time))
       )
    {
        break;
    }
    MNumber++;
}
SetPage(Page);
if ((MeasurementsInArchive==0)||(MeasurementsInArchive-MNumber+1>MeasurementsInArchive))
{
   AddOutInt(0);
   AddOutConst(trEmptyStr);
   AddOutInt(0);
} else {
   AddOutInt(MeasurementsInArchive-MNumber+1);
   AddOutConst(trEmptyStr);
   AddOutInt(MeasurementsInArchive);
}
AddOutEnd();
}


// THRESH GROUP GammaRed GammaYellow TempCoefThresh GammaTrendThresh Hysteresis TgRedThresholld TgYellowThresholld TgVariationThresholld;
void SetThresh(char Group,
               float ParamFloat1,
               float ParamFloat2,
               float ParamFloat3,
               float ParamFloat4,
               char  Param2,
               float ParamFloat5,
               float ParamFloat6,
               float ParamFloat7
               )
{
if (Group>1) Group=0;
Setup.GammaSetupInfo.GammaSideSetup[Group].GammaRedThresholld=ParamFloat1/0.05;
Setup.GammaSetupInfo.GammaSideSetup[Group].GammaYellowThresholld=ParamFloat2/0.05;
Setup.GammaSetupInfo.GammaSideSetup[Group].TCoefficient=ParamFloat3*1.0000001/0.002;
Setup.GammaSetupInfo.GammaSideSetup[Group].TrendAlarm=ParamFloat4/1; //acm, 4-27 change from .2 to 1
Setup.GammaSetupInfo.AlarmHysteresis=Param2;
Setup.GammaSetupInfo.GammaSideSetup[Group].TgRedThresholld=(int)(ParamFloat5*100.0);
Setup.GammaSetupInfo.GammaSideSetup[Group].TgYellowThresholld=(int)(ParamFloat6*100.0);
Setup.GammaSetupInfo.GammaSideSetup[Group].TgVariationThresholld=(int)(ParamFloat7*100.0);

//ReCalc Sost
//GetFullStatus(GammaStatus,Parameters.TrSide[HVSide].Trend,Parameters.TrSide[HVSide].KT,&FullStatus,&Parameters.TrSide[HVSide].AlarmStatus);
//OutGammaStatus(FullStatus);
}


// THRESH ?;
void GetThresh(char Group)
{
if (Group!=0) Group=0;
AddOutConst(trCommandTHRESH);
AddOutConst(trEmptyStr);
AddOutInt(Group+1);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].GammaRedThresholld*0.05);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].GammaYellowThresholld*0.05);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].TCoefficient*0.002);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].TrendAlarm*1);//acm, 4-27 change from .2 to 1
AddOutInt(Setup.GammaSetupInfo.AlarmHysteresis);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].TgRedThresholld*0.01);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].TgYellowThresholld*0.01);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].TgVariationThresholld*0.01);
AddOutEnd();
}

// ALARM GROUP RelayON RelayOnTimeOn RelayOnTime TempCoefAlarmON TrendAlarmON TgAlarmON VarTgAlarmON;
void SetAlarm(char Group,
              char Param1,
              char Param2,
              char Param3,
              char Param4,
              char Param5,
              char Param6,
              char Param7
              )
{
#ifndef __arm
char Page=GetPage();
#endif

if (Group>1) Group=0;

if (Param1) {
   if (Param2) Setup.SetupInfo.Relay=2; else Setup.SetupInfo.Relay=1;
}else
   Setup.SetupInfo.Relay=0;
Setup.SetupInfo.TimeOfRelayAlarm=Param3;
if (Param4) Setup.GammaSetupInfo.GammaSideSetup[Group].AlarmEnable|=(1<<1); else Setup.GammaSetupInfo.GammaSideSetup[HVSide].AlarmEnable&=~(1<<1);
if (Param5) Setup.GammaSetupInfo.GammaSideSetup[Group].AlarmEnable|=(1<<2); else Setup.GammaSetupInfo.GammaSideSetup[HVSide].AlarmEnable&=~(1<<2);
if (Param6) Setup.GammaSetupInfo.GammaSideSetup[Group].AlarmEnable|=(1<<3); else Setup.GammaSetupInfo.GammaSideSetup[HVSide].AlarmEnable&=~(1<<3);
if (Param7) Setup.GammaSetupInfo.GammaSideSetup[Group].AlarmEnable|=(1<<4); else Setup.GammaSetupInfo.GammaSideSetup[HVSide].AlarmEnable&=~(1<<4);

//ReCalc Sost
FullStatus=stUnknown;
if ((Setup.GammaSetupInfo.ReadOnSide&(1<<Group))) {
   SetPage(ParametersPage);
   GetFullStatus(GammaStatus,Parameters->TrSide[Group].Trend,Parameters->TrSide[Group].KT,&FullStatus,&Parameters->TrSide[Group].AlarmStatus,Group);
   SetPage(Page);
}
OutGammaStatus(FullStatus,0);
}


// ALARM Group ?;
void GetAlarm(char Group)
{
if (Group>1) Group=0;
AddOutConst(trCommandALARM);
AddOutConst(trEmptyStr);
AddOutInt(Group+1);
if (Setup.SetupInfo.Relay) AddOutInt(1); else AddOutInt(0);
if (Setup.SetupInfo.Relay==2) AddOutInt(1); else AddOutInt(0);
AddOutInt(Setup.SetupInfo.TimeOfRelayAlarm);
if (Setup.GammaSetupInfo.GammaSideSetup[Group].AlarmEnable&(1<<1)) AddOutInt(1); else AddOutInt(0);
if (Setup.GammaSetupInfo.GammaSideSetup[Group].AlarmEnable&(1<<2)) AddOutInt(1); else AddOutInt(0);
if (Setup.GammaSetupInfo.GammaSideSetup[Group].AlarmEnable&(1<<3)) AddOutInt(1); else AddOutInt(0);
if (Setup.GammaSetupInfo.GammaSideSetup[Group].AlarmEnable&(1<<4)) AddOutInt(1); else AddOutInt(0);
AddOutEnd();
}


//SETPARAM GROUP InpImped1 InpImped2 InpImped3 RatedVoltageKV RatedCurrent;
void SetSetParam(char Group,
               float ParamFloat1,
               float ParamFloat2,
               float ParamFloat3,
               float ParamFloat4,
               float ParamFloat5)
{
if (Group>1) Group=0;
Setup.GammaSetupInfo.GammaSideSetup[Group].InputImpedance[0]=ParamFloat1*100;
Setup.GammaSetupInfo.GammaSideSetup[Group].InputImpedance[1]=ParamFloat2*100;
Setup.GammaSetupInfo.GammaSideSetup[Group].InputImpedance[2]=ParamFloat3*100;
Setup.GammaSetupInfo.GammaSideSetup[Group].RatedVoltage=ParamFloat4;
Setup.GammaSetupInfo.GammaSideSetup[Group].RatedCurrent=ParamFloat5;
}

//SETPARAM GROUP ?
void GetSetParam(char Group)
{
if (Group>1) Group=0;
AddOutConst(trCommandSETPARAM);
AddOutConst(trEmptyStr);
AddOutInt(Group+1);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].InputImpedance[0]/100.0);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].InputImpedance[1]/100.0);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].InputImpedance[2]/100.0);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].RatedVoltage);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].RatedCurrent);
}

// MISC GammaTrendDays GammaAveMeas TempCoefCalc
//      UnbalLim DayForDiag DaysToCalculateBASELINE;

void SetMisc(char Param1,
             char Param2,
             char Param3,
             char Param4,
             char Param5,
             char Param6,
            char Param7  // acm, v2.00
//             float ParamFloat1,
//             float ParamFloat2,
//             float ParamFloat3,
//             float ParamFloat4
             )
{
Setup.GammaSetupInfo.DaysToCalculateTrend=Param1;
if (Setup.GammaSetupInfo.DaysToCalculateTrend<MinDaysForTrend) Setup.GammaSetupInfo.DaysToCalculateTrend=MinDaysForTrend;
Setup.GammaSetupInfo.AveragingForGamma=Param2;
//????????????????????????????????????
//Setup.SetupInfo.Free=Param3;
Setup.GammaSetupInfo.DaysToCalculateTCoefficient=Param3;
//Setup.SetupInfo.MaxParameterChange=Param5;
Setup.GammaSetupInfo.AllowedPhaseDispersion=Param4;

if (Param5<MinDaysForDiagnosis) Setup.GammaSetupInfo.MinDiagGamma=MinDaysForDiagnosis;
else Setup.GammaSetupInfo.MinDiagGamma=Param5;
if (Param6<MinDaysForBaseLine) Setup.GammaSetupInfo.DaysToCalculateBASELINE=MinDaysForBaseLine;
else Setup.GammaSetupInfo.DaysToCalculateBASELINE=Param6;

Setup.GammaSetupInfo.AveragingForGamma=Param7;  // acm, v2.00
//Setup.SetupInfo.MinDiagGamma=Param7;
//Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[0]=(unsigned int)(ParamFloat1*100.0);
//Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[1]=(unsigned int)(ParamFloat2*100.0);
//Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[2]=(unsigned int)(ParamFloat3*100.0);
//Setup.GammaSetupInfo.GammaSideSetup[HVSide].RatedVoltage=ParamFloat4;

//Init Date of Calc KT
//InitDateOfCalcKT(EncodeDate(DateTime.Year,DateTime.Month,DateTime.Day));
}


// MISC ?;
void GetMisc(void)
{
AddOutConst(trCommandMISC);
AddOutConst(trEmptyStr);
AddOutInt(Setup.GammaSetupInfo.DaysToCalculateTrend);
AddOutInt(Setup.GammaSetupInfo.AveragingForGamma);
AddOutInt(Setup.GammaSetupInfo.AveragingForGamma_X);  //acm, v2.00
//??????????????????????????????????????????????
//AddOutInt(Setup.SetupInfo.Free);
AddOutInt(Setup.GammaSetupInfo.DaysToCalculateTCoefficient);
//AddOutInt(Setup.SetupInfo.MaxParameterChange);
AddOutInt(Setup.GammaSetupInfo.AllowedPhaseDispersion);
AddOutInt(Setup.GammaSetupInfo.MinDiagGamma);
AddOutInt(Setup.GammaSetupInfo.DaysToCalculateBASELINE);
/*
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[0]/100.0);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[1]/100.0);
AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[2]/100.0);
AddOutFloat(Setup.GammaSetupInfo.GammaSideSetup[HVSide].RatedVoltage);
*/
//AddOutFloat(0);
AddOutEnd();
}


// LOG NUMBER ?;
void GetLog(char Number)
{
 long l=0;
 char LogIndex=0,Side;

AddOutConst(trCommandLOG);
AddOutConst(trEmptyStr);
AddOutInt(Number);


if ((Number>0)&&(Number<=MaxLogData)) LogIndex=LogNumToIndex(Number);
LoadLog(LogIndex);
if ((LogIndex<MaxLogData)&&IsDate(LogData.Year+(long)2000,LogData.Month,LogData.Day)&&(LogData.Hour<24)&&(LogData.Min<60)) {
   l=((long)LogData.Year+(long)2000)*(long)10000;
   l+=(long)LogData.Month*(long)100;
   l+=(long)LogData.Day;
   AddOutDate(l);
   l=(long)LogData.Hour*(long)10000;
   l+=(long)LogData.Min*(long)100;
   AddOutTime(l);
   AddOutInt(LogData.LogCode);

   for (Side=0;Side<MaxTransformerSide;Side++) {
       AddOutInt(LogData.DefectCode[Side]);
//   l=(5*LogData[LogIndex].Gamma);
//   f=l/10.0;
//   l=f;
//   if (f>l) l++;
//   AddOutFloat(l/10.0);
       AddOutFloat((float)LogData.Gamma[Side]*0.01);
       AddOutFloat((float)LogData.GammaPhase[Side]*0.01);
       AddOutInt(-70+LogData.Temperature[Side]);
       AddOutFloat((float)LogData.KT[Side]*0.002);
       AddOutFloat((float)LogData.KTPhase[Side]*1.5);
       AddOutFloat((float)LogData.Trend[Side]*1); //acm, 4-27 change from .2 to 1
   }
} else {
   AddOutInt(0);
   AddOutInt(0);
   AddOutInt(0);

   for (Side=0;Side<MaxTransformerSide;Side++) {
       AddOutInt(0);
       AddOutFloat(0);
       AddOutFloat(0);
       AddOutInt(0);
       AddOutFloat(0);
       AddOutFloat(0);
       AddOutFloat(0);
   }
}

AddOutEnd();
}


// MEM ?;
void GetMem(void)
{
AddOutConst(trCommandMEM);
AddOutConst(trEmptyStr);
AddOutInt(MeasurementsInArchive);
AddOutEnd();
}


// DEFAULT
void SetDefault(void)
{
/* acm, change to do same thing as PDM
char NDevice;
NDevice=Setup.SetupInfo.DeviceNumber;
SetupValid=0x0000;
LoadSetup(1,0);
Setup.SetupInfo.DeviceNumber=NDevice;
*/

#define lcDefault           30  		// acm, should be located in logfile.h

LoadSetup(1,1);//DelSetup();			// new function, defined in archive.cpp, LoadSetup() looks same thing
SetSetupParam();
SaveLog(lcDefault);						// passing lcDefault just stores lcDefault in a log field, no harm I guess

}


// CALIBRT T1K T1B T2K T2B T3K T3B T4K T4B;
void SetCalibrT(float ParamFloat1,
                float ParamFloat2,
                float ParamFloat3,
                float ParamFloat4,
                float ParamFloat5,
                float ParamFloat6,
                float ParamFloat7,
                float ParamFloat8
             )
{
Setup.CalibrationCoeff.TemperatureK[0]=ParamFloat1;
Setup.CalibrationCoeff.TemperatureB[0]=ParamFloat2;
Setup.CalibrationCoeff.TemperatureK[1]=ParamFloat3;
Setup.CalibrationCoeff.TemperatureB[1]=ParamFloat4;
Setup.CalibrationCoeff.TemperatureK[2]=ParamFloat5;
Setup.CalibrationCoeff.TemperatureB[2]=ParamFloat6;
Setup.CalibrationCoeff.TemperatureK[3]=ParamFloat7;
Setup.CalibrationCoeff.TemperatureB[3]=ParamFloat8;
}


// CALIBRT ?;
void GetCalibrT(void)
{
AddOutConst(trCommandCALIBRT);
AddOutConst(trEmptyStr);
AddOutFloat(Setup.CalibrationCoeff.TemperatureK[0]);
AddOutFloat(Setup.CalibrationCoeff.TemperatureB[0]);
AddOutFloat(Setup.CalibrationCoeff.TemperatureK[1]);
AddOutFloat(Setup.CalibrationCoeff.TemperatureB[1]);
AddOutFloat(Setup.CalibrationCoeff.TemperatureK[2]);
AddOutFloat(Setup.CalibrationCoeff.TemperatureB[2]);
AddOutFloat(Setup.CalibrationCoeff.TemperatureK[3]);
AddOutFloat(Setup.CalibrationCoeff.TemperatureB[3]);
AddOutEnd();
}

// CALIBRA A1K A1B A2K A2B A3K A3B A4K A4B;
void SetCalibrA(float ParamFloat1,
                float ParamFloat2,
                float ParamFloat3,
                float ParamFloat4,
                float ParamFloat5,
                float ParamFloat6,
                float ParamFloat7,
                float ParamFloat8
             )
{
Setup.CalibrationCoeff.CurrentK[0]=ParamFloat1;
Setup.CalibrationCoeff.CurrentB[0]=ParamFloat2;
Setup.CalibrationCoeff.CurrentK[1]=ParamFloat3;
Setup.CalibrationCoeff.CurrentB[1]=ParamFloat4;
Setup.CalibrationCoeff.CurrentK[2]=ParamFloat5;
Setup.CalibrationCoeff.CurrentB[2]=ParamFloat6;
Setup.CalibrationCoeff.CurrentK[3]=ParamFloat7;
Setup.CalibrationCoeff.CurrentB[3]=ParamFloat8;
}


// CALIBRA ?;
void GetCalibrA(void)
{
AddOutConst(trCommandCALIBRA);
AddOutConst(trEmptyStr);
AddOutFloat(Setup.CalibrationCoeff.CurrentK[0]);
AddOutFloat(Setup.CalibrationCoeff.CurrentB[0]);
AddOutFloat(Setup.CalibrationCoeff.CurrentK[1]);
AddOutFloat(Setup.CalibrationCoeff.CurrentB[1]);
AddOutFloat(Setup.CalibrationCoeff.CurrentK[2]);
AddOutFloat(Setup.CalibrationCoeff.CurrentB[2]);
AddOutFloat(Setup.CalibrationCoeff.CurrentK[3]);
AddOutFloat(Setup.CalibrationCoeff.CurrentB[3]);
AddOutEnd();
}

// CALIBRH Offset Slope;
void SetCalibrH(float ParamFloat1,
                float ParamFloat2
             )
{
Setup.CalibrationCoeff.HumidityOffset[0]=ParamFloat1;
Setup.CalibrationCoeff.HumiditySlope[0]=ParamFloat2;
}


// CALIBRH ?;
void GetCalibrH(void)
{
AddOutConst(trCommandCALIBRH);
AddOutConst(trEmptyStr);
AddOutFloat(Setup.CalibrationCoeff.HumidityOffset[0]);
AddOutFloat(Setup.CalibrationCoeff.HumiditySlope[0]);
AddOutEnd();
}

// READINIT GROUP ?;
void GetInit(char Group)
{ long l;
//  float f;
//READINIT Group BalGamm PhaseA BalTemp TbalCoef CarA0 CarB0 CarC0 ChShift BalDate;
if (Group>1) Group=0;
AddOutConst(trCommandREADINIT);
AddOutConst(trEmptyStr);
AddOutInt(Group+1);
if (IsDate(Setup.InitialParameters.Year,Setup.InitialParameters.Month,Setup.InitialParameters.Day>0)&&
   (Setup.InitialParameters.Hour<24)&&
   (Setup.InitialParameters.Min<60)) {
/*
   l=InitialParameters.TrSideParam[HVSide].NGammaAmplitude/10;
   f=l/10.0;
   l=f;
   if (f>l) l++;
*/
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].GammaAmplitude*0.01);
//   AddOutFloat((float)InitialParameters.TrSideParam[HVSide].GammaAmplitude*0.05);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].GammaPhase*0.01);
   AddOutInt(-70+Setup.InitialParameters.TrSideParam[Group].Temperature);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].KT*0.002);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].KTPhase*1.5);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].PhaseAmplitude[BushingA]/10.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].PhaseAmplitude[BushingB]/10.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].PhaseAmplitude[BushingC]/10.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].ChPhaseShift/100.0);
   l=((long)Setup.InitialParameters.Year+(long)2000)*(long)10000;
   l+=(long)Setup.InitialParameters.Month*(long)100;
   l+=(long)Setup.InitialParameters.Day;
   AddOutDate(l);
   l=(long)Setup.InitialParameters.Hour*(long)10000;
   l+=(long)Setup.InitialParameters.Min*(long)100;
   AddOutTime(l);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].SourceAmplitude[BushingA]/100.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].SourceAmplitude[BushingB]/100.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].SourceAmplitude[BushingC]/100.0);

//   AddOutFloat((float)InitialParameters.TrSideParam[HVSide].SignalPhase[BushingA]/100.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].SignalPhase[BushingB]/100.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].SignalPhase[BushingC]/100.0);
//   AddOutFloat((float)InitialParameters.TrSideParam[HVSide].SourcePhase[BushingA]/100.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].SourcePhase[BushingB]/100.0);
   AddOutFloat((float)Setup.InitialParameters.TrSideParam[Group].SourcePhase[BushingC]/100.0);

} else {
   AddOutFloat(0);
   AddOutFloat(0);
   AddOutInt(0);
   AddOutFloat(0);
   AddOutFloat(0);
   AddOutFloat(0);
   AddOutFloat(0);
   AddOutFloat(0);
   AddOutFloat(0);
   AddOutDate(0);
   AddOutTime(0);
   AddOutFloat(0);
   AddOutFloat(0);
   AddOutFloat(0);

   AddOutFloat(0);
   AddOutFloat(0);
   AddOutFloat(0);
   AddOutFloat(0);

}
AddOutEnd();
}


// READREC REC GROUP ?;
// READREC REC GROUP Date Time ErrCode Alarm Gamma GammaPhase Temper TCoef Trend;
void GetRec(int Param1,char Group)
{long l;
#ifndef __arm
 char Page=GetPage();
#endif
 float f;
 int i;
 struct stInsulationParameters TParam;
 struct stInsulationParameters *Param=&TParam;

if (Group>1) Group=0;
AddOutConst(trCommandREADREC);
AddOutConst(trEmptyStr);
if ((MeasurementsInArchive==0)||(Param1==0)||((unsigned int)Param1>MeasurementsInArchive)) {
   AddOutInt(0);
   AddOutInt(0);
} else {
#ifndef __arm
   SetPage(TempPage);
#endif
   if (!LoadData(Param,MeasurementNumToIndex(Param1),TempPage)) {
      AddOutInt(0);
      AddOutInt(0);
      goto ProEnd;
   }
   SetPage(Page);
   AddOutInt(Param1);
   AddOutInt(Group+1);
   SetPage(TempPage);
   l=((long)Param->Year+(long)2000)*(long)10000;
   l+=(long)Param->Month*(long)100;
   l+=(long)Param->Day;
   SetPage(Page);
   AddOutDate(l);
   SetPage(TempPage);
   l=(long)Param->Hour*(long)10000;
   l+=(long)Param->Min*(long)100;
   SetPage(Page);
   AddOutTime(l);
   SetPage(TempPage);
   i=Param->TrSide[Group].DefectCode;
   SetPage(Page);
   AddOutInt(i);
   SetPage(TempPage);
   i=Param->TrSide[Group].AlarmStatus;
   SetPage(Page);
   AddOutInt(i);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].Gamma*0.01;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].GammaPhase*0.01;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   i=-70+Param->Temperature[Group];
   SetPage(Page);
   AddOutInt(i);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].KT*0.002;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].KTPhase*1.5;
   SetPage(Page);
   AddOutFloat(f);

   SetPage(TempPage);
   f=(float)Param->TrSide[Group].PhaseAmplitude[BushingA]/10.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].PhaseAmplitude[BushingB]/10.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].PhaseAmplitude[BushingC]/10.0;
   SetPage(Page);
   AddOutFloat(f);

   SetPage(TempPage);
   f=(float)Param->TrSide[Group].ChPhaseShift/100.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)(Param->TrSide[Group].Trend*1); //acm, 4-27 change from .2 to 1
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].SourceAmplitude[BushingA]/100.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].SourceAmplitude[BushingB]/100.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].SourceAmplitude[BushingC]/100.0;
   SetPage(Page);
   AddOutFloat(f);

//   AddOutFloat((float)Param.TrSide[HVSide].SignalPhase[BushingA]/100.0);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].SignalPhase[BushingB]/100.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].SignalPhase[BushingC]/100.0;
   SetPage(Page);
   AddOutFloat(f);
//   AddOutFloat((float)Param.TrSide[HVSide].SourcePhase[BushingA]/100.0);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].SourcePhase[BushingB]/100.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Param->TrSide[Group].SourcePhase[BushingC]/100.0;
   SetPage(Page);
   AddOutFloat(f);

   SetPage(TempPage);
   f=(float)Param->TrSide[Group].Frequency;
   SetPage(Page);
   AddOutFloat(f);

//   LoadData(&Parameters,LastMeasurement-1);
}
ProEnd:
SetPage(Page);
AddOutEnd();
}

// ERROR ?;
void GetError(void)
{ u16 C;
   AddOutConst(trCommandERROR);
   AddOutConst(trEmptyStr);
   C=Error;
//   C&=~(1<<(unsigned long)erStop);
//   C&=~(1<<(unsigned long)erPause);
   AddOutLong(C);
   AddOutEnd();
}

// ERRORBIT ?;
void GetErrorBit(void)
{ u16 C;
  char i;
   AddOutConst(trCommandERRORBIT);
   AddOutConst(trEmptyStr);
   C=Error;
   for (i=0;i<16;i++)  if (C&(1<<i)) AddOutInt(1); else AddOutInt(0);
   AddOutEnd();
}


// DIAG ?;
void GetDiagnosis(int Param1,char Group)
{long l;
#ifndef __arm
 char Page=GetPage();
#endif
 float f;
#ifdef __emulator
   struct stInsulationParameters TParam;
   struct stInsulationParameters *Param=&TParam;
#else
  extern struct stInsulationParameters *TempParam;
#endif
 struct stDiagnosis Diag;
 _TDateTime DiagDate;

if (Group>1) Group=0;
AddOutConst(trCommandDIAG);
AddOutConst(trEmptyStr);
AddOutInt(Param1);
AddOutInt(Group+1);
if ((!MeasurementsInArchive)||(!Param1)||((unsigned int)Param1>MeasurementsInArchive)) {
   AddOutInt(0);
   AddOutInt(0);
   AddOutInt(0);
   AddOutInt(0);
   AddOutInt(0);
   AddOutInt(0);
   AddOutInt(0);
   AddOutInt(0);
} else {
   SetPage(TempPage);
   LoadData(TempParam,MeasurementNumToIndex(Param1),TempPage);
   SetPage(TempPage);
   l=((long)TempParam->Year+(long)2000)*(long)10000;
   l+=(long)TempParam->Month*(long)100;
   l+=(long)TempParam->Day;
   SetPage(Page);
   AddOutDate(l);
   SetPage(TempPage);
   l=(long)TempParam->Hour*(long)10000;
   l+=(long)TempParam->Min*(long)100;
   SetPage(Page);
   AddOutTime(l);
   SetPage(TempPage);
   DiagDate = DoPackDateTime(EncodeDate(TempParam->Year,TempParam->Month,TempParam->Day),
                             EncodeTime(TempParam->Hour,TempParam->Min,  0));
   DoSumDiag(DiagDate,&Diag,/*NORMALDiagRegime*/STABLEDiagRegime,CurDiagRegime,Param1,CurDiagType,TempPage,0);
   SetPage(TempPage);
   f=(float)Diag.TgParam[Group].C[0]/10.0l;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Diag.TgParam[Group].C[1]/10.0l;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Diag.TgParam[Group].C[2]/10.0l;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Diag.TgParam[Group].tg[0]/100.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Diag.TgParam[Group].tg[1]/100.0;
   SetPage(Page);
   AddOutFloat(f);
   SetPage(TempPage);
   f=(float)Diag.TgParam[Group].tg[2]/100.0;
   SetPage(Page);
   AddOutFloat(f);
   AddOutEnd();
}
SetPage(Page);
}


// BASELINE GROUP [Date]?;
void GetStable(char Side,unsigned long NewDate)
{ char i;
  unsigned int Year,Month,Day,Hour,Min,Sec;
  long l;
  _TDate Date;
  _TTime Time;


if (NewDate>0) {
   //YYYYMMDD
   l=NewDate;
   Year=(unsigned int)(l/(long)10000);
   l-=((long)Year*(long)10000);
   Year-=2000;
   Month=(char)(l/(long)100);
   l-=(long)Month*(long)100;
   Day=l;
   RunBaseLine(DoPackDateTime(EncodeDate(Year%100,Month,Day),EncodeTime(23,59,59)),CurDiagRegime);
}

AddOutConst(trCommandSTABLE);
AddOutConst(trEmptyStr);
AddOutInt(Side+1);

DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate,&Date,&Time);
DecodeDate(Date,&Year,&Month,&Day);
DecodeTime(Time,&Hour,&Min,&Sec);
//DecodeDate(Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate,&Year,&Month,&Day);
//DecodeTime(Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate,&Hour,&Min,&Sec);
Year=Year%100+2000;
//if ((IsDate(Year,Month,Day))&&(Hour>0)&&(Hour<24)&&(Min>0)&&(Min<60)&&(Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved==1)) {
if (Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved==1) {
   l=(long)Year*(long)10000;
   l+=(long)Month*(long)100;
   l+=(long)Day;
   AddOutDate(l);
   l=(long)Hour*(long)10000;
   l+=(long)Min*(long)100;
   AddOutTime(l);
   AddOutInt(-70+Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETemperature);
   for (i=0;i<MaxBushingCountOnSide;i++) {
      AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[i]/10.0);
      AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaC[i]/100.0);
      AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[i]/100.0);
      AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaTg[i]/100.0);
   }
   for (i=0;i<MaxBushingCountOnSide;i++) {
       if (Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved==2) {
          AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].StableSourceAmplitude[i]/10.0);
          AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].StableSourcePhase[i]/100.0);
       } else {
          AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].StablePhaseAmplitude[i]/10.0);
          AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].StableSignalPhase[i]/100.0);
       }
   }

} else {
   AddOutDate(0);
   AddOutTime(0);
   AddOutInt(0);
   for (i=0;i<MaxBushingCountOnSide;i++) {
      AddOutFloat(0.0);
      AddOutFloat(0.0);
      AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[i]/100.0);
      AddOutFloat(0.0);
   }
   for (i=0;i<MaxBushingCountOnSide;i++) {
      AddOutFloat(0.0);
      AddOutFloat(0.0);
   }
}
AddOutEnd();
}


// HEATTEST ?;
void GetHEATTEST(void)
{ char i;
  unsigned int Year,Month,Day,Hour,Min,Sec;
  long l;

AddOutConst(trCommandHEATTEST);
AddOutConst(trEmptyStr);
DecodeDate(DateTimeToDate(Setup.GammaSetupInfo.GammaSideSetup[HVSide].HeatDate),&Year,&Month,&Day);
DecodeTime(DateTimeToTime(Setup.GammaSetupInfo.GammaSideSetup[HVSide].HeatDate),&Hour,&Min,&Sec);
if (Year<2000) Year+=2000;
if ((IsDate(Year,Month,Day))&&(Hour>0)&&(Hour<24)&&(Min>0)&&(Min<60)) {
   l=(long)Year*(long)10000;
   l+=(long)Month*(long)100;
   l+=(long)Day;
   AddOutDate(l);
   l=(long)Hour*(long)10000;
   l+=(long)Min*(long)100;
   AddOutTime(l);
   AddOutInt(-70+Setup.GammaSetupInfo.GammaSideSetup[HVSide].MinTemperature1);
   AddOutInt(-70+Setup.GammaSetupInfo.GammaSideSetup[HVSide].AvgTemperature1);
   AddOutInt(-70+Setup.GammaSetupInfo.GammaSideSetup[HVSide].MaxTemperature1);
   for (i=0;i<MaxBushingCountOnSide;i++) {
      AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[HVSide].K[i]);
      AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[HVSide].B[i]);
   }
} else {
   AddOutDate(0);
   AddOutTime(0);
   AddOutInt(0);
   AddOutInt(0);
   AddOutInt(0);
   for (i=0;i<MaxBushingCountOnSide;i++) {
      AddOutFloat(0.0);
      AddOutFloat(0.0);
   }
}
AddOutEnd();
}


// OFFLINEPARAM ?;
void GetOFFLINEPARAM(int Group)
{unsigned char i;
if (Group>1) Group=0;
AddOutConst(trCommandOFFLINEPARAM);
AddOutConst(trEmptyStr);
AddOutInt(Group+1);
AddOutInt(-70+Setup.GammaSetupInfo.GammaSideSetup[HVSide].Temperature0);
for (i=0;i<MaxBushingCountOnSide;i++) {
    AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].C0[i]/10.0);
    AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].Tg0[i]/100.0);
}
AddOutEnd();
}

void SetOFFLINEPARAM(int Group,int Param1, float Param2, float Param3, float Param4, float Param5, float Param6, float Param7)
{ unsigned int Year,Month,Day,Hour,Min,Sec;

if (Group>1) Group=0;
Setup.GammaSetupInfo.GammaSideSetup[Group].Temperature0=70+Param1;
Setup.GammaSetupInfo.GammaSideSetup[Group].C0[0]=(unsigned int)(Param2*10.0);
Setup.GammaSetupInfo.GammaSideSetup[Group].Tg0[0]=(unsigned int)(Param3*100.0);
Setup.GammaSetupInfo.GammaSideSetup[Group].C0[1]=(unsigned int)(Param4*10.0);
Setup.GammaSetupInfo.GammaSideSetup[Group].Tg0[1]=(unsigned int)(Param5*100.0);
Setup.GammaSetupInfo.GammaSideSetup[Group].C0[2]=(unsigned int)(Param6*10.0);
Setup.GammaSetupInfo.GammaSideSetup[Group].Tg0[2]=(unsigned int)(Param7*100.0);
DecodeDate(DateTimeToDate(Setup.GammaSetupInfo.GammaSideSetup[Group].STABLEDate),&Year,&Month,&Day);
DecodeTime(DateTimeToTime(Setup.GammaSetupInfo.GammaSideSetup[Group].STABLEDate),&Hour,&Min,&Sec);
if (Year<2000) Year+=2000;
if (Setup.GammaSetupInfo.GammaSideSetup[Group].STABLESaved!=1) {
   Setup.GammaSetupInfo.GammaSideSetup[Group].STABLETg[0]=Param3*100.0;
   Setup.GammaSetupInfo.GammaSideSetup[Group].STABLETg[1]=Param5*100.0;
   Setup.GammaSetupInfo.GammaSideSetup[Group].STABLETg[2]=Param7*100.0;
}

}


// CHDIV ?;
void GetCHDIV(int Group)
{unsigned char i;
if (Group>1) Group=0;
AddOutConst(trCommandCHDIV);
AddOutConst(trEmptyStr);
AddOutInt(Group+1);
for (i=0;i<MaxBushingCountOnSide;i++) {
    AddOutFloat((float)Setup.GammaSetupInfo.GammaSideSetup[Group].InputCoeff[i]);
}
AddOutEnd();
}

void SetCHDIV(int Group,float Param1, float Param2,float Param3)
{
if (Group>1) Group=0;
Setup.GammaSetupInfo.GammaSideSetup[Group].InputCoeff[0]=Param1;
Setup.GammaSetupInfo.GammaSideSetup[Group].InputCoeff[1]=Param2;
Setup.GammaSetupInfo.GammaSideSetup[Group].InputCoeff[2]=Param3;
}


// NEGTG ?;
void GetNEGTG(void)
{
AddOutConst(trCommandNEGTG);
AddOutConst(trEmptyStr);
if (Setup.GammaSetupInfo.NEGtg==0xFF) Setup.GammaSetupInfo.NEGtg=1;
AddOutInt(Setup.GammaSetupInfo.NEGtg);
AddOutEnd();
}

void SetNEGTG(int Param1)
{
if ((Param1==1)||(Param1==0)) Setup.GammaSetupInfo.NEGtg=Param1;
}

void DeviceReset(void)
{
Restart();
}

//REGDATA ?;
void GetRegData(void)
{
AddOutConst(trCommandREGDATA);
AddOutConst(trEmptyStr);
if (Setup.SetupInfo.ReadAnalogFromRegister) AddOutInt(1); else AddOutInt(0);
//����������� �� ��D �������
AddOutInt((int)ExtTemperature[0]-70);
// acm, 7-28 dump out other 3 values for heck of it, cross fingers this doesn't break some app.  Note, not modifying write command as I don't have time.
// and no, I don't know why this code resides in an .H file...
AddOutInt((int)ExtTemperature[1]-70); AddOutInt((int)ExtTemperature[2]-70); AddOutInt((int)ExtTemperature[3]-70);
//�������� �������� �� ��D �������
AddOutInt(ExtLoadActive);
//�������� ���������� �� ��D �������
AddOutInt((int)ExtLoadReactive-127);
//��������� �� ��D �������
AddOutInt(ExtHumidity);
//��������� ��� �� ��D �������
AddOutInt(ExtRPN);
AddOutEnd();
}

//REGDATA ReadFromReg Temperature LoadActivePercent LoadReactivePercent Humidity LTCPosition;
void SetRegData(char Param1,char Param2,char Param3,char Param4,char Param5,char Param6)
{
if (Param1) Setup.SetupInfo.ReadAnalogFromRegister=1; else Setup.SetupInfo.ReadAnalogFromRegister=0;
//����������� �� ��D �������
ExtTemperature[0]=(s16)(Param2+70);  								// acm, 5-18-12, change char to s16
// acm, 7-28 no need to add write new temp register feature, now
//�������� �������� �� ��D �������
ExtLoadActive=Param3;
//�������� ���������� �� ��D �������
ExtLoadReactive=(char)(Param4+127);
//��������� �� ��D �������
ExtHumidity=Param5;
//��������� ��� �� ��D �������
ExtRPN=Param6;

}

void GetProcRype(void)
{
AddOutConst(trCommandPROCTYPE);
AddOutConst(trEmptyStr);
AddOutConst(strPROCTYPE);
AddOutEnd();
}

//AUTOBALANCE Date Hour WorkDays BalanceRunning NoLoadTestRunning;
void GetAutoBalance(void)
{long l;
AddOutConst(trCommandAUTOBALANCE);
AddOutConst(trEmptyStr);
l=((long)Setup.SetupInfo.AutoBalans.Year%100+2000)*(long)10000;
l+=(long)Setup.SetupInfo.AutoBalans.Month*(long)100;
l+=(long)Setup.SetupInfo.AutoBalans.Day;
AddOutDate(l);
AddOutInt(Setup.SetupInfo.AutoBalans.Hour);
if (Setup.SetupInfo.AutoBalansActive)
   AddOutInt(Setup.SetupInfo.AutoBalansActive-1);
else
   AddOutInt(Setup.SetupInfo.WorkingDays);
if (Setup.SetupInfo.AutoBalansActive)
   AddOutInt(1);
else
   AddOutInt(0);

if (Setup.SetupInfo.NoLoadTestActive)
   AddOutInt(1);
else
   AddOutInt(0);
AddOutEnd();
}

//AUTOBALANCE Date Hour WorkDays BalanceRunning NoLoadTestRunning;
void SetAutoBalance(long DateValue,char Param1,char Param2,char Param3,char Param4)
{long ltmp;
ltmp=DateValue;
Setup.SetupInfo.AutoBalans.Year=(unsigned int)(ltmp/(long)10000);
ltmp-=((long)Setup.SetupInfo.AutoBalans.Year*(long)10000);
Setup.SetupInfo.AutoBalans.Year-=2000;
Setup.SetupInfo.AutoBalans.Month=(char)(ltmp/(long)100);
ltmp-=(long)Setup.SetupInfo.AutoBalans.Month*(long)100;
Setup.SetupInfo.AutoBalans.Day=ltmp;
Setup.SetupInfo.AutoBalans.Hour=Param1;
Setup.SetupInfo.WorkingDays=Param2;
if (Param3) Setup.SetupInfo.AutoBalansActive=Setup.SetupInfo.WorkingDays+1;
else Setup.SetupInfo.AutoBalansActive=0;
if (Param4) Setup.SetupInfo.NoLoadTestActive=1;
else Setup.SetupInfo.NoLoadTestActive=0;
TestSetup();
}

void ClearLog(void)
{int i;

  LogData.Year=0;
  LogData.Month=0;
  LogData.Day=0;
  LogData.Hour=0;
  LogData.Min=0;
  LogData.LogCode=0;

  for (i=0;i<MaxLogData;i++) SaveCurentLog(i);
  InitLog();
  SaveLog(lcLogCleared);


}

#endif
