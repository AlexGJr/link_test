#ifndef _archive_h
#define _archive_h

#include "MeasurementDefs.h"
//#define MaxData 85

//extern unsigned int MaxData;
//extern unsigned char MeasurementInSector;
extern char IsInitialParam;

//extern __no_init __eeprom struct stInsulationParameters ArchiveData[MaxData];
extern unsigned int MeasurementsInArchive,LastMeasurement;
extern unsigned int equByteSectorFlash,equCountSectorFlash;
extern char FlashEnable;

char InitMeasurementsArchive(void);
void SaveData(struct stInsulationParameters *Param);
unsigned int MeasurementNumToIndex(unsigned int Num);
char LoadData(struct stInsulationParameters *Param,unsigned int Num,char Page);
unsigned int LoadArchGamma(unsigned int Num);
void ClearArchive(void);
void InitInitialParameters(void);
void LoadLastMeasurement(void);
char WorkWithArchive(char View);
void ClaerAllSource(void);
char DelArchive(void);
void LoadArchDate(struct stInsulationParameters *Buf,unsigned int Num,char aPage);
void ClearAllData(void);
void ClearInitialTgDelta(char Side);
void SetPause(void);
unsigned int GetMOnDate(unsigned long Date,unsigned long Time);
void SaveFileIndexes(void * MeasListAddr);
void LoadFileIndexes(void * MeasListAddr);
u16 LoadFileIndex(u16 LoadIndex);
void ClearAvg(void);
void ClearInitialZk(void);

#endif
