#ifndef measurementDefs_h
#define measurementDefs_h

#include "Defs.h"
#include "SysUtil.h"

#define MaxLoads 10

#pragma pack(1)

// Calculated Zk parameters with 10% load steps
struct stZkTable {
    float ZkP[MaxCorrelationSide][MaxLoads][MaxBushingCountOnSide];
};
#define szZkTable sizeof(struct stZkTable)

//Calculated parameters for all channels. AG not used anywhere
struct stPDParameters {
       //Date of measurement
       char Day;
       char Month;
       unsigned short Year;
       char Hour;
       char Min;
       float Frequency; //Power system frequency (50/60)
       float RatedVoltage;  //kV
       short Temperature; //Temperature*10
       char Reserved[64];

};
#define szPDParameters sizeof(struct stPDParameters)


//Measurement parameters for each channel
//extern struct stMeasurementTable *MsTable;
//Calculated parameters for each channel
extern struct stdBLevels dBLevels;  //ag not used
extern struct stPDParameters *PDParameters; //ag not used

void StartMeasurement(void);

//Data structure per set. Inculded in stInsulationParameters
struct stTransformerSideParameters {
       unsigned short SourceAmplitude[MaxBushingCountOnSide];       //The amplitude of the phase signal, mV, believed mA*10
       unsigned short PhaseAmplitude[MaxBushingCountOnSide];		// mV in balancing channel*10
       unsigned short Gamma;  //Imbalance amplitude %*100
       unsigned short GammaPhase; //Imbalance phase *100 degrees
       char KT; //Imbalance temperature coefficient *0.002
       char AlarmStatus;  //bit0-Imbalance Alarm, bit1-trend, bit2-temperature coefficient, bit3-Imbalance Warning
       // 2.10 redefined below unsigned short RemainingLife[MaxBushingCountOnSide]; //Residual life %*10, Not used.
       unsigned short DAC_CalAmplitude[MaxBushingCountOnSide];  //2.10 start using to store calibration results from ReadCalibrPhases()
       char DefectCode;  // acm, v1.79 start using this.  Bit 0, phaseB/C swap, bit 1=hi balance encountered, one char/side.  Same values stored in data record, confusing?
       short ChPhaseShift;  //?calibrated phase shift between ch A and Imb, Degrees*100
       char Trend;  //Imbalance trend, believe in % per year
       char KTPhase;  //Gamma Temperature Coefficient Phase, *1.5
       unsigned short SignalPhase[MaxBushingCountOnSide]; //phase shift between A&A, A&B and A&C in imbalance channel
       unsigned short SourcePhase[MaxBushingCountOnSide]; //phase shift between A&A, A&B and A&C in input channels
       float Frequency; //Power system frequency Hz, current
       char Temperature;  //Temperature, whatever temperature trasferred as correlating temperature
       unsigned short C[MaxBushingCountOnSide]; //Current calculated capacitance pF*10
       short tg[MaxBushingCountOnSide]; //Current calculated TAN Deta*100
       unsigned short ExternalSyncShift;  //Phase shift (*100). Does not look like used anywhere

       char Reserved[1];
};
//26 bytes
#define szTransformerSideParameters sizeof(struct stTransformerSideParameters)

//Struture enabled for leakage inductance calculations. Not used.
struct stSideToSideData {
       float Amplitude1[MaxBushingCountOnSide]; //Signal Amplitude for set 1 (HV), Volts
       float Amplitude2[MaxBushingCountOnSide]; //Signal Amplitude for set 2 (LV), Volts
       float PhaseShift[MaxBushingCountOnSide]; //Phase shift relative to set1, likely to set2, Deg*100
       float CurrentAmpl[MaxBushingCountOnSide];  //Load current in set 1, Amps
       float CurrentPhase[MaxBushingCountOnSide]; //Phase shift between current and voltage signal, Deg*100
       float ZkAmpl[MaxBushingCountOnSide]; //Zk value in Ohms (for initial this is not Zk, but Uk)
       float ZkPhase[MaxBushingCountOnSide];  //Phase shif between Zk and current, Deg*100
       float CurrentChannelShift;
       unsigned int ZkPercent[MaxBushingCountOnSide]; //delta Zk, %

       char Reserved[16];
};
#define szSideToSideData sizeof(struct stSideToSideData)


//Overall Data structure
struct stInsulationParameters {
       char Day;  //Date of measurement
       char Month;
       char Year;  //-2000
       char Hour;
       char Min;
       char TrSideCount;  //Sets enabled and passed calibration in current measurement. Bit coded.

       struct stTransformerSideParameters TrSide[MaxTransformerSide]; //Structure of parameters specific for a set

       struct stSideToSideData SideToSideData[MaxCorrelationSide];  //Structure of measurement results between sets. Not really used.

       char RPN[MaxTransformerSide];  //LTC position
       float Current[MaxTransformerSide];  //Load current %FL as set in reg17. DE 3-11-16 float(4bytes) must not cross a PAGE boundary Current[0] was 245,246,247,PAGEGAP,248
       char Temperature[4]; //Temperature (-70 - +185 0C) with step 1 0C
       float CurrentR[MaxTransformerSide];  //Reactive load current as set in reg18
       char Humidity; //Relative Humidity % as set in reg19
       char CalcZk; //Not used.
	   
       char AveragingForGamma; 	// not used after v2.00
		// X==0, avg freeze, kinda dumb
		// X==.5, 2pt avg, avg changes more quickly
		// X==.33, old alg
		// X==1, avg off, becomes last measured Unn
       char AveragingForGamma_X;	//2.05 upgrade sets 99-almost none. v2.00, changed averating rutine. (100-X)/100 - effect of previous measurement
       s16 voltage1, voltage2, voltage; // 2.00 as set in reg 26, 27, 28

       unsigned short NewFeatures; //2.03 copied from new modbus register 116 "LoadCurrentThresholdSettings" and set bit 15 to flag New Features Record
       unsigned short Error1; //2.03 copied from register 9
       unsigned short Error2; //2.03 copied from register 3
       //2.05 Balancing time included to a record
       char BalDay;
       char BalMonth;
       char BalYear;  //-2000
       char BalHour;
       char BalMin;
       char Reserved[1]; //reduced by 5 bytes for balancig date/time. reduced by 6 bytes from 12 to 6    

       struct stZkTable ZkTable;  //enclosed structure for Zk. Not used.
};
//40 ����
#define szInsulationParameters sizeof(struct stInsulationParameters)

#pragma pack()

#endif

