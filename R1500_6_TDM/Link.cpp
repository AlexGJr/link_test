#include "Commands.h"
#include "Trans.h"
#include "ADCDevice.h"
#include "GammaMeasurement.h"
#include "RTC.h"
#include "Setup.h"
//#include "Archive.h"
#include "Diagnosis.h"
#include "DateTime.h"
#include "RAM.h"
#include "EEPROM.h"
#include "Utils.h"
#include "SysUtil.h"

#include "ModBusTCP\UART.h"
#include "ModBusTCP\ModbusServer.h"
#include <string.h>
#include "Error.h"
#include "KeyBoard.h"
#include "DrvFlash.h"
#include "LogFile.h"

#include "KeyBoard.h"
#include "Graph.h"
#include "LCD.h"
#include "Archive.h"
#include "BoardAdd.h"
#include "WatchDog.h"

#include "TRSTR.h"    // acm, 3-12-10 do this to define FW revision
#include <stdlib.h>			// acm, case sensitive?  Yes, eliminates linker error.
#include "Setup.h"			// acm, resolve Savesetup calls for Understand
#include <string.h>	// acm, v2.01
#include "graph.h"

//Start Balance
extern unsigned int AutoBalance;
//extern unsigned int FastWriteCount; //DE 3-30-16
extern char *BufSend;
extern volatile char  FlagSendMess;
extern unsigned long TimeNextMeas;//����� ���������� ���������� ������
//��������� ���������� ���������
extern signed char MeasurementResult;

extern void TestSetup(); // acm, do this so Understand resolves
extern void SaveSetup(); // acm, do this so Understand resolves
extern void SetSetupParam();

unsigned int ReSetSetup=0, RecalcTime=0;  // acm, add RecalcTime as done in PDM
unsigned int DelFrom=0,DelTo=0;
unsigned int ModBusArchMesurementNumOnDate=0;
unsigned int ModBusArchMesurementNum=0;
struct StDateTime ArchDateTime={0,0,0,0,0,0};
unsigned int MeasLoaded=0;
//����������� �� ��D �������
char ExtTemperature[4]= {0,0,0,0};  // acm, 7-8-09 add 2nd copy per Claude
//�������� �������� �� ��D �������
s16 ExtLoadActive=0;					// acm, 5-9-12 change from U32 to s16
//�������� ���������� �� ��D �������
s16 ExtLoadReactive=127;				// acm, 5-9-12 change from U32 to s16
//��������� �� ��D �������
char ExtHumidity=0;
//��������� ��� �� ��D �������
char ExtRPN=0;

s16 LoadCurrent2=0;
s16 LoadCurrent3=0;  // acm, v2, add 2 currents, same as PDM
s16 voltage1 =0;
s16 voltage2 =0;
s16 voltage =0;
s16 EventTrigger =0; //DE v2.03 add EventTrigger for BHM2C50

void SetDeviceBusy(void)
{
  unsigned long DelayCnt=0;
  
  while (FlagSendMess) {								// acm, FlagSendMess always 0.  Evidence found in files not linked into project
    if (DelayCnt++>(unsigned long)200000) {		// this flag set in UART_WRITEBLOCK, probably for a different hardware variant.
      break;										// No Theory, as USART IRQ 3, 30 msec PIT timer 1, USART IRQ could be pre-empted by PIT, but should
    }												// return back to USART IRQ, but how could FlagSendMess could ever be set here?
    DelayMks(10);
  }
  BusyFlag=1;
}

void SetDeviceNoBusy(void)
{
  BusyFlag=0;
}

static void SaveDataToEEPROM(char *Buf,unsigned int Addr,unsigned int Count)
{ unsigned int i;

for (i=0;i<Count;i++) SetEEPROMData(Addr+i,*(Buf+i));

}



void GetNextTime(_TDateTime CurDateTime);

int SendAnswer(unsigned int Cmnd,unsigned int Info1,unsigned int Info2, unsigned int Info3,unsigned int Info4, char *BufMess)
{ unsigned int Err=0;
unsigned int i;
_TDateTime DiagDate;
struct stDiagnosis tmpDiagData;
#ifndef __arm
char Page=GetPage();
#endif
char c;
//  char SinPage;
//  struct stSetupInfo tmpSetup;
//  struct stInitialParameters InitParam;
#ifdef __emulator
struct stInsulationParameters TParam;
struct stInsulationParameters *TempParam=&TParam;
#endif
unsigned int sn; //2.05 to preserve serial number
sn=Setup.SetupInfo.SN;

if (BusyFlag) {
  Err=cmndDeviceBusy;
  BufSend=(char *)&Err;
  memmove(BufMess,(char*)BufSend,2);
  return 2;
} else

if (Cmnd==cmndGetMeasurementCount) {
  Err=MeasurementsInArchive;
  BufSend=(char *)&Err;
  memmove(BufMess,(char*)BufSend,2);
  return 2;
} else

if (Cmnd==cmndMeasurementHeader){
  if ((!MeasurementsInArchive)||(!Info1)||(Info1>MeasurementsInArchive)) {
    Err=cmndErrorParameter;
    goto SayError;
  }
  SetPage(TempPage);;
  LoadData(TempParam,MeasurementNumToIndex(Info1),TempPage);
  BufSend=(char *)TempParam;
  
  // acm, 4-28, return modified Trend, to account for re-definition/algorithm change, as IHM expects a value 5x larger.  Since value is stored in a char, need to multiply by 5 with range check.
  if(TempParam->TrSide[0].Trend>51) TempParam->TrSide[0].Trend=255; else TempParam->TrSide[0].Trend*=5;
  if(TempParam->TrSide[1].Trend>51) TempParam->TrSide[1].Trend=255; else TempParam->TrSide[1].Trend*=5;
  
  for (i=0;i<szInsulationParameters;i++) {
    SetPage(TempPage);
    c=*((char*)BufSend+i);
    SetPage(Page);
    *((char*)BufMess+i)=c;
  }
  //         memmove(BufMess,(char*)BufSend,szInsulationParameters);
  return szInsulationParameters-szZkTable;
} else

if (Cmnd==cmndMeasurementDiag){
  if ((!MeasurementsInArchive)||(!Info1)||(Info1>MeasurementsInArchive)) {
    Err=cmndErrorParameter;
    goto SayError;
  }
  SetPage(TempPage);
  LoadData(TempParam,MeasurementNumToIndex(Info1),TempPage);
  DiagDate=DoPackDateTime(EncodeDate(TempParam->Year,
                                     TempParam->Month,
                                     TempParam->Day),
  EncodeTime(TempParam->Hour,
             TempParam->Min,
             0));
  DoSumDiag(DiagDate,&tmpDiagData,/*NORMALDiagRegime*/STABLEDiagRegime,CurDiagRegime,Info1,CurDiagType,TempPage,0);
  SetPage(Page);
  BufSend=(char *)&tmpDiagData;
  memmove(BufMess,(char*)BufSend,szDiagnosis);
  return szDiagnosis;
} else

if (Cmnd==cmndLastMeasurementHeader){
  BufSend=(char *)Parameters;
  for (i=0;i<szInsulationParameters;i++) {
    SetPage(ParametersPage);
    c=*((char*)BufSend+i);
    SetPage(Page);
    *((char*)BufMess+i)=c;
  }
  //       memmove(BufMess,(char*)BufSend,szInsulationParameters);
  return szInsulationParameters-szZkTable;
} else
if (Cmnd==cmndLastMeasurementDiag) {
  BufSend=(char *)Diagnosis;
  for (i=0;i<szDiagnosis;i++) {
#ifndef __arm
    SetPage(ParametersPage);
#endif
    c=*((char*)BufSend+i);
#ifndef __arm
    SetPage(Page);
#endif
    *((char*)BufMess+i)=c;
  }
  //       memmove(BufMess,(char*)BufSend,szDiagnosis);
  return szDiagnosis;
} else
if (Cmnd==cmndMeasurementDataHV){
  if (!MeasurementsInArchive) {
    Err=cmndErrorParameter;
    goto SayError;
  }
  if (Info1) {
    if (Info2) {
      BufSend=(char *)(BaseAddrSig4[HVSide]);
#ifndef __arm
      SinPage=GammaPage7;
#endif
    } else {
      BufSend=(char *)(BaseAddrSig1[HVSide]);
#ifndef __arm
      SinPage=GammaPage4;
#endif
    }
  }
  if (UpLoadCount>256) {
    for (i=0;i<(256<<1);i++) {
#ifndef __arm
      SetPage(SinPage);
      c=*((char*)BufSend+i);
      SetPage(Page);
#else
      c=0;
#endif
      *((char*)BufMess+i)=c;
    }
    //            memmove(BufMess,(char*)BufSend,2048<<1);
    return (256<<1);
  } else {
    for (i=0;i<(UpLoadCount<<1);i++) {
#ifndef __arm
      SetPage(SinPage);
      c=*((char*)BufSend+i);
      SetPage(Page);
#else
      c=0;
#endif
      *((char*)BufMess+i)=c;
    }
    //            memmove(BufMess,(char*)BufSend,UpLoadCount<<1);
    return UpLoadCount<<1;
  }
} else
if (Cmnd==cmndMeasurementDataLV){
  if (!MeasurementsInArchive) {
    Err=cmndErrorParameter;
    goto SayError;
  }
  if (Info1) {
    if (Info2) {
      BufSend=(char *)(BaseAddrSig4[LVSide]);
#ifndef __arm
      SinPage=GammaPage3;
#endif
    } else {
      BufSend=(char *)(BaseAddrSig1[LVSide]);
#ifndef __arm
      SinPage=GammaPage0;
#endif
    }
  }
  if (UpLoadCount>256) {
    for (i=0;i<(256<<1);i++) {
#ifndef __arm
      SetPage(SinPage);
      c=*((char*)BufSend+i);
      SetPage(Page);
#else
      c=0;
#endif
      *((char*)BufMess+i)=c;
    }
    //            memmove(BufMess,(char*)BufSend,2048<<1);
    return (256<<1);
  } else {
    for (i=0;i<(UpLoadCount)<<1;i++) {
#ifndef __arm
      SetPage(SinPage);
      c=*((char*)BufSend+i);
      SetPage(Page);
#else
      c=0;
#endif
      *((char*)BufMess+i)=c;
    }
    //            memmove(BufMess,(char*)BufSend,UpLoadCount<<1);
    return UpLoadCount<<1;
  }
} else


if (Cmnd==cmndGetDeviceSetup){
  BufSend=(char *)&Setup;
  for (i=0;i<szSetup;i++)
    BufMess[i]=*((char*)BufSend+i);
  return szSetup;
}  else

if (Cmnd==cmndPartGetDeviceSetup){
  if ((Info1>szSetup)||(Info1==0)||(Info2==0)) goto Err3;
  if ((szSetup-Info1+1) < Info2) Info2=szSetup-Info1+1;
  BufMess[0]=Info1;
  BufMess[1]=Info1>>8;
  BufMess[2]=Info2;
  BufMess[3]=Info2>>8;
  if (Info2>MaxPart) Info2=MaxPart;
  BufSend=(char *)&Setup+Info1-1;
  for (i=0;i<Info2;i++)
    BufMess[4+i]=*((char*)BufSend+i);
  return Info2+4;
Err3:
  Err=cmndErrorParameter;
  BufSend=(char *)&Err;
  memmove(BufMess,(char*)BufSend,2);
  return 2;
}

if (Cmnd==cmndPartSetDeviceSetup) {
  if ((Info1==0)||(Info2==0)) goto Err5;
  for (i=0;i<Info2;i++) {
    if (((Info1-1)+i)<szSetup)
      *((char*)&Setup+(Info1-1)+i)=BufMess[6+i];
  }
  Setup.SetupInfo.SN=sn; //2.06 restore serial number
  ReSetSetup=1;
  BufMess[0]=(char)cmndPartSetDeviceSetup;
  BufMess[1]=(char)((unsigned int)cmndPartSetDeviceSetup>>8);
  SaveSetup();
  //         TimeNextMeas=GetNextMeasTime(DateTimeNow());
  return 2;
Err5:
  Err=cmndErrorParameter;
  BufSend=(char *)&Err;
  memmove(BufMess,(char*)BufSend,2);
  return 2;
}

if (Cmnd==cmndSetDeviceSetup){
  
  SaveDataToEEPROM(&BufMess[4],((unsigned int)BufMess[3]<<8)+BufMess[2]+SetupAddr,((unsigned int)BufMess[1]<<8)+BufMess[0]);
  if ((((unsigned int)BufMess[3]<<8)+BufMess[2])+(((unsigned int)BufMess[1]<<8)+BufMess[0])>=sizeof(struct stSetup)) {
    // Setup.SetupInfo.SN=sn; //2.06 restore serial number
    ReSetSetup=1;
  }
  
  BufSend=(char *)&BufMess[4];
  BufMess[4]=(char)cmndSetDeviceSetup;
  BufMess[5]=(char)(cmndSetDeviceSetup>>8);
  return 2;
}  else

if (Cmnd==cmndGetDateTime){
  GET_TIME_INBUF();
  BufSend=(char *)&DateTime;
  memmove(BufMess,(char*)BufSend,equStDateTime);
  return equStDateTime;
}  else
if (Cmnd==cmndSetDateTime){
  for (i=0;i<equStDateTime;i++) {
    *((char*)&DateTime+i)=BufMess[2+i];
  }
  
  /*
  ClearLCD();
  OutInt(0,0,BufMess[0],3);
  OutInt(24,0,BufMess[1],3);
  OutInt(48,0,BufMess[2],3);
  OutInt(72,0,BufMess[3],3);
  OutInt(0,8,BufMess[4],3);
  OutInt(24,8,BufMess[5],3);
  OutInt(48,8,BufMess[6],3);
  OutInt(72,8,BufMess[7],3);
  //         OutDate(0,0);
  //         OutTime(0,8);
  Redraw();
  WaitReadKey();
  */
  // acm, back this out, user doesn't see any indication from IHM, and from communicator apparently it re-issues command, not desired affect
  //        if (SET_TIME_FROMBUF()){		// acm, 11-28, modify to return good/bad.  Bad means set time rejected, user should get MODBUS error msg
  SET_TIME_FROMBUF();
  GetNextTime(DateTimeNowNoSec());
  SetPage(Page);
  BufMess[0]=(char)cmndSetDateTime;
  BufMess[1]=(char)((unsigned int)cmndSetDateTime>>8);
  return 2;//}
} else
/*
if (Cmnd==cmndGetInitialParam){
LoadInitParamToBuffer((struct stInitialParameters *)OutBuf);
BufSend=(unsigned int)OutBuf;
SendMess(szInitialParameters);
while (FlagSendMess) ;
} else
*/
{
  Err=cmndErrorCommand;
SayError:
  BufSend=(char *)&Err;
  memmove(BufMess,(char*)BufSend,2);
  return 2;
}

}

int ModBusProcessString(char *BufMess ,char i)
{
  BufMess[i]=0;
  
  InBuf=BufMess;
  Translate();
  if ((OutBuf[0]!=';')&&(CurOutSymbol>1)) {
    memmove(BufMess,(char*)OutBuf,CurOutSymbol);
    return CurOutSymbol;
  } else {
    return 0;
  }
  //  return 0;
}


//Register Addresses
//                 	        from	to
//Binary Status registers	1	3
//General Information	        1	21
//Device Settings	        51	450
//Device Logs	                451	570
//Contol Commands	        601	650
//Initial Readings All Groups	651	774
//Window
//Base Line All Groups	        801	900
//Window
//Current Reading All Groups	901	1024
//Window
//Archive Reading All Groups	1051	1174
//Window

#define rgBinaryStatus         (int)0
#define rgGeneralInformation   (int)0
#define rgDeviceSettings       (int)50
#define rgGeneralGammaSettings rgDeviceSettings+(int)70
#define rgDeviceSettingsGroup1 rgDeviceSettings+(int)100
#define rgDeviceSettingsGroup2 rgDeviceSettings+(int)130
#define rgDeviceSettingsGroup3 rgDeviceSettings+(int)160
#define rgDeviceSettingsGroup4 rgDeviceSettings+(int)190
#define rgCalibrationCoeff     rgDeviceSettings+(int)220
#define rgDeviceLogs           (int)450
#define rgContolCommands       (int)600
#define rgInitialReadings1     (int)650
#define rgInitialReadings2     (int)681
#define rgInitialReadings3     (int)712
#define rgInitialReadings4     (int)743
#define rgBaseLine1            (int)800
#define rgBaseLine2            (int)825
#define rgBaseLine3            (int)850
#define rgBaseLine4            (int)875
#define rgCurrentReading       (int)900
#define rgCurrentReading1      (int)900
#define rgCurrentReading2      (int)931
#define rgCurrentReading3      (int)962
#define rgCurrentReading4      (int)993
// #define rgCurrentReadingZk12   (int)1025 //2.10
#define rgDAC_CalAmplitude   (int)1025  //2.10
#define rgArchiveReading       (int)1050
#define rgArchiveReading1      (int)1050
#define rgArchiveReading2      (int)1081
#define rgArchiveReading3      (int)1112
#define rgArchiveReading4      (int)1143
#define rgArchiveReadingZk12   (int)1175
#define rgTDMCurrentReading    (int)2500
#define rgTDMCurrentReading1   (int)2510
#define rgTDMCurrentReading2   (int)2540
#define rgTDMCurrentReadingZk12 (int)2570
#define rgTDMCurrentReadingI    (int)2580


char IsRegAddressCorrect(int StartAddr,int Count)
{
  if (((StartAddr>=0)&&(StartAddr<=42)&&(StartAddr+Count<=43))||  // 2.06 ag fix this to allow ResetCount-42/v2.01 fix v2 off by 1 bug, and open up larger for 40-42// v2 add few more regs to 28 acm, 7-8-09 expand for new temp reg below
      ((StartAddr>=50)&&(StartAddr<=449)&&(StartAddr+Count<=570))|| // acm, looks like bug to me, will leave alone for now
        ((StartAddr>=450)&&(StartAddr<=569)&&(StartAddr+Count<=570))||
          ((StartAddr>=600)&&(StartAddr<=649)&&(StartAddr+Count<=776))||  // acm, looks like bug to me, will leave alone for now
            ((StartAddr>=650)&&(StartAddr<=775)&&(StartAddr+Count<=776))||
              ((StartAddr>=800)&&(StartAddr<=899)&&(StartAddr+Count<=1024))||
                ((StartAddr>=900)&&(StartAddr<=1030)&&(StartAddr+Count<=1031))||
                  ((StartAddr>=1050)&&(StartAddr<=1180)&&(StartAddr+Count<=1181))||
                    ((StartAddr>=2500)&&(StartAddr<=2740)&&(StartAddr+Count<=2741))
                      ) return 1;
  
  return 0;
}

char IsCoilAddressCorrect(int StartAddr,int Count)
{
  if (((StartAddr>=0)&&(StartAddr<=2)&&(StartAddr+Count<=3))
      ) return 1;
  return 0;
}
extern unsigned long RainTo;  //DE 4-1-16 Rain timer
extern unsigned int cntTemp;
extern unsigned int PausedFrom;
extern unsigned int ReadSingle;
extern unsigned int RunXX;
extern unsigned char Silenced;

// acm, v2.01, defined in Main
extern char Str_to_month(char const * date);

int GetRegisterValue(int raddr, int rlen, char* data)
{
  
#ifdef __emulator
  struct stInsulationParameters TParam;
  struct stInsulationParameters *Param=&TParam;
  struct stDiagnosis TDiag;
  //  struct stDiagnosis *Diag=&TDiag;
#else
  struct stInsulationParameters *Param =TempParam;
  //  struct stDiagnosis *Diag=(struct stDiagnosis *)((0xFFFF-szInsulationParameters)-sizeof(struct stDiagnosis));
#endif
  
  int i,l,m,v;
  //unsigned int j;
  _TDateTime tmp;
  int MeasLoadedNum=-1;
  //float f,Q02Limit;
  long tmpDate,tmpTime;
  unsigned int MeasNumOk=0;
#ifndef __arm
  char Page=GetPage();
#endif
  unsigned int Year,Month,Day,Hour,Min,Sec;
  _TDate Date;
  _TTime Time;
  //_TDateTime DiagDate;
  unsigned int LogIndex;
  
#define LoadMeasurement(a) { if ((!MeasLoaded)||(MeasLoadedNum!=a)) if ((MeasNumOk)&&(a<MeasurementsInArchive)) {/*LoadArchDate(Param,a,TempPage);  DiagDate=DateToDateTime(EncodeDate(Param->Year,Param->Month,Param->Day))+TimeToDateTime(EncodeTime(Param->Hour,Param->Min,0)); DoSumDiag(DiagDate,Diag,STABLEDiagRegime,CurDiagRegime,MeasurementsInArchive-a,CurDiagType,TempPage,0);*/ LoadData(Param,a,TempPage); MeasLoaded=1; MeasLoadedNum=a;} else {MeasLoaded=0; MeasLoadedNum=0;} }
#define CheckMeasurementNum(a) {if ((a>=rgArchiveReading)&&(a<rgTDMCurrentReading)) {if ((MeasurementsInArchive>=ModBusArchMesurementNum)&&(ModBusArchMesurementNum>0)) { m=MeasurementsInArchive-ModBusArchMesurementNum; MeasNumOk=1;} else {m=0; MeasNumOk=0;} } else {m=0; MeasNumOk=0;}}
  
#define DeviceID 15002
  
  char s[5]={'\0','\0','\0','\0','\0'}; // v2.01, int to string, for register 40-2
  
  l=1;
  
  for (i=raddr;i<raddr+rlen;i++) {
    v=0;
    /*
    //Binary Status registers	1	3
    if (i==rgBinaryStatus+0) {
    //Wellow alarm
    if ((MeasurementsInArchive)&&(FullStatus==stWarning)) v=1;
    goto SetData;
  }
    if (i==rgBinaryStatus+1) {
    //Red alarm
    if ((MeasurementsInArchive)&&(FullStatus==stAlarm)) v=1;
    goto SetData;
  }
    if (i==rgBinaryStatus+2) {
    //ErrorCode
    v=Error;
    goto SetData;
  }
    */
    //General Information	        4	21
    if (i==rgGeneralInformation+0) {
      //Device ID modbus register #1
      v=DeviceID;
      goto SetData;
    }
    if (i==rgGeneralInformation+1) {
      //Firmware version modbus register #2
      //       v=SoftVersionValue;
      v=(u16)trVersionIntValue;
      goto SetData;
    }
    if (i==rgGeneralInformation+2) {
      //Device Health Status - extended.  Upper 16 bits contain additional errors for BHM (Main and PDM don't have that many yet) modbus register #3 
      // note, need to make sure Main will read this register...
      v=Error>>16;
      goto SetData;
    }
    if (i==rgGeneralInformation+3) {
      //Device Alarm status modbus register #4
      if (FullStatus) v=FullStatus-1;
      v|=(Setup.InitialParameters.TrSideParam[0].DefectCode<<8) | (Setup.InitialParameters.TrSideParam[1].DefectCode<<10);  // acm, v1.79, encode set 1/2 hi imbalance and phase swap bits to MODBUS map here
      goto SetData;
    }
    if (i==rgGeneralInformation+4) {
      //Alarm Source Group 1 modbus register #5
      //0 -No Alarms, 1 - Alarm on Gamma magnitude, 2 - Alarm on Gamma Trend, 3 - Alarm on Temperature variations, 4 - Alarm on tangent delta
      SetPage(ParametersPage);
      if ((MeasurementsInArchive)&&(Parameters->TrSideCount&(1<<0))) {
        v=Parameters->TrSide[0].AlarmStatus;
      }
      goto SetData;
    }
    if (i==rgGeneralInformation+5) {
      //Alarm Source Group 2 modbus register #6
      //0 -No Alarms, 1 - Alarm on Gamma magnitude, 2 - Alarm on Gamma Trend, 3 - Alarm on Temperature variations, 4 - Alarm on tangent delta
      SetPage(ParametersPage);
      if ((MeasurementsInArchive)&&(Parameters->TrSideCount&(1<<1))) {
        v=Parameters->TrSide[1].AlarmStatus;
      }
      goto SetData;
    }
    if (i==rgGeneralInformation+6) {
      //Alarm Source Group 3 modbus register #7 future
      //0 -No Alarms, 1 - Alarm on Gamma magnitude, 2 - Alarm on Gamma Trend, 3 - Alarm on Temperature variations, 4 - Alarm on tangent delta
      goto SetData;
    }
    if (i==rgGeneralInformation+7) {
      //Alarm Source Group 4 modbus register #8 future
      //0 -No Alarms, 1 - Alarm on Gamma magnitude, 2 - Alarm on Gamma Trend, 3 - Alarm on Temperature variations, 4 - Alarm on tangent delta
      goto SetData;
    }
    if (i==rgGeneralInformation+8) {
      //Device Health Status modbus register #9
      //v=(Error&~(((unsigned long)1<<erUnitOff)|((unsigned long)1<<erStop)|((unsigned long)1<<erPause)));
      v=Error; // acm, Claude wants erUnitOff reported as error.  Other bits don't fit in 16 bit register, see new register 3 for upper 16 bits.
      goto SetData;
    }
    if (i==rgGeneralInformation+9) {
      //Device time Month
      GET_TIME_INBUF();
      v=DateTime.Month;
      goto SetData;
    }
    if (i==rgGeneralInformation+10) {
      //Device time Day
      GET_TIME_INBUF();
      v=DateTime.Day;
      goto SetData;
    }
    if (i==rgGeneralInformation+11) {
      //Device time Year
      GET_TIME_INBUF();
      v=DateTime.Year%100+2000;
      goto SetData;
    }
    if (i==rgGeneralInformation+12) {
      //Device time Hour
      GET_TIME_INBUF();
      v=DateTime.Hour;
      goto SetData;
    }
    if (i==rgGeneralInformation+13) {
      //Device time Min
      GET_TIME_INBUF();
      v=DateTime.Min;
      goto SetData;
    }
    if (i==rgGeneralInformation+14) {
      //Number of Records in the device memory
      v=MeasurementsInArchive;
      goto SetData;
    }
    if (i==rgGeneralInformation+15) {
      //Temperature
      v=ExtTemperature[0];  // acm 7-29 change this to 4 element array, other elements set later
      goto SetData;
    }
    if (i==rgGeneralInformation+16) {
      //Load Active %
      v=ExtLoadActive;
      goto SetData;
    }
    if (i==rgGeneralInformation+17) {
      //Load Reactive %
      v=ExtLoadReactive;
      goto SetData;
    }
    if (i==rgGeneralInformation+18) {
      //��������� �� ��D �������
      v=ExtHumidity;
      goto SetData;
    }
    if (i==rgGeneralInformation+19) {
      //��������� ��� �� ��D �������
      v=ExtRPN;
      goto SetData;
    }
    if (i>=rgGeneralInformation+20 && i<=rgGeneralInformation+22) { // acm, 7-29 return 2nd, 3rd, or forth element of array.  2nd oil temp plus two others TBD by calling app.
      //Temperature
      v=ExtTemperature[1+i-(rgGeneralInformation+20)];
      goto SetData;
    }
    if (i==rgGeneralInformation+23) {
      //new v2, per PDM
      v=LoadCurrent2;
      goto SetData;
    }
    if (i==rgGeneralInformation+24) {
      //new v2, per PDM
      v=LoadCurrent3;
      goto SetData;
    }
    if (i==rgGeneralInformation+25) {
      //new v2, per PDM
      v=voltage1;
      goto SetData;
    }
    if (i==rgGeneralInformation+26) {
      //new v2, per PDM
      v=voltage2;
      goto SetData;
    }
    if (i==rgGeneralInformation+27) {
      //new v2, per PDM
      v=voltage;
      goto SetData;
    }
    if (i==rgGeneralInformation+29) {
      //new v2.03, Event Trigger for BHM2C50
      v=EventTrigger;
      goto SetData;
    }
    // v2.01
    if (i==rgGeneralInformation+39) {
      v=Str_to_month((char const *)fw_build_date);	
      goto SetData;
    }
    // v2.01
    if (i==rgGeneralInformation+40) {
      s[0]=s[1]='0';
      s[2]=fw_build_date[4];
      s[3]=fw_build_date[5];
      v=(s16)StrToInt(s);	//return build date
      goto SetData;
    }
    // v2.01
    if (i==rgGeneralInformation+41) {
      s[0]=fw_build_date[7];
      s[1]=fw_build_date[8];
      s[2]=fw_build_date[9];
      s[3]=fw_build_date[10];
      v=(s16)StrToInt(s);	//return build year
      goto SetData;
    }
    if (i==rgGeneralInformation+42) {  //2.06 reg 43
      //new v2.06, Reset Count
      v=Setup.SetupInfo.ResetCount;
      goto SetData;
    }
    
    
    //Device Settings	        51	450
    if (i==rgDeviceSettings+0) {
      //��� ���������� ������� (0-����� �������� 1-�� �������)
      v=Setup.SetupInfo.ScheduleType;
      goto SetData;
    }
    if (i==rgDeviceSettings+1) {
      //���� � ������ ���������� �������
      v=(unsigned int)Setup.SetupInfo.dTime.Hour*100+(unsigned int)Setup.SetupInfo.dTime.Min;
      goto SetData;
    }
    if ((i>=rgDeviceSettings+2)&&(i<=rgDeviceSettings+51)) {
      //���� � ������ ���������� �������
      v=(unsigned int)Setup.SetupInfo.MeasurementsInfo[i-(rgDeviceSettings+2)].Hour*100+(unsigned int)Setup.SetupInfo.MeasurementsInfo[i-(rgDeviceSettings+2)].Min;
      goto SetData;
    }
    if (i==rgDeviceSettings+52) {
      //������ ���������� ���� register #103
      v=Setup.SetupInfo.DisplayFlag;
      goto SetData;
    }
    if (i==rgDeviceSettings+53) {
      //Out Time reg #104
      v=Setup.SetupInfo.OutTime;
      goto SetData;
    }
    if (i==rgDeviceSettings+54) {
      //Relay reg #105
      v=Setup.SetupInfo.Relay;
      goto SetData;
    }
    if (i==rgDeviceSettings+55) {
      //Time of Relay test output ,sec reg #106
      v=Setup.SetupInfo.TimeOfRelayAlarm;
      goto SetData;
    }
    if (i==rgDeviceSettings+56) {
      //Device Number reg #107
      v=Setup.SetupInfo.DeviceNumber;
      goto SetData;
    }
    if (i==rgDeviceSettings+57) {
      //�������� ������ � ���������� reg #108
      //0-9600
      //1-38400
      //2-57600
      //3 - 115200
      //4 - 230400
      //5 - 500000
      //6 - 1000000
      if (Setup.SetupInfo.BaudRate==6) v=(int)((unsigned int)10000);
      else if (Setup.SetupInfo.BaudRate==5) v=(int)((unsigned int)5000);
      else if (Setup.SetupInfo.BaudRate==4) v=(int)((unsigned int)2304);
      else if (Setup.SetupInfo.BaudRate==3) v=(int)((unsigned int)1152);
      else if (Setup.SetupInfo.BaudRate==2) v=(int)((unsigned int)576);
      else if (Setup.SetupInfo.BaudRate==1) v=(int)((unsigned int)384);
      else v=96;
      goto SetData;
    }
    if (i==rgDeviceSettings+58) {
      //��� ������������� ��������� ModBus reg #109
      //0-RTU
      //1-TCP
      v=Setup.SetupInfo.ModBusProtocol;
      goto SetData;
    }
    if (i==rgDeviceSettings+59) {
      //Analog Parameters Input Selector reg #110
      //0-From Device Analog Inputs,
      //1 - From Regeisters
      v=Setup.SetupInfo.ReadAnalogFromRegister;
      goto SetData;
    }
    if (i==rgDeviceSettings+60) {
      //Calc Zk reg #111
      v=Setup.ZkSetupInfo.CalcZkOnSide;
      goto SetData;
    }
    if (i==rgDeviceSettings+61) {
      //Days to calculate Zk reg #112
      v=Setup.ZkSetupInfo.ZkDays;
      goto SetData;
    }
    
    //DWE 10-13-15 Adding new read registers starting here    
    if (i==rgDeviceSettings+62) {
      //Job1 reg #113
      v=Setup.SetupInfo.Job1;
      goto SetData;
    }
    if (i==rgDeviceSettings+63) {
      //Job2 reg #114
      v=Setup.SetupInfo.Job2;
      goto SetData;
    }
    if (i==rgDeviceSettings+64) {
      //LoadCurrentThresholdLevel reg #115
      v=Setup.SetupInfo.LoadCurrentThresholdLevel;
      goto SetData;
    }
    if (i==rgDeviceSettings+65) {
      //LoadCurrentThresholdSettings reg #116
      v=Setup.SetupInfo.LoadCurrentThresholdSettings;
      goto SetData;
    }
    if (i==rgDeviceSettings+66) {
      //ConfigSaveCount reg #117
      v=Setup.SetupInfo.ConfigSaveCount;
      goto SetData;
    }
    if (i==rgDeviceSettings+67) {
      //SN reg #118
      v=Setup.SetupInfo.SN;
      goto SetData;
    }
    if (i==rgDeviceSettings+68) {
      //Operating Frequency reg #119
      v=Setup.SetupInfo.InternalSyncFrequency;
      goto SetData;
    }
    if (i==rgDeviceSettings+69) {
      //Calibration threshold reg #120
      v = Setup.SetupInfo.CalibTreshould;
      goto SetData;
    }
    
    //End of rgDeviceSettings reserved to 120
    
    if (i==rgGeneralGammaSettings+0) {
      //Averaging for Gamma
      v=Setup.GammaSetupInfo.AveragingForGamma;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+1) {
      //Days to Calculate Trend (Can not be less then 10 days)
      v=Setup.GammaSetupInfo.DaysToCalculateTrend;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+2) {
      //Days to Calculate Temperature Coefficient
      v=Setup.GammaSetupInfo.DaysToCalculateTCoefficient;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+3) {
      //Days to Calculate BASELINE
      v=Setup.GammaSetupInfo.DaysToCalculateBASELINE;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+4) {
      //���������� ���������� ��� �� 120
      v=Setup.GammaSetupInfo.AllowedPhaseDispersion;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+5) {
      //Alarm Hysteresis (Releases an Alarm, if an alarming parameter drops below (PA - PA*AH%) of alarm threshold PA)
      v=Setup.GammaSetupInfo.AlarmHysteresis;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+6) {
      //�����(���), ����� ������� ������������� �����
      v=Setup.GammaSetupInfo.ReReadOnAlarm;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+7) {
      //����� ���� ��� ������� ��������
      v=Setup.GammaSetupInfo.LoadChannel;
      goto SetData;
    }
    // X==0, avg freeze, kinda dumb
    // X==.5, 2pt avg, avg changes more quickly
    // X==.33, old alg, pre-2.00 behavior
    // X==1, avg off, becomes last measured Unn
    if (i==rgGeneralGammaSettings+8) {
      //Averaging for Gamma Coefficient, new for v2.00
      v=Setup.GammaSetupInfo.AveragingForGamma_X;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+0) {
      //Status
      if (Setup.GammaSetupInfo.ReadOnSide&(1<<HVSide)) v=1; else v=0;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+1) {
      //������� ���������� (kV)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].RatedVoltage*10.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+2) {
      //������� ��� (A)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].RatedCurrent*10.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+3) {
      //0 bit is 1 - unused
      //1 bit is 1 - Trend to Alarm ON
      //2 bit is 1 - Temperature coefficient to Alarm ON
      v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].AlarmEnable;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+4) {
      //������ Gamma � % (Byte*0.05)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].GammaYellowThresholld*0.05*10.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+5) {
      //������ Gamma � % (Byte*0.05)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].GammaRedThresholld*0.05*10.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+6) {
      //Gamma Temperature Coefficient Alarm Threshold (Byte*0.002)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].TCoefficient*0.002*1000.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+7) {
      //Gamma Trend Alarm Threshold (Byte*0.2	  Max- 5%/Yr)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].TrendAlarm*1*10.0);  //acm, 4-27 change from .2 to 1
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+8) {
      //������ Tg *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].TgYellowThresholld);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+9) {
      //������ Tg *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].TgRedThresholld);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+10) {
      //������ Tg *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].TgVariationThresholld);
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup1+11)&&(i<=rgDeviceSettingsGroup1+13)) {
      //Input Impedabce *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[BushingA+(i-(rgDeviceSettingsGroup1+11))]);
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup1+14)&&(i<=rgDeviceSettingsGroup1+16)) {
      //Off-Line Tangent Value *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].Tg0[BushingA+(i-(rgDeviceSettingsGroup1+14))]);
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup1+17)&&(i<=rgDeviceSettingsGroup1+19)) {
      //Off-Line Capacity  Value *10
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[HVSide].C0[BushingA+(i-(rgDeviceSettingsGroup1+17))]);
      goto SetData;
    }
    
    if (i==rgDeviceSettingsGroup2+0) {
      //Status
      if (Setup.GammaSetupInfo.ReadOnSide&(1<<LVSide)) v=1; else v=0;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+1) {
      //������� ���������� (kV)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].RatedVoltage*10.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+2) {
      //������� ��� (A)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].RatedCurrent*10.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+3) {
      //0 bit is 1 - unused
      //1 bit is 1 - Trend to Alarm ON
      //2 bit is 1 - Temperature coefficient to Alarm ON
      v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].AlarmEnable;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+4) {
      //������ Gamma � % (Byte*0.05)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].GammaYellowThresholld*0.05*10.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+5) {
      //������ Gamma � % (Byte*0.05)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].GammaRedThresholld*0.05*10.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+6) {
      //Gamma Temperature Coefficient Alarm Threshold (Byte*0.002)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].TCoefficient*0.002*1000.0);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+7) {
      //Gamma Trend Alarm Threshold (Byte*0.2	  Max- 5%/Yr)
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].TrendAlarm*1*10.0);  //acm, 4-27 change from .2 to 1
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+8) {
      //������ Tg *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].TgYellowThresholld);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+9) {
      //������ Tg *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].TgRedThresholld);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+10) {
      //������ Tg *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].TgVariationThresholld);
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup2+11)&&(i<=rgDeviceSettingsGroup2+13)) {
      //Input Impedabce *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].InputImpedance[BushingA+(i-(rgDeviceSettingsGroup2+11))]);
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup2+14)&&(i<=rgDeviceSettingsGroup2+16)) {
      //Off-Line Tangent Value *100
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].Tg0[BushingA+(i-(rgDeviceSettingsGroup2+14))]);
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup2+17)&&(i<=rgDeviceSettingsGroup2+19)) {
      //Off-Line Capacity Value *10
      v=(int)(Setup.GammaSetupInfo.GammaSideSetup[LVSide].C0[BushingA+(i-(rgDeviceSettingsGroup2+17))]);
      goto SetData;
    }
   // ************** Logs 
    //LOG Month
    if ((i==rgDeviceLogs+0)||
        (i==rgDeviceLogs+6)||
          (i==rgDeviceLogs+12)||
            (i==rgDeviceLogs+18)||
              (i==rgDeviceLogs+24)||
                (i==rgDeviceLogs+30)||
                  (i==rgDeviceLogs+36)||
                    (i==rgDeviceLogs+42)||
                      (i==rgDeviceLogs+48)||
                        (i==rgDeviceLogs+54)||
                          (i==rgDeviceLogs+60)||
                            (i==rgDeviceLogs+66)||
                              (i==rgDeviceLogs+72)||
                                (i==rgDeviceLogs+78)||
                                  (i==rgDeviceLogs+84)||
                                    (i==rgDeviceLogs+90)||
                                      (i==rgDeviceLogs+96)||
                                        (i==rgDeviceLogs+102)||
                                          (i==rgDeviceLogs+108)||
                                            (i==rgDeviceLogs+114)
                                              ) {
                                                LogIndex=LogNumToIndex(((i-rgDeviceLogs)/6+1));
                                                LoadLog(LogIndex);
                                                if ((LogIndex<MaxLogData)&&IsDate(LogData.Year+(long)2000,LogData.Month,LogData.Day)&&(LogData.Hour<24)&&(LogData.Min<60)) {
                                                  v=LogData.Month;
                                                }
                                              }
    //LOG Day
    if ((i==rgDeviceLogs+1)||
        (i==rgDeviceLogs+7)||
          (i==rgDeviceLogs+13)||
            (i==rgDeviceLogs+19)||
              (i==rgDeviceLogs+25)||
                (i==rgDeviceLogs+31)||
                  (i==rgDeviceLogs+37)||
                    (i==rgDeviceLogs+43)||
                      (i==rgDeviceLogs+49)||
                        (i==rgDeviceLogs+55)||
                          (i==rgDeviceLogs+61)||
                            (i==rgDeviceLogs+67)||
                              (i==rgDeviceLogs+73)||
                                (i==rgDeviceLogs+79)||
                                  (i==rgDeviceLogs+85)||
                                    (i==rgDeviceLogs+91)||
                                      (i==rgDeviceLogs+97)||
                                        (i==rgDeviceLogs+103)||
                                          (i==rgDeviceLogs+109)||
                                            (i==rgDeviceLogs+115)
                                              ) {
                                                LogIndex=LogNumToIndex(((i-rgDeviceLogs)/6+1));
                                                LoadLog(LogIndex);
                                                if ((LogIndex<MaxLogData)&&IsDate(LogData.Year+(long)2000,LogData.Month,LogData.Day)&&(LogData.Hour<24)&&(LogData.Min<60)) {
                                                  v=LogData.Day;
                                                }
                                              }
    //LOG Year
    if ((i==rgDeviceLogs+2)||
        (i==rgDeviceLogs+8)||
          (i==rgDeviceLogs+14)||
            (i==rgDeviceLogs+20)||
              (i==rgDeviceLogs+26)||
                (i==rgDeviceLogs+32)||
                  (i==rgDeviceLogs+38)||
                    (i==rgDeviceLogs+44)||
                      (i==rgDeviceLogs+50)||
                        (i==rgDeviceLogs+56)||
                          (i==rgDeviceLogs+62)||
                            (i==rgDeviceLogs+68)||
                              (i==rgDeviceLogs+74)||
                                (i==rgDeviceLogs+80)||
                                  (i==rgDeviceLogs+86)||
                                    (i==rgDeviceLogs+92)||
                                      (i==rgDeviceLogs+98)||
                                        (i==rgDeviceLogs+104)||
                                          (i==rgDeviceLogs+110)||
                                            (i==rgDeviceLogs+116)
                                              ) {
                                                LogIndex=LogNumToIndex(((i-rgDeviceLogs)/6+1));
                                                LoadLog(LogIndex);
                                                if ((LogIndex<MaxLogData)&&IsDate(LogData.Year+(long)2000,LogData.Month,LogData.Day)&&(LogData.Hour<24)&&(LogData.Min<60)) {
                                                  v=LogData.Year%100+(long)2000;
                                                }
                                              }
    //LOG Hour
    if ((i==rgDeviceLogs+3)||
        (i==rgDeviceLogs+9)||
          (i==rgDeviceLogs+15)||
            (i==rgDeviceLogs+21)||
              (i==rgDeviceLogs+27)||
                (i==rgDeviceLogs+33)||
                  (i==rgDeviceLogs+39)||
                    (i==rgDeviceLogs+45)||
                      (i==rgDeviceLogs+51)||
                        (i==rgDeviceLogs+57)||
                          (i==rgDeviceLogs+63)||
                            (i==rgDeviceLogs+69)||
                              (i==rgDeviceLogs+75)||
                                (i==rgDeviceLogs+81)||
                                  (i==rgDeviceLogs+87)||
                                    (i==rgDeviceLogs+93)||
                                      (i==rgDeviceLogs+99)||
                                        (i==rgDeviceLogs+105)||
                                          (i==rgDeviceLogs+111)||
                                            (i==rgDeviceLogs+117)
                                              ) {
                                                LogIndex=LogNumToIndex(((i-rgDeviceLogs)/6+1));
                                                LoadLog(LogIndex);
                                                if ((LogIndex<MaxLogData)&&IsDate(LogData.Year+(long)2000,LogData.Month,LogData.Day)&&(LogData.Hour<24)&&(LogData.Min<60)) {
                                                  v=LogData.Hour;
                                                }
                                              }
    //LOG Min
    if ((i==rgDeviceLogs+4)||
        (i==rgDeviceLogs+10)||
          (i==rgDeviceLogs+16)||
            (i==rgDeviceLogs+22)||
              (i==rgDeviceLogs+28)||
                (i==rgDeviceLogs+34)||
                  (i==rgDeviceLogs+40)||
                    (i==rgDeviceLogs+46)||
                      (i==rgDeviceLogs+52)||
                        (i==rgDeviceLogs+58)||
                          (i==rgDeviceLogs+64)||
                            (i==rgDeviceLogs+70)||
                              (i==rgDeviceLogs+76)||
                                (i==rgDeviceLogs+82)||
                                  (i==rgDeviceLogs+88)||
                                    (i==rgDeviceLogs+94)||
                                      (i==rgDeviceLogs+100)||
                                        (i==rgDeviceLogs+106)||
                                          (i==rgDeviceLogs+112)||
                                            (i==rgDeviceLogs+118)
                                              ) {
                                                LogIndex=LogNumToIndex(((i-rgDeviceLogs)/6+1));
                                                LoadLog(LogIndex);
                                                if ((LogIndex<MaxLogData)&&IsDate(LogData.Year+(long)2000,LogData.Month,LogData.Day)&&(LogData.Hour<24)&&(LogData.Min<60)) {
                                                  v=LogData.Min;
                                                }
                                              }
    //LOG Code
    if ((i==rgDeviceLogs+5)||
        (i==rgDeviceLogs+11)||
          (i==rgDeviceLogs+17)||
            (i==rgDeviceLogs+23)||
              (i==rgDeviceLogs+29)||
                (i==rgDeviceLogs+35)||
                  (i==rgDeviceLogs+41)||
                    (i==rgDeviceLogs+47)||
                      (i==rgDeviceLogs+53)||
                        (i==rgDeviceLogs+59)||
                          (i==rgDeviceLogs+65)||
                            (i==rgDeviceLogs+71)||
                              (i==rgDeviceLogs+77)||
                                (i==rgDeviceLogs+83)||
                                  (i==rgDeviceLogs+89)||
                                    (i==rgDeviceLogs+95)||
                                      (i==rgDeviceLogs+101)||
                                        (i==rgDeviceLogs+107)||
                                          (i==rgDeviceLogs+113)||
                                            (i==rgDeviceLogs+119)
                                              ) {
                                                LogIndex=LogNumToIndex(((i-rgDeviceLogs)/6+1));
                                                LoadLog(LogIndex);
                                                if ((LogIndex<MaxLogData)&&IsDate(LogData.Year+(long)2000,LogData.Month,LogData.Day)&&(LogData.Hour<24)&&(LogData.Min<60)) {
                                                  v=LogData.LogCode;
                                                }
                                              }
    
    
    if ((i==rgCalibrationCoeff+0)||
        (i==rgCalibrationCoeff+2)||
          (i==rgCalibrationCoeff+4)||
            (i==rgCalibrationCoeff+6))
    {
      //Temperature Coef Channel
      v=(int)(Setup.CalibrationCoeff.TemperatureK[(i-(rgCalibrationCoeff+0))/2]*100);
      goto SetData;
    }
    if ((i==rgCalibrationCoeff+1)||
        (i==rgCalibrationCoeff+3)||
          (i==rgCalibrationCoeff+5)||
            (i==rgCalibrationCoeff+7))
    {
      //Temperature Shift Channel
      v=(int)(Setup.CalibrationCoeff.TemperatureB[(i-(rgCalibrationCoeff+0))/2]*100);
      goto SetData;
    }
    if ((i==rgCalibrationCoeff+10)||
        (i==rgCalibrationCoeff+12)||
          (i==rgCalibrationCoeff+14)||
            (i==rgCalibrationCoeff+16))
    {
      //Analog input channel 1 Coef
      v=(int)(Setup.CalibrationCoeff.CurrentK[(i-(rgCalibrationCoeff+10))/2]*100);
      goto SetData;
    }
    if ((i==rgCalibrationCoeff+11)||
        (i==rgCalibrationCoeff+13)||
          (i==rgCalibrationCoeff+15)||
            (i==rgCalibrationCoeff+17))
    {
      //Analog input channel 1 Shift
      v=(int)(Setup.CalibrationCoeff.CurrentB[(i-(rgCalibrationCoeff+10))/2]*100);
      goto SetData;
    }
    if (i==rgCalibrationCoeff+20) {
      //Humidity Offset
      v=(int)(Setup.CalibrationCoeff.HumidityOffset[0]*10);
      goto SetData;
    }
    if (i==rgCalibrationCoeff+21) {
      //Humidity Slope
      v=(int)(Setup.CalibrationCoeff.HumiditySlope[0]*10);
      goto SetData;
    }
    
    
    //Device Logs	                451	570
    
    
    ////Contol Commands	        601	650
    //    if (i==rgContolCommands+1) {
    //       //Pause the device
    //       if (Error&((unsigned long)1<<erPause)) {
    //          GET_TIME_INBUF();
    //          j=(unsigned int)DateTime.Hour*(unsigned int)60+(unsigned int)DateTime.Min;
    //          if (j>=PausedFrom)
    //             j-=PausedFrom;
    //          else
    //             j=(MaxMinPerDay-PausedFrom)+j;
    //          j=MaxPauseMin-j;
    //          v=(j/60*100)+j%60;
    //       }
    //       goto SetData;
    //    }
    //*****************Contol Commands	        601	650
    if (i==rgContolCommands+1) {      //reg 602
      //Pause the device
      if (Error&((unsigned long)1<<erPause)) {
        tmp=GetTic32();
        tmp=MaxPauseMin-(tmp-PausedFrom)/32/60;
        v=(tmp/60*100)+tmp%60;
      }
      goto SetData;
    }
    
    if (i==rgContolCommands+2) {
      //Stops the device
      if (Setup.SetupInfo.Stopped==1) v=1;
      goto SetData;
    }
    if (i==rgContolCommands+5) {
      //Balance
      v=AutoBalance;
      goto SetData;
    }
    if (i==rgContolCommands+7) {
      //Delete Archive Rec # from
      v=DelFrom;
      goto SetData;
    }
    if (i==rgContolCommands+8) {
      //Delete Archive Rec # to
      v=DelTo;
      goto SetData;
    }
    if (i==rgContolCommands+9) {
      //Starts deleting the above records
      goto SetData;
    }
    // DE 5-12-15 tried this but did not fix "device data delete failed"    
    //    if (i==rgContolCommands+9) {
    //       //Starts deleting the above records
    //       v=MeasurementsInArchive;
    //       goto SetData;
    //    }
    //    if (i==rgContolCommands+10) {
    //       v=MeasurementsInArchive;
    //       goto SetData;
    //    }
    if (i==rgContolCommands+12) {
      //The Registers for the Latest Record Request (after the date), Month
      v=ArchDateTime.Month;
      goto SetData;
    }
    if (i==rgContolCommands+13) {
      //The Registers for the Latest Record Request (after the date), Day
      v=ArchDateTime.Day;
      goto SetData;
    }
    if (i==rgContolCommands+14) {
      //The Registers for the Latest Record Request (after the date), Year
      v=ArchDateTime.Year%100+2000;
      goto SetData;
    }
    if (i==rgContolCommands+15) {
      //The Registers for the Latest Record Request (after the date), Hour
      v=ArchDateTime.Hour;
      goto SetData;
    }
    if (i==rgContolCommands+16) {
      //The Registers for the Latest Record Request (after the date), Min
      v=ArchDateTime.Min;
      goto SetData;
    }
    if (i==rgContolCommands+17) {
      //Order Number of record after the date above
      
      //Get Measurement Number On Arch Date
      if ((IsDate(ArchDateTime.Year,ArchDateTime.Month,ArchDateTime.Day))&&
          (IsTime(ArchDateTime.Hour,ArchDateTime.Min,ArchDateTime.Sec))) {
            
            if (ArchDateTime.Year<2000)
              tmpDate=((long)ArchDateTime.Year+(long)2000)*(long)10000;
            else
              tmpDate=(long)ArchDateTime.Year*(long)10000;
            tmpDate+=(long)ArchDateTime.Month*(long)100;
            tmpDate+=(long)ArchDateTime.Day;
            tmpTime=(long)ArchDateTime.Hour*(long)10000;
            tmpTime+=(long)ArchDateTime.Min*(long)100;
            tmpTime+=(long)ArchDateTime.Sec;
            ModBusArchMesurementNumOnDate=GetMOnDate(tmpDate,tmpTime);
            v=ModBusArchMesurementNumOnDate;
          }
      goto SetData;
    }
    if (i==rgContolCommands+18) {
      v=ModBusArchMesurementNum;
      goto SetData;
    }
    //Auto Balance
    if (i==rgContolCommands+21) {
      // Auto balans Month
      v=Setup.SetupInfo.AutoBalans.Month;
    }
    if (i==rgContolCommands+22) {
      // Auto balans Day
      v=Setup.SetupInfo.AutoBalans.Day;
    }
    if (i==rgContolCommands+23) {
      // Auto balans Year
      v=Setup.SetupInfo.AutoBalans.Year%100+2000;
    }
    if (i==rgContolCommands+24) {
      // Auto balans Hour
      v=Setup.SetupInfo.AutoBalans.Hour;
    }
    if (i==rgContolCommands+25) {
      // Auto balans Unit Work
      if (Setup.SetupInfo.AutoBalansActive)
        v=Setup.SetupInfo.AutoBalansActive-1;
      else
        v=Setup.SetupInfo.WorkingDays;
    }
    if (i==rgContolCommands+26) {
      // Auto balans Run
      if (Setup.SetupInfo.AutoBalansActive)
        v=1;
    }
    if (i==rgContolCommands+27) {
      // No LOad Test Run
      if (Setup.SetupInfo.NoLoadTestActive)
        v=1;
    }
    //    if (i==rgContolCommands+28) {      //reg 629
    //       // FastWriteCount
    //          v=FastWriteCount;
    //    }
    if (i==rgContolCommands+29) {      //reg 630  //DE 4-1-16 read Rain timer
      if (RainTo==0){
        v=0;}
      else{
        v = (unsigned int)(RainTo/32/60)+1;}
    }
    
    
    //Initial Readings All Groups	651	774
    if ((i==rgInitialReadings1+0)||(i==rgInitialReadings2+0)||(i==rgInitialReadings3+0)||(i==rgInitialReadings4+0)) {
      v=Setup.InitialParameters.Month;
      goto SetData;
    }
    if ((i==rgInitialReadings1+1)||(i==rgInitialReadings2+1)||(i==rgInitialReadings3+1)||(i==rgInitialReadings4+1)) {
      v=Setup.InitialParameters.Day;
      goto SetData;
    }
    if ((i==rgInitialReadings1+2)||(i==rgInitialReadings2+2)||(i==rgInitialReadings3+2)||(i==rgInitialReadings4+2)) {
      v=Setup.InitialParameters.Year%100+2000;
      goto SetData;
    }
    if ((i==rgInitialReadings1+3)||(i==rgInitialReadings2+3)||(i==rgInitialReadings3+3)||(i==rgInitialReadings4+3)) {
      v=Setup.InitialParameters.Hour;
      goto SetData;
    }
    if ((i==rgInitialReadings1+4)||(i==rgInitialReadings2+4)||(i==rgInitialReadings3+4)||(i==rgInitialReadings4+4)) {
      v=Setup.InitialParameters.Min;
      goto SetData;
    }
    //Initial Readings Groups	1
    if ((i>=rgInitialReadings1+5)&&(i<=rgInitialReadings1+7)) {
      //Capacitance - Off Line
      v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].C0[i-(rgInitialReadings1+5)];
      goto SetData;
    }
    if ((i>=rgInitialReadings1+8)&&(i<=rgInitialReadings1+10)) {
      //Tangent - Off Line
      v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].Tg0[i-(rgInitialReadings1+8)];
      goto SetData;
    }
    if (i==rgInitialReadings1+11) {
      //Temperature Off Line
      v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].Temperature0;
      goto SetData;
    }
    if ((i>=rgInitialReadings1+12)&&(i<=rgInitialReadings1+14)) {
      //Input Impedance
      v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[i-(rgInitialReadings1+12)];
      goto SetData;
    }
    if ((i>=rgInitialReadings1+15)&&(i<=rgInitialReadings1+17)) {
      //Sensor Capacitance
      v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputC[i-(rgInitialReadings1+15)];
      goto SetData;
    }
    if (i==rgInitialReadings1+18) {
      //Gamma
      v=Setup.InitialParameters.TrSideParam[HVSide].GammaAmplitude;
      goto SetData;
    }
    if (i==rgInitialReadings1+19) {
      //Gamma Phase To A
      v=Setup.InitialParameters.TrSideParam[HVSide].GammaPhase;
      goto SetData;
    }
    if ((i>=rgInitialReadings1+20)&&(i<=rgInitialReadings1+22)) {
      //Bushing Current
      v=Setup.InitialParameters.TrSideParam[HVSide].SourceAmplitude[i-(rgInitialReadings1+20)];
      goto SetData;
    }
    if ((i>=rgInitialReadings1+23)&&(i<=rgInitialReadings1+25)) {
      //Bushing Current Phase
      v=Setup.InitialParameters.TrSideParam[HVSide].SourcePhase[i-(rgInitialReadings1+23)];
      goto SetData;
    }
    if (i==rgInitialReadings1+26) {
      //Temperature
      v=Setup.InitialParameters.TrSideParam[HVSide].Temperature;
      goto SetData;
    }
    if (i==rgInitialReadings1+27) {
      //Load Active
      v=Setup.InitialParameters.TrSideParam[HVSide].Current;
      goto SetData;
    }
    if (i==rgInitialReadings1+28) {
      //Load Active
      v=Setup.InitialParameters.TrSideParam[HVSide].RPN;
      goto SetData;
    }
    //Initial Readings Groups	2
    if ((i>=rgInitialReadings2+5)&&(i<=rgInitialReadings2+7)) {
      //Capacitance - Off Line
      v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].C0[i-(rgInitialReadings2+5)];
      goto SetData;
    }
    if ((i>=rgInitialReadings2+8)&&(i<=rgInitialReadings2+10)) {
      //Tangent - Off Line
      v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].Tg0[i-(rgInitialReadings2+8)];
      goto SetData;
    }
    if (i==rgInitialReadings2+11) {
      //Temperature Off Line
      v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].Temperature0;
      goto SetData;
    }
    if ((i>=rgInitialReadings2+12)&&(i<=rgInitialReadings2+14)) {
      //Input Impedance
      v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].InputImpedance[i-(rgInitialReadings2+12)];
      goto SetData;
    }
    if ((i>=rgInitialReadings2+15)&&(i<=rgInitialReadings2+17)) {
      //Sensor Capacitance
      v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].InputC[i-(rgInitialReadings2+15)];
      goto SetData;
    }
    if (i==rgInitialReadings2+18) {
      //Gamma
      v=Setup.InitialParameters.TrSideParam[LVSide].GammaAmplitude;
      goto SetData;
    }
    if (i==rgInitialReadings1+19) {
      //Gamma Phase To A
      v=Setup.InitialParameters.TrSideParam[LVSide].GammaPhase;
      goto SetData;
    }
    if ((i>=rgInitialReadings2+20)&&(i<=rgInitialReadings2+22)) {
      //Bushing Current
      v=Setup.InitialParameters.TrSideParam[LVSide].SourceAmplitude[i-(rgInitialReadings2+20)];
      goto SetData;
    }
    if ((i>=rgInitialReadings2+23)&&(i<=rgInitialReadings2+25)) {
      //Bushing Current Phase
      v=Setup.InitialParameters.TrSideParam[LVSide].SourcePhase[i-(rgInitialReadings2+23)];
      goto SetData;
    }
    if (i==rgInitialReadings2+26) {
      //Temperature
      v=Setup.InitialParameters.TrSideParam[LVSide].Temperature;
      goto SetData;
    }
    if (i==rgInitialReadings2+27) {
      //Load Active
      v=Setup.InitialParameters.TrSideParam[LVSide].Current;
      goto SetData;
    }
    if (i==rgInitialReadings2+28) {
      //Load Active
      v=Setup.InitialParameters.TrSideParam[LVSide].RPN;
      goto SetData;
    }
    
    //Base Line All Groups	        801	900
    //Base Line Group 1
    if (i==rgBaseLine1+0){
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLEDate,&Date,&Time);
            DecodeDate(Date,&Year,&Month,&Day);
            v=Month;
          }
      goto SetData;
    }
    if (i==rgBaseLine1+1) {
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLEDate,&Date,&Time);
            DecodeDate(Date,&Year,&Month,&Day);
            v=Day;
          }
      goto SetData;
    }
    if (i==rgBaseLine1+2) {
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLEDate,&Date,&Time);
            DecodeDate(Date,&Year,&Month,&Day);
            v=Year%100+2000;
          }
      goto SetData;
    }
    if (i==rgBaseLine1+3){
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLEDate,&Date,&Time);
            DecodeTime(Time,&Hour,&Min,&Sec);
            v=Hour;
          }
      goto SetData;
    }
    if (i==rgBaseLine1+4){
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLEDate,&Date,&Time);
            DecodeTime(Time,&Hour,&Min,&Sec);
            v=Min;
          }
      goto SetData;
    }
    if (i==rgBaseLine1+5){
      //Average Temperature
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].AvgTemperature1;
          }
      goto SetData;
    }
    if (i==rgBaseLine1+6){
      //Average Load Active %
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLEAvgCurrent;
          }
      goto SetData;
    }
    if ((i>=rgBaseLine1+7)&&(i<=rgBaseLine1+9)){
      //Capacitance
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLEC[i-(rgBaseLine1+7)];
          }
      goto SetData;
    }
    if ((i>=rgBaseLine1+10)&&(i<=rgBaseLine1+12)){
      //Tg
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLETg[i-(rgBaseLine1+10)];
          }
      goto SetData;
    }
    if ((i>=rgBaseLine1+13)&&(i<=rgBaseLine1+15)){
      //Bushing Current
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].StableSourceAmplitude[i-(rgBaseLine1+13)];
          }
      goto SetData;
    }
    if ((i>=rgBaseLine1+16)&&(i<=rgBaseLine1+18)){
      //Bushing Current Phase
      if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[HVSide].StableSourcePhase[i-(rgBaseLine1+16)];
          }
      goto SetData;
    }
    //Base Line Group 2
    if (i==rgBaseLine2+0){
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLEDate,&Date,&Time);
            DecodeDate(Date,&Year,&Month,&Day);
            v=Month;
          }
      goto SetData;
    }
    if (i==rgBaseLine2+1) {
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLEDate,&Date,&Time);
            DecodeDate(Date,&Year,&Month,&Day);
            v=Day;
          }
      goto SetData;
    }
    if (i==rgBaseLine2+2) {
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLEDate,&Date,&Time);
            DecodeDate(Date,&Year,&Month,&Day);
            v=Year%100+2000;
          }
      goto SetData;
    }
    if (i==rgBaseLine2+3){
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLEDate,&Date,&Time);
            DecodeTime(Time,&Hour,&Min,&Sec);
            v=Hour;
          }
      goto SetData;
    }
    if (i==rgBaseLine2+4){
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            DoUnPackDateTime(Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLEDate,&Date,&Time);
            DecodeTime(Time,&Hour,&Min,&Sec);
            v=Min;
          }
      goto SetData;
    }
    if (i==rgBaseLine2+5){
      //Average Temperature
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].AvgTemperature1;
          }
      goto SetData;
    }
    if (i==rgBaseLine2+6){
      //Average Load Active %
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLEAvgCurrent;
          }
      goto SetData;
    }
    if ((i>=rgBaseLine2+7)&&(i<=rgBaseLine2+9)){
      //Capacitance
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLEC[i-(rgBaseLine2+7)];
          }
      goto SetData;
    }
    if ((i>=rgBaseLine2+10)&&(i<=rgBaseLine2+12)){
      //Tg
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLETg[i-(rgBaseLine2+10)];
          }
      goto SetData;
    }
    if ((i>=rgBaseLine2+13)&&(i<=rgBaseLine2+15)){
      //Bushing Current
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].StableSourceAmplitude[i-(rgBaseLine2+13)];
          }
      goto SetData;
    }
    if ((i>=rgBaseLine2+16)&&(i<=rgBaseLine2+18)){
      //Bushing Current Phase
      if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==1)||
          (Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved==2)) {
            v=Setup.GammaSetupInfo.GammaSideSetup[LVSide].StableSourcePhase[i-(rgBaseLine2+16)];
          }
      goto SetData;
    }
    
    //Current Reading All Groups	901	1024
    if (MeasurementsInArchive) {
      SetPage(ParametersPage);
      if ((i==rgCurrentReading1+0)||(i==rgCurrentReading2+0)||(i==rgCurrentReading3+0)||(i==rgCurrentReading4+0)) {
        v=Parameters->Month;
        goto SetData;
      }
      if ((i==rgCurrentReading1+1)||(i==rgCurrentReading2+1)||(i==rgCurrentReading3+1)||(i==rgCurrentReading4+1)) {
        v=Parameters->Day;
        goto SetData;
      }
      if ((i==rgCurrentReading1+2)||(i==rgCurrentReading2+2)||(i==rgCurrentReading3+2)||(i==rgCurrentReading4+2)) {
        v=Parameters->Year%100+2000;
        goto SetData;
      }
      if ((i==rgCurrentReading1+3)||(i==rgCurrentReading2+3)||(i==rgCurrentReading3+3)||(i==rgCurrentReading4+3)) {
        v=Parameters->Hour;
        goto SetData;
      }
      if ((i==rgCurrentReading1+4)||(i==rgCurrentReading2+4)||(i==rgCurrentReading3+4)||(i==rgCurrentReading4+4)) {
        v=Parameters->Min;
        goto SetData;
      }
      //Current Reading Group 1
      if (i==rgCurrentReading1+5) {
        //Gamma
        v=Parameters->TrSide[HVSide].Gamma;
        goto SetData;
      }
      if (i==rgCurrentReading1+6) {
        //Gamma Phase
        v=Parameters->TrSide[HVSide].GammaPhase;
        goto SetData;
      }
      if (i==rgCurrentReading1+7) {
        //Gamma Trend
        v=(int)((float)Parameters->TrSide[HVSide].Trend*1*10.0); //acm, 4-27 change from .2 to 1
        goto SetData;
      }
      if (i==rgCurrentReading1+8) {
        //Temper. Coef.
        v=(int)((float)Parameters->TrSide[HVSide].KT*0.002*100.0);
        goto SetData;
      }
      if (i==rgCurrentReading1+9) {
        //Temper. Coef. Phase
        v=(int)((float)Parameters->TrSide[HVSide].KTPhase*1.5*100.0);
        goto SetData;
      }
      if ((i>=rgCurrentReading1+10)&&(i<=rgCurrentReading1+12)){
        //Bushing Current
        v=Parameters->TrSide[HVSide].SourceAmplitude[i-(rgCurrentReading1+10)];
        goto SetData;
      }
      if ((i>=rgCurrentReading1+13)&&(i<=rgCurrentReading1+15)){
        //Bushing Current Phase
        v=Parameters->TrSide[HVSide].SourcePhase[i-(rgCurrentReading1+13)];
        goto SetData;
      }
      if ((i>=rgCurrentReading1+16)&&(i<=rgCurrentReading1+18)){
        //Capacity
        v=Diagnosis->TgParam[HVSide].C[i-(rgCurrentReading1+16)];
        goto SetData;
      }
      if ((i>=rgCurrentReading1+19)&&(i<=rgCurrentReading1+21)){
        //Tg
        v=Diagnosis->TgParam[HVSide].tg[i-(rgCurrentReading1+19)];
        goto SetData;
      }
      if (i==rgCurrentReading1+22) {
        //Frequency
        v=(int)((float)Parameters->TrSide[HVSide].Frequency*100.0);
        goto SetData;
      }
      if (i==rgCurrentReading1+23) {
        //Temperature
        v=Parameters->TrSide[HVSide].Temperature;
        goto SetData;
      }
      if (i==rgCurrentReading1+24) {
        //Load Active %
        v=Parameters->Current[HVSide];
        goto SetData;
      }
      if (i==rgCurrentReading1+25) {
        //Load Reactive %
        v=Parameters->CurrentR[HVSide]+127;
        goto SetData;
      }
      if (i==rgCurrentReading1+26) {
        //Humidity %
        v=Parameters->Humidity;
        goto SetData;
      }
      if (i==rgCurrentReading1+27) {
        //LTC Position Number
        v=Parameters->RPN[HVSide];
        goto SetData;
      }
      if (i==rgCurrentReading1+28) {
        //Alarm Status
        v=Parameters->TrSide[HVSide].AlarmStatus;
        goto SetData;
      }
      
      //Current Reading Group 2
      if (i==rgCurrentReading2+5) {
        //Gamma
        v=Parameters->TrSide[LVSide].Gamma;
        goto SetData;
      }
      if (i==rgCurrentReading2+6) {
        //Gamma Phase
        v=Parameters->TrSide[LVSide].GammaPhase;
        goto SetData;
      }
      if (i==rgCurrentReading2+7) {
        //Gamma Trend
        v=(int)((float)Parameters->TrSide[LVSide].Trend*1*10.0); //acm, 4-27 change from .2 to 1
        goto SetData;
      }
      if (i==rgCurrentReading2+8) {
        //Temper. Coef.
        v=(int)((float)Parameters->TrSide[LVSide].KT*0.002*100.0);
        goto SetData;
      }
      if (i==rgCurrentReading2+9) {
        //Temper. Coef. Phase
        v=(int)((float)Parameters->TrSide[LVSide].KTPhase*1.5*100.0);
        goto SetData;
      }
      if ((i>=rgCurrentReading2+10)&&(i<=rgCurrentReading2+12)){
        //Bushing Current
        v=Parameters->TrSide[LVSide].SourceAmplitude[i-(rgCurrentReading2+10)];
        goto SetData;
      }
      if ((i>=rgCurrentReading2+13)&&(i<=rgCurrentReading2+15)){
        //Bushing Current Phase
        v=Parameters->TrSide[LVSide].SourcePhase[i-(rgCurrentReading2+13)];
        goto SetData;
      }
      if ((i>=rgCurrentReading2+16)&&(i<=rgCurrentReading2+18)){
        //Capacity
        v=Diagnosis->TgParam[LVSide].C[i-(rgCurrentReading2+16)];
        goto SetData;
      }
      if ((i>=rgCurrentReading2+19)&&(i<=rgCurrentReading2+21)){
        //Tg
        v=Diagnosis->TgParam[LVSide].tg[i-(rgCurrentReading2+19)];
        goto SetData;
      }
      if (i==rgCurrentReading2+22) {
        //Frequency
        v=(int)((float)Parameters->TrSide[LVSide].Frequency*100.0);
        goto SetData;
      }
      if (i==rgCurrentReading2+23) {
        //Temperature
        v=Parameters->TrSide[LVSide].Temperature;
        goto SetData;
      }
      if (i==rgCurrentReading2+24) {
        //Load Active %
        v=Parameters->Current[LVSide];
        goto SetData;
      }
      if (i==rgCurrentReading2+25) {
        //Load Reactive %
        v=Parameters->CurrentR[LVSide]+127;
        goto SetData;
      }
      if (i==rgCurrentReading2+26) {
        //Humidity %
        v=Parameters->Humidity;
        goto SetData;
      }
      if (i==rgCurrentReading2+27) {
        //LTC Position Number
        v=Parameters->RPN[LVSide];
        goto SetData;
      }
      if (i==rgCurrentReading2+28) {
        //Alarm Status
        v=Parameters->TrSide[LVSide].AlarmStatus;
        goto SetData;
      }

      //2.10 Current calibration results from DAC calibration. Redefined from Zk
      if (i==rgDAC_CalAmplitude+0) {
        //Set1 A
        v = Parameters->TrSide[0].DAC_CalAmplitude[0];
        goto SetData;
      }
      if (i==rgDAC_CalAmplitude+1) {
        //Set1 B
        v = Parameters->TrSide[0].DAC_CalAmplitude[1];
        goto SetData;
      }
      if (i==rgDAC_CalAmplitude+2) {
        //Set1 C
        v = Parameters->TrSide[0].DAC_CalAmplitude[2];
        goto SetData;
      }
      if (i==rgDAC_CalAmplitude+3) {
        //Set2 A
        v = Parameters->TrSide[1].DAC_CalAmplitude[0];
        goto SetData;
      }
      if (i==rgDAC_CalAmplitude+4) {
        //Set2 B
        v = Parameters->TrSide[1].DAC_CalAmplitude[1];
        goto SetData;
      }
      if (i==rgDAC_CalAmplitude+5) {
        //Set2 C
        v = Parameters->TrSide[1].DAC_CalAmplitude[2];
        goto SetData;
      }      
      //Current Reading Zk Group 1 - Group 2 redefined in 2.10 above
/*      if (i==rgCurrentReadingZk12+0) {
        //Zk % A-a
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkPercent[BushingA],1,65535);
        goto SetData;
      }
      if (i==rgCurrentReadingZk12+1) {
        //Zk % B-b
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkPercent[BushingB],1,65535);
        goto SetData;
      }
      if (i==rgCurrentReadingZk12+2) {
        //Zk % C-c
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkPercent[BushingC],1,65535);
        goto SetData;
      }
      if (i==rgCurrentReadingZk12+3) {
        //Zk Ohm A-a
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkAmpl[BushingA],0.1,65535);
        goto SetData;
      }
      if (i==rgCurrentReadingZk12+4) {
        //Zk Ohm B-b
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkAmpl[BushingB],0.1,65535);
        goto SetData;
      }
      if (i==rgCurrentReadingZk12+5) {
        //Zk Ohm C-c
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkAmpl[BushingC],0.1,65535);
        goto SetData;
      }*/
      
      //Archive Reading All Groups	1051	1174
      if (ModBusArchMesurementNum<=MeasurementsInArchive) {
        SetPage(TempPage);
        if (i>=rgArchiveReading1) {
          //Set Measurement Num to m
          CheckMeasurementNum(i);
          //Load Last Measurement
          LoadMeasurement(m);
          SetPage(TempPage);
        }
        
        if ((i==rgArchiveReading1+0)||(i==rgArchiveReading2+0)||(i==rgArchiveReading3+0)||(i==rgArchiveReading4+0)) {
          //Month
          if (MeasLoaded) v=Param->Month;
          goto SetData;
        }
        if ((i==rgArchiveReading1+1)||(i==rgArchiveReading2+1)||(i==rgArchiveReading3+1)||(i==rgArchiveReading4+1)) {
          //Day
          if (MeasLoaded) v=Param->Day;
          goto SetData;
        }
        if ((i==rgArchiveReading1+2)||(i==rgArchiveReading2+2)||(i==rgArchiveReading3+2)||(i==rgArchiveReading4+2)) {
          //Year
          if (MeasLoaded) v=Param->Year%100+2000;
          goto SetData;
        }
        if ((i==rgArchiveReading1+3)||(i==rgArchiveReading2+3)||(i==rgArchiveReading3+3)||(i==rgArchiveReading4+3)) {
          //Hour
          if (MeasLoaded) v=Param->Hour;
          goto SetData;
        }
        if ((i==rgArchiveReading1+4)||(i==rgArchiveReading2+4)||(i==rgArchiveReading3+4)||(i==rgArchiveReading4+4)) {
          //Min
          if (MeasLoaded) v=Param->Min;
          goto SetData;
        }
        //Archive Reading Group 1
        if (i==rgArchiveReading1+5) {
          //Gamma
          if (MeasLoaded) v=Param->TrSide[HVSide].Gamma;
          goto SetData;
        }
        if (i==rgArchiveReading1+6) {
          //Gamma Phase
          if (MeasLoaded) v=Param->TrSide[HVSide].GammaPhase;
          goto SetData;
        }
        if (i==rgArchiveReading1+7) {
          //Gamma Trend
          if (MeasLoaded) v=(int)((float)Param->TrSide[HVSide].Trend*1*10.0);  //acm, 4-27 change from .2 to 1
          goto SetData;
        }
        if (i==rgArchiveReading1+8) {
          //Temper. Coef.
          if (MeasLoaded) v=(int)((float)Param->TrSide[HVSide].KT*0.002*100.0);
          goto SetData;
        }
        if (i==rgArchiveReading1+9) {
          //Temper. Coef. Phase
          if (MeasLoaded) v=(int)((float)Param->TrSide[HVSide].KTPhase*1.5*100.0);
          goto SetData;
        }
        if ((i>=rgArchiveReading1+10)&&(i<=rgArchiveReading1+12)){
          //Bushing Current
          if (MeasLoaded) v=Param->TrSide[HVSide].SourceAmplitude[i-(rgArchiveReading1+10)];
          goto SetData;
        }
        if ((i>=rgArchiveReading1+13)&&(i<=rgArchiveReading1+15)){
          //Bushing Current Phase
          if (MeasLoaded) v=Param->TrSide[HVSide].SourcePhase[i-(rgArchiveReading1+13)];
          goto SetData;
        }
        if ((i>=rgArchiveReading1+16)&&(i<=rgArchiveReading1+18)){
          //Capacity
          //       if (MeasLoaded) v=Diag->TgParam[HVSide].C[i-(rgArchiveReading1+16)];
          if (MeasLoaded) v=Param->TrSide[HVSide].C[i-(rgArchiveReading1+16)];
          goto SetData;
        }
        if ((i>=rgArchiveReading1+19)&&(i<=rgArchiveReading1+21)){
          //Tg
          //       if (MeasLoaded) v=Diag->TgParam[HVSide].tg[i-(rgArchiveReading1+19)];
          if (MeasLoaded) v=Param->TrSide[HVSide].tg[i-(rgArchiveReading1+19)];
          goto SetData;
        }
        if (i==rgArchiveReading1+22) {
          //Frequency
          if (MeasLoaded) v=(int)((float)Param->TrSide[HVSide].Frequency*100.0);
          goto SetData;
        }
        if (i==rgArchiveReading1+23) {
          //Temperature
          if (MeasLoaded) v=Param->TrSide[HVSide].Temperature;
          goto SetData;
        }
        if (i==rgArchiveReading1+24) {
          //Load Active %
          if (MeasLoaded) v=Param->Current[HVSide];
          goto SetData;
        }
        if (i==rgArchiveReading1+25) {
          //Load Reactive %
          if (MeasLoaded) v=Param->CurrentR[HVSide]+127;
          goto SetData;
        }
        if (i==rgArchiveReading1+26) {
          //Humidity %
          if (MeasLoaded) v=Param->Humidity;
          goto SetData;
        }
        if (i==rgArchiveReading1+27) {
          //LTC Position Number
          if (MeasLoaded) v=Param->RPN[HVSide];
          goto SetData;
        }
        if (i==rgArchiveReading1+28) {
          //Alarm Status
          if (MeasLoaded) v=Param->TrSide[HVSide].AlarmStatus;
          goto SetData;
        }
        
        //Archive Reading Group 2
        if (i==rgArchiveReading2+5) {
          //Gamma
          if (MeasLoaded) v=Param->TrSide[LVSide].Gamma;
          goto SetData;
        }
        if (i==rgArchiveReading2+6) {
          //Gamma Phase
          if (MeasLoaded) v=Param->TrSide[LVSide].GammaPhase;
          goto SetData;
        }
        if (i==rgArchiveReading2+7) {
          //Gamma Trend
          if (MeasLoaded) v=(int)((float)Param->TrSide[LVSide].Trend*1*10.0); //acm, 4-27 change from .2 to 1
          goto SetData;
        }
        if (i==rgArchiveReading2+8) {
          //Temper. Coef.
          if (MeasLoaded)
            v=(int)((float)Param->TrSide[LVSide].KT*0.002*100.0);
          goto SetData;
        }
        if (i==rgArchiveReading2+9) {
          //Temper. Coef. Phase
          if (MeasLoaded)
            v=(int)((float)Param->TrSide[LVSide].KTPhase*1.5*100.0);
          goto SetData;
        }
        if ((i>=rgArchiveReading2+10)&&(i<=rgArchiveReading2+12)){
          //Bushing Current
          if (MeasLoaded)
            v=Param->TrSide[LVSide].SourceAmplitude[i-(rgArchiveReading2+10)];
          goto SetData;
        }
        if ((i>=rgArchiveReading2+13)&&(i<=rgArchiveReading2+15)){
          //Bushing Current Phase
          if (MeasLoaded)
            v=Param->TrSide[LVSide].SourcePhase[i-(rgArchiveReading2+13)];
          goto SetData;
        }
        if ((i>=rgArchiveReading2+16)&&(i<=rgArchiveReading2+18)){
          //Capacity
          if (MeasLoaded)
            //          v=Diag->TgParam[LVSide].C[i-(rgArchiveReading2+16)];
            v=Param->TrSide[LVSide].C[i-(rgArchiveReading2+16)];
          goto SetData;
        }
        if ((i>=rgArchiveReading2+19)&&(i<=rgArchiveReading2+21)){
          //Tg
          if (MeasLoaded)
            //          v=Diag->TgParam[LVSide].tg[i-(rgArchiveReading2+19)];
            v=Param->TrSide[LVSide].tg[i-(rgArchiveReading2+19)];
          goto SetData;
        }
        if (i==rgArchiveReading2+22) {
          //Frequency
          if (MeasLoaded)
            v=(int)((float)Param->TrSide[LVSide].Frequency*100.0);
          goto SetData;
        }
        if (i==rgArchiveReading2+23) {
          //Temperature
          if (MeasLoaded)
            v=Param->TrSide[LVSide].Temperature;
          goto SetData;
        }
        if (i==rgArchiveReading2+24) {
          //Load Active %
          if (MeasLoaded)
            v=Param->Current[LVSide];
          goto SetData;
        }
        if (i==rgArchiveReading2+25) {
          //Load Reactive %
          if (MeasLoaded)
            v=Param->CurrentR[LVSide]+127;
          
          
          goto SetData;
        }
        if (i==rgArchiveReading2+26) {
          //Humidity %
          if (MeasLoaded)
            v=Param->Humidity;
          goto SetData;
        }
        if (i==rgArchiveReading2+27) {
          //LTC Position Number
          if (MeasLoaded)
            v=Param->RPN[LVSide];
          goto SetData;
        }
        if (i==rgArchiveReading2+28) {
          //Alarm Status
          if (MeasLoaded) v=Param->TrSide[LVSide].AlarmStatus;
          goto SetData;
        }
        
        //Archive Reading Zk Group 1 - Group 2
        if (i==rgArchiveReadingZk12+0) {
          //Zk % A-a
          v=DivAndRound(Param->SideToSideData[HVSide].ZkPercent[BushingA],1,65535);
          goto SetData;
        }
        if (i==rgArchiveReadingZk12+1) {
          //Zk % B-b
          v=DivAndRound(Param->SideToSideData[HVSide].ZkPercent[BushingB],1,65535);
          goto SetData;
        }
        if (i==rgArchiveReadingZk12+2) {
          //Zk % C-c
          v=DivAndRound(Param->SideToSideData[HVSide].ZkPercent[BushingC],1,65535);
          goto SetData;
        }
        if (i==rgArchiveReadingZk12+3) {
          //Zk A-a
          v=DivAndRound(Param->SideToSideData[HVSide].ZkAmpl[BushingA],0.1,65535);
          goto SetData;
        }
        if (i==rgArchiveReadingZk12+4) {
          //Zk B-b
          v=DivAndRound(Param->SideToSideData[HVSide].ZkAmpl[BushingB],0.1,65535);
          goto SetData;
        }
        if (i==rgArchiveReadingZk12+5) {
          //Zk C-c
          v=DivAndRound(Param->SideToSideData[HVSide].ZkAmpl[BushingC],0.1,65535);
          goto SetData;
        }
        
        
      } //if (ModBusArchMesurementNum<=MeasurementsInArchive)
      
      // TDM CurrentReading
      SetPage(ParametersPage);
      if (i==rgTDMCurrentReading+0) {
        v=Parameters->Month;
        goto SetData;
      }
      if (i==rgTDMCurrentReading+1) {
        v=Parameters->Day;
        goto SetData;
      }
      if (i==rgTDMCurrentReading+2) {
        v=Parameters->Year%100+2000;
        goto SetData;
      }
      if (i==rgTDMCurrentReading+3) {
        v=Parameters->Hour;
        goto SetData;
      }
      if (i==rgTDMCurrentReading+4) {
        v=Parameters->Min;
        goto SetData;
      }
      if (i==rgTDMCurrentReading+6) {
        if ((Parameters->TrSide[HVSide].AlarmStatus&(1<<0))||(Parameters->TrSide[LVSide].AlarmStatus&(1<<0)))
          v=2;
        else
          if ((Parameters->TrSide[HVSide].AlarmStatus&(1<<4))||(Parameters->TrSide[LVSide].AlarmStatus&(1<<4)))
            v=1;
        goto SetData;
      }
      if (i==rgTDMCurrentReading+7) {
        //Device Health Status
        v=(Error&~(((unsigned long)1<<erStop)|((unsigned long)1<<erPause)));
        goto SetData;
      }
      
      
      
      //TDM Current Reading Group 1
      if (i==rgTDMCurrentReading1+0) {
        //Status
        if (Parameters->TrSideCount&(1<<HVSide)) v=1;
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+1) {
        //Gamma
        v=Parameters->TrSide[HVSide].Gamma;
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+2) {
        //Gamma Phase
        v=Parameters->TrSide[HVSide].GammaPhase;
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+3) {
        //Gamma Trend
        v=(int)((float)Parameters->TrSide[HVSide].Trend*1*10.0);  //acm, 4-27 change from .2 to 1
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+4) {
        //Temper. Coef.
        v=(int)((float)Parameters->TrSide[HVSide].KT*0.002*100.0);
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+5) {
        //Temper. Coef. Phase
        v=(int)((float)Parameters->TrSide[HVSide].KTPhase*1.5*100.0);
        goto SetData;
      }
      if ((i>=rgTDMCurrentReading1+6)&&(i<=rgTDMCurrentReading1+8)){
        //Bushing Current
        v=Parameters->TrSide[HVSide].SourceAmplitude[i-(rgTDMCurrentReading1+6)];
        goto SetData;
      }
      if ((i>=rgTDMCurrentReading1+9)&&(i<=rgTDMCurrentReading1+11)){
        //Bushing Current Phase
        v=Parameters->TrSide[HVSide].SourcePhase[i-(rgTDMCurrentReading1+9)];
        goto SetData;
      }
      if ((i>=rgTDMCurrentReading1+12)&&(i<=rgTDMCurrentReading1+14)){
        //Capacity
        v=Diagnosis->TgParam[HVSide].C[i-(rgTDMCurrentReading1+12)];
        goto SetData;
      }
      if ((i>=rgTDMCurrentReading1+15)&&(i<=rgTDMCurrentReading1+17)){
        //Tg
        v=Diagnosis->TgParam[HVSide].tg[i-(rgTDMCurrentReading1+15)];
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+18) {
        //Frequency
        v=(int)((float)Parameters->TrSide[HVSide].Frequency*100.0);
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+19) {
        //Temperature
        v=Parameters->TrSide[HVSide].Temperature;
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+20) {
        //Load Active %
        v=Parameters->Current[HVSide];
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+21) {
        //Load Reactive %
        v=Parameters->CurrentR[HVSide]+127;
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+22) {
        //Humidity %
        v=Parameters->Humidity;
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+23) {
        //LTC Position Number
        v=Parameters->RPN[HVSide];
        goto SetData;
      }
      if (i==rgTDMCurrentReading1+24) {
        //Alarm Status
        v=Parameters->TrSide[HVSide].AlarmStatus;
        goto SetData;
      }
      
      //TDM Current Reading Group 2
      if (i==rgTDMCurrentReading2+0) {
        //Status
        if (Parameters->TrSideCount&(1<<LVSide)) v=1;
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+1) {
        //Gamma
        v=Parameters->TrSide[LVSide].Gamma;
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+2) {
        //Gamma Phase
        v=Parameters->TrSide[LVSide].GammaPhase;
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+3) {
        //Gamma Trend
        v=(int)((float)Parameters->TrSide[LVSide].Trend*1*10.0); //acm, 4-27 change from .2 to 1
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+4) {
        //Temper. Coef.
        v=(int)((float)Parameters->TrSide[LVSide].KT*0.002*100.0);
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+5) {
        //Temper. Coef. Phase
        v=(int)((float)Parameters->TrSide[LVSide].KTPhase*1.5*100.0);
        goto SetData;
      }
      if ((i>=rgTDMCurrentReading2+6)&&(i<=rgTDMCurrentReading2+8)){
        //Bushing Current
        v=Parameters->TrSide[LVSide].SourceAmplitude[i-(rgTDMCurrentReading2+6)];
        goto SetData;
      }
      if ((i>=rgTDMCurrentReading2+9)&&(i<=rgTDMCurrentReading2+11)){
        //Bushing Current Phase
        v=Parameters->TrSide[LVSide].SourcePhase[i-(rgTDMCurrentReading2+9)];
        goto SetData;
      }
      if ((i>=rgTDMCurrentReading2+12)&&(i<=rgTDMCurrentReading2+14)){
        //Capacity
        v=Diagnosis->TgParam[LVSide].C[i-(rgTDMCurrentReading2+12)];
        goto SetData;
      }
      if ((i>=rgTDMCurrentReading2+15)&&(i<=rgTDMCurrentReading2+17)){
        //Tg
        v=Diagnosis->TgParam[LVSide].tg[i-(rgTDMCurrentReading2+15)];
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+18) {
        //Frequency
        v=(int)((float)Parameters->TrSide[LVSide].Frequency*100.0);
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+19) {
        //Temperature
        v=Parameters->TrSide[LVSide].Temperature;
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+20) {
        //Load Active %
        v=Parameters->Current[LVSide];
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+21) {
        //Load Reactive %
        v=Parameters->CurrentR[LVSide]+127;
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+22) {
        //Humidity %
        v=Parameters->Humidity;
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+23) {
        //LTC Position Number
        v=Parameters->RPN[LVSide];
        goto SetData;
      }
      if (i==rgTDMCurrentReading2+24) {
        //Alarm Status
        v=Parameters->TrSide[LVSide].AlarmStatus;
        goto SetData;
      }
      
      //TDM Current Reading Zk Group 1 - Group 2
      if (i==rgTDMCurrentReadingZk12+0) {
        //Zk % A-a
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkPercent[BushingA],1,65535);
        goto SetData;
      }
      if (i==rgTDMCurrentReadingZk12+1) {
        //Zk % B-b
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkPercent[BushingB],1,65535);
        goto SetData;
      }
      if (i==rgTDMCurrentReadingZk12+2) {
        //Zk % C-c
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkPercent[BushingC],1,65535);
        goto SetData;
      }
      if (i==rgTDMCurrentReadingZk12+3) {
        //Zk A-a
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkAmpl[BushingA],0.1,65535);
        goto SetData;
      }
      if (i==rgTDMCurrentReadingZk12+4) {
        //Zk B-b
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkAmpl[BushingB],0.1,65535);
        goto SetData;
      }
      if (i==rgTDMCurrentReadingZk12+5) {
        //Zk C-c
        v=DivAndRound(Parameters->SideToSideData[HVSide].ZkAmpl[BushingC],0.1,65535);
        goto SetData;
      }
      
      if (i==rgTDMCurrentReadingI+0) {
        //Load Current %
        v=LoadCurrentP;
        goto SetData;
      }
      if (i==rgTDMCurrentReadingI+1) {
        //Load Current A
        v=LoadCurrentA;
        if (v-LoadCurrentA>=0.5) v++;
        goto SetData;
      }
      if (i==rgTDMCurrentReadingI+2) {
        //Load Current B
        v=LoadCurrentB;
        if (v-LoadCurrentB>=0.5) v++;
        goto SetData;
      }
      if (i==rgTDMCurrentReadingI+3) {
        //Load Current C
        v=LoadCurrentC;
        if (v-LoadCurrentC>=0.5) v++;
        goto SetData;
      }
      
    } //if (MeasurementsInArchive)
    
    
  SetData:
    SetPage(Page);
    data[l]=(char)(v>>8);l++;
    data[l]=(char)(v&0xFF);l++;
  }
  data[0]=l-1;
  
  SetPage(Page);
  
  return l;
  
}

_TDateTime cur_time, new_time1;  // acm, u32.  Using int causes odd problem?

int SetRegisterValue(int raddr, int rlen, char* data)
{
  
  unsigned int i,l,j;
  int v,ReSetDate;
#ifndef __arm
  char Page=GetPage();
#endif
  _TDateTime CurTime;	// acm, link.cpp needs this defined, what up?
  
  l=1;
  
  ReSetDate=0;
  
  GET_TIME_INBUF();	// acm, record current time, as DateTime structure maybe overwritten next
  cur_time = DoPackDateTime(EncodeDate(DateTime.Year%100+2000,DateTime.Month,DateTime.Day),
                            EncodeTime(DateTime.Hour,DateTime.Min,  DateTime.Sec));
  
  for (i=raddr;i<raddr+rlen;i++) {
    
    //    v= ((int*)data)[i-raddr];
    v= ((u16)data[((i-raddr)<<1)+1])+(((u16)data[(i-raddr)<<1])<<8);
    
    //Binary Status registers	1	3
    
    //General Information	        4	21
    
    if (i==rgGeneralInformation+3) {  	// acm, 8-14-11, allow status to be writeable, useful to test Alarms
      //Device Alarm status
      FullStatus=v;					// acm, v1.79, change v+1 to v, less confusing, nobody will notice difference at this poing
      OutLED(FullStatus);
      OutRelay(FullStatus);			// drive relay(s) if enabled in Setup
      goto SetData;
    }
    
    if (i==rgGeneralInformation+9) {
      //Device time Month
      DateTime.Month=v;
      ReSetDate=1;
      goto SetData;
    }
    if (i==rgGeneralInformation+10) {
      //Device time Day
      DateTime.Day=v;
      ReSetDate=1;
      goto SetData;
    }
    if (i==rgGeneralInformation+11) {
      //Device time Year
      DateTime.Year=v%100;
      ReSetDate=1;
      goto SetData;
    }
    if (i==rgGeneralInformation+12) {
      //Device time Hour
      DateTime.Hour=v;
      ReSetDate=1;
      goto SetData;
    }
    if (i==rgGeneralInformation+13) {
      //Device time Min
      DateTime.Min=v;
      ReSetDate=1;
      goto SetData;
    }
    if (i==rgGeneralInformation+15) {
      //Temperature
      if((v>=0)&&(v<255))	 // acm, 5-9-12 enforce range check on both BHM and PDM for consistency.  Value stored with measurement as char.
        ExtTemperature[0]=v;				// acm, 7-29 zeroth element of 4 element array, 2nd oil temp plus two others TBD by calling app.
      goto SetData;
    }
    if (i==rgGeneralInformation+16) {
      //Load Active %
      ExtLoadActive=v;   //ag 3-7-16 Clear erLoCurrent if good transfer
      //ag 12-26-16 commented //if (ExtLoadActive >= Setup.GammaSetupInfo.GammaSideSetup[HVSide].RatedCurrent * 0.05) 
      //Error&=~((unsigned long)1<<erLoCurrent);
      goto SetData;
    }
    if (i==rgGeneralInformation+17) {
      //Load Reactive %
      ExtLoadReactive=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+18) {
      //Humidity from SCADA
      ExtHumidity=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+19) {
      //LTC position from SCADA
      ExtRPN=v;
      goto SetData;
    }
    if (i>=rgGeneralInformation+20 && i<=rgGeneralInformation+22) {  // acm, 7-8-09, add per claude 2nd temperature register for DR, plus 2 more, now 4 total
      //Temperature#2
      if((v>=0)&&(v<255))	 // acm, 5-9-12 enforce range check on both BHM and PDM for consistency.  Value stored with measurement as char.
        ExtTemperature[1+i-(rgGeneralInformation+20)]=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+23) {
      //new v2, per PDM
      LoadCurrent2=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+24) {
      //new v2, per PDM
      LoadCurrent3=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+25) {
      //new v2, per PDM
      voltage1=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+26) {
      //new v2, per PDM
      voltage2=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+27) {
      //new v2, per PDM
      voltage=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+29) {
      //DE new v2.03, EventTrigger for BHM2C50
      EventTrigger=v;
      goto SetData;
    }
    if (i==rgGeneralInformation+42) {  //2.06 reg 43
      //new v2.06, Reset Count
      Setup.SetupInfo.ResetCount=v;
      goto SetData;
    }
    
    //Device Settings	        50	450
    if (i==rgDeviceSettings+0) {
      //��� ���������� ������� (0-����� �������� 1-�� �������)
      Setup.SetupInfo.ScheduleType=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+1) {
      //���� � ������ ���������� �������
      Setup.SetupInfo.dTime.Hour=v/100;
      Setup.SetupInfo.dTime.Min=v%100;
      goto SetData;
    }
    if ((i>=rgDeviceSettings+2)&&(i<=rgDeviceSettings+51)) {
      //���� � ������ ���������� �������
      Setup.SetupInfo.MeasurementsInfo[i-(rgDeviceSettings+2)].Hour=v/100;
      Setup.SetupInfo.MeasurementsInfo[i-(rgDeviceSettings+2)].Min=v%100;
      goto SetData;
    }
    if (i==rgDeviceSettings+52) {
      //������ ���������� ����
      Setup.SetupInfo.DisplayFlag=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+53) {
      //Out Time
      Setup.SetupInfo.OutTime=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+54) {
      //Relay
      Setup.SetupInfo.Relay=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+55) {
      //Time of Relay test output ,sec
      Setup.SetupInfo.TimeOfRelayAlarm=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+56) {
      //Device Number
      Setup.SetupInfo.DeviceNumber=v;
      ReSetSetup=1;
      goto SetData;
    }
    if (i==rgDeviceSettings+57) {
      //�������� ������ � ����������
      //0-9600
      //1-38400
      //2-57600
      //3-115200
      //4-230400
      //5-500000
      //6-1000000
      if (v==10000) Setup.SetupInfo.BaudRate=6;
      else if (v==5000) Setup.SetupInfo.BaudRate=5;
      else if (v==2304) Setup.SetupInfo.BaudRate=4;
      else if (v==1152) Setup.SetupInfo.BaudRate=3;
      else if (v==576) Setup.SetupInfo.BaudRate=2;
      else if (v==384) Setup.SetupInfo.BaudRate=1;
      else Setup.SetupInfo.BaudRate=0;//v=96;
      ReSetSetup=1;
      goto SetData;
    }
    if (i==rgDeviceSettings+58) {     //DE 2-18-16 50+58=108 reg109
      //��� ������������� ��������� ModBus
      //0-RTU
      //1-TCP
      Setup.SetupInfo.ModBusProtocol=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+59) {  //DE 2-18-16 50+59=109 reg110
      //Analog Parameters Input Selector
      //0-From Device Analog Inputs,
      //1 - From Regeisters
      Setup.SetupInfo.ReadAnalogFromRegister=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+60) {  //DE 2-18-16 50+60=110 reg111
      //Calc Zk
      Setup.ZkSetupInfo.CalcZkOnSide=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+61) {  //DE 2-18-16 50+61=111 reg112
      //Days to calculate Zk
      Setup.ZkSetupInfo.ZkDays=v;
      goto SetData;
    }
    
    //DWE 10-13-15 Adding new write registers starting here
    if (i==rgDeviceSettings+62) {
      //Job1 reg #113
      Setup.SetupInfo.Job1=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+63) {
      //Job2 reg #114
      Setup.SetupInfo.Job2=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+64) {
      //LoadCurrentThresholdLevel reg #115
      Setup.SetupInfo.LoadCurrentThresholdLevel=v;
      goto SetData;
    }
    if (i==rgDeviceSettings+65) {
      //LoadCurrentThresholdSettings reg #116
      Setup.SetupInfo.LoadCurrentThresholdSettings=v;
      goto SetData;
    }    
    //    if (i==rgDeviceSettings+66) { //do not allow write
    //       //ConfigSaveCount reg #117
    //       Setup.SetupInfo.ConfigSaveCount=v;
    //       goto SetData;
    //    }    
    //    if (i==rgDeviceSettings+67) { //do not allow write
    //       //SN reg #118
    //       Setup.SetupInfo.SN=v;
    //       goto SetData;
    //    }    
    if (i==rgDeviceSettings+68) {
      //InternalSyncFrequency reg #119
      Setup.SetupInfo.InternalSyncFrequency = v;
      goto SetData;
    }
    if (i==rgDeviceSettings+69) {
      //2.10 Calibration threshold reg #120
      if (v < 99 && v >= 1) {
        Setup.SetupInfo.CalibTreshould = v;
      }
      goto SetData;
    }
    //end of DeviceSettings No reserve since 2.10
    
    
    if (i==rgGeneralGammaSettings+0) {
      //Averaging for Gamma
      Setup.GammaSetupInfo.AveragingForGamma=v;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+1) {
      //Days to Calculate Trend (Can not be less then 15 days)
      Setup.GammaSetupInfo.DaysToCalculateTrend=v;
      ReSetSetup=1;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+2) {
      //Days to Calculate Temperature Coefficient
      Setup.GammaSetupInfo.DaysToCalculateTCoefficient=v;
      ReSetSetup=1;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+3) {
      //Days to Calculate BASELINE
      Setup.GammaSetupInfo.DaysToCalculateBASELINE=v;
      ReSetSetup=1;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+4) {
      //���������� ���������� ��� �� 120 //Allowable deviation phases of 120
      Setup.GammaSetupInfo.AllowedPhaseDispersion=v;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+5) {
      //Alarm Hysteresis (Releases an Alarm, if an alarming parameter drops below (PA - PA*AH%) of alarm threshold PA)
      Setup.GammaSetupInfo.AlarmHysteresis=v;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+6) {
      //�����(���), ����� ������� ������������� �����
      Setup.GammaSetupInfo.ReReadOnAlarm=v;
      goto SetData;
    }
    if (i==rgGeneralGammaSettings+7) {
      //����� ���� ��� ������� ��������
      if (v<5) Setup.GammaSetupInfo.LoadChannel=v;
      goto SetData;
    }
    // X==0, avg freeze, kinda dumb
    // X==.5, 2pt avg, avg changes more quickly
    // X==.33, old alg, pre-2.00 behavior
    // X==1, avg off, becomes last measured Unn
    if (i==rgGeneralGammaSettings+8) {
      //Averaging for Gamma Coefficient, new for v2.00
      Setup.GammaSetupInfo.AveragingForGamma_X=v;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+0) {
      //Status
      if (v==1) Setup.GammaSetupInfo.ReadOnSide|=(1<<HVSide);
      else if (v==0) Setup.GammaSetupInfo.ReadOnSide&=~(1<<HVSide);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+1) {
      //������� ���������� (kV)
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].RatedVoltage=(float)v/10.0;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+2) {
      //������� ��� (A)
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].RatedCurrent=(float)v/10.0;
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].RatedCurrent=(float)v/10.0;  //ag3-7-16 Setting both rated current to the same value from 2.03 and higher
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+3) {
      //0 bit is 1 - unused
      //1 bit is 1 - Trend to Alarm ON
      //2 bit is 1 - Temperature coefficient to Alarm ON
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].AlarmEnable=v;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+4) {
      //������ Gamma � % (Byte*0.05)
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].GammaYellowThresholld=(char)((float)v/10.0/0.05);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+5) {
      //������ Gamma � % (Byte*0.05)
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].GammaRedThresholld=(char)((float)v/10.0/0.05);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+6) {
      //Gamma Temperature Coefficient Alarm Threshold (Byte*0.002)
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].TCoefficient=(char)((float)v/1000.0/0.002);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+7) {
      //Gamma Trend Alarm Threshold (Byte*0.2	  Max- 5%/Yr)
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].TrendAlarm=(char)((float)v/10.0/1);  //acm, 4-27 change from .2 to 1
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+8) {
      //������ Tg *100
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].TgYellowThresholld=v;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+9) {
      //������ Tg *100
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].TgRedThresholld=v;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup1+10) {
      //������ Tg *100
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].TgVariationThresholld=v;
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup1+11)&&(i<=rgDeviceSettingsGroup1+13)) {
      //Input Impedabce *100
      (Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputImpedance[BushingA+(i-(rgDeviceSettingsGroup1+11))])=v;
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup1+14)&&(i<=rgDeviceSettingsGroup1+16)) {
      //Off-Line Tangent Value *100
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].Tg0[BushingA+(i-(rgDeviceSettingsGroup1+14))]=v;
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup1+17)&&(i<=rgDeviceSettingsGroup1+19)) {
      //Off-Line Capacity Value *10
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].C0[BushingA+(i-(rgDeviceSettingsGroup1+17))]=v;
      goto SetData;
    }
    
    if (i==rgDeviceSettingsGroup2+0) {
      //Status
      if (v==1) Setup.GammaSetupInfo.ReadOnSide|=(1<<LVSide);
      else if (v==0) Setup.GammaSetupInfo.ReadOnSide&=~(1<<LVSide);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+1) {
      //������� ���������� (kV)
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].RatedVoltage=(float)v/10.0;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+2) {
      //������� ��� (A)
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].RatedCurrent=(float)v/10.0;
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].RatedCurrent=(float)v/10.0;  //ag3-7-16 Setting both rated current to the same value from 2.03 and higher
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+3) {
      //0 bit is 1 - unused
      //1 bit is 1 - Trend to Alarm ON
      //2 bit is 1 - Temperature coefficient to Alarm ON
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].AlarmEnable=v;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+4) {
      //������ Gamma � % (Byte*0.05)
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].GammaYellowThresholld=(char)((float)v/10.0/0.05);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+5) {
      //������ Gamma � % (Byte*0.05)
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].GammaRedThresholld=(char)((float)v/10.0/0.05);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+6) {
      //Gamma Temperature Coefficient Alarm Threshold (Byte*0.002)
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].TCoefficient=(char)((float)v/1000.0/0.002);
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+7) {
      //Gamma Trend Alarm Threshold (Byte*0.2	  Max- 5%/Yr)
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].TrendAlarm=(char)((float)v/10.0/1); //acm, 4-27 change from .2 to 1
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+8) {
      //������ Tg *100
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].TgYellowThresholld=v;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+9) {
      //������ Tg *100
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].TgRedThresholld=v;
      goto SetData;
    }
    if (i==rgDeviceSettingsGroup2+10) {
      //������ Tg *100
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].TgVariationThresholld=v;
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup2+11)&&(i<=rgDeviceSettingsGroup2+13)) {
      //Input Impedabce *100
      (Setup.GammaSetupInfo.GammaSideSetup[LVSide].InputImpedance[BushingA+(i-(rgDeviceSettingsGroup2+11))])=v;
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup2+14)&&(i<=rgDeviceSettingsGroup2+16)) {
      //Off-Line Tangent Value *100
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].Tg0[BushingA+(i-(rgDeviceSettingsGroup2+14))]=v;
      goto SetData;
    }
    if ((i>=rgDeviceSettingsGroup2+17)&&(i<=rgDeviceSettingsGroup2+19)) {
      //Off-Line Capacity Value *10
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].C0[BushingA+(i-(rgDeviceSettingsGroup2+17))]=v;
      goto SetData;
    }
    
    if ((i==rgCalibrationCoeff+0)||
        (i==rgCalibrationCoeff+2)||
          (i==rgCalibrationCoeff+4)||
            (i==rgCalibrationCoeff+6))
    {
      //Temperature Coef Channel
      Setup.CalibrationCoeff.TemperatureK[(i-(rgCalibrationCoeff+0))/2]=(float)v/100.0;
      goto SetData;
    }
    if ((i==rgCalibrationCoeff+1)||
        (i==rgCalibrationCoeff+3)||
          (i==rgCalibrationCoeff+5)||
            (i==rgCalibrationCoeff+7))
    {
      //Temperature Shift Channel
      Setup.CalibrationCoeff.TemperatureB[(i-(rgCalibrationCoeff+0))/2]=(float)v/100.0;
      goto SetData;
    }
    
    if ((i==rgCalibrationCoeff+10)||
        (i==rgCalibrationCoeff+12)||
          (i==rgCalibrationCoeff+14)||
            (i==rgCalibrationCoeff+16))
    {
      //Analog input channel Coef
      Setup.CalibrationCoeff.CurrentK[(i-(rgCalibrationCoeff+10))/2]=(float)v/100.0;
      goto SetData;
    }
    if ((i==rgCalibrationCoeff+11)||
        (i==rgCalibrationCoeff+13)||
          (i==rgCalibrationCoeff+15)||
            (i==rgCalibrationCoeff+17))
    {
      //Analog input channel Shift
      Setup.CalibrationCoeff.CurrentB[(i-(rgCalibrationCoeff+10))/2]=(float)v/100.0;
      goto SetData;
    }
    if (i==rgCalibrationCoeff+20) {
      //Humidity Offset
      Setup.CalibrationCoeff.HumidityOffset[0]=(float)v/10.0;
      goto SetData;
    }
    if (i==rgCalibrationCoeff+21) {
      //Humidity Slope
      Setup.CalibrationCoeff.HumiditySlope[0]=(float)v/10.0;
      goto SetData;
    }
    
    
    
    //Contol Commands	        601	650
    if (i==rgContolCommands+0) 
    {
      //Start a single measurement
      if (v==1) ReadSingle=1;
      goto SetData;
    }
    if (i==rgContolCommands+1) 
    {
      //Pause and silence the device //ag 3-13-17
      if (v==0) 
      {
        PausedFrom=0; 
        Error&=~((unsigned long)1<<erPause); 
        Silenced=0;
      }
      if (v==1 || v==0xFFF)
      {
        SetPause();
        if (v==0xFFF) 
          Silenced=2; //2.10 silenced
        else 
          Silenced=0;
      }
      goto SetData;
    }
    if (i==rgContolCommands+2) {
      //Stops the device
      if (v==1) {
        Setup.SetupInfo.Stopped=1;
        Error|=((unsigned long)1<<erStop);
      }
      if (v==0) {
        Setup.SetupInfo.Stopped=0;
        if (Setup.GammaSetupInfo.ReadOnSide) Error&=~((unsigned long)1<<erStop);
      }
      goto SetData;
    }
    if (i==rgContolCommands+3) {
      //Reset
      if (v==1) Restart();
      goto SetData;
    }
    if (i==rgContolCommands+4) {
      //Reset Alarm
      if (v==1) OutGammaStatus(FullStatus=0,0);
      goto SetData;
    }
    if (i==rgContolCommands+5) {
      //Perform Balance Procedure
      if (v==1) AutoBalance=1;
      goto SetData;
    }
    if (i==rgContolCommands+6) {
      //Perform BaseLine Procedure
      if (v) {
        GET_TIME_INBUF();
        if (IsDate(DateTime.Year%100+2000,v/100,v%100)) {
          CurTime = DoPackDateTime(EncodeDate(DateTime.Year%100+2000,v/100,v%100),
                                   EncodeTime(23,59,59));
          RunBaseLine(CurTime,CurDiagRegime);
        }
      }
      goto SetData;
    }
    if (i==rgContolCommands+7) {
      //Delete Archive Rec # from
      DelFrom=v;
      goto SetData;
    }
    if (i==rgContolCommands+8) {
      //Delete Archive Rec # to
      DelTo=v;
      goto SetData;
    }
    if (i==rgContolCommands+9) {
      //Starts deleting the above records
      if (v==1) {
        if ((DelFrom)&&(DelTo)&&(DelFrom<=DelTo)&&(DelFrom<=MeasurementsInArchive)&&(DelTo<=MeasurementsInArchive)) {
          SetPage(MeasListPage);
          for (j=DelFrom;j<=DelTo;j++) {
#ifdef __arm
            FileDelete(LoadFileIndex(MeasurementsInArchive-j));
#else
            FileDelete(((unsigned short*)MeasList)[MeasurementsInArchive-j]);
#endif
          }
#ifdef __arm
          //����� ����������
          CreateList((u16 *)MeasListTempAddr,&MeasurementsInArchive,stForward/*����� � ������*/);
          SaveFileIndexes(MeasListTempAddr);
#else
          if (DelFrom>1) {
            SetPage(MeasListPage);
            for (j=0;j<(MeasurementsInArchive-(DelTo-DelFrom+1)-(MeasurementsInArchive-DelTo));j++) {
              ((unsigned short*)MeasList)[MeasurementsInArchive-DelTo+j]=((unsigned short*)MeasList)[MeasurementsInArchive-DelFrom+j+1];
            }
            MeasurementsInArchive=MeasurementsInArchive-(DelTo-DelFrom+1);
            LoadLastMeasurement();
          } else {
            MeasurementsInArchive=MeasurementsInArchive-(DelTo-DelFrom+1);
          }
#endif
          DelFrom=DelTo=0;
        }
      }
      goto SetData;
    }
    
    if (i==rgContolCommands+12) {
      //The Registers for the Latest Record Request (after the date), Month
      ArchDateTime.Month=v;
      goto SetData;
    }
    if (i==rgContolCommands+13) {
      //The Registers for the Latest Record Request (after the date), Day
      ArchDateTime.Day=v;
      goto SetData;
    }
    if (i==rgContolCommands+14) {
      //The Registers for the Latest Record Request (after the date), Year
      ArchDateTime.Year=v%100;
      goto SetData;
    }
    if (i==rgContolCommands+15) {
      //The Registers for the Latest Record Request (after the date), Hour
      ArchDateTime.Hour=v;
      goto SetData;
    }
    if (i==rgContolCommands+16) {
      //The Registers for the Latest Record Request (after the date), Min
      ArchDateTime.Min=v;
      goto SetData;
    }
    if (i==rgContolCommands+17) {
      //Order Number of record after the date above
      ModBusArchMesurementNumOnDate=v;
      goto SetData;
    }
    if (i==rgContolCommands+18) {
      ModBusArchMesurementNum=v;
      if (ModBusArchMesurementNum>MeasurementsInArchive) ModBusArchMesurementNum=MeasurementsInArchive;
      MeasLoaded=0;
      goto SetData;
    }
    if (i==rgContolCommands+19) {
      //Recall setup from Firmware Settings
      if (v==1) {
        //SetupValid=0xFFFF; //DE 4-13-16 removed
        LoadSetup(1,0);
      }
    }
    if (i==rgContolCommands+20) {
      //Run XX Test
      if (v==1) RunXX=1;
    }
    
    //Auto Balance
    if (i==rgContolCommands+21) {
      // Auto balans Month
      if ((v<13)&&(v>0)) Setup.SetupInfo.AutoBalans.Month=v;
    }
    if (i==rgContolCommands+22) {
      // Auto balans Day
      if ((v<32)&&(v>0))
        Setup.SetupInfo.AutoBalans.Day=v;
    }
    if (i==rgContolCommands+23) {
      // Auto balans Year
      Setup.SetupInfo.AutoBalans.Year=v%100;
    }
    if (i==rgContolCommands+24) {
      // Auto balans Hour
      if ((v<24)&&(v>=0)) Setup.SetupInfo.AutoBalans.Hour=v;
    }
    if (i==rgContolCommands+25) {
      // Auto balans Unit Work
      Setup.SetupInfo.WorkingDays=v;
    }
    if (i==rgContolCommands+26) {
      // Auto balans Run
      if (v) Setup.SetupInfo.AutoBalansActive=Setup.SetupInfo.WorkingDays+1;
      else Setup.SetupInfo.AutoBalansActive=0;
    }
    if (i==rgContolCommands+27) {
      // No LOad Test Run
      if (v) Setup.SetupInfo.NoLoadTestActive=1;
      else Setup.SetupInfo.NoLoadTestActive=0;
    }
    //    if (i==rgContolCommands+28) {    //reg 629
    //       // Fast write count //DE 3-28-16
    //       FastWriteCount=v;
    //    }
    //
    if (i==rgContolCommands+29) {      //reg 630  //DE4-1-16 set Rain timer
      //Rain time (minutes)
      cntTemp=GetTic32();
      if (v>0) RainTo=30*32*60;	//change for T3 digital rain signalss ets for 30 minutes
      else RainTo=0;
      Error|=((unsigned long)1<<erRainTimer);
      if (v==0) Error&=~((unsigned long)1<<erRainTimer);
      goto SetData;
    }
    if ((i==rgContolCommands+30) && (v==77)) {      //reg 631  //2.05 10-25-16 regeterate serial number
      Setup.SetupInfo.SN = GetRand(1, 65535); 
      goto SetData;
    }
    
    if ((i>=rgInitialReadings1+15)&&(i<=rgInitialReadings1+17)) {
      //Sensor Capacitance
      Setup.GammaSetupInfo.GammaSideSetup[HVSide].InputC[i-(rgInitialReadings1+15)]=v;
      goto SetData;
    }
    if ((i>=rgInitialReadings2+15)&&(i<=rgInitialReadings2+17)) {
      //Sensor Capacitance
      Setup.GammaSetupInfo.GammaSideSetup[LVSide].InputC[i-(rgInitialReadings2+15)]=v;
      goto SetData;
    }
    
  SetData:
    l+=2;
  }// acm, observation, this function handles both single and multiple register writes
  //      at this point all registers have been written.
  // 		Best practice RTC update, multi-write 5 registers for autonomous operation.
  // 		rlen == register count, 1 is single register, == 5 writes 5 registers.
  
  if (ReSetDate) 
  {
    new_time1 = DoPackDateTime(EncodeDate(DateTime.Year%100+2000,DateTime.Month,DateTime.Day),
                               EncodeTime(DateTime.Hour,DateTime.Min,  0));
    // acm, patch to ignore setting RTC too often, e.g. if Master set to update many times per minute.
    // also enforce 5 register autonmous time update, rejecting single writes.  This helps protect data archive.
    
    // acm, should change to update no matter what delta if CLK_ERROR...
    if(/*(rlen==5) &&*/ ((abs(new_time1-cur_time) >181) || (Error&(unsigned long)(1<<erClock)) || (Error&(unsigned long)(1<<erClock2)) || (Error&(unsigned long)(1<<erClock_rd_TWI))))	// 3 min delta should ensure 1 month logging without time update.  If clock error, set time ASAP	
    {
      //		DateTime.Sec=0;  			// acm, don't clear out seconds, messes things up if RTC updated frequently... 12-29-11  But with new 300 delta algorithm, makes more sense to clear in order to sync with master
      // also observed, enables a clk_err condition to occur...
      SET_TIME_FROMBUF(); SET_TIME_FROMBUF(); // acm 2-18-12 double set to ensure time set sticks.  Otherwise window (~10 sec with DRMCC) where fresh_bit set but new time not set...
      RecalcTime=1;				// next interval time needs to be recalculated
      //ReSetSetup=1;				// resaving setup not necessary, change to like done with PDM
    }
    else GET_TIME_INBUF();			// reject time update, update DateTime structure with local RTC time
  }
  
  TestSetup();							// acm, update just the Setup bytes that changed to FRAM (observation, this strategy & ReSetSetup flag redundant!)  Ideally BHM & PDM would use consistent methods, doesn't appear to be significant performance issue.
  
  return (l-1);
}

int ScanMessages(void)	
{
#ifndef __emulator
  unsigned int Cur;
  int Result=0;
#ifndef __arm
  char Page=GetPage();
#endif
  
  for (CurUartPort=0;CurUartPort<UartPorts;CurUartPort++) {
    
    if (CurUartPort==USBPort) {
      Cur=USBReaded();
      UartCurRead[CurUartPort]=Cur;
    } else {
      Cur=UartCurRead[CurUartPort];
    }
    
    if (Cur) {  // acm, I believe Cur>0 means there are characters in rx buffer to be parsed
      if (ServerModbusListening(ID_Slave, CurUartPort==USBPort)>=0) Result=1;// acm, 1-18-11, add parameter to allow disable TCP over USB, so user cannot kill communication
    }
    if (ReSetSetup) {  // acm, note, ReSetSetup only set for IHM Setup change (MODBUS cmd 0x72), or user MODBUS setup change of ID, Baud
      SetSetupParam();  //ag 3-9-16 swaped sequence first check setup and then save
      SaveSetup();         
      //        GetNextTime(DateTimeNowNoSec());  // reconcile to PDM, only update next measurement time on clock update
      ReSetSetup=0;
    }
    
    if (RecalcTime)
    {
      if (Setup.SetupInfo.ScheduleType) GetNextTime(DateTimeNowNoSec());  // acm, don't recalc if type interval, can't hurt?  Idea is not to skip a pending measurement // acm, defined in main, PDM calls it GetNextMeasTime()
      RecalcTime=0;
    }
  }
#ifndef __arm
  SetPage(Page);
#endif
  return Result;
#else
  return 0;
#endif
}



