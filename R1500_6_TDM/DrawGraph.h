
struct StOneGraph{
           char X1,Y1,X2,Y2;
           char StartPage;
           short * BaseAddress;
           unsigned int CountX;
           int dX1,dX2;
           long dY1,dY2,YMax,YMin;
           unsigned int XLeft,XRight;
           unsigned int MaxXLeft,MaxXRight;
};

void InitGraph(struct StOneGraph *Graph,
                           char StartPage,
                           short *Addr,
                           int Count
                           );

void DrawGraph(struct StOneGraph *Graph,
                      char X1,char Y1,
                      char X2,char Y2);
