#ifndef __arm
#include "Protocol.h"
#else
#include "BoardAdd.h"
#endif
#include "Error.h"
#include "LCD.h"
#include "Graph.h"
#include "KeyBoard.h"
#include "DefKB.h"
#include "StrConst.h"
#include "Defs.h"
#include "Utils.h"
#include "Link.h"
#include "TypesDef.h"


volatile ErrorType Error=0;

ROM char ErrorMsg[MaxKnownError][MsgSize]=
      {
       {'S','e','t','1',' ','T','e','s','t',' ','f','a','i','l',0},
       {'S','e','t','2',' ','T','e','s','t',' ','f','a','i','l',0},
       {'F','l','a','s','h','W','r','i','t','e','F','a','i','l',0},
       {'F','l','a','s','h','R','e','a','d',' ','F','a','i','l',0},
       {' ',' ','S','e','t',' ','1',' ','O','F','F',' ',' ',' ',0},
       {' ',' ','S','e','t',' ','2',' ','O','F','F',' ',' ',' ',0},
       {' ','P','h','a','s','e','s',' ','S','e','t',' ','1',' ',0},
       {' ','P','h','a','s','e','s',' ','S','e','t',' ','2',' ',0},
       {' ',' ','C','a','l','i','b','r','a','t','i','o','n',' ',0},
       {' ',' ','S','r','c',' ','A',' ','O','f','f',' ',' ',' ',0},
       {' ',' ','S','r','c',' ','B',' ','O','f','f',' ',' ',' ',0},
       {' ',' ','S','r','c',' ','C',' ','O','f','f',' ',' ',' ',0},
       {' ',' ',' ','U','n','i','t',' ','O','F','F',' ',' ',' ',0},
       {' ',' ','F','r','e','q','u','e','n','c','y',' ',' ',' ',0},
       {' ',' ','L','o',' ','s','i','g','n','a','l',' ',' ',' ',0},
       {' ',' ','H','i',' ','s','i','g','n','a','l',' ',' ',' ',0},
       {' ',' ',' ',' ','S','t','o','p','p','e','d',' ',' ',' ',0},
       {' ',' ',' ',' ','P','a','u','s','e','d',' ',' ',' ',' ',0},
       {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0},
       {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0},
       {' ',' ',' ',' ','C','l','o','c','k',' ',' ',' ',' ',' ',0}
      };


KEY ShowError(unsigned int DelaySec,unsigned long CurError,KEY WaitKeyDown)
{ int i=0;
  unsigned int CurDelaySec=0;
  unsigned int CurDelayMSec=0;
  KEY ch;
#ifndef OldR1500_6
  extern
#ifdef __arm
  unsigned int
#else
  char
#endif
  AutoBalance;
#endif
  extern
#ifdef __arm
  unsigned int
#else
  char
#endif
  ReadSingle;

ClearLCD();
while (i<MaxKnownError) {
  if (CurError&((unsigned long)1<<i)) {
     ReadKey();
     ClearLCD();
     SetFont(Font8x8);
     if ((i!=erPause)&&(i!=erStop)&&(i!=erUnitOff))
        OutString(32,0,ErrorStr);
     OutString(0,8,ErrorMsg[i]);
     Redraw();
     CurDelaySec=0;
     CurDelayMSec=0;

     while ((ch=ReadKey())==KB_NO) {
         Delay(1);
         if (++CurDelayMSec>=970) {
            CurDelayMSec=0;
            CurDelaySec++;
            if (CurDelaySec>=DelaySec) break;
         }
         if (ScanMessages()) {
            ClearLCD();
            SetFont(Font8x8);
            if ((i!=erPause)&&(i!=erStop)&&(i!=erUnitOff))
               OutString(32,0,ErrorStr);
            OutString(0,8,ErrorMsg[i]);
            Redraw();
         }
     }
//     WaitReadKeyWithDelay(DelaySec,KB_ESC);
     if (ch==KB_MOD) return KB_MOD;
     if (ch==KB_MEM) return KB_MEM;
     if (ch==KB_ESC) return KB_NO;
     if (AutoBalance||ReadSingle) return KB_NO;
  }
  i++;
}
return KB_NO;
}
