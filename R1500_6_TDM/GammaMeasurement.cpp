#ifndef __arm
#include "Protocol.h"
#include "EEPROM.h"
#endif
#include <math.h>
#include "Defs.h"
#include "Utils.h"
#include "KeyBoard.h"
#include "DefKB.h"
#ifdef __arm
#include "ADCDevice.h"
#else
#include "ADC.h"
#endif
#include "CalcParam.h"
#include "MeasurementDefs.h"
#include "RTC.h"
#include "DateTime.h"
#include "SysUtil.h"
#include <stdlib.h>
#include "Error.h"
#include "Setup.h"
#include "SysUtil.h"
#include "Complex.h"
#include "Archive.h"
#include "Link.h"
#include "LogFile.h"
#include "Flash.h"
#include "Diagnosis.h"
#include "tgDelta.h"
#include "SetChannels.h"
#include "RAM.h"
#include "DrvFlash.h"
#include "TypesDef.h"
#ifdef __arm
#include "BoardAdd.h"
#endif

#ifndef __arm
#include "CalcTK.h"
#include "CalcTrend.h"
#endif

#include "Graph.h"
#include "LCD.h"
#include <string.h>
#include "StrConst.h"
#include "CrossMeasurement.h"

#include "DACDevice.h"
#include "WatchDog.h"		// acm, 2-27

#include "CalcParam.h"
extern void ImpPhaseCalc(s16 *a, s16 *x, int NPoints, float *Freq,float *Ampl, float *Faza, float *Ampl1,char Page0,char Page1);

#define MaxRepeat 2

//!!!!!!!!!!!!!!!!!!!!11

#ifdef __arm
unsigned int
#else
char
#endif
MeasurementReloaded=1;


#ifndef __emulator
#ifdef __arm
__no_init struct stInsulationParameters TParameters;
__no_init struct stInsulationParameters TempParameters;
__no_init struct stDiagnosis TDiagnosis;
#else
struct stInsulationParameters *Parameters=(struct stInsulationParameters *)ParamsAddress;
struct stDiagnosis *Diagnosis=(struct stDiagnosis *)DiagAddress;
#endif
#else
struct stInsulationParameters TParameters;
struct stDiagnosis TDiagnosis;
#ifdef __arm
struct stInsulationParameters TempParameters;
#endif
#endif

#ifdef __arm
struct stInsulationParameters *Parameters=&TParameters;
struct stDiagnosis *Diagnosis=&TDiagnosis;
struct stInsulationParameters *TempParam=&TempParameters;
#endif


//Number of confirming measurement Not Used
unsigned int SaveNum=0;

//Current state of Gamma
char GammaStatus=stNormal;

//Current state by all parameters
char FullStatus=stUnknown;

//The current status of all parameters
signed char MeasurementResult;

#ifdef __arm
unsigned int
#else
char
#endif
DiagResult;

//unsigned int FastWriteCount; //DE 3-30-16

float PhaseAAmplHV,PhaseAAmplLV=0;

unsigned int CurMeasurement=0;  // acm, 11-3-10, # of saved measurements since power up, but not used anywhere currently

extern float WatchAmpl[MaxTransformerSide];

float AmplCoeff[MaxTransformerSide][3];

float AvgGammaAmpl[MaxTransformerSide] ={0.0,0.0};  // acm, average Unn magnitude(%, sum of phase A/B/C to measured Unn circuit current)
float AvgGammaPhase[MaxTransformerSide]={0.0,0.0};  // ""phase, imaginary part of vector
char  Avgs[MaxTransformerSide]={0,0};				// flag to indicate whether to clear average

extern int TestSetupCRC(void);  // acm, 3-10-10 explicitly do this even though not doing works
extern void TestSetup(); // acm, do this so Understand resolves
extern void SaveSetup(); // acm, do this so Understand resolves

extern char stop_yellow_flashing;  // acm, v1.79, define in main(), flag indicates if Yellow should flash or not

// acm, 3-11-10, test code to verify algorithm working.  Allegheny saw issue with one unit, try to replicate.
// test:  create counter initialized to 0 at power up.  Increment after each measurement.  If == 10, set pot of phase A to 0 just once.
//        hookup 1st bushing set to tester, measure every 4 minutes, analyze results...

unsigned int diag_measurement_count;	// count, init to 0 at power up in main(), if = 10 set flag below
char Side=0;
#define POT20k_ohms 0
#define POT120_ohms 255
extern struct stSetup Setup;

char PhaseErrorFlag = 0;

static void FreePartFLASH(unsigned int Size)
{
#ifndef __arm
  char Page=GetPage();
  
  SetPage(MeasListPage);
#endif
  //deleting older
  while(1){
    if(FreeFlash()>=Size)break;
    if(!MeasurementsInArchive)break;
#ifndef __arm
    FileDelete(((unsigned short*)MeasList)[--MeasurementsInArchive]);
#else
    FileDelete(LoadFileIndex(--MeasurementsInArchive));
#endif
  }
#ifndef __arm
  SetPage(Page);
#endif
}

static char SaveCurrentMeasurement(void)  // acm, save Parameters structure to Flash, "file indices" to FRAM, but unused function...
{
#define MaxReadError 5
  TFile f;
  _TDateTime DT;
#ifndef __arm
  unsigned int n;
  char Page=GetPage();
  
  SetPage(ParametersPage);
#endif
  //2.06 ag added balancing date/time to data
  Parameters->BalYear = Setup.InitialParameters.Year;
  Parameters->BalMonth = Setup.InitialParameters.Month;
  Parameters->BalDay = Setup.InitialParameters.Day;
  Parameters->BalHour = Setup.InitialParameters.Hour;
  Parameters->BalMin = Setup.InitialParameters.Min;
  //DWE 10-9-15 get New Data Features
  Parameters->NewFeatures = Setup.SetupInfo.LoadCurrentThresholdSettings|0x8000; //plus modbus register 116
  Parameters->Error1=Error;
  Parameters->Error2=(Error>>16);
  DT=DoPackDateTime(EncodeDate(Parameters->Year,Parameters->Month,Parameters->Day),
                    EncodeTime(Parameters->Hour,Parameters->Min,0));
  FreePartFLASH(szInsulationParameters);
  if (!FileCreate(&f,ftStandart,0,&DT)) {
    Error|=((ErrorType)1<<erFlashWriteFail);
    return 0;
  }
  if ((unsigned int)FileWrite(&f, (char *)Parameters, 0,szInsulationParameters)!=szInsulationParameters) {
    Error|=((ErrorType)1<<erFlashWriteFail);
    return 0;
  }
#ifndef __arm
  SetPage(MeasListPage);
#endif
  
  // Re Sort List of Measurements
  CreateList((u16 *)MeasListTempAddr,&MeasurementsInArchive,stForward/*����� � ������*/);
  
#ifdef __arm
  SaveFileIndexes(MeasListTempAddr);
#else
  SetPage(Page);
#endif
  EventTrigger = EventTrigger | NewMeasurement; //Flag new measurement
  return 1;
}

float Gamma_for_trend_newwarning_check[MaxTransformerSide]={0,0} ; // acm, init in GetGammaStatus(), ref in GetFullStatus(), and in diagnosic.cpp/dosum
// above variable is initialized to latest measured gamma's just prior to GetGammStatus/GetFullStatus execution from StartMeasure calls
char GetGammaStatus(float Value,char Side,char OldStatus,char AllowAlarms)
{ char Result=stNormal;
unsigned int CurrentSettings=0;
char LoadGreaterThanSetting=0;
char LoadSmallerThanSetting=0;

if(!AllowAlarms)
{
  CurrentSettings = Setup.SetupInfo.LoadCurrentThresholdSettings; //use only least 4 bits
  
  //DE 8-15-16 add check for set off here
  if (!(Setup.GammaSetupInfo.ReadOnSide&(1<<0)) && (CurrentSettings &(3<<(2*0))))  //if Set1 OFF then clear set 1 setting
  {
    CurrentSettings &=0xFFFC;
  }
  if(!(Setup.GammaSetupInfo.ReadOnSide&(1<<1)) && (CurrentSettings &(3<<(2*1)))) //if Set1 OFF then clear set 2 setting
  {
    CurrentSettings &=0xFFF3;
  }  
  
  CurrentSettings = CurrentSettings & 0x0F;
  CurrentSettings = CurrentSettings >> (Side*2);
  CurrentSettings = CurrentSettings & 0x03;        //use only least 2 bits
  
  if(ExtLoadActive >= Setup.SetupInfo.LoadCurrentThresholdLevel) LoadGreaterThanSetting = 1;
  if(ExtLoadActive < Setup.SetupInfo.LoadCurrentThresholdLevel) LoadSmallerThanSetting = 1;
  
  switch (CurrentSettings) {
  case 0: AllowAlarms =1; break;
  case 1: if (LoadSmallerThanSetting)
  {AllowAlarms =1; break;}
  else {Error|=((unsigned long)(1<<(erSide1Threshold + Side)|1<<(erSide1Critical + Side))); break;}
  case 3: if (LoadGreaterThanSetting)
  {AllowAlarms =1; break;}
  else {Error|=((unsigned long)(1<<(erSide1Threshold + Side)|1<<(erSide1Critical + Side))); break;}
  }
}

if ((Value>(float)Setup.GammaSetupInfo.GammaSideSetup[Side].GammaRedThresholld*0.05) && (AllowAlarms))  Result=stAlarm;
else if ((Value>(float)Setup.GammaSetupInfo.GammaSideSetup[Side].GammaYellowThresholld*0.05) && (AllowAlarms))  Result=stWarning;
if (((OldStatus==stAlarm)&&
     (Result<stAlarm)&&
       (Value>(float)Setup.GammaSetupInfo.GammaSideSetup[Side].GammaRedThresholld*0.05-(((float)Setup.GammaSetupInfo.GammaSideSetup[Side].GammaRedThresholld*0.05)*((float)Setup.GammaSetupInfo.AlarmHysteresis*0.01)))) && (AllowAlarms))
{
  Result=stAlarm;
}
if((Error&((unsigned long)1<<erRainTimer)) || !AllowAlarms) Result = OldStatus; //DE 4-5-16 allow alarms if new feature Load current is set or rain timer is set 
Gamma_for_trend_newwarning_check[Side]=Value;  // acm, init temp array, passed to GetFullStatus().

return Result;
}

//#pragma optimize=s 2  //DE 12-9-15 turn off optimization so vars are resolved
void GetFullStatus(char GammaStatus,char Trend,char KT,char *Status,char *AlarmStatus,char Side)
{       //DE 2-23-16 changed for LoadCurrentThresholdLevels
  char TrendStatus=stNormal,KTStatus=stNormal;
  float temp;
  
  temp = ((float)Setup.GammaSetupInfo.GammaSideSetup[Side].GammaRedThresholld*0.05-Gamma_for_trend_newwarning_check[Side]);
  if (temp<0.0) temp=0.0;  // acm, 4-30, if trend=0, then alarm/warning won't get set
  if ((Gamma_for_trend_newwarning_check[Side] >= 2.0) && (Trend*1 > (temp/Setup.GammaSetupInfo.DaysToCalculateTrend/3*365))) TrendStatus=stWarning; //acm, 4-27 change from .2 to 1
  
  if (KT>Setup.GammaSetupInfo.GammaSideSetup[Side].TCoefficient)   KTStatus=stWarning;
  
  //4-2-12 correct MODBUS map, change encoded type to bit coded
  //bit 0 Gamma magnitude alarm
  //bit 1 Trend warning
  //bit 2 KT Temperature variation
  //bit 3 
  //bit 4 Gamma magnitude warning
  *AlarmStatus=0;
  
  /* acm, v1.79 noticed Alarm Enable bits don't correspond to Alarm Status bits (Trend & KT swapped), and Gamma warning is really bit 4 not bit 1.  Remedy, update MODBUS MAP.
  0 bit � not used
  1 bit - Temperature coefficient 
  2 bit - Trend */
  
  if (GammaStatus==stAlarm) *AlarmStatus|=(1<<0);
  if (GammaStatus==stWarning) *AlarmStatus|=(1<<4);
  if ((TrendStatus==stWarning)&&(Setup.GammaSetupInfo.GammaSideSetup[Side].AlarmEnable&0x04)) *AlarmStatus|=(1<<1);
  if ((KTStatus==stWarning)&&(Setup.GammaSetupInfo.GammaSideSetup[Side].AlarmEnable&0x02)) *AlarmStatus|=(1<<2);
  
  if (GammaStatus>*Status) *Status=GammaStatus;
  // *Status=GammaStatus;
  
  if (Setup.GammaSetupInfo.GammaSideSetup[Side].AlarmEnable&0x04) {
    if (TrendStatus>*Status) *Status=TrendStatus;
  }
  if (Setup.GammaSetupInfo.GammaSideSetup[Side].AlarmEnable&0x02) {
    if (KTStatus>*Status) *Status=KTStatus;
  }
  
}

void OutLED(char Status)
{
  if (Status==stAlarm) {LED1Red;}
  else if (Status==stWarning) {LED1Yellow;}
  else {
#ifdef __arm
    AllLEDOff;
#endif
    LED1Green;}
}

void OutRelay(char Status)
{
  extern unsigned int OutRelayFrom;
  
  if (Setup.SetupInfo.Relay) {
    if (Status==stAlarm) {
      SetRelayAlarm;
      if (Setup.SetupInfo.Relay==2) {
#ifdef __arm
        GET_TIME_INBUF();
#else
        GET_TIME();
#endif
        OutRelayFrom=(unsigned int)DateTime.Min*(unsigned int)60+(unsigned int)DateTime.Sec;
        if (!OutRelayFrom) OutRelayFrom=1;
      }
    } else {
      if (Status==stWarning) {
        SetRelayWarning;
        if (Setup.SetupInfo.Relay==2) {
#ifdef __arm
          GET_TIME_INBUF();
#else
          GET_TIME();
#endif
          OutRelayFrom=(unsigned int)DateTime.Min*(unsigned int)60+(unsigned int)DateTime.Sec;
          if (!OutRelayFrom) OutRelayFrom=1;
        }
      } else {
        SetRelayNormal;
      }
    }
  } else
    SetRelayNormal;
}

void OutGammaStatus(char local_fullStatus,char SkipRelayOut)
{
  
  // acm, 4-24, desire to wait until re-read to set both Relay and LED simultaneously 
  // also, rename GammaStatus variable to local_fullStatus, to clarify difference from global scope GammaStatus (this function actually passed Fullstatus).  C should keep this straight, but my memory is fragile.
  if (!SkipRelayOut){
    OutLED(local_fullStatus);
    OutRelay(local_fullStatus);
  }
  
}

#ifndef __arm

unsigned int ReadADC(char Channel, float Freq)
{
#ifndef __emulator
  
  extern unsigned int WaveTime;    //Sin period in ticks of timer 1 (clk/8)
  
#define ADCReadCount 200
#define ReadPerPeriod1 60.0
  
  char i,c;
  unsigned long Sum=0;
  unsigned int Delay;
  
#ifdef R1450
  ADCSR=0x85;             //Enable Internal ADC
#else
  ADCSR=0x85;             //Enable Internal ADC
#endif
  if ((Freq<=1)||(Freq>=200)) if (!DefaultFrequency) Freq=50.0; else Freq=60.0;
  Delay=(unsigned int)(1.0/Freq*1000.0/ReadPerPeriod1*1000.0);
  ADMUX=Channel; //Set channel
  ADCSR|=(1<<ADSC);      //Start ADC
  while (ADCSR&(1<<ADSC)) ;
  
  for (i=0; i<ADCReadCount; i++) {
    ADCSR|=(1<<ADSC);      //Start ADC
    while (ADCSR&(1<<ADSC)) ;
    c=ADCL;
    Sum+=(int)(c+((int)(ADCH&0x3)<<8));
    DelayMkSec(Delay);
  }
  ADCSR&= ~(1<<ADEN);       //Disable Internal ADC
  return Sum/ADCReadCount;
#else
  return 0;
#endif
}

unsigned int ftoui(float f)
{ unsigned int Value;
if (f>=0) Value=f; else Value=0-f;
if (f-Value>0.5) Value++;
return Value;
}
#endif


static float absfloat(float f)
{
  if (f>0) return f; else return 0-f;
}

char RunCalibration(float *Phase,float *Freq,float *Ampl,char NoBalans, char Side)
{
  
  char Repeat,i,Result=1;
  float A1,A1Points;
  struct TComp tmpComplex1,tmpComplex2,ResultComplex;
  
  SetInputChannel(Side,chPhaseA);
  
  SetGain(4,AmplifIndex[Side][BushingOnGammaCh]);
  Delay(50);
  
  Repeat=0;
  
CalibrAgain:
  
  A1Points=0;
  if (!(((*Freq>23)&&(*Freq<27))||((*Freq>48)&&(*Freq<52))||((*Freq>58)&&(*Freq<62)))) *Freq=0;
  ResultComplex.re=0;
  ResultComplex.im=0;
  
#ifndef __emulator
  //  __delay_cycles(8000);
#endif
  
  for (i=0;i<(CalibrRepeat);i++) {
    //while (1){
#ifdef __emulator
    SetInputChannel(Side,chPhaseA);
#endif
    ReadData(0,0,chReadAllChannels,Side,SetNullLine,0);
    
    tmpComplex1.re=0;
    
    *Freq=0; // acm, force Freq recalc by zero crossing method 
    
    ImpPhaseCalc((s16 *)(AddrSig[0]),
                 (s16 *)(AddrSig[3]),
                 UpLoadCount,
                 //                   &tmpComplex1.im,
                 Freq,
                 &tmpComplex2.re,&tmpComplex2.im,&tmpComplex1.re,GammaPage0,GammaPage3);
    
    A1Points+=tmpComplex1.re;
    tmpComplex2.re=tmpComplex2.re*(float)ADCStep/GammaGainValue[AmplifIndex[Side][BushingOnGammaCh]];
    tmpComplex1.re=ResultComplex.re;
    tmpComplex1.im=ResultComplex.im;
    vec_add(&tmpComplex1,&tmpComplex2,&ResultComplex);
  }
  
  *Phase=ResultComplex.im;
  if (*Phase>180) *Phase-=360;
  
  A1=ResultComplex.re/(CalibrRepeat);
  *Ampl=A1;
  if (A1-*Ampl>=0.5) (*Ampl)++; //*Ampl++;  acm, static analysis check fail, likely not big deal, does change SIM file.
  A1Points=(A1Points/(CalibrRepeat))*((float)ADCStep/GainValue[AmplifIndex[Side][BushingCh]])*2;
  
  if (NoBalans) {
    if (IsInitialParam) {
      if (*Ampl<(float)Setup.InitialParameters.TrSideParam[Side].PhaseAmplitude[BushingA]/10.0*0.5) {
        Repeat++;
        if (Repeat>MaxRepeat) {
          Error|=((unsigned long)1<<(erLoSignal));  // ag 2-12-16 changed error definition to erLoSignal, Error|=((unsigned long)1<<(erSet1Off+Side)); 
          Error|=((unsigned long)1<<(erSide1Critical+Side)); //ag 2.06 added back //DE 3-7-16 moved DE 2-25-16 flag critical error to include in diagnosis
        } else goto CalibrAgain;
      }
    }
  }
  
  if (A1Points<60) {
    Repeat++;
    if (Repeat>MaxRepeat) {
      Error|=((unsigned long)1<<erPhaseAOff); //ag 2-12-16, changed erLoSignal to erPhaseAOff to comply to new definitions
      Error|=((unsigned long)1<<(erSide1Critical+Side)); //ag 2.06 added back//DE 3-7-16 moved DE 2-25-16 flag critical error to include in diagnosis
    } else goto CalibrAgain;
  }
  
  if (A1Points>2900) {
    Repeat++;
    if (Repeat>MaxRepeat) {
      Error|=((unsigned long)1<<erHiSignal);
      Error|=((unsigned long)1<<(erSide1Critical+Side)); //ag 2.06 back//DE 3-7-16 moved DE 2-25-16 flag critical error to include in diagnosis
    } else goto CalibrAgain;
  }
  if (!(((*Freq>23)&&(*Freq<27))||((*Freq>48)&&(*Freq<52))||((*Freq>58)&&(*Freq<62)))){
    Repeat++;
    if (Repeat>MaxRepeat) {
      Error|=((unsigned long)1<<erFreq);
      Error|=((unsigned long)1<<(erSide1Critical+Side)); //ag 2.06
    } else goto CalibrAgain;
  }
  else Error&=~((unsigned long)1<<erFreq); //2.06 ag clear, if passed
  
  if ((absfloat(*Phase)>Setup.GammaSetupInfo.AllowedPhaseDispersion)&&(absfloat(*Phase)<360-Setup.GammaSetupInfo.AllowedPhaseDispersion)) {
    Repeat++;
    if (Repeat>MaxRepeat) {
      Error|=((unsigned long)1<<erChannels);
      Error|=((unsigned long)1<<(erSide1Critical+Side));//ag 2.06 back //DE 3-7-16 moved DE 2-25-16 flag critical error to include in diagnosis
    } else goto CalibrAgain;
  }
  if(Error&((unsigned long)1<<erRainTimer)) Error|=((unsigned long)1<<(erSide1Critical+Side));//ag 2.06
  
  return Result;
}


void ReadAmlPhase(unsigned int *Ampl, float *Phase, float PshHV, float Freq, char Side)
{
  
  char i;
  float f;
  struct TComp tmpComplex1,tmpComplex2,ResultComplex;
  
  SetGain(4,AmplifIndex[Side][BushingOnGammaCh]);
  ResultComplex.re=0;
  ResultComplex.im=0;
  
  for (i=0;i<CalibrRepeat;i++) {
    
#ifdef __emulator
    if (i) ReadData(0,0,chReadAllChannels,Side,NoSetNullLine,0);
    else ReadData(0,0,chReadAllChannels,Side,SetNullLine,0);
#else
    ReadData(0,0,chReadAllChannels,Side,SetNullLine,0);
#endif
    
    Freq=0;  // acm, force Freq recalc by zero crossing method (downside, if recalc results in error/zero vector, returned input currents stored as 0, even though Unn maybe calculated, results in confusing data analysis)
    
    tmpComplex1.re=0;	// acm, force recalc of amplitude
    ImpPhaseCalc((s16 *)(AddrSig[0]),
                 (s16 *)(AddrSig[3]),
                 UpLoadCount,
                 &Freq,
                 &tmpComplex2.re,&tmpComplex2.im,&tmpComplex1.re,GammaPage0,GammaPage3);
    
    
    tmpComplex2.re=tmpComplex2.re*(float)ADCStep/GammaGainValue[AmplifIndex[Side][BushingOnGammaCh]];
    tmpComplex1.re=ResultComplex.re;
    tmpComplex1.im=ResultComplex.im;
    vec_add(&tmpComplex1,&tmpComplex2,&ResultComplex);
  }
  *Phase=ResultComplex.im-PshHV;
  
  PhaseRound(Phase);
  
  f=ResultComplex.re/CalibrRepeat/*ChAmplDiff*/*10.0;
  (*Ampl)=f;
  if (f-(*Ampl)>=0.5) (*Ampl)++;
}


char s[21], s1[21];  // acm, transgrid debug message output
//#pragma optimize=s 2  //DE 12-9-15 turn off optimization so Coeff are resolved
char ReadSourcePhases(float *AmplA,float *AmplB,float *AmplC,float *ABPhaseShift,float *ACPhaseShift, char TrSide, float *Freq,char OnBalance)
{
  char i,j,Result=1,BadAPhase=0; //ag2-10-16 added ariable flag indicating that phase A does not have a signal
  float CoeffB,CoeffC;
  struct TComp tmpComplex1,tmpComplex2,tmp,tmpComplex3,tmpComplex4,ResultComplex;
  float ACShift,ABShift;
  unsigned int TempCurrentSettings=0;
  //SetGain
  ACShift=RunCalibrAmpl(TrSide, AmplCoeff[TrSide]);
  if (ACShift<0.5) return 0;
  if (fabs(*Freq)<0.5) *Freq=ACShift;
  
  if (!OnBalance) {
    
    SetInputChannel(TrSide,chSourcePhaseA);				// acm, chA replicated to ch B/C, Unn masked
    
    *AmplA=0;
    *AmplB=0;
    *AmplC=0;
    tmpComplex1.im=tmpComplex1.re=tmpComplex2.im=tmpComplex2.re=tmpComplex3.im=tmpComplex3.re=0;
    CoeffB=0;
    CoeffC=0;
    PhaseErrorFlag = 0;
    
    for (i=0;i<CalibrRepeat;i++) {
#ifdef __emulator
      SetInputChannel(TrSide,chSourcePhaseA);
#endif
      ReadData(0,0,chReadAllChannels,TrSide,SetNullLine,0);
      
      *Freq=0;
      
      tmpComplex3.re=0;// acm, force recalc of amplitude
      
      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[1]),
                   UpLoadCount,
                   Freq,
                   &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage1);
      //Averaging the frequency
      //       if (i==0) *Freq=(*Freq+ACShift)/2.0;
      
      *AmplA+=tmpComplex3.re;
      *AmplB+=tmpComplex4.re;
      
      tmp.re=tmpComplex4.re;
      tmp.im=tmpComplex4.im;
      vec_add(&tmpComplex2,&tmp,&ResultComplex);
      tmpComplex2.re=ResultComplex.re;
      tmpComplex2.im=ResultComplex.im;
      
      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[2]),
                   UpLoadCount,
                   //                   &tmpComplex3.im,
                   Freq,
                   &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage2);
      
      
      *AmplC+=tmpComplex4.re;
      tmp.re=tmpComplex4.re;
      tmp.im=tmpComplex4.im;
      vec_add(&tmpComplex1,&tmp,&ResultComplex);
      tmpComplex1.re=ResultComplex.re;
      tmpComplex1.im=ResultComplex.im;
      
      if (*AmplA<10) //ag 2-10-16 To resolve case when signal on the phaseA does not exist 
      {
        CoeffB+=1;
        CoeffC+=1;
        BadAPhase=1;
      }
      else
      {CoeffB+=*AmplA/(*AmplB);  // acm, 
      CoeffC+=*AmplA/(*AmplC);  // CoeffBC calculated with input signals (belw AmplCoeff[] calcuated with DAC).  Theory, normalize A/B/C magnitude for better phase calculation?
      }
    }
    
    *AmplA/=CalibrRepeat;
    *AmplB/=CalibrRepeat;
    *AmplC/=CalibrRepeat;
    
    CoeffB/=CalibrRepeat;
    CoeffC/=CalibrRepeat;
    ACShift=tmpComplex1.im;
    ABShift=tmpComplex2.im;
    
  } else { //OnBalance
    CoeffB=1;
    CoeffC=1;
    ACShift=0;
    ABShift=0;
  }
  
  *AmplA=0;
  *AmplB=0;
  *AmplC=0;
  tmpComplex1.im=tmpComplex1.re=tmpComplex2.im=tmpComplex2.re=0;
  
  SetInputChannel(TrSide,chSourcePhases); // acm, chA, chB, chC sampled, Unn masked
  
  for (i=0;i<CalibrRepeat;i++) {
#ifdef __emulator
    SetInputChannel(TrSide,chSourcePhases);
#endif
    ReadData(0,0,chReadAllChannels,TrSide,SetNullLine,0);
    tmpComplex3.re=0;  //Ampl  // acm, force recalc of amplitude
    
    if (BadAPhase)  
    {
      for (j=0;j<3;j++)
      {
        ImpPhaseCalc((s16 *)(AddrSig[j]),
                     (s16 *)(AddrSig[j]),
                     UpLoadCount,
                     Freq,
                     &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage1);
        if (j==0) {*AmplA+=tmpComplex4.re;}
        if (j==1) {*AmplB+=tmpComplex4.re;}
        if (j==2) {*AmplC+=tmpComplex4.re;}
      }
    }
    else
    {ImpPhaseCalc((s16 *)(AddrSig[0]),
                  (s16 *)(AddrSig[1]),
                  UpLoadCount,
                  Freq,
                  &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage1);
     
     *AmplA+=tmpComplex3.re;
     *AmplB+=tmpComplex4.re;
     
     tmp.re=tmpComplex4.re;
     tmp.im=tmpComplex4.im;
     vec_add(&tmpComplex2,&tmp,&ResultComplex);
     tmpComplex2.re=ResultComplex.re;
     tmpComplex2.im=ResultComplex.im;
     ImpPhaseCalc((s16 *)AddrSig[0],
                  (s16 *)AddrSig[2],
                  UpLoadCount,
                  Freq,
                  &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage2);
     
     *AmplC+=tmpComplex4.re;
     tmp.re=tmpComplex4.re;
     tmp.im=tmpComplex4.im;
     vec_add(&tmpComplex1,&tmp,&ResultComplex);
     tmpComplex1.re=ResultComplex.re;
     tmpComplex1.im=ResultComplex.im;
    }
  }
  
  *AmplA/=CalibrRepeat; //ag All 25mA A=7569ticks, B=7662, C=7713
  *AmplA=(*AmplA*ADCStep)/GainValue[AmplifIndex[TrSide][BushingCh]]; //ag All 25mA A=347,B=351, C=353  ADCStep=9.15e-2, GainValue=2.0, AmplifIndex=1
  *AmplB/=CalibrRepeat;//*CoeffB;
  *AmplB*=CoeffB;
  *AmplB=(*AmplB*ADCStep)/GainValue[AmplifIndex[TrSide][BushingCh]]; //ag B=0mA, A=346, B=0.62, C=354
  *AmplC/=CalibrRepeat;//*CoeffC; //ag B=0mA, AmpC=7692ticks
  *AmplC*=CoeffC;
  *AmplC=(*AmplC*ADCStep)/GainValue[AmplifIndex[TrSide][BushingCh]]; //ag C=0mA, A=346, B=351, C=0.61
  
  //ag 2-12-16 The logic now only to compare to 60 and 2900, no comparison to initial data.
  BadAPhase=0;
  if(*AmplA*2 < 60)
  {
    Error|=((unsigned long)1<<erPhaseAOff);
    Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06 back //DE 3-7-16 moved to 989 //DE 2-25-16 flag critical error to include in diagnosis
    Error&=~((unsigned long)1<<erFreq);
    PhaseErrorFlag = TrSide+1;
    BadAPhase++;
    Result=0;
  }
  if(*AmplB*2 < 60)
  {
    Error|=((unsigned long)1<<erPhaseBOff);
    //Error&=~((unsigned long)1<<erFreq); //2.06 ag commented Only phA used for freq
    Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06 back //DE 3-7-16 moved to 989  //DE 2-25-16 flag critical error to include in diagnosis
    PhaseErrorFlag = TrSide+1;
    BadAPhase++;
    Result=0;
  }
  if(*AmplC*2 < 60)
  {
    Error|=((unsigned long)1<<erPhaseCOff);
    //Error&=~((unsigned long)1<<erFreq); //2.06 ag commented Only phA used for freq
    Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06 back //DE 3-7-16 moved to 989  //DE 2-25-16 flag critical error to include in diagnosis
    PhaseErrorFlag = TrSide+1;
    BadAPhase++;
    Result=0;
  }
  if(BadAPhase==3) Error|=((unsigned long)1<<(erSet1Off+TrSide));
  
  if ((*AmplA*2>2900)||(*AmplB*2>2900)||(*AmplC*2>2900)) 
  {
    Error|=((unsigned long)1<<erHiSignal);
    Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06 back //DE 3-7-16 moved to 989  //DE 2-25-16 flag critical error to include in diagnosis
    Result=0;
  }
  
  //DE 8-15-16 add check for set off here //AG 8-17-16 changed to !(Setup.***)
  TempCurrentSettings = Setup.SetupInfo.LoadCurrentThresholdSettings; //use only least 4 bits
  if(!(Setup.GammaSetupInfo.ReadOnSide&(1<<0)) && (TempCurrentSettings &(3<<(2*0))))  //if Set1 OFF then clear set 1 setting
  {
    TempCurrentSettings &=0xFFFC;
  }
  if(!(Setup.GammaSetupInfo.ReadOnSide&(1<<1)) && (TempCurrentSettings &(3<<(2*1)))) //if Set1 OFF then clear set 2 setting
  {
    TempCurrentSettings &=0xFFF3;
  }  
  //DE 8-15-16
  
  if ( (TempCurrentSettings & (3<<(2*TrSide))) && (ExtLoadActive < Setup.GammaSetupInfo.GammaSideSetup[TrSide].RatedCurrent * 0.05) )
  {
    Error|=((unsigned long)1<<erLoCurrent);
    Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06
  }
  
  //subtracting the shift of phase A
  ACShift=tmpComplex1.im+(360-ACShift);
  ABShift=tmpComplex2.im+(360-ABShift);
  
  if (ACShift>=360) ACShift=ACShift-360;
  if (ABShift>=360) ABShift=ABShift-360;
  
  if (ACShift<0) ACShift=ACShift+360;
  if (ABShift<0) ABShift=ABShift+360;
  
  *ABPhaseShift=ABShift;
  *ACPhaseShift=ACShift;
  
  
  
  if ((fabs(ABShift-120)>Setup.GammaSetupInfo.AllowedPhaseDispersion)) {
    Error|=((unsigned long)1<<(erPhaseShiftSet1+TrSide));
    Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06 back //DE 3-7-16 moved to 989  //DE 2-25-16 flag critical error to include in diagnosis
    if ((Error&0x0E00)&&(PhaseErrorFlag == (TrSide+1))) Error&=~((unsigned long)1<<(erPhaseShiftSet1+TrSide));
    Result=0; // DE 2-2-16 removed to allow measure to continue
  }
  if ((fabs(ACShift-240)>Setup.GammaSetupInfo.AllowedPhaseDispersion)) {
    Error|=((unsigned long)1<<(erPhaseShiftSet1+TrSide));
    Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06
    if ((Error&0x0E00)&&(PhaseErrorFlag == (TrSide+1))) Error&=~((unsigned long)1<<(erPhaseShiftSet1+TrSide));
    Result=0; // DE 2-2-16 removed to allow measure to continue
  }
  // acm, AmplCoeff[TrSide] calculated above RunCalibrAmpl(), done in addition to CoefcB/C?  Used to calculate SourceAmplitude cur...
  *AmplA*=AmplCoeff[TrSide][0];  //ag All=25mA, A=1129, B=1141, C=1159, AmplCoeff(3.26, 3.252, 3.278)
  *AmplB*=AmplCoeff[TrSide][1];	  //ag B=0mA, A=1087, B=1.94, C=1118, AmplCoeff(3.138, 3.132, 3.159)
  *AmplC*=AmplCoeff[TrSide][2]; //ag C=0mA,  A=1081, B=1106, C=1.9, AmplCoeff(3.091, 3.153, 3.179)
  
  return Result;
}
//#pragma optimize=s 2  //ag 3-8-16 turn off optimization so all are resolved
signed char StartMeasure(char Save,char GammaAvg)
{
  //Measure all parameters
  char Repeat;
  int i;
#ifndef __arm
  int ii;
#endif
  signed char Result;
  float A1;
  // acm, PshHV - filter phase error???  phase between ch A and gama????
  float PshHV[MaxTransformerSide+1],Freq; // acm, 3-8-09, add one to PshHV to eliminate a compiler warning message.  Verified compiled code, original code initialized 3 fp words on stack but only two words allocated.  New code allocated additional fp word, result of previous bug unkown.
  float PhaseShift;
  struct TComp tmpComplex1,tmpComplex2,ResultComplex;
  char CurrentGammaStatus=stNormal,tmpStatus;
  char ABCAmpl;
  char NeedSaveLog=0;
  //  char AGain,BGain,CGain,ReReadAmplitude=0;
  
  float AmplA,AmplB,AmplC;
  char OldAlarmStatus[MaxTransformerSide];	// acm, used to save log status, unsure about philosophy
  char AvrgGamma;
  char TrSide;
  _TDateTime DiagDate;
  unsigned int CurAmpl;
  unsigned long OldError;
  char OldGammaStatus[MaxTransformerSide];
  float ABPhaseShift,ACPhaseShift;
  unsigned int ZkPercent[MaxBushingCountOnSide];
  int noise_event_counter; //acm
  
#ifdef SinTest
  unsigned int n;
#define Ampl1 200
#define Ampl2 100
#define Ampl2n1 0
#define Ampl2n2 0
#endif
  char FullStatus_tmp=0;	// v2.00, GetFullStatus(&FullStatus_tmp), MUST init to elminate case where BHM may report back to Main/SCADA odd values, e.g. 112, 254 AND masking Alarm (a show stopper)! Note, Understand codecheck doesn't catch uninit pointer // acm, v1.79, replaces FullStatus.  FullStatus which shows up in Modbus reg 4, must'nt be set until after re-read finished.
  char AlarmStatus_tmp[2];		// MODBUS reg 5/6
#define X (float)Setup.GammaSetupInfo.AveragingForGamma_X/100			// acm, v2.00 new averaging coefficient 
  
  // acm, 2-22-09, Alexander idea, Idea is to indirectly detect when digital POT value settings are corrupt.  If Gamma/Unn vector difference between current
  // and previous measurement is greater than limit (e.g. 1%), take a third measurement and save "good"/toss bad value (software noise rejection).
  // Need to analyze:  performance hit, extra RAM usage.
  
  noise_event_counter=0;  // counter controls how many times StartMeasure() is re-run, current srategy is 3 back-back large Gamma increases isn't a noise event. 3 
  SetFont(Font6x5);
  ResetWatchDog(); 		// precaution, as we're adding delay to this function, really need to analyze where these are needed
  
  /*	FullStatus_tmp=FullStatus; // acm, v2.00, init to previous measurment, so if there is read error & side disabled preserves Alarm Status
  4-24-11, this doesn't help.  Upon further thought, what happens is if error (erCal if use low input current on one channel, ~9.5mA/25/26), logic
  disables channel1 and sets error bit, move onto next channel, that measurment ok, so alarm turned off.  Green blinks fast.  Not going to rework
  just to keep Red LED on as not priority, rather will document LED behavior.
  */	
  OldError=Error; //ag 3-8-16 moved to here from below to not modify on redo measurement
redo_measurement:
  TestSetupCRC();  // acm 2-13-09, Botov fix.  Enhanced by acm below, at end of measurement before save to FRAM/Flash check for CRC mismatch.
  
redo_measurement2:   // enter here to skip redundant call to TestSetupCRC
  Result=0;
  PshHV[0]=0; PshHV[1]=0; PshHV[2]=0;
  Freq=0;
  
  //InitDAC();  //acm, reduce bad POT noise event window by peforming this function prior to first ADC read, several lines down.
  
  Error&=~((unsigned long)1<<erPhaseShiftSet1);
  Error&=~((unsigned long)1<<erPhaseShiftSet2);
  Error&=~((unsigned long)1<<erPhaseShiftSetSpot1);	// acm, add v2.00, help isolate transgrid error, differentiate that this error happened line 823
  Error&=~((unsigned long)1<<erPhaseShiftSetSpot2);	// acm, add v2.00, help isolate transgrid error, differentiate that this error happened line 827
  Error&=~((unsigned long)1<<erChannels);
  Error&=~((unsigned long)1<<erUnitOff);
  Error&=~((unsigned long)1<<erPhaseAOff);
  Error&=~((unsigned long)1<<erPhaseBOff);
  Error&=~((unsigned long)1<<erPhaseCOff);
  Error&=~((unsigned long)1<<erFreq);
  Error&=~((unsigned long)1<<erLoSignal);
  Error&=~((unsigned long)1<<erHiSignal);
  Error&=~((unsigned long)1<<erSet1Off);
  Error&=~((unsigned long)1<<erSet2Off);
  Error&=~((unsigned long)1<<erSide1Threshold);	        // dwe, clear Side12 Threshold test failed 
  Error&=~((unsigned long)1<<erSide2Threshold);	        // dwe, clear Side2 Threshold test failed
  Error&=~((unsigned long)1<<erSide1Critical);
  Error&=~((unsigned long)1<<erSide2Critical);
  Error&=~((unsigned long)1<<erLoCurrent);
  Error&=~((unsigned long)1<<erwarnFlashYelHiUnnBal);  // acm, reset error condition at next measurement (after balancing), thought is user has been informed, and >1% imbalance is acceptable
  //  Error&=~((unsigned long)1<<erRainTimer);	        // dwe, clear erRainTimer //DE 4-6-16 changed
  Error&=~((unsigned long)1<<erFlashWriteFail);	        // dwe, clear erFlashWriteFail 
  Error&=~((unsigned long)1<<erFlashReadFail);	        // dwe, clear erFlashReadFail 
  Error&=~((unsigned long)1<<erHVTestFail);	//2.05 ag 12-16-16 this has never been cleared
  Error&=~((unsigned long)1<<erLVTestFail);	//2.05 ag 12-16-16 this has never been cleared
  
  if(Save != svNoSave) stop_yellow_flashing = 1;	// acm, v2.00 after 1st measurement halt yellow LED flashing if set prior to alert hi imbalance or phase swap (Yellow LED now resumes Warning function).
  
  SetPage(ParametersPage);
  for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
    OldAlarmStatus[TrSide]=Parameters->TrSide[TrSide].AlarmStatus;		// preserve AlarmStatus, before Parameters-> cleared
    if (OldAlarmStatus[TrSide]&(1<<0)) OldGammaStatus[TrSide]=stAlarm;		// preserve GammaStatus 
    else
      if (OldAlarmStatus[TrSide]&(1<<4)) OldGammaStatus[TrSide]=stWarning;
      else OldGammaStatus[TrSide]=stNormal;
  }
  
  InitDAC();  /* acm, 2-10-12 move this here up a few lines, ahead of Parameters-> clear.  This fixes end case where SetSetupParam() clears red alarm inappropriately: alarm will alternate on/off between successive
  large Unn readings.  Expected behavior is measurement 1, if Unn too large, re-read in 5min, then if measurement still too large, drive LED & Relay. Following measurements keep red LED on.
  Verified this behavior started a long time ago in v1.4, just never noticed.  SetSetupParm() clears red alarm state. Solution move Init_dac() here before Parameters-> structure initialized 0.
  Effect is SetupParm() call then has valid data to drive LED with, otherwise, it clears red alarm in specific instance.
  */  
  // acm, v2.00, 4-4-12, to fix clobber of FullStatus, modify LoadSetup(0,1), avoid SetSetupParam() call.  SetSetupParam() does several things probably unnecessary.  Reinits FullStatus and AlarmStatus to last
  // measurement, which isn't desired fore re-measure situation.  Maybe better solution to 2-10-12 change, could move InitDAC() back to previous spot.
  
  for (i=0;i<szInsulationParameters;i++) // acm, easy way to zero out all Parameters->, but zero's AlarmStatus, not good for SCADA.
    *((char*)Parameters+i)=0;
  
  // acm, v2.00, restore AlarmStatus so SCADA doesn't see glitch
  for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) Parameters->TrSide[TrSide].AlarmStatus=OldAlarmStatus[TrSide];
  
#ifdef __arm
  GET_TIME_INBUF();
#else
  GET_TIME();
#endif
  
  Parameters->Day=DateTime.Day;
  Parameters->Month=DateTime.Month;
  Parameters->Year=DateTime.Year%100;
  Parameters->Hour=DateTime.Hour;
  Parameters->Min=DateTime.Min;
  Parameters->TrSideCount=0;		// acm, this means both sides "disabled".  If all cal tests pass, set1/side0 or set2/side1 enabled.
  
  SetPage(ParametersPage);
  if (Setup.SetupInfo.ReadAnalogFromRegister) {		// acm, if IHM user selects "Data from SCADA"
    Parameters->Temperature[0]=ExtTemperature[0];
    Parameters->Temperature[1]=ExtTemperature[1];
    Parameters->Temperature[2]=ExtTemperature[2];
    Parameters->Temperature[3]=ExtTemperature[3];
    //LTC position from external SCADA
    Parameters->RPN[0]=ExtRPN;
    Parameters->RPN[1]=ExtRPN;
    //Active load from external SCADA
    Parameters->Current[HVSide]=ExtLoadActive;
    Parameters->Current[LVSide]=LoadCurrent2; // acm, v2, replace redundant current with current 2 like PDM.  ExtLoadActive;
    //Reactive load from external SCADA
    Parameters->CurrentR[HVSide]=ExtLoadReactive-127;
    Parameters->CurrentR[LVSide]=LoadCurrent3; // // acm, v2, replace redundant current with current 2 like PDM.  ExtLoadReactive-127; 
    //Humidity from external SCADA
    Parameters->Humidity=ExtHumidity;
    Parameters->voltage1=voltage1;					// acm, v2
    Parameters->voltage2=voltage2;					// acm, v2
    Parameters->voltage=voltage;					// acm, v2
  } else {
#ifndef TDM
    Parameters->Temperature[0]=(char)(ReadTemperature(0)+70);
    Parameters->Temperature[1]=(char)(ReadTemperature(1)+70);
    Parameters->Temperature[2]=(char)(ReadTemperature(2)+70);
    Parameters->Temperature[3]=(char)(ReadTemperature(3)+70);
    //Humidity
    Parameters->Humidity=ReadHumidity();
    Parameters->RPN[0]=ReadLTC();
    Parameters->RPN[1]=Parameters->RPN[0];
#else
    Parameters->Temperature[0]=ExtTemperature[0];
    Parameters->Temperature[1]=ExtTemperature[1];
    Parameters->Temperature[2]=ExtTemperature[2];
    Parameters->Temperature[3]=ExtTemperature[3];
    //LTC position from external SCADA
    Parameters->RPN[0]=ExtRPN;
    Parameters->RPN[1]=ExtRPN;
    //Reactive load from external SCADA
    Parameters->CurrentR[HVSide]=ExtLoadReactive-127;
    Parameters->CurrentR[LVSide]=LoadCurrent3; // // acm, v2, replace redundant current with current 2 like PDM.  ExtLoadReactive-127; 
    //Humidity from external SCADA
    Parameters->Humidity=ExtHumidity;
    Parameters->voltage1=voltage1;				// acm, v2
    Parameters->voltage2=voltage2;				// acm, v2
    Parameters->voltage=voltage;				// acm, v2
#endif
    
    ReadActiveLoad();  // acm, remember, this path data not from SCADA/registers, which normally not done. Current channels no longer supported.
    Parameters->Current[HVSide]=LoadCurrentP;
    Parameters->Current[LVSide]=LoadCurrentP;
    
    // Saving currents for all phases even, if not calculating Zk.
    if (Setup.CalibrationCoeff.CurrentChannelOnPhase[0]) {       
      Parameters->SideToSideData[HVSide].CurrentAmpl[0]=LoadCurrentA;  
    }   
    if (Setup.CalibrationCoeff.CurrentChannelOnPhase[1]) {       
      Parameters->SideToSideData[HVSide].CurrentAmpl[1]=LoadCurrentB;  
    }   
    if (Setup.CalibrationCoeff.CurrentChannelOnPhase[2]) {       
      Parameters->SideToSideData[HVSide].CurrentAmpl[2]=LoadCurrentC;  
    }       
  }
  
#ifndef TDM
  if ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].TemperatureConfig>0)&&
      (Setup.GammaSetupInfo.GammaSideSetup[HVSide].TemperatureConfig<4))
  {
    Parameters->TrSide[HVSide].Temperature=Parameters->Temperature[Setup.GammaSetupInfo.GammaSideSetup[HVSide].TemperatureConfig-1];
    
  } else Parameters->TrSide[HVSide].Temperature=70;
  if ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].TemperatureConfig>0)&&
      (Setup.GammaSetupInfo.GammaSideSetup[LVSide].TemperatureConfig<4))
  {
    Parameters->TrSide[LVSide].Temperature=Parameters->Temperature[Setup.GammaSetupInfo.GammaSideSetup[LVSide].TemperatureConfig-1];
  } else Parameters->TrSide[LVSide].Temperature=70;
#else
  Parameters->TrSide[HVSide].Temperature=ExtTemperature[0];  //acm, 7-28
  Parameters->TrSide[LVSide].Temperature=ExtTemperature[0];  // set to this value to maintain consistancy with previous algorithm used in diagnosis.c
#endif
  
  for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
    
    WatchAmpl[TrSide]=0;
    
    AddrSig[0]=(s16*)BaseAddrSig1[TrSide];  // acm, these values periodically initialized in timer0_c_irq_handler_foradc, 4 channels of ADC converter
    AddrSig[1]=(s16*)BaseAddrSig2[TrSide];
    AddrSig[2]=(s16*)BaseAddrSig3[TrSide];
    AddrSig[3]=(s16*)BaseAddrSig4[TrSide];
    
    ClaerAllSource();
    
    //"for debugging"
    //Parameters.TrSideCount|=(1<<TrSide);
    
    if  (!(Setup.GammaSetupInfo.ReadOnSide&(1<<TrSide))) continue;  //DE 2-26-16 put back in skip if channel disabled DE 2-15-16 removed to allow msmt even if CH disabled acm, if channel not enabled skip
    
    Freq=0;  // acm, ReadSourcePhases() calculates initial Freq, using zero crossing method.
    ReadSourcePhases(&AmplA,&AmplB,&AmplC,&ABPhaseShift,&ACPhaseShift,TrSide,&Freq,0);
    
    if (TrSide==0) {
      Parameters->SideToSideData[TrSide].CurrentChannelShift=tmpComplex1.re;
    }
    
    SetPage(ParametersPage);
    // acm, AmplABC is Source Amplitude input current signal (dependent on input impedance/which jumpers cut which needs to be user input into Setup)
    AmplA=AmplA*0.7071/(Setup.GammaSetupInfo.GammaSideSetup[TrSide].InputImpedance[BushingA]/100.0)*100.0; //ag All=25mA A=24.96mA, B=25.21, C=25.59 on 3200Ohms,
    Parameters->TrSide[TrSide].SourceAmplitude[BushingA]=(unsigned int)AmplA;
    Parameters->TrSide[TrSide].SourceAmplitude[BushingA]++; //DE 2-4-16
    
    AmplB=AmplB*0.7071/(Setup.GammaSetupInfo.GammaSideSetup[TrSide].InputImpedance[BushingB]/100.0)*100.0; //ag B=0mA A=24.96mA, B=0.035mA, C=24,68mA 
    Parameters->TrSide[TrSide].SourceAmplitude[BushingB]=(unsigned int)AmplB;
    Parameters->TrSide[TrSide].SourceAmplitude[BushingB]++; //DE 2-4-16
    
    AmplC=AmplC*0.7071/(Setup.GammaSetupInfo.GammaSideSetup[TrSide].InputImpedance[BushingC]/100.0)*100.0; //ag C=0mA A=23.66mA, B=24.44mA, C=0.04mA 
    Parameters->TrSide[TrSide].SourceAmplitude[BushingC]=(unsigned int)AmplC;
    Parameters->TrSide[TrSide].SourceAmplitude[BushingC]++; //DE 2-4-16
    
    Parameters->TrSide[TrSide].SourcePhase[BushingA]=0;
    Parameters->TrSide[TrSide].SourcePhase[BushingB]=DivAndRound(ABPhaseShift,0.01,65535);
    Parameters->TrSide[TrSide].SourcePhase[BushingC]=DivAndRound(ACPhaseShift,0.01,65535);
    
    //  if(PhaseErrorFlag) goto SAVE_DATA_ANYWAY; //DE 2-15-16 commented out to allow all msmt DE 2-8-16 skip all phase msmt if ampl was too low
    
    //Calibration 
    //phase A signal on both channels 
    //defined amplitude and phase error
    
    //  SetInputChannel(TrSide,chPhaseA);
    if (RunCalibration(/*&ChAmplitudeDifference,*/&PshHV[TrSide],&Freq,&A1,1,TrSide)) {
      SetPage(ParametersPage);
      AmplA=PshHV[TrSide]*100.0;
      Parameters->TrSide[TrSide].ChPhaseShift=(int)AmplA;
      if (AmplA-Parameters->TrSide[TrSide].ChPhaseShift>0.5) Parameters->TrSide[TrSide].ChPhaseShift++;
      Parameters->TrSide[TrSide].Frequency=Freq;
    } else {
      SetPage(ParametersPage);
    }
    
    //  if(PhaseErrorFlag) goto SAVE_DATA_ANYWAY; //DE 2-15-16 skip all phase msmt if ampl was too low
    
    WatchAmpl[TrSide]=A1;
    A1*=10;
    Parameters->TrSide[TrSide].PhaseAmplitude[BushingA]=(unsigned int)(A1);
    Parameters->TrSide[TrSide].SignalPhase[BushingA]=0;
    //  if (A1-Parameters->TrSide[TrSide].PhaseAmplitude[BushingA]>=0.5) Parameters->TrSide[TrSide].PhaseAmplitude[BushingA]++;  // DE 2-2-16 removed to allow measure to continue
    Parameters->TrSide[TrSide].PhaseAmplitude[BushingA]++;  // DE 2-2-16 added to allow save measure always
    //A phase shift on the Phase A and the amplitude
    SetInputChannel(TrSide,chPhaseA);
    Parameters->TrSide[TrSide].PhaseAmplitude[BushingA]=0;
    ReadAmlPhase(&CurAmpl,&PhaseShift,PshHV[TrSide],Freq,TrSide);// finally read the ADC's. Parameters eventually saved to FRAM (Savelog, end of function)
    SetPage(ParametersPage);
    Parameters->TrSide[TrSide].PhaseAmplitude[BushingA]=CurAmpl;
    
    //Phase B shift in respect of Phase A
    SetInputChannel(TrSide,chPhaseB);
    
    Repeat=0;
  ReadLeftABAgain:
    
    SetPage(ParametersPage);
    Parameters->TrSide[TrSide].PhaseAmplitude[BushingB]=0;
    ReadAmlPhase(&CurAmpl,&PhaseShift,PshHV[TrSide],Freq,TrSide);
    SetPage(ParametersPage);
    Parameters->TrSide[TrSide].PhaseAmplitude[BushingB]=CurAmpl;
    //  Parameters->TrSide[TrSide].SignalPhase[BushingB]=(unsigned int)(PhaseShift*100);
    Parameters->TrSide[TrSide].SignalPhase[BushingB]=DivAndRound(PhaseShift,0.01,65535);
    
    if ((absfloat(PhaseShift-120)>Setup.GammaSetupInfo.AllowedPhaseDispersion)) {
      Repeat++;
      if (Repeat>MaxRepeat)
      {
        if(!(PhaseErrorFlag == (TrSide+1) )) { //DE 2-16-16 Added check if PhaseErrorFlag is set for this BHM set
          Error|=((unsigned long)1<<(erPhaseShiftSet1+TrSide));
          Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06 back  //DE 3-7-16 moved to 1919//DE 2-25-16 flag critical error to include in diagnosis
          Result=msStatusErrorABShift;}
      } else goto ReadLeftABAgain;
    }
    
    //Phase shift phase C relative to phase A
    SetInputChannel(TrSide,chPhaseC);
    Repeat=0;
  ReadLeftACAgain:
    
    SetPage(ParametersPage);
    Parameters->TrSide[TrSide].PhaseAmplitude[BushingC]=0;
    ReadAmlPhase(&CurAmpl,&PhaseShift,PshHV[TrSide],Freq,TrSide);  
    SetPage(ParametersPage);
    Parameters->TrSide[TrSide].PhaseAmplitude[BushingC]=CurAmpl;
    Parameters->TrSide[TrSide].SignalPhase[BushingC]=DivAndRound(PhaseShift,0.01,65535);
    if (absfloat(PhaseShift-240)>Setup.GammaSetupInfo.AllowedPhaseDispersion) {
      Repeat++;
      if (Repeat>MaxRepeat)
      {        
        if(!(PhaseErrorFlag == (TrSide+1) )) { //DE 2-16-16 Added check if PhaseErrorFlag is set for this BHM set
          Error|=((unsigned long)1<<(erPhaseShiftSet1+TrSide));
          Error|=((unsigned long)1<<(erSide1Critical+TrSide));//ag 2.06 back //DE 3-7-16 moved to 1919 //DE 2-25-16 flag critical error to include in diagnosis
          Result=msStatusErrorACShift;}
      } else goto ReadLeftACAgain;
    }
    
    //Checking the amplitudes (Maybe Off)
    SetPage(ParametersPage);
    ABCAmpl=0;
    if (IsInitialParam) {
      if ((float)Parameters->TrSide[TrSide].PhaseAmplitude[BushingA]<(float)Setup.InitialParameters.TrSideParam[TrSide].PhaseAmplitude[BushingA]*0.5)
        ABCAmpl|=(1<<0);
      if ((float)Parameters->TrSide[TrSide].PhaseAmplitude[BushingB]<(float)Setup.InitialParameters.TrSideParam[TrSide].PhaseAmplitude[BushingB]*0.5)
        ABCAmpl|=(1<<1);
      if ((float)Parameters->TrSide[TrSide].PhaseAmplitude[BushingC]<(float)Setup.InitialParameters.TrSideParam[TrSide].PhaseAmplitude[BushingC]*0.5)
        ABCAmpl|=(1<<2);
      // If all three signals differ by more than 50%
      // then the transformer is taken out of operation and the measurement is done.
      // (acm, set disabled, but no error recorded...)
      // ABCAmpl=0; //DE 2-16-16 removed//DE 2-4-16 added to always measure
      // One or two signal OK, and one or two others do not, the fault in the input circuits, or a very strong imbalance.
      // Check after 1 minute, fault indication.
      if ((ABCAmpl>0)&&(ABCAmpl<0x07)) {
        Result=msStatusErrorABCAmpl;
        //continue;  //DE 2-15-16 removed to allow msmt
      }
    }
    
    //DoReading:
    //Measurement
    
    //Set 0-Line for Gamma gain
    NullLine[TrSide][3]=NullLine[TrSide][4];
    //Set Gain to 1
    SetInputChannel(TrSide,chGamma);
    SetGain(4,AmplifIndex[TrSide][GammaCh]);
    Delay(50);
    
    A1=0;
    ResultComplex.re=0;
    ResultComplex.im=0;
    
    //  Setup.GammaSetupInfo.AveragingForGamma=3;
    if (Setup.GammaSetupInfo.AveragingForGamma==0) 
    {
      Setup.GammaSetupInfo.AveragingForGamma=1;
      TestSetup(); 	// acm, 2-3-12, save 1 parameter change.  Discovered during code review, otherwise testsetupCRC later will fail
    }
    if (GammaAvg>0) AvrgGamma=GammaAvg;
    else AvrgGamma=Setup.GammaSetupInfo.AveragingForGamma;
    
    //AG Start of number of acquisitions set by Setup  
    for (i=0;i<AvrgGamma;i++) {
#ifdef SinTest
      PshHV=0;
      
      ReadData(0,0,chReadAllChannels);
      SetPage0;
      for (n=0;n<UpLoadCount;n++) {
        PhaseShift=Ampl1*sin(PiValue2*n/120.0 + 0)
          +(0+i)*sin(PiValue2*n/5 + 0) +
            (0+i)*sin(PiValue2*n/400 + 0);
        *((s16 *)AddrSig[0]+n)=PhaseShift;
        if (PhaseShift-*((s16 *)AddrSig[0]+n)>=0.5) *((s16 *)AddrSig[0]+n)+=1;
      }
      
      SetPage3;
      for (n=0;n<UpLoadCount;n++) {
        PhaseShift= Ampl2*sin(PiValue2*n/120.0 + PiValue)
          +(0+i)*sin(PiValue2*n/5 + 0) +
            (0+i)*sin(PiValue2*n/400 + 0);
        *((s16 *)AddrSig[3]+n)=PhaseShift;
        if (PhaseShift-*((s16 *)AddrSig[3]+n)>=0.5) *((s16 *)AddrSig[3]+n)+=1;
        
      }
#else
#ifdef __emulator
      SetInputChannel(TrSide,chGamma);
#endif
      ReadData(0,0,chReadAllChannels,TrSide,SetNullLine,0);
#endif
      
      tmpComplex1.re=0;// acm, force recalc of amplitude
      tmpComplex1.im=0;
      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[3]),
                   UpLoadCount,
                   //                   &tmpComplex1.im,
                   &Freq,
                   &tmpComplex2.re,&tmpComplex2.im,&tmpComplex1.re,GammaPage0,GammaPage3);
      tmpComplex2.re=tmpComplex2.re*(float)ADCStep/GammaGainValue[AmplifIndex[TrSide][GammaCh]]/*ChAmplitudeDifference*/;
      tmpComplex1.re=ResultComplex.re;
      tmpComplex1.im=ResultComplex.im;
      
      vec_add(&tmpComplex1,&tmpComplex2,&ResultComplex);
    }
    //AG End of averaging by number of acquisitions
    
    if ((ResultComplex.im!=0)||(ResultComplex.re!=0))  {
      ResultComplex.im-=PshHV[TrSide];			    // acm, calibration result, subtract filter phase error?
    }
    PhaseRound(&ResultComplex.im);
    
    ResultComplex.re/=AvrgGamma;                            // acm, divide by number of measurements taken (Unn Avg setting in IHM, user changeable)
    SetPage(ParametersPage);
    
    // *************************************************************************************
    // acm, PhaseShift name improper, variable reused for amplitude normalization calculation
    // *************************************************************************************
    //AG Current Gamma reading normalized to % of averaged by three phase currents
    PhaseShift = ((float)Parameters->TrSide[TrSide].PhaseAmplitude[BushingA]+(float)Parameters->TrSide[TrSide].PhaseAmplitude[BushingB]+(float)Parameters->TrSide[TrSide].PhaseAmplitude[BushingC])/3.0/10.0;
    if (PhaseShift)
    {
      ResultComplex.re = ResultComplex.re/PhaseShift*100.0;
    }
    else
    {
      ResultComplex.re = 0;
    }
    
    //2.10 ag fixing end case below when only one acquisition. Only do, if more than one acquisition.
    if (AvrgGamma > 1){
      vec_sub(&ResultComplex,&tmpComplex1,&tmpComplex2);  // acm, subtract last measurement from averaged (not ideal, bad reads should be checked above)
      
      if (tmpComplex2.re > 0.5 && noise_event_counter <1 && !(Error & (unsigned long)1<<(erSet1Off+TrSide)))  // if vec_sub(), i.e. difference of average and last measurement magnitude is 50% larger than last AND we've seen less than 3 previous noise events, redo measurement.  3 noise events in a row will not be tossed, and will get saved below.
      {
        noise_event_counter++;  // (not cleared until when StartMeasure called next time)
        
        goto redo_measurement;  // redo measurement from scratch
      }
    }
    
    // acm, v2.00, new averaging algorithm (default same as pre v2.00 measurement interval<=1hr case)
    //note, X defined above as (float)Setup.GammaSetupInfo.AveragingForGamma_X/100
    if (Avgs[TrSide]) 						// if Watch() detects Red Alarm Unn, it still sets Avgs=0, resetting Unn_avg
    {
      tmpComplex1.re=(1-X)*AvgGammaAmpl[TrSide];		// old_avg, .re==phasor magnitude (Polar coordinate)
      tmpComplex1.im=AvgGammaPhase[TrSide];			// .im==phasor phase angle (Polar coordinate)
      tmpComplex2.re=X*ResultComplex.re;			// X*Unn
      tmpComplex2.im=ResultComplex.im;				// X==0, avg freeze, kinda dumb
      vec_add(&tmpComplex1,&tmpComplex2,&ResultComplex);	// X==.5, 2pt avg, avg changes more quickly
      PhaseRound(&ResultComplex.im);				// X==.33, old alg
    } 								// X==1, avg off, becomes last measured Unn
    
    AvgGammaAmpl[TrSide]=ResultComplex.re;
    AvgGammaPhase[TrSide]=ResultComplex.im;
    if (!Avgs[TrSide]) Avgs[TrSide]=1;				// restart averaging (e.g. following Red Alarm detect)
    
    //SAVE_DATA_ANYWAY: //DE 2-8-16        
    
    SetPage(ParametersPage);
    
    if (Error & (unsigned long)1<<(erSet1Off+TrSide))
    {
      Parameters->TrSide[TrSide].GammaPhase=0;                  // store in s16 * 100
      Parameters->TrSide[TrSide].Gamma=0;
    }
    else
    {
      Parameters->TrSide[TrSide].GammaPhase=DivAndRound(ResultComplex.im,0.01,65535); // store in s16 * 100
      Parameters->TrSide[TrSide].Gamma=DivAndRound(ResultComplex.re,0.01,65535);
    }
    
#ifdef __emulator
    if (TrSide) Parameters->TrSide[TrSide].Gamma=482;
    else Parameters->TrSide[TrSide].Gamma=520;
#endif
    
    Parameters->TrSideCount|=(1<<TrSide);	// acm, side passed all cal tests, measurement valid, indicate this by setting TrSide bit 0 or 1
    
#ifndef __arm
    //Save source Set 1
    if (TrSide==0) {
      for (i=0;i<UpLoadCount;i++) {
        SetPage(GammaPage0);
        ii=AddrSig[0][i];
        SetPage(GammaPage4);
        AddrSig[0][i]=ii;
        SetPage(GammaPage3);
        ii=AddrSig[0][i];
        SetPage(GammaPage7);
        AddrSig[0][i]=ii;
      }
    }
#endif
    
  }//TrSide. End measurement on one side
  
  // acm, v2.00, include AveragingForGamma,
  Parameters->AveragingForGamma_X=X;
  Parameters->AveragingForGamma=Setup.GammaSetupInfo.AveragingForGamma;
  
  if (!TestSetupCRC()) goto redo_measurement2; // acm, 2-24-09, if miscompare due to rare noise event redo measurement (before results are saved..)
  
  SetPage(ParametersPage);
  if (Parameters->TrSideCount==0) //ag2-12-16. Only gets here, if both sets are disabled. Changing to errStop. New meaning is ether stopped or no sets enabled
  {
    Error|=((unsigned long)1<<erStop);
    
    Result=msStatusErrorABCAmpl;
    goto ErrExit;
  }
  // acm, 9-11-13, this path now taken, replicates log messages...
  //  else 
  //if (Parameters->TrSideCount!=0x11) SaveLogIfNewError(OldError,Error); // acm, 9-5-12, capture errors to log if one set or other experience (calibration) error.  This case, still store measurement value.
  
  SetPage(ParametersPage);  // acm, EncodeDate() returns # days from year 2000 (correcting for leap years).  EncodeTime returns # seconds of current day, DoPackDateTime() returns total # seconds since 2000.
  DiagDate = DoPackDateTime(EncodeDate(Parameters->Year,Parameters->Month,Parameters->Day),
                            EncodeTime(Parameters->Hour,Parameters->Min,  0));
  
  GammaTrend(DiagDate,KTPhase,KT,Trend,MeasurementsInArchive,AddOnLine);
  
  SetPage(ParametersPage);
  //FullStatus=stUnknown;  // acm, v1.79, This maps to MODBUS reg 4, can't change until we know we are not re-reading
  for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
    Parameters->TrSide[TrSide].Trend=Trend[TrSide];
    Parameters->TrSide[TrSide].KTPhase=KTPhase[TrSide];
    Parameters->TrSide[TrSide].KT=KT[TrSide];
    
    //Parameters->TrSide[TrSide].AlarmStatus=stUnknown;   // acm, v1.79, don't clobber MODBUS reg 5/6 until after
    //if  (Setup.GammaSetupInfo.ReadOnSide&(1<<TrSide)==0) continue;   //DE 2-15-16 removed to allow msmt
    tmpStatus=GetGammaStatus(Parameters->TrSide[TrSide].Gamma*0.01,TrSide,OldGammaStatus[TrSide],0);
    GetFullStatus(tmpStatus,Parameters->TrSide[TrSide].Trend,Parameters->TrSide[TrSide].KT,(char *)&FullStatus_tmp,&AlarmStatus_tmp[TrSide],TrSide);
    if (tmpStatus>CurrentGammaStatus) CurrentGammaStatus=tmpStatus;// acm, CurrentGammaStatus = larger of set 1 and set 2 gamma
  }
  
  if ((CurrentGammaStatus>GammaStatus)&&		// acm, GammaStatus is previous measurment Unn status?
      (CurrentGammaStatus==stAlarm)&&
        (MeasurementResult!=msStatusReRead)  // acm, MeasurementResult=StarMeasure(), or Result previous measurement, set in Main()
          )
  {
    //ReRead
    // acm, 4-29, set FullStatus unknown until next re-read done.  This causes MODBUS holding register not to get set, a problem as DR was reaing this and seeing warning/alarm too early.
    if (Setup.GammaSetupInfo.ReReadOnAlarm) {Result=msStatusReRead; }//FullStatus=stUnknown;} // acm, v1.79 don't clobber status yet.  4-29 "fix" incorrect.
  }
  
  //	if (Result!=msStatusReRead) FullStatus = FullStatus_tmp; // acm, v1.79, now report results to world here, as measurement finally done (perhaps after a re-read)
  
  SetPage(ParametersPage);
  for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
    if ((FullStatus_tmp>=stWarning)&& (OldAlarmStatus[TrSide]!=AlarmStatus_tmp[TrSide])&&(AlarmStatus_tmp[TrSide]>0)) {  //ag 3-8-16 removed evnt from alarm to none
      NeedSaveLog|= (1<<TrSide);                                              //ag 3-8-16 encode TrSide to save log
      OldAlarmStatus[TrSide]=~OldAlarmStatus[TrSide];
      OldAlarmStatus[TrSide]=OldAlarmStatus[TrSide]&AlarmStatus_tmp[TrSide];  // acm, reference AlarmStatus_tmp
    }
  }
  
  if (Result!=msStatusReRead)  					// acm, as I understand, both FullStatus and GammStatus refer to last valid measurment
  {
    FullStatus = FullStatus_tmp; 				// acm, v1.79, now report results to world here, as measurement finally done (perhaps after a re-read)
    GammaStatus=CurrentGammaStatus; 				// acm, note - reset old status value here as new status processing complete.
    Parameters->TrSide[0].AlarmStatus = AlarmStatus_tmp[0];  	// acm, same logic applies to MODBUS register 5/6.  Question - at top, OldAlarmStatus init with previous measurement, this implementation loads with 2nd previous measurement, ok?
    Parameters->TrSide[1].AlarmStatus = AlarmStatus_tmp[1];
  }
  
  if ((Save==svSaveOnlyRED)&&(GammaStatus<stAlarm)) goto NoAlarmExit;
  
  if ((IsInitialParam==1)&&
      ((Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved!=1)&&(Setup.GammaSetupInfo.GammaSideSetup[HVSide].STABLESaved!=2))&&
        ((Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved!=1)&&(Setup.GammaSetupInfo.GammaSideSetup[LVSide].STABLESaved!=2))
          )
  {
    RunBaseLine(DiagDate,CurDiagRegime);
  }
  
  SetPage(ParametersPage);
  DiagResult=DoSumDiag(DiagDate,Diagnosis,/*NORMALDiagRegime*/STABLEDiagRegime,CurDiagRegime,MeasurementsInArchive,CurDiagType,ParametersPage,AddOnLine);
  for (TrSide=0;TrSide<MaxTransformerSide;TrSide++)
  {
    Parameters->TrSide[TrSide].DefectCode=Setup.InitialParameters.TrSideParam[TrSide].DefectCode;  //acm, v1.79, store balancing defect code with data for offline analysis
    for (i=0;i<MaxBushingCountOnSide;i++) 
    {
      Parameters->TrSide[TrSide].C[i]=Diagnosis->TgParam[TrSide].C[i];
      Parameters->TrSide[TrSide].tg[i]=Diagnosis->TgParam[TrSide].tg[i];
    }
  }
  
  SetPage(ParametersPage);
  if (Setup.ZkSetupInfo.CalcZkOnSide) {
    StartCrossMeasure(Parameters,Freq,PshHV[LVSide]);
    DoCalcAvgZk(DiagDate,&Parameters->ZkTable,
                (unsigned int *)&ZkPercent,
                MeasurementsInArchive,             //Number of measurements or 0 if unknown
                AddOnLine);
    Parameters->SideToSideData[HVSide].ZkPercent[BushingA]=ZkPercent[BushingA];
    Parameters->SideToSideData[HVSide].ZkPercent[BushingB]=ZkPercent[BushingB];
    Parameters->SideToSideData[HVSide].ZkPercent[BushingC]=ZkPercent[BushingC];
  }
  
  //SAVE_DATA_ANYWAY:
  
  ////************* DE 3-30-16 added Fast record write *********************
  //   if( FastWriteCount )
  //   {
  // _TDateTime *DTtemp;
  // _TDate NewDate;
  // _TTime NewTime;
  // unsigned int *NewYear, *NewMonth, *NewDay, *NewHour, *NewMinute, *NewSecond;
  //// StDateTime NewTime;
  //     ClearLCD();
  //     SetFont(Font8x8);
  //     OutString(30,8,XXTestStr2);
  //     Redraw();
  ////     SetDeviceBusy();
  //     
  //     for(i=0;i<FastWriteCount;i++)
  //     {
  //       if(i%10==0)
  //       {
  //         OutInt(0,0,FastWriteCount-i,6);
  //         Redraw();
  //       }
  //       SaveCurrentMeasurement();
  //       CurMeasurement++; // write results to Flash acm-CurMeasurement unused
  //     }
  //     FastWriteCount = 0;
  //       SET_TIME_FROMBUF();
  //       Delay(1);
  //   }
  ////************* DE 3-30-16 end added Fast record write *********************  
  
  //2.06 ag moved up here to save correct errors with reading //2.06 ag added check for ch ON//DE 8-8-16 fixed XFMR OFF
  if (Setup.GammaSetupInfo.ReadOnSide==3 && Error&((ErrorType)1<<erSet1Off) && Error&((ErrorType)1<<erSet2Off)) Error|=((ErrorType)1<<erUnitOff);
  else if (Setup.GammaSetupInfo.ReadOnSide==1 && Error&((ErrorType)1<<erSet1Off)) Error|=((ErrorType)1<<erUnitOff);
  else if (Setup.GammaSetupInfo.ReadOnSide==2 && Error&((ErrorType)1<<erSet2Off)) Error|=((ErrorType)1<<erUnitOff);
  
  if ((Error&(ErrorType)1<<erUnitOff)) Error&=~(((ErrorType)1<<erSet1Off)|((ErrorType)1<<erSet2Off)); //2.06 ag clear setoff errors if unit off
  
  if ((int)Save) {                 // acm, why are we not saving measured data during balancing?  2-10-12 still unsure, but init parameters saved in Setup, rest meaningless.
    if (SaveCurrentMeasurement())
    {
      CurMeasurement++;           // write results to Flash acm-CurMeasurement unused
    }; 
    
    //Set Avg count to 1
    for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
      if (Avgs[TrSide]) {
        AvgGammaAmpl[TrSide]/=Avgs[TrSide];		// acm, why are we changing avggamma after saving data when average was already calculated above?
        Avgs[TrSide]=1;
      } else {
        AvgGammaAmpl[TrSide]=0;
      }  
    }    
    
    if (NeedSaveLog) {
      for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
        if (NeedSaveLog&(1<<TrSide))                  //ag 3-8-16 decode TrSide to save log
        {
          if ((OldAlarmStatus[TrSide]&(1<<4))&&(GammaStatus==stWarning)) SaveLog(lcGammaWarning);
          if ((OldAlarmStatus[TrSide]&(1<<0))&&(GammaStatus==stAlarm)) SaveLog(lcGammaAlarmEvent);
          if (OldAlarmStatus[TrSide]&(1<<1)) SaveLog(lcTrendAlarmEvent);
          if (OldAlarmStatus[TrSide]&(1<<2)) SaveLog(lcTKAlarmEvent);
        }
      }
    }
  }
  
NoAlarmExit:
  if (Result!=msStatusReRead) OutGammaStatus(FullStatus, 0);  // acm, v1.79, if reread, don't drive LED/relay explicitly.  Otherwise, pass FullStatus, not _tmp version
  // acm, observation, Result==reread alternating every measurement after 1st alarm encountered...
  
ErrExit:
  
  SaveLogIfNewError(OldError,Error); // ag 3-8-16 moved here to have all errors set
  
  SetInputChannel(HVSide,chGamma);  // acm, what the heck?  Guess, setup for 5min WatchFunc() that runs in foreground loop, but this still appears redundant.
  if (Result<0) {					// acm, Result<0 is error set above somewhere
    
    ClaerAllSource();				// acm, clear A/D array
    
    for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) Avgs[TrSide]=0;
  }
#ifdef __arm
  ClaerAllSource();
#endif
  
  //for debug
  //     CurMeasurement++;
  
  
  return Result;
}



void DoSingle(void)
{
  
  extern
#ifdef __arm
    unsigned int
#else
      char
#endif
        ReadSingle;
  
  ReadSingle=1;
}

void ClearInitParam(char Side)
{ char CurSide,MaxSide;

if (Side==AllSides)  {
  
  //Date of measurement
  Setup.InitialParameters.Year=
    Setup.InitialParameters.Month=
      //Time of measurement
      Setup.InitialParameters.Day=
        Setup.InitialParameters.Hour=
          Setup.InitialParameters.Min=0;
  
  Side=0;
  MaxSide=MaxTransformerSide;
} else { MaxSide=Side; }
//Parameters for each set of bushings
//Phase shift between measuring channels (degrees)*100
for (CurSide=Side;CurSide<MaxSide;CurSide++) {
  Setup.InitialParameters.TrSideParam[CurSide].ChPhaseShift=
    //Phase signal amplitude in mV
    Setup.InitialParameters.TrSideParam[CurSide].PhaseAmplitude[BushingA]=
      Setup.InitialParameters.TrSideParam[CurSide].PhaseAmplitude[BushingB]=
        Setup.InitialParameters.TrSideParam[CurSide].PhaseAmplitude[BushingC]=
          Setup.InitialParameters.TrSideParam[CurSide].SourceAmplitude[BushingA]=
            Setup.InitialParameters.TrSideParam[CurSide].SourceAmplitude[BushingB]=
              Setup.InitialParameters.TrSideParam[CurSide].SourceAmplitude[BushingC]=
                //temperature (-70 - +185 0C) with step 1 0C
                Setup.InitialParameters.TrSideParam[CurSide].Temperature=
                  //Gamma, %*0.01
                  Setup.InitialParameters.TrSideParam[CurSide].GammaAmplitude=
                    //Phase  ,degrees, Byte*0.01
                    Setup.InitialParameters.TrSideParam[CurSide].GammaPhase=
                      //Initial Gamma Temperature Coefficient, *0.002
                      Setup.InitialParameters.TrSideParam[CurSide].KT=
                        //Defect Code at Balancing
                        Setup.InitialParameters.TrSideParam[CurSide].DefectCode=
                          Setup.InitialParameters.TrSideParam[CurSide].KTPhase=
                            Setup.InitialParameters.TrSideParam[CurSide].SignalPhase[BushingA]=
                              Setup.InitialParameters.TrSideParam[CurSide].SignalPhase[BushingB]=
                                Setup.InitialParameters.TrSideParam[CurSide].SignalPhase[BushingC]=
                                  Setup.InitialParameters.TrSideParam[CurSide].SourcePhase[BushingA]=
                                    Setup.InitialParameters.TrSideParam[CurSide].SourcePhase[BushingB]=
                                      Setup.InitialParameters.TrSideParam[CurSide].SourcePhase[BushingC]=
                                        0;
  //  if (Error & AnyDataError) Error|=((unsigned long)1<<(erSide1Critical+Side)); //DE 3-7-16 comment out flag critical errors to include in diagnosis
}

IsInitialParam=0;

#ifdef __arm
SaveSetup();
#endif
}
