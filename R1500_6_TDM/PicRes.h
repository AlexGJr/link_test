#ifndef _PicRes_h
#define _PicRes_h

#include "Defs.h"

#define szReadLR   42
extern ROM char ReadLR[szReadLR];
#define szDownArr   13
extern ROM char DownArr[szDownArr];
#define szRArr    9
extern ROM char RArr[szRArr];
#define szClockIco   50
extern ROM char ClockIco[szClockIco];
#define szDateIco   50
extern ROM char DateIco[szDateIco];
#define szBalIco   48
extern ROM char BalIco[szBalIco];
#define szBRateIco   50
extern ROM char BRateIco[szBRateIco];
#define szScheduleIco   36
extern ROM char ScheduleIco[szScheduleIco];
#define szCompIco   60
extern ROM char CompIco[szCompIco];
#define szDeviceNumIco   60
extern ROM char DeviceNumIco[szDeviceNumIco];
#define szStoppedIco   50
extern ROM char StoppedIco[szStoppedIco];
#define szFillPauseIco   50
extern ROM char FillPauseIco[szFillPauseIco];
#define szShowTimeIco   50
extern ROM char ShowTimeIco[szShowTimeIco];
#define szAvgIco   66
extern ROM char AvgIco[szAvgIco];
#define szRelayIco   48
extern ROM char RelayIco[szRelayIco];
#define szCheckOn   20
extern ROM char CheckOn[szCheckOn];
#define szCheckOff   20
extern ROM char CheckOff[szCheckOff];
#define szLogoSM   60
extern ROM char LogoSM[szLogoSM];
#define szHysterIco   48
extern ROM char HysterIco[szHysterIco];
#define szReReadIco   48
extern ROM char ReReadIco[szReReadIco];
#define szTrendDaysIco   48
extern ROM char TrendDaysIco[szTrendDaysIco];
#define szPhaseDispIco   48
extern ROM char PhaseDispIco[szPhaseDispIco];
#define szAlarmEnIco   48
extern ROM char AlarmEnIco[szAlarmEnIco];
#define szThreshldsIco   48
extern ROM char ThreshldsIco[szThreshldsIco];
#define szRatedVoltIco   48
extern ROM char RatedVoltIco[szRatedVoltIco];
#define szBushCIco   48
extern ROM char BushCIco[szBushCIco];
#define szBushTgIco   48
extern ROM char BushTgIco[szBushTgIco];
#define szBushTgTempIco   48
extern ROM char BushTgTempIco[szBushTgTempIco];
#define szDBCIco   48
extern ROM char DBCIco[szDBCIco];
#define szInpImpIco   50
extern ROM char InpImpIco[szInpImpIco];
#define szSingleIco   50
extern ROM char SingleIco[szSingleIco];
#define szClearIco   50
extern ROM char ClearIco[szClearIco];
#define szClearAllIco   50
extern ROM char ClearAllIco[szClearAllIco];
#define szBaseLineIco   50
extern ROM char BaseLineIco[szBaseLineIco];
#define szBaseLineDaysIco   50
extern ROM char BaseLineDaysIco[szBaseLineDaysIco];
#define szZkIco   50
extern ROM char ZkIco[szZkIco];
#define szLinkIco   10
extern ROM char LinkIco[szLinkIco];
#define szTClbrIco   48
extern ROM char TClbrIco[szTClbrIco];
#define szHClbrIco   48
extern ROM char HClbrIco[szHClbrIco];
#define szCurrentIco   50
extern ROM char CurrentIco[szCurrentIco];
#define szABalIco   48
extern ROM char ABalIco[szABalIco];

#endif
