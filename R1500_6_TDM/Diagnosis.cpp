#ifndef __arm
#include "Protocol.h"
#endif
#include "SysUtil.h"
#include "Archive.h"
#include "Trans.h"
#include "Diagnosis.h"
#include "Setup.h"
#include "MeasurementDefs.h"
#include "GammaMeasurement.h"
#include "Utils.h"
#include "RAM.h"
#include "DateTime.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "Complex.h"
#include "Error.h"

/*
#include "Graph.h"
#include "LCD.h"
#include "KeyBoard.h"
#include "RTC.h"
*/

//Gamma Temperature Coefficient, *0.002
char KT[MaxTransformerSide]={0,0};
//Gamma Temperature Coefficient Phase, *1.5
char KTPhase[MaxTransformerSide]={0,0};

//Rate of Imbalance change (times per year *0.2  //acm, 4-27 change from .2 to 1
char Trend[MaxTransformerSide]={0,0};

extern float Gamma_for_trend_newwarning_check[MaxTransformerSide]; // acm, 4-28, get from GammaMeasurement.cpp/StartMeasure

//Golubev's Pi
//#define Pi (3.14159265358979/180)
#define Pi 0.017453292519943295769236907684886

//New Diag!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11

//char GetAllow(unsigned int Setting, unsigned int Level)    //DE 3-1-16 add threshold allow errors check function extern void TestSetup(); // acm, do this so Understand resolves
//{
//        unsigned int CurrentSettings=0;
//        char LoadGreaterThanSetting=0;
//        char LoadSmallerThanSetting=0;
//        char SetThresholdSetting=0;
//        char AllowAlarms=0;
//
//CurrentSettings = Setup.SetupInfo.LoadCurrentThresholdSettings; //use only least 4 bits 
//CurrentSettings = CurrentSettings & 0x0F;
//CurrentSettings = CurrentSettings >> (Side*2);
//CurrentSettings = CurrentSettings & 0x03;        //use only least 2 bits
//
//if(ExtLoadActive > Setup.SetupInfo.LoadCurrentThresholdLevel) LoadGreaterThanSetting = 1;
//if(ExtLoadActive < Setup.SetupInfo.LoadCurrentThresholdLevel) LoadSmallerThanSetting = 1;
//
//if(CurrentSettings == 0) SetThresholdSetting = 0;
//if(CurrentSettings == 1) SetThresholdSetting = 1;
//if(CurrentSettings == 3) SetThresholdSetting = 3;
//
//if(SetThresholdSetting == 0) AllowAlarms =1;
//if((SetThresholdSetting == 1) && LoadSmallerThanSetting) AllowAlarms = 1;
//if((SetThresholdSetting == 3) && LoadGreaterThanSetting) AllowAlarms = 1;
//return AllowAlarms  
//}

static void FindMeasurements(_TDateTime StartDate,_TDateTime EndDate,unsigned int *StartNum,unsigned int *EndNum,unsigned int MinDaysNeeded,unsigned int Num)
{ unsigned int i,cnt1;
_TDateTime CurDate;//,MEndDate;

#ifdef __arm
struct stInsulationParameters *Param=TempParam;
#else
#ifdef __emulator
struct stInsulationParameters TParam;
struct stInsulationParameters *Param=&TParam;
#else
struct stInsulationParameters *Param =(struct stInsulationParameters *) (0xFFFF-szInsulationParameters);
#endif
#endif

cnt1=0;
*StartNum=1;
*EndNum=0;
if (Num>0) i=Num;
else i=MeasurementsInArchive;
while (i>0) {
  LoadArchDate(Param,MeasurementNumToIndex(i),TempPage);
  SetPage(TempPage);
  CurDate=DoPackDateTime(EncodeDate(Param->Year,
                                    Param->Month,
                                    Param->Day),
  EncodeTime(Param->Hour,
             Param->Min,
             0));
  if (cnt1) {
    if (CurDate<StartDate) {
      *StartNum=i+1;
      break;
    } else cnt1++;
  } else {
    if (CurDate<=EndDate) {
      *EndNum=i;
      //             MEndDate=CurDate;
      cnt1++;
    }
  }
  i--;
}
//  LoadArchDate(Param,MeasurementNumToIndex(*StartNum),TempPage);
LoadArchDate(Param,MeasurementNumToIndex(1),TempPage);
SetPage(TempPage);
CurDate=DoPackDateTime(EncodeDate(Param->Year,
                                  Param->Month,
                                  Param->Day),
EncodeTime(Param->Hour,
           Param->Min,
           0));
//  DoUnPackDateTime(CurDate,&Date1,&Time);
//  DoUnPackDateTime(MEndDate,&Date2,&Time);
//  if (Date2-Date1+1<MinDaysNeeded) {
if (DateTimeToDate(EndDate)-DateTimeToDate(CurDate)+1<MinDaysNeeded) {
  *StartNum = *EndNum = cnt1 = 0;
}

if (cnt1 > (MaxBaseLineMeasurements-1)) {						//acm, 3-24-10 attempt to fix off by one error that for reason unkown zero's out trend until time advances to next day
  *StartNum = *EndNum - (MaxBaseLineMeasurements-1) +1;
}
}

//#pragma optimize=s 2  //DE 2-26-16 turn off optimization so vars are resolved
char DoSumDiag(_TDateTime DiagDate,                    //End Data for Diagnosis
               struct stDiagnosis *DiagData,          //Results of diagnostics
               char FlagBase,                         //1 - on Base Line data, 0 - on offline data
               char InputData,                        //1-  Input, 0-Gamma
               unsigned int MesurementNum,            //����� ������ ��� 0 ���� �� ��������
               char DoOnLineDiag,                     //1- On-Line diagnosis, 0-Sum diagnosis
               char DiagDataPage,                     //
               char UseOnLineData                     //Add On-Line data from Parameters structure
                 )
{
  
#define av_d Setup.GammaSetupInfo.MinDiagGamma
  
  char av_bbb; //acm, 4-29, used to pass av_d or 5 days for new sliding window algorithm, below.
  
  float init0[MaxBushingCountOnSide*2+1];       //C0a, C0b, C0c, Tg0a, Tg0b, Tg0c, Temp0 at off line test with temp correction
  float par1[MaxBushingCountOnSide*2];          //current calculated parameters Ia, Ib, Ic, Fia, Fib, Fic, Temperature, Gamma, Gamma Phase     averaged for calculations over av_c
  float par0[MaxBushingCountOnSide*2];          //parameters I0a, I0b, I0c, Fi0a, Fi0b, Fi0c, Temperature    from learn averaged over av_l
  _TDateTime DateStart;  //date for start diagnostics
  
  unsigned int i;
  unsigned int ii
#ifdef __OldDiag
    ,l1,l2,ObxodFlag=0
#endif
#ifndef __arm
      ,Page=GetPage()
#endif
        ;
  unsigned int StartData,EndData,Count,AvgCount
#ifdef __OldDiag
    ,CountInArch
#endif
      ;
  float ISum,I0Sum,dFi;
  float dl1,dl2;
#ifdef __OldDiag
  float kuku,ParToCompare,dl_add;
#endif
  
  unsigned int Side;
#ifdef __emulator
  struct stInsulationParameters TParam;
  struct stInsulationParameters *Param=&TParam;
#else
#ifdef __arm
  struct stInsulationParameters *Param =TempParam;
#else
  struct stInsulationParameters *Param =(struct stInsulationParameters *) (0xFFFF-szInsulationParameters);
#endif
#endif
  
  struct TComp AvgGamma, //Average Imbalance value for time window
  CurGamma, //Current Imbalance value for time window
  ResGamma, //Temporary Imbalance value in calculations
  DiagGamma;//Average Imblance value without large deviations - for diagnostics
  
#define MaxCorrectionGammaValue 2.0  //acm 3-5, AG request change from 5 to 2
  float CoeffOnGamma=1.0;
#ifndef __OldDiag
  int Zone;
  int DeltaC;
#endif
  
  
  SetPage(TempPage);
  for (Side=0;Side<MaxTransformerSide;Side++) {
    
    SetPage(DiagDataPage);
    for (i=0;i<MaxBushingCountOnSide;i++) {
      (*DiagData).TgParam[Side].tg[i]=Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[i];
      (*DiagData).TgParam[Side].C[i] =Setup.GammaSetupInfo.GammaSideSetup[Side].C0[i];
    }
    
#ifndef __OldDiag
    // If not Initial data - continue
    if (!IsInitialParam) 
    {  
      SetPage(Page); 
      continue; 
    }
    if (!(Parameters->TrSideCount&(1<<Side))) 
    {  
      SetPage(Page); 
      continue; 
    }
#endif
    //************* ag 6-27-17 Never called with DoOnLineDiag been set
    if (DoOnLineDiag) {
      // If not Initial data - continue
      if (!IsInitialParam) 
      {  
        SetPage(Page); 
        continue; 
      }
      // If not data on this side - continue
      if (UseOnLineData) {
        if (!(Parameters->TrSideCount & (1<<Side)) || (ErrorType)(Error & (1<<(erSide1Critical+Side)))) //ag 2.10 added check, if current data is with critical error
        {  
          SetPage(Page); 
          continue; 
        }
      } 
      else 
      {
        if (MesurementNum) //ag 6-27-17 comment. Believe that this is to redo diagnostics for the record MeasurementNum and back to the history from this record
        {
          SetPage(TempPage);
          LoadData(Param,MeasurementNumToIndex(MesurementNum),TempPage);
          if (!(Param->TrSideCount&(1<<Side)) || (((int)Param->Error2)&(1<<(erSide1Critical-16+Side)))) //ag 2.10 added check, if measurement requested data is with critical error
          {  
            SetPage(Page); 
            continue; 
          }
        } 
        else 
        {  
          SetPage(Page); 
          continue; 
        }
      }
      
      FlagBase=0;
      
      for (ii=0;ii<MaxBushingCountOnSide;ii++) {
        init0[ii]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].C0[ii]/10.0;
        init0[ii+MaxBushingCountOnSide]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[ii]/100.0;
        if (InputData) {
          par0[ii]=(float)Setup.InitialParameters.TrSideParam[Side].SourceAmplitude[ii]/100.0;
          par0[ii+MaxBushingCountOnSide]=Setup.InitialParameters.TrSideParam[Side].SourcePhase[ii]/100.0;
        } else {
          par0[ii]=(float)Setup.InitialParameters.TrSideParam[Side].PhaseAmplitude[ii]/10.0;
          par0[ii+MaxBushingCountOnSide]=Setup.InitialParameters.TrSideParam[Side].SignalPhase[ii]/100.0;
        }
      }
      init0[6] = -70+Setup.GammaSetupInfo.GammaSideSetup[Side].Temperature0;
      
      if (UseOnLineData) {
        for (ii=0;ii<MaxBushingCountOnSide;ii++) {
          SetPage(ParametersPage);
          if (InputData) {
            dl1=(float)Parameters->TrSide[Side].SourceAmplitude[ii]/100.0;
            dl2=(float)Parameters->TrSide[Side].SourcePhase[ii]/100.0;
            par1[ii]=dl1;
            par1[ii+MaxBushingCountOnSide]=dl2;
          } else {
            dl1=(float)Parameters->TrSide[Side].PhaseAmplitude[ii]/10.0;
            dl2=(float)Parameters->TrSide[Side].SignalPhase[ii]/100.0;
            par1[ii]=dl1;
            par1[ii+MaxBushingCountOnSide]=dl2;
          }
        }
        DiagGamma.im=(float)Parameters->TrSide[Side].Gamma/100.0;
        DiagGamma.re=(float)Parameters->TrSide[Side].GammaPhase/100.0;
      } else {
        SetPage(TempPage);
        LoadData(Param,MeasurementNumToIndex(MesurementNum),TempPage);
        for (ii=0;ii<MaxBushingCountOnSide;ii++) {
          SetPage(TempPage);
          if (InputData) {
            dl1=(float)Param->TrSide[Side].SourceAmplitude[ii]/100.0;
            dl2=(float)Param->TrSide[Side].SourcePhase[ii]/100.0;
            par1[ii]=dl1;
            par1[ii+MaxBushingCountOnSide]=dl2;
          } else {
            dl1=(float)Param->TrSide[Side].PhaseAmplitude[ii]/10.0;
            dl2=(float)Param->TrSide[Side].SignalPhase[ii]/100.0;
            par1[ii]=dl1;
            par1[ii+MaxBushingCountOnSide]=dl2;
          }
        }
        DiagGamma.im=(float)Param->TrSide[Side].Gamma/100.0;
        DiagGamma.re=(float)Param->TrSide[Side].GammaPhase/100.0;
      }
      
    } 
    //**************** ag 6-27-17 Above never called. See similar mark above
    else 
    {
#ifdef __OldDiag
      SetPage(DiagDataPage);
      if (((Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved==1)||(Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved==2))&&
          (Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate>0)&&
            ((unsigned long)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate!=0xFFFFFFFF)) {
              for (i=0;i<MaxBushingCountOnSide;i++) {
                (*DiagData).TgParam[Side].tg[i]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[i];
                (*DiagData).TgParam[Side].C[i] =Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[i];
              }
            } else {
              for (i=0;i<MaxBushingCountOnSide;i++) {
                (*DiagData).TgParam[Side].tg[i]=Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[i];
                (*DiagData).TgParam[Side].C[i] =Setup.GammaSetupInfo.GammaSideSetup[Side].C0[i];
              }
            }
      
      //''Reading Initial data C0 and Tg0
      if ((FlagBase)&&
          ((Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved==1)||(Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved==2))&&
            ((Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate>0)&&
             ((unsigned long)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate!=0xFFFFFFFF))
              ) {
                //BaseLine
                for (ii=0;ii<MaxBushingCountOnSide;ii++) {
                  init0[ii]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[ii]/10.0;
                  init0[ii+MaxBushingCountOnSide]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[ii]/100.0;
                }
                init0[6] = -70+Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETemperature;
                if (InputData) {
                  for (ii=0;ii<MaxBushingCountOnSide;ii++) {
                    par0[ii]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].StableSourceAmplitude[ii]/10.0;
                    par0[ii+MaxBushingCountOnSide]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].StableSourcePhase[ii]/100.0;
                  }
                } else {
                  for (ii=0;ii<MaxBushingCountOnSide;ii++) {
                    par0[ii]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].StablePhaseAmplitude[ii]/10.0;
                    par0[ii+MaxBushingCountOnSide]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].StableSignalPhase[ii]/100.0;
                  }
                }
              } else {
                for (ii=0;ii<MaxBushingCountOnSide;ii++) {
                  init0[ii]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].C0[ii]/10.0;
                  init0[ii+MaxBushingCountOnSide]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[ii]/100.0;
                  par0[ii]=0;
                  par0[ii+MaxBushingCountOnSide]=0;
                }
                init0[6] = -70+Setup.GammaSetupInfo.GammaSideSetup[Side].Temperature0;
                FlagBase=0;
              }
#else
      for (ii=0;ii<MaxBushingCountOnSide;ii++) {
        SetPage(DiagDataPage);
        (*DiagData).TgParam[Side].tg[ii]=Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[ii];
        (*DiagData).TgParam[Side].C[ii] =Setup.GammaSetupInfo.GammaSideSetup[Side].C0[ii];
        SetPage(TempPage);
        init0[ii]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].C0[ii]/10.0;
        init0[ii+MaxBushingCountOnSide]=(float)Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[ii]/100.0;
        if (InputData) {
          par0[ii]=(float)Setup.InitialParameters.TrSideParam[Side].SourceAmplitude[ii]/100.0;
          par0[ii+MaxBushingCountOnSide]=(float)Setup.InitialParameters.TrSideParam[Side].SourcePhase[ii]/100.0;
        } else {
          par0[ii]=(float)Setup.InitialParameters.TrSideParam[Side].PhaseAmplitude[ii]/10.0;
          par0[ii+MaxBushingCountOnSide]=(float)Setup.InitialParameters.TrSideParam[Side].SignalPhase[ii]/100.0;
        }
      }
      init0[6] = -70+Setup.GammaSetupInfo.GammaSideSetup[Side].Temperature0;
      FlagBase=0;
#endif
      
      // acm, 4-29, reduce "sliding window" to 5 days if Unn increase > 2% to speed capacitance and power factor changes seen by user
      if (Gamma_for_trend_newwarning_check[Side] > 3.0) av_bbb=7; else av_bbb=av_d;
      
      //     DateStart= DateToDateTime(((_TDate)DateTimeToDate(DiagDate)-(_TDate)(av_d-1)));
      DateStart= DateToDateTime(((_TDate)DateTimeToDate(DiagDate)-(_TDate)(av_bbb-1)));
      
      //    '' Find if data range is sufficient
      FindMeasurements(DateStart,DiagDate,&StartData,&EndData,/*MinDaysForDiagnosis*/0,MesurementNum);
      if ((StartData>0)&&(EndData>0)&&(EndData>=StartData))
        Count=EndData-StartData+1;
      else Count=0;
      
      Count+=UseOnLineData;
      if (Count < MaxDiagCalcMeasurements) { SetPage(Page); continue;}
      
      // Calc Middle
      SetPage(TempPage);
      AvgCount=0;
      AvgGamma.re=0;
      AvgGamma.im=0;
      for (i=StartData;i<=EndData;i++) {
        LoadData(Param,MeasurementNumToIndex(i),TempPage);
        if ( (Param->TrSideCount&(1<<Side)) && ( !(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))) ) ) { //DE 3-1-16  exclude records with critical errors
          CurGamma.re=(float)Param->TrSide[Side].Gamma/100.0;
          CurGamma.im=(float)Param->TrSide[Side].GammaPhase/100.0;
          vec_add(&AvgGamma,&CurGamma,&ResGamma);
          AvgGamma.re=ResGamma.re;
          AvgGamma.im=ResGamma.im;
          AvgCount++;
        }
      }
      if (AvgCount)AvgGamma.re/=AvgCount;  //ag 3-9-16 was division by zero without if
      
      // Calc delta
      SetPage(TempPage);
      ISum=0;
      AvgCount=0;
      for (i=StartData;i<=EndData;i++) {
        LoadData(Param,MeasurementNumToIndex(i),TempPage);
        if ( (Param->TrSideCount&(1<<Side)) && ( !(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))) ) ) { //DE 3-1-16  exclude records with critical errors
          CurGamma.re=(float)Param->TrSide[Side].Gamma/100.0;
          CurGamma.im=(float)Param->TrSide[Side].GammaPhase/100.0;
          vec_sub(&CurGamma,&AvgGamma,&ResGamma);
          ISum+=(ResGamma.re*ResGamma.re);
          AvgCount++;
        }
      }
      if (AvgCount)ISum=ISum/AvgCount; //ag 3-9-16 was division by zero without if
      dFi=pow(ISum,0.5);
      if (dFi<0.1) dFi=0.1;
      //Analyze dFi
      
      //'Calculating averaged par1 for diagnostics based on av_c days--------------------------------------
      for (ii=0;ii<(MaxBushingCountOnSide<<1);ii++) par1[ii]=0;
      DiagGamma.re=0;
      DiagGamma.im=0;
      SetPage(TempPage);
      for (i=StartData;i<=EndData;i++) {
        LoadData(Param,MeasurementNumToIndex(i),TempPage);
        if ( (Param->TrSideCount&(1<<Side)) && ( !(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))) ) ) { //DE 3-1-16  exclude records with critical errors
          CurGamma.re=(float)Param->TrSide[Side].Gamma/100.0;
          CurGamma.im=(float)Param->TrSide[Side].GammaPhase/100.0;
          vec_sub(&CurGamma,&AvgGamma,&ResGamma);
          // Diviation less then 2*dFi
          if (fabs(ResGamma.re)<=2*dFi) {
            //Calculation of averaged imbalance for following diagnostics
            vec_add(&DiagGamma,&CurGamma,&ResGamma);
            DiagGamma.re=ResGamma.re;
            DiagGamma.im=ResGamma.im;
            for (ii=0;ii<MaxBushingCountOnSide;ii++) {
              if (InputData) {
                par1[ii]=par1[ii] + (float)Param->TrSide[Side].SourceAmplitude[ii]/100.0;
                par1[ii+MaxBushingCountOnSide]=par1[ii+MaxBushingCountOnSide] + (float)Param->TrSide[Side].SourcePhase[ii]/100.0;
              } else {
                par1[ii]=par1[ii] + (float)Param->TrSide[Side].PhaseAmplitude[ii]/10.0;
                if (ii>0)
                  par1[ii+MaxBushingCountOnSide]=par1[ii+MaxBushingCountOnSide] + (float)Param->TrSide[Side].SignalPhase[ii]/100.0;
              }
            }
          } else Count--;
        } else Count--;
      }
      
      if (UseOnLineData) {
        SetPage(ParametersPage);
        if ((Parameters->TrSideCount&(1<<Side)) && !(Error & (ErrorType)(1<<(erSide1Critical+Side)))) {    //ag 6-27-17 returened to originaland added current error handling commented out dougs as for now if ( (Param->TrSideCount&(1<<Side)) && ( !(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))) )) { //DE 3-1-16  exclude records with critical errors
          CurGamma.re=(float)Parameters->TrSide[Side].Gamma/100.0;
          CurGamma.im=(float)Parameters->TrSide[Side].GammaPhase/100.0;
          //Calculation of averaged imbalance for following diagnostics
          vec_sub(&CurGamma,&AvgGamma,&ResGamma);
          // Diviation less then 2*dFi
          if (fabs(ResGamma.re)<=2*dFi) {
            //Calculation of averaged imbalance for following diagnostics
            vec_add(&DiagGamma,&CurGamma,&ResGamma);
            DiagGamma.re=ResGamma.re;
            DiagGamma.im=ResGamma.im;
            for (ii=0;ii<MaxBushingCountOnSide;ii++) {
              SetPage(ParametersPage);
              if (InputData) {
                dl1=(float)Parameters->TrSide[Side].SourceAmplitude[ii]/100.0;
                dl2=(float)Parameters->TrSide[Side].SourcePhase[ii]/100.0;
              } else {
                dl1=(float)Parameters->TrSide[Side].PhaseAmplitude[ii]/10.0;
                if (ii>0) dl2=(float)Parameters->TrSide[Side].SignalPhase[ii]/100.0;
                else dl2=0;
              }
              SetPage(TempPage);
              par1[ii]=par1[ii] + dl1;
              par1[ii+MaxBushingCountOnSide]=par1[ii+MaxBushingCountOnSide] + dl2;
            }
          } else Count--;
        } else Count--;
        SetPage(TempPage);
      }
      
      if (Count) DiagGamma.re/=Count; //ag 3-9-16 was division by zero without if
      
      if (Count < MaxDiagCalcMeasurements) {SetPage(Page); continue;}
      for (ii=0;ii<(MaxBushingCountOnSide<<1);ii++) {
        par1[ii] = par1[ii]/(Count);
      }
      par1[3]=0;                          //'A to A phase = 0
      
      
      
#ifdef __OldDiag
      //'Calculating par0 in case if BaseLine does not exists. par0 calculated on all available data assuming that BaseLine self start if sufficient data exists.
      if (FlagBase==0) {
        for (ii=0;ii<(MaxBushingCountOnSide<<1);ii++) par0[ii]=0;
        CountInArch=0;
        SetPage(TempPage);
        for (i=1;i<=EndData;i++) {
          LoadData(Param,MeasurementNumToIndex(i),TempPage);
          if ( (Param->TrSideCount&(1<<Side)) && ( !(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))) ) && ( !(((int)Param->Error2)&(1<<(erSide1Threshold-16+Side))) ) ) { //DE 3-1-16  exclude records with critical errors
            for (ii=0;ii<MaxBushingCountOnSide;ii++) {
              if (InputData) {
                par0[ii]=par0[ii] + (float)Param->TrSide[Side].SourceAmplitude[ii]/100.0;
                par0[ii+MaxBushingCountOnSide]=par0[ii+MaxBushingCountOnSide] + (float)Param->TrSide[Side].SourcePhase[ii]/100.0;
              } else {
                par0[ii]=par0[ii] + (float)Param->TrSide[Side].PhaseAmplitude[ii]/10.0;
                par0[ii+MaxBushingCountOnSide]=par0[ii+MaxBushingCountOnSide] + (float)Param->TrSide[Side].SignalPhase[ii]/100.0;
              }
            }
            CountInArch++;
          }
        }
        
        if (UseOnLineData) {
          if ((Parameters->TrSideCount&(1<<Side)) && !(Error & (ErrorType)(1<<(erSide1Critical+Side)))) {    //ag 6-27-17 returened to original and added current error check commented out dougs as for now //if ( (Param->TrSideCount&(1<<Side)) && ( !(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))) ) && ( !(((int)Param->Error2)&(1<<(erSide1Threshold-16+Side))) ) ) { //DE 3-1-16  exclude records with critical errors
            for (ii=0;ii<MaxBushingCountOnSide;ii++) {
              SetPage(ParametersPage);
              if (InputData) {
                dl1=(float)Parameters->TrSide[Side].SourceAmplitude[ii]/100.0;
                dl1=(float)Parameters->TrSide[Side].SourcePhase[ii]/100.0;
              } else {
                dl1=(float)Parameters->TrSide[Side].PhaseAmplitude[ii]/10.0;
                dl2=(float)Parameters->TrSide[Side].SignalPhase[ii]/100.0;
              }
              SetPage(TempPage);
              par0[ii]=par0[ii] + dl1;
              par0[ii+MaxBushingCountOnSide]=par0[ii+MaxBushingCountOnSide] + dl2;
            }
            CountInArch++;
          }
          SetPage(TempPage);
        }
        for (ii=0;ii<(MaxBushingCountOnSide<<1);ii++) {
          if (CountInArch)par0[ii]=par0[ii]/(CountInArch); //ag 3-9-16 was division by zero without if
        }
        par0[3] = 0;                          //'A to A phase = 0
        
        /*
        ''------------------------------------------
        If ObxodFlag2 <> 0 Then GoTo obxod2
        ''--------------------------Temporal Correction for temperature
        Call Cor(par, jj + 1, 6, Cr)
        
        
        If Abs(Cr(4, 0) + Cr(5, 0)) >= 0.8 Or Abs(Cr(4, 0)) >= 0.5 Or Abs(Cr(5, 0)) >= 0.5 Then    'Correlation exists
        uu = 1000
        For ii = 3 To 5
        l2 = ii + 1
        If l2 > 5 Then l2 = l2 - 6
        If uu >= Abs(Cr(ii, 1) - Cr(l2, 1)) Then
        uu = Abs(Cr(ii, 1) - Cr(l2, 1))
        dFi = 0.5 * (Cr(ii, 1) + Cr(l2, 1))
        
        For vv = 3 To 5
        Slp = Pi * (Cr(vv, 1) - dFi)
        init0(vv) = init1(vv) + Slp * (par0(6) - init1(6)) * 100
        Next vv
        End If
        Next ii
        Else
        End If
        End If
        ''---------------------------------------------------------------
        obxod2:
        */
        
      }
#endif
    }
    
    CoeffOnGamma=DiagGamma.re/MaxCorrectionGammaValue;
    if (CoeffOnGamma>1.0) CoeffOnGamma=1.0;
    
    // For Test  
    //  CoeffOnGamma=1.0;
    
    //'---------------------------------------------------------------------------------------
    //''Selection based on symmetrical or two best angles. I prefer flag=0, but keep flag=1 for the future
    
    //   flag=0;
    //   If flag = 0 Then
    
#ifdef __OldDiag
    
    SetPage(TempPage);
    I0Sum = par0[0] + par0[1] + par0[2];
    ISum = par1[0] + par1[1] + par1[2];
    kuku = I0Sum / ISum;
    dFi = ((par1[4] - par0[4]) + (par1[5] - par0[5])) / 3;
    //        Else
    
    /*
    t1 = 1000;
    t2 = 1000;
    for (ii=0;ii<MaxBushingCountOnSide;ii++){
    l1 = ii + 1;
    l2 = ii + 4;
    if (l1 > 2) l1 = l1 - 3;
    if (l2 > 5) l2 = l2 - 6;
    
    if (t1 >= fabs(par1[ii][jj] / par0[ii] - par1[l1][jj] / par0[l1])) {
    t1 = fabs(par1[ii][jj] / par0[ii] - par1[l1][jj] / par0[l1]);
    kuku = (par0[ii] + par0[l1]) / (par1[ii][jj] + par1[l1][jj]);
  }
    
    if (t2 >= Abs(par1[ii + 3][jj] - par0[ii + 3] - par1[l2][jj] + par0[l2])) {
    t2 = Abs(par1[ii + 3][jj] - par0[ii + 3] - par1[l2, jj] + par0[l2]);
    dFi = (par1[ii + 3][jj] + par1[l2][jj] - par0[ii + 3] - par0[l2]) / 2;
  }
  }
    */
    
    //'----------------------------------------------------------------------------------------------
    //'Calculation of current diagnostics------------------------------------------------------------
    /*
    for (ii=0;ii<MaxBushingCountOnSide;ii++) {
    if (par0[ii]==0) (*DiagData).TgParam[Side].C[ii]  = (unsigned int)Round(init0[ii] * 10.0);
    else (*DiagData).TgParam[Side].C[ii]  = (unsigned int)Round((init0[ii] * (par1[ii] / par0[ii]) * kuku)*10.0);
    (*DiagData).TgParam[Side].tg[ii] = (unsigned int)Round((init0[ii + MaxBushingCountOnSide] + ((par1[ii + MaxBushingCountOnSide] - par0[ii + MaxBushingCountOnSide]) - dFi) * Pi * 100.0)*100.0);
  }
    */
    for (ii=0;ii<MaxBushingCountOnSide;ii++) {		// acm, par0[] stored "Tangent Delta"
      dl1 = 1.0+((par1[ii] / par0[ii]) * kuku -1.0)*CoeffOnGamma;
      if (par0[ii]==0) par0[ii] = init0[ii];
      else par0[ii] = init0[ii] * dl1;
      par0[ii+ MaxBushingCountOnSide] = (init0[ii + MaxBushingCountOnSide] + ((par1[ii + MaxBushingCountOnSide] - par0[ii + MaxBushingCountOnSide]) - dFi) * Pi* 100.0);
    }
    
    //''Correction diagnostics from symetrical approach. If obxod it shut down.
    if (ObxodFlag==0) {
      //    ''Check for one tangent increase
      //    'Dim del(5) As Double
      ParToCompare = 0.002 * (init0[0] + init0[1] + init0[2]) / 3;
      for (ii=0;ii<(MaxBushingCountOnSide<<1);ii++) {
        if (ii > 2) ParToCompare = 0.1;
        l1 = ii + 1;
        l2 = ii + 2;
        if ((ii > 2) && (l1 > 5)) l1 = l1 - 3;
        if ((ii > 2) && (l2 > 5)) l2 = l2 - 3;
        if ((ii < 3) && (l1 > 2)) l1 = l1 - 3;
        if ((ii < 3) && (l2 > 2)) l2 = l2 - 3;
        dl1 = fabs(par0[ii] - init0[ii]);
        dl2 = fabs(par0[l1] - init0[l1] - par0[l2] + init0[l2]);
        dl_add =  (par0[l1] - init0[l1] + par0[l2] - init0[l2]);
        
        if ((dl1 > 2 * dl2) && (dl1 > ParToCompare)) {
          par0[ii] = par0[ii] - 0.5 * dl_add;
          par0[l1] = par0[l1] - 0.5 * dl_add;
          par0[l2] = par0[l2] - 0.5 * dl_add;
        }
        /*
        if (ii<3) {
        if ((dl1 > 2 * dl2) && (dl1 > ParToCompare)) {
        par0[ii] = par0[ii] - 0.5 * dl_add;
        par0[l1] = par0[l1] - 0.5 * dl_add;
        par0[l2] = par0[l2] - 0.5 * dl_add;
      }
      } else {
        if ((dl1 > 2 * dl2) && (dl1 > ParToCompare)) {
        if (dl_add<0) dFi=0.85; else dFi=0.15;
        par0[l1] = par0[l1] - dFi * dl_add;
        par0[l2] = par0[l2] - dFi * dl_add;
      }
      }
        */
      }
    }
#else
    //Analyze delta C
    //Base Avg
    I0Sum=(par0[0]+par0[1]+par0[2])/3.0;
    //Base delta
    par0[3]=(par0[0]-I0Sum);///I0Sum*100.0;
    par0[4]=(par0[1]-I0Sum);///I0Sum*100.0;
    par0[5]=(par0[2]-I0Sum);///I0Sum*100.0;
    
    //Current Avg
    ISum=(par1[0]+par1[1]+par1[2])/3.0;
    //Base delta
    par1[3]=(par1[0]-ISum);///ISum*100.0;
    par1[4]=(par1[1]-ISum);///ISum*100.0;
    par1[5]=(par1[2]-ISum);///ISum*100.0;
    
    par1[0]=(par1[3]-par0[3])*1.5/par0[0]*100.0;
    par1[1]=(par1[4]-par0[4])*1.5/par0[1]*100.0;
    par1[2]=(par1[5]-par0[5])*1.5/par0[2]*100.0;
    
    DeltaC=0;
    if ((par1[0]>0)&&(par1[0]>=2)&&(par1[1]<0-(par1[0]*0.25))&&(par1[2]<0-(par1[0]*0.25))) {
      DeltaC=1;
      par0[0]=par1[0]; par0[1]=0; par0[2]=0;
    } else
      if ((par1[1]>0)&&(par1[1]>=2)&&(par1[0]<0-(par1[1]*0.25))&&(par1[2]<0-(par1[1]*0.25))) {
        DeltaC=2;
        par0[0]=0; par0[1]=par1[1]; par0[2]=0;
      } else
        if ((par1[2]>0)&&(par1[2]>=2)&&(par1[0]<0-(par1[2]*0.25))&&(par1[1]<0-(par1[2]*0.25))) {
          DeltaC=3;
          par0[0]=0; par0[1]=0; par0[2]=par1[2];
        } else {
          par0[0]=0; par0[1]=0; par0[2]=0;
        }
    if (par0[0]>0) par0[0]=init0[0]+(init0[0]/100.0*par0[0]*CoeffOnGamma);
    else par0[0]=init0[0];
    if (par0[1]>0) par0[1]=init0[1]+(init0[1]/100.0*par0[1]*CoeffOnGamma);
    else par0[1]=init0[1];
    if (par0[2]>0) par0[2]=init0[2]+(init0[2]/100.0*par0[2]*CoeffOnGamma);
    else par0[2]=init0[2];
    
    for (ii=3;ii<MaxBushingCountOnSide*2;ii++) par0[ii] = init0[ii];
    
    if (!DeltaC) {
      //Analyze delta tg
      
      //- initial Gamma value
      CurGamma.re=(float)Setup.InitialParameters.TrSideParam[Side].GammaAmplitude/100.0;
      CurGamma.im=(float)Setup.InitialParameters.TrSideParam[Side].GammaPhase/100.0;
      vec_sub(&DiagGamma,&CurGamma,&ResGamma);
      DiagGamma.re=ResGamma.re;
      DiagGamma.im=ResGamma.im;
      
      //Select phase angle and calc delta tangent
      Zone=0;
      if ((DiagGamma.im>=90)&&(DiagGamma.im<210)) Zone=1;
      else if ((DiagGamma.im>=210)&&(DiagGamma.im<330)) Zone=2;
      else if ((DiagGamma.im>=330)||(DiagGamma.im<90))Zone=3;
      if (Zone) {
        if ((DiagGamma.im<=90)&&(Zone==3))
          ResGamma.im=DiagGamma.im-90;
        else
          ResGamma.im=DiagGamma.im-90-120*(Zone-1);
        vec_360(&ResGamma);
        ResGamma.re=DiagGamma.re;
        vec_to_cpx(&ResGamma);
        dl1=(fabs(ResGamma.re+ResGamma.im*0.5))*CoeffOnGamma;
        dl2=fabs(ResGamma.im)/0.866*CoeffOnGamma;
        
        switch (Zone) {
        case 1:par0[3] = init0[3] + dl1;
        par0[4] = init0[4] + dl2;
        break;
        case 2:par0[4] = init0[4] + dl1;
        par0[5] = init0[5] + dl2;
        break;
        case 3:if (DiagGamma.im<90) {
          par0[5] = init0[5] + dl2;
          par0[3] = init0[3] + dl1;
        } else {
          par0[5] = init0[5] + dl1;
          par0[3] = init0[3] + dl2;
        }
        break;
        }
      }
    }
#endif
    
    SetPage(DiagDataPage);
    
    /*
    Why is tan delta and capacitance test done on transformers?
    Answer:
    In a pure capacitor current leads the voltage by 90 degrees. when a resistor is introduced in between the current leads an angle less than 90 degrees.
    
    Similarly in a Dielectric material when a cavity or deterioration starts , the life of the material starts deteriorating , as there is a resistance getting added and hence
    leakage current increases In tan Delta we find the difference in the angle and periodically note down the pace at which deterioration takes place.
    */
    
    for (ii=0;ii<MaxBushingCountOnSide;ii++) {  // acm, 3-31-10, possibly related to bad values AG noticed on display, cleared up after next measurment? 
      (*DiagData).TgParam[Side].C[ii]  = (unsigned int)Round(par0[ii] * 10.0);
      (*DiagData).TgParam[Side].tg[ii] = (int)Round(par0[ii+ MaxBushingCountOnSide]*100.0);
    }
    SetPage(Page);
    
  }
  return 1;
}



static void Cor(
                struct stSourceForCorrelation *ar, //'(ar() - array the includes k-1 what to correlate and the last to what correlate
                unsigned int n,
                unsigned int k,
                struct stCorrelationData *C)
{
  
  // acm, 4-28 AG verified with simulation that 64 bit float is required or get overflow errors
  double sum[MaxBushingCountOnSide+1];          //''sum of values
  double sum2[MaxBushingCountOnSide+1];         //''sum of squares
  double Sig[MaxBushingCountOnSide+1];          // ''Sigma
  double Myu[MaxBushingCountOnSide+1];          //''Correlation momentum
  double Coef;//,temp;
  double temp;
  
  /*
  double sum[MaxBushingCountOnSide+1];          //''sum of values
  double sum2[MaxBushingCountOnSide+1];         //''sum of squares
  double Sig[MaxBushingCountOnSide+1];          // ''Sigma
  double Myu[MaxBushingCountOnSide+1];          //''Correlation momentum
  double Coef;//,temp;
  double temp;
  */
  unsigned int jj,ii;
  
  //  Coef=1.0/(float)n;
  Coef=1.0/(double)n;		// acm, 3-27-10, correct minor type mis-match, but should result in more precise calculation
  
  
  for (ii=0;ii<=k;ii++) {
    sum[ii] = 0;
    sum2[ii] = 0;
    for (jj=0;jj<n;jj++) {
      sum[ii] = sum[ii] + ((*ar).data[ii][jj]);
      sum2[ii] = sum2[ii] + ((*ar).data[ii][jj]) * ((*ar).data[ii][jj]);
    }
  }
  
  for (ii=0;ii<=k;ii++) {        //''Average
    sum[ii] = Coef * sum[ii];
    sum2[ii] = Coef * sum2[ii];
    //      sum[ii] = sum[ii]/(float)n;
    //      sum2[ii] = sum2[ii]/(float)n;
    temp=sum2[ii] - (sum[ii]*sum[ii]);
    if (temp > 0)
      Sig[ii] = sqrt(temp);
    else
      Sig[ii] = 0;
  }
  
  for (ii=0;ii<k;ii++) {
    Myu[ii]=0;
    for (jj=0;jj<n;jj++) {
      //           Myu[ii] = Myu[ii] + Coef * ((*ar).data[ii][jj] - sum[ii]) * ((*ar).data[k][jj] - sum[k]);
      Myu[ii]+=(((*ar).data[ii][jj] - sum[ii]) * ((*ar).data[k][jj] - sum[k]));
      
    }
    Myu[ii] = Coef * Myu[ii];
  }
  
  for (ii=0;ii<k;ii++) {
    (*C).ccr[ii][0] = 0;
    (*C).ccr[ii][1] = 0;
    (*C).ccr[ii][2] = 0;
    if ((Sig[ii]!=0) && (Sig[k]!=0))
      (*C).ccr[ii][0] = Myu[ii] / Sig[ii] / Sig[k];   //'Correlation
    if (Sig[k]!=0)
      (*C).ccr[ii][1] = (*C).ccr[ii][0] * Sig[ii] / Sig[k]; //'Slope
    (*C).ccr[ii][2] = sum[ii] - (*C).ccr[ii][1] * sum[k]; //'Base
    
  }
}



typedef float Tpar[MaxBushingCountOnSide*2+1][MaxBaseLineMeasurements];
#ifndef __emulator

#ifndef __arm

#define TempDataBaseAddr  (unsigned int)0x8000
#define SizeOfTempData    (unsigned int)(((unsigned int)MaxBushingCountOnSide*2+1)*sizeof(float)*((unsigned int)MaxBaseLineMeasurements+1))
#define TempDataBaseAddr2 (unsigned int)(TempDataBaseAddr+SizeOfTempData)
#else
#define TempDataBaseAddr           (&Sig[0][0])
#define TempDataBaseAddr2          (&Sig[2][0])
#endif

// acm, 3-16-10, note following two large arrays (4 bytes/float * 7 * 700 = 19600 = 0x4c90 bytes, 4 bytes/float * 4 * 700 = 11200 = 0x2bc0 bytes) are addressed in region defined in RAM.cpp (&Sig[4][5120]),
// complicated because this region also used as ADC buffers.  Bounds are checked in FindMeasurements(), though one customer dataset appears to be 24 measurements/day * 30 days, or 720 measurements?
// Presume ok. two arrays to consume all 0xa000 Sig bytes, ADC must be disabled...

//'parameters Ia, Ib, Ic, Fia, Fib, Fic, temperature -  the entire array of data
Tpar *par =(Tpar *)TempDataBaseAddr;
//'Array for correlations. (parameter1, parameter2) Parameter 2 - normally temperature
struct stSourceForCorrelation *ar=(struct stSourceForCorrelation *)TempDataBaseAddr2;

#else
Tpar Ppar;
Tpar *par=&Ppar;
struct stSourceForCorrelation Par;
struct stSourceForCorrelation *ar=&Par;
#endif

char RunBaseLine(_TDateTime BaseLineDate,
                 char InputData            //1-  Input, 0-Gamma       ''Selecting Gamma channel or inputs for analysis
                   )
{
  
  //''Number of days for initial system learning. Normally = 30!
#define av_l Setup.GammaSetupInfo.DaysToCalculateBASELINE     //'days for learn
  
  float init1[MaxBushingCountOnSide*2+1];  //'C0a, C0b, C0c, Tg0a, Tg0b, Tg0c, Temp0 at off line test will be back up
  float init0[MaxBushingCountOnSide*2+1];  //'C0a, C0b, C0c, Tg0a, Tg0b, Tg0c, Temp0 at off line test with temp correction
  float par0[MaxBushingCountOnSide*2+1];   //'parameters I0a, I0b, I0c, Fi0a, Fi0b, Fi0c, Temperature    from learn averaged over av_l
  
  unsigned int i;
  unsigned int cnt;
  _TDateTime CurDate, StartDate;
  struct stCorrelationData Corr;
  float dFi,Slp,uu;
  char l2
#ifndef __arm
    ,Page=GetPage()
#endif
      ;
  unsigned int StartNum,EndNum;
  //struct stInsulationParameters Param;
  unsigned int Skipped;
  char Side;
#ifdef __emulator
  struct stInsulationParameters TParam;
  struct stInsulationParameters *Param=&TParam;
  unsigned int ii;
#else
#ifdef __arm
  struct stInsulationParameters *Param =TempParam;
  unsigned int ii;
#else
  struct stInsulationParameters *Param =(struct stInsulationParameters *) (0xFFFF-szInsulationParameters);
  char ii;
#endif
#endif
  
  
  if (MeasurementsInArchive<MaxBaseCalcMeasurements) return 0;
  
  for (Side=0;Side<MaxTransformerSide;Side++) {
    
    //''Reading Initial data C0 and Tg0----------------
    for (ii=0;ii<MaxBushingCountOnSide;ii++) {
      init0[ii] = (float)Setup.GammaSetupInfo.GammaSideSetup[Side].C0[ii]/10.0;
      init0[ii + MaxBushingCountOnSide] = (float)Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[ii]/100.0;
    }
    init0[6] = -70 + Setup.GammaSetupInfo.GammaSideSetup[Side].Temperature0;
    
    //'Will be used for compensation of temperature in tan if correlates
    for (ii=0;ii<MaxBushingCountOnSide*2+1;ii++)
      init1[ii] = init0[ii];
    //'------------------------------------------------
    
    SetPage(TempPage);
    LoadData(Param,MeasurementNumToIndex(1),TempPage);
    CurDate=EncodeDate(Param->Year,Param->Month,Param->Day);
    
    if (DateTimeToDate(BaseLineDate)-CurDate<av_l) return 0;
    
    //'' Find how many records in data and is this sufficient to learn
    StartDate=DateToDateTime(DateTimeToDate(BaseLineDate)-(av_l));
    
    FindMeasurements(StartDate,BaseLineDate,&StartNum,&EndNum,av_l/*MinDaysForBaseLine*/,0);
    if ((StartNum>0)&&(EndNum>0)&&(EndNum>=StartNum))
      cnt=EndNum-StartNum+1;
    else cnt=0;
    //  cnt+=UseOnLineData;
    if (cnt<MaxBaseCalcMeasurements) return 0;
    
    //'Reading all data array from sh and also Gamma array--------------------------------------
    SetPage(TempPage);
    Skipped=0;
    for (i=0;i<=EndNum-StartNum;i++) {
      
      LoadData(Param,MeasurementNumToIndex(i+StartNum),TempPage);
      SetPage(TempPage);
      
      if ( (Param->TrSideCount&(1<<Side)) && ( !(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))) ) ) { //DE 3-1-16  exclude records with critical errors
        if (InputData) {
          dFi=(float)Param->TrSide[Side].SourcePhase[1]/100.0;
          Slp=(float)Param->TrSide[Side].SourcePhase[2]/100.0;
        } else {
          dFi=(float)Param->TrSide[Side].SignalPhase[1]/100.0;
          Slp=(float)Param->TrSide[Side].SignalPhase[2]/100.0;
        }
      } else {
        dFi=0;
        Slp=0;
      }
      if (!((dFi>100)&&(dFi<140)&&(Slp>220)&&(Slp<260))) {
        Skipped++;
        continue;
      }
      
      if (BoardType()&(1<<7)) {
        //         par[6][(i-Skipped)] = (int)-70 + (int)Param->Temperature[Side];//'Tmperature
        (*par)[6][(i-Skipped)] = (int)-70 + (int)Param->Temperature[0];//'Tmperature
      } else {
        (*par)[6][(i-Skipped)] = (int)-70 + (int)Param->TrSide[Side].Temperature;//'Tmperature
      }
      
      for (ii=0;ii<MaxBushingCountOnSide;ii++) {
        if (InputData) {
          (*par)[ii][(i-Skipped)]=(float)Param->TrSide[Side].SourceAmplitude[ii]/100.0;
          (*par)[ii+MaxBushingCountOnSide][(i-Skipped)]=(float)Param->TrSide[Side].SourcePhase[ii]/100.0;
        } else {
          (*par)[ii][(i-Skipped)]=(float)Param->TrSide[Side].PhaseAmplitude[ii]/10.0;
          (*par)[ii+MaxBushingCountOnSide][(i-Skipped)]=(float)Param->TrSide[Side].SignalPhase[ii]/100.0;
        }
      }
      
      (*par)[3][(i-Skipped)]=0;                //'A to A phase = 0
      if (i==EndNum-StartNum) {
        SetPage(TempPage);
        CurDate=DoPackDateTime(EncodeDate(Param->Year,Param->Month,Param->Day),
                               EncodeTime(Param->Hour,Param->Min,0));
      }
      
    }
    /*
    if (UseOnLineData) {
    SetPage(ParametersPage);
    
    if (InputData) {
    dFi=(float)Parameters->TrSide[Side].SourcePhase[1]/100.0;
    Slp=(float)Parameters->TrSide[Side].SourcePhase[2]/100.0;
  } else {
    dFi=(float)Parameters->TrSide[Side].SignalPhase[1]/100.0;
    Slp=(float)Parameters->TrSide[Side].SignalPhase[2]/100.0;
  }
    if (!((dFi>100)&&(dFi<140)&&(Slp>220)&&(Slp<260))) {
    Skipped++;
    goto CurSkipped;
  }
    
    if (BoardType()==128) {
    dFi = (int)-70 + (int)Parameters->Temperature[Side];//'Tmperature
  } else {
    dFi = (int)-70 + (int)Parameters->TrSide[Side].Temperature;//'Tmperature
  }
    
    SetPage(TempPage);
    *par[6][(i-Skipped)] = dFi;//'Tmperature
    
    for (ii=0;ii<MaxBushingCountOnSide;ii++) {
    SetPage(ParametersPage);
    if (InputData) {
    dFi=(float)Parameters->TrSide[Side].SourceAmplitude[ii]/10.0;
    Slp=(float)Parameters->TrSide[Side].SourcePhase[ii]/100.0;
  } else {
    dFi=(float)Parameters->TrSide[Side].PhaseAmplitude[ii]/10.0;
    Slp=(float)Parameters->TrSide[Side].SignalPhase[ii]/100.0;
  }
    SetPage(TempPage);
    *par[ii][cnt-1-Skipped]=dFi;
    *par[ii+MaxBushingCountOnSide][cnt-1-Skipped]=Slp;
  }
    
    SetPage(TempPage);
    *par[3][cnt-1-Skipped]=0;                //'A to A phase = 0
    SetPage(ParametersPage);
    CurDate=DoPackDateTime(EncodeDate(Parameters->Year,Parameters->Month,Parameters->Day),
    EncodeTime(Parameters->Hour,Parameters->Min,0));
    
  }
    CurSkipped:
    SetPage(TempPage);
    */
    //'----------------------------------------------------------------
    
    //'---------------------------------------------------------------
    //'Finding par0 at temperature of off line tests if correlation exists. Otherwise par0 is averaged values.
    
    cnt-=Skipped;
    //if (!cnt) {SetPage(Page); return 0;} 2.10 ag modified to check for <MaxBaseCalcMeasurements
    if (cnt < MaxBaseCalcMeasurements) {SetPage(Page); return 0;}
    
    //   for (ii=0;ii<7;ii++) par0[ii] = 0;
    SetPage(TempPage);
    for (ii=0;ii<7;ii++) {
      par0[ii] = 0;
      for (i=0;i<cnt;i++) {
        par0[ii] = par0[ii] + (*par)[ii][i];
      }
      par0[ii] = par0[ii]/cnt;
    }
    
    //'Investigating an opportunity of essential temperature dependency in mag. and angles over learning period
    //        Dim ccr
    for (i=0;i<cnt;i++) {
      for (ii=0;ii<MaxBushingCountOnSide+1;ii++)
        ar->data[ii][i] = (*par)[ii+MaxBushingCountOnSide][i];
    }
    
    Cor(ar, cnt , MaxBushingCountOnSide, &Corr);
    
    //''Linear coefficients for tan Only. I do not do a temperature compensation for Magnitudes.
    //Dim Slp As Double    ''Slope of par
    
    if ((fabs(Corr.ccr[0][0] + Corr.ccr[1][0]) >= 0.8)||(fabs(Corr.ccr[0][0]) >= 0.5)||(fabs(Corr.ccr[1][0]) >= 0.5)) {    //'Correlation exists
      uu = 1000;
      for (ii=0;ii<MaxBushingCountOnSide;ii++) {
        l2 = ii + 1;
        if (l2 > 2) l2 = l2 - 3;
        if (uu >= fabs(Corr.ccr[ii][1] - Corr.ccr[l2][1])) {
          uu = fabs(Corr.ccr[ii][1] - Corr.ccr[l2][1]);
          dFi = 0.5 * (Corr.ccr[ii][1] + Corr.ccr[l2][1]);
          for (i=3;i<=5;i++) {
            Slp = Pi * (Corr.ccr[i-3][1] - dFi);
            //                init0[i] = init1[i] + Slp * (par0[6] - init1[6]) * 100.0;
            init0[i] = init1[i] + Slp * /*(par0[6] - init1[6]) */ 100.0;
          }
          
        }
      }
    }
    //''-------------------------------------------------------------
    //'Data output
    
    Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate=CurDate;
    
    //  =DateToDateTime(EncodeDate(DateTime.Year,DateTime.Month,DateTime.Day))+
    //                             TimeToDateTime(EncodeTime(DateTime.Hour,DateTime.Min,DateTime.Sec));
    //Temperature at STABLE tangent measurement (-70 - +185 0C) with step 1 0C
    Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETemperature= (char)(par0[6]+70);
    for (ii=0;ii<MaxBushingCountOnSide;ii++) {
      //tg*100 ,% at STABLE
      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[ii]=(unsigned int)Round(init0[MaxBushingCountOnSide+ii]*100.0);
      //C*10 ,pF
      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[ii]=(unsigned int)Round(init0[ii]*10.0);
      if (InputData) {
        //Signal amplitude of a phase, mV
        Setup.GammaSetupInfo.GammaSideSetup[Side].StableSourceAmplitude[ii]=(unsigned int)Round(par0[ii]*10.0);
        //phase, deg
        Setup.GammaSetupInfo.GammaSideSetup[Side].StableSourcePhase[ii]=(unsigned int)Round(par0[MaxBushingCountOnSide+ii]*100.0);
      } else {
        //Signal amplitude of a phase, mV
        Setup.GammaSetupInfo.GammaSideSetup[Side].StablePhaseAmplitude[ii]=(unsigned int)Round(par0[ii]*10.0);
        //phase, deg
        Setup.GammaSetupInfo.GammaSideSetup[Side].StableSignalPhase[ii]=(unsigned int)Round(par0[MaxBushingCountOnSide+ii]*100.0);
      }
      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaC[ii]=0;
      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaTg[ii]=0;
      
    }
    if (!InputData) Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved=1; else Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved=2;
  }
  TestSetup();
  
  return 1;
}


char GammaTrend(_TDateTime DiagDate,
                char *GammaTempPh,
                char *GammaTemp,
                char *GammaTrend,
                unsigned int MesurementNum,             //measurement number or 0, if unknown
                char UseOnLineData
                  )
{
  //#define av_c  15 //''Number days for sliding window. Used for temperature.
  //#define av_t  30 //''Number days for trend.
  
#define av_c  Setup.GammaSetupInfo.DaysToCalculateTCoefficient       //''Number days for sliding window. Used for temperature.
#define av_t  Setup.GammaSetupInfo.DaysToCalculateTrend              //''Number days for trend.
  
  //char Gamma_jumpA, GammajumpB;	// acm, new, flag indicates increase in Unn of 2% for chA or B, meaning we need to shorten windows to 5 days
  
  //''Gamma correlations to temperature and trend----------------------------------------------------
  //    Dim GammaData
  //    Dim GammaDate As Date
  //    Dim GammaTemp As Double
  //    Dim GammaTempPh As Double
  //    Dim GammaTrend  As Double
  
  //    Dim ar() As Double
  
  //'Array for correlations. (parameter1, parameter2) Parameter 2 - normally temperature
  //static __no_init struct stSourceForCorrelation ar @TempDataBaseAddr2;
  struct stCorrelationData res;
  _TDateTime /*GammaDate,*/DateStart;
  unsigned int i,StartNum,EndNum,cnt,SkipInArch;
  float f1,f2,f3,Tmin,TMax;
  //struct stInsulationParameters Param;
  char Side;
#ifdef __emulator
  struct stInsulationParameters TParam;
  struct stInsulationParameters *Param=&TParam;
#else
#ifdef __arm
  struct stInsulationParameters *Param =TempParam;
#else
  struct stInsulationParameters *Param =(struct stInsulationParameters *) (0xFFFF-szInsulationParameters);
#endif
#endif
  
  if (MeasurementsInArchive+UseOnLineData<MaxTrendCalcMeasurements) {
    for (Side=0;Side<MaxTransformerSide;Side++) {
      GammaTemp[Side] = 0;
      GammaTempPh[Side] = 0;
      GammaTrend[Side] = 0;
    }
    return 0;
  }
  
  for (Side=0;Side<MaxTransformerSide;Side++) {
    
    GammaTemp[Side] = 0;
    GammaTempPh[Side] = 0;
    GammaTrend[Side] = 0;
    
    if (!(Parameters->TrSideCount & (1<<Side)) || (Error & (ErrorType)(1<<(erSide1Critical+Side))))  //2.10 ag 6-27-17  exclude current data, if critical error
    {
      UseOnLineData = 0;
    }
    else
      UseOnLineData = AddOnLine;
    
    /*
    LoadData((struct stInsulationParameters *)OutBuf,MeasurementNumToIndex(MeasurementsInArchive));
    GammaDate=DateToDateTime(EncodeDate(((struct stInsulationParameters *)OutBuf)->Year,
    ((struct stInsulationParameters *)OutBuf)->Month,
    ((struct stInsulationParameters *)OutBuf)->Day))+
    TimeToDateTime(EncodeTime(((struct stInsulationParameters *)OutBuf)->Hour,
    ((struct stInsulationParameters *)OutBuf)->Min,
    0));
    */
    SetPage(TempPage);
    LoadData(Param,MeasurementNumToIndex(1),TempPage);
    DateStart=EncodeDate(Param->Year,Param->Month,Param->Day);  // acm, EncodeDate() reuturns # days from 2000, using last read time from archive data
    //    '' Find if data range is sufficient
    
    if (DateTimeToDate(DiagDate) - DateStart < av_c-1) continue;  // acm, DateTimeToDate() returns back #days from 2000
    DateStart= DateToDateTime(DateTimeToDate(DiagDate)-(av_c-1));
    
    FindMeasurements(DateStart,DiagDate,&StartNum,&EndNum,MinDaysForTrend,MesurementNum);
    if ((StartNum>0)&&(EndNum>0)&&(EndNum>=StartNum))
      cnt=EndNum-StartNum+1+UseOnLineData;
    else cnt=0;
    
    if (cnt<MaxTrendCalcMeasurements) continue;
    
    
    //'Temperature coeff------------------------------------------------------
    
    //        Dim res
    //        ReDim ar(2, cnt - cnt1)
    
    //  for (i=StartNum;i<=EndNum;i++) {
    Tmin=1000;
    TMax=-100;
    SkipInArch=0;
    for (i=0;i<=EndNum-StartNum;i++) {
      LoadData(Param,MeasurementNumToIndex(StartNum+i),TempPage);
      SetPage(TempPage);
      if ( (Param->TrSideCount&(1<<Side)) && (!(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))))  //DE 3-1-16  exclude records with critical errors
          && ((int)Param->TrSide[Side].Temperature > 30)) {                                            //2.10 ag 6-26-17 exclude records with temperature below -40
            ar->data[0][i-SkipInArch] = (float)Param->TrSide[Side].Gamma/100.0 *
              cos((float)Param->TrSide[Side].GammaPhase/100.0 * Pi); //'Gamma x-coordinate
            ar->data[1][i-SkipInArch] = (float)Param->TrSide[Side].Gamma/100.0 *
              sin((float)Param->TrSide[Side].GammaPhase/100.0 * Pi); //'Gamma y-coordinate
            if (BoardType()&(1<<7)) {
              //            ar.data[2][i-SkipInArch] = (int)-70 + (int)Param->Temperature[Side];//'Tmperature
              ar->data[2][i-SkipInArch] = (int)-70 + (int)Param->Temperature[0];//'Tmperature
            } else {
              ar->data[2][i-SkipInArch] = (int)-70 + (int)Param->TrSide[Side].Temperature;//'Tmperature
            }
            if (ar->data[2][i-SkipInArch]<Tmin) Tmin=ar->data[2][i-SkipInArch];  // acm, determining temperature range of stored data
            if (ar->data[2][i-SkipInArch]>TMax) TMax=ar->data[2][i-SkipInArch];
          } else SkipInArch++;  // acm, skip data points that aren't on side 0 or 1 ie group temperatures per side?
    }
    //DE 2-26-16 no change
    //2.10 ag 6-27-17 add check for bad temperature reading <-40C
    if (UseOnLineData && ((int)Parameters->TrSide[Side].Temperature > 30)) 
    {                                                  
      SetPage(ParametersPage);
      f1=(float)Parameters->TrSide[Side].Gamma/100.0 *
        cos((float)Parameters->TrSide[Side].GammaPhase/100.0 * Pi); //'Gamma x-coordinate
      f2=(float)Parameters->TrSide[Side].Gamma/100.0 *
        sin((float)Parameters->TrSide[Side].GammaPhase/100.0 * Pi); //'Gamma y-coordinate
      if (BoardType()&(1<<7)) {
        //         f3 = (int)-70 + (int)Param->Temperature[Side];//'Tmperature
        f3 = (int)-70 + (int)Parameters->Temperature[0];//'Tmperature
      } else {
        f3 = (int)-70 + (int)Parameters->TrSide[Side].Temperature;//'Tmperature
      }
      //      f3=-70 + Parameters->Temperature[Side]; //'Temperature
      SetPage(TempPage);
      ar->data[0][EndNum-StartNum+UseOnLineData-SkipInArch] = f1; //'Gamma x-coordinate
      ar->data[1][EndNum-StartNum+UseOnLineData-SkipInArch] = f2; //'Gamma y-coordinate
      ar->data[2][EndNum-StartNum+UseOnLineData-SkipInArch] = f3; //'Temperature
      if (f3<Tmin) Tmin=f3;
      if (f3>TMax) TMax=f3;
    }
    /*
    OutInt(0,0,cnt,5);
    OutInt(0,8,SkipInArch,5);
    Redraw();
    WaitReadKey();
    */
    cnt-=SkipInArch;
    if (cnt<MaxTrendCalcMeasurements) continue;
    if ((Tmin>=TMax)||(TMax-Tmin<10)) goto CalcTrend;
    
    Cor(ar, cnt, 2, &res);
    
    //OutFloat(0,0,res.ccr[0][0],11,5);
    //OutFloat(0,8,res.ccr[1][0],11,5);
    //Redraw();
    //WaitReadKey();
    
    if ((fabs(res.ccr[0][0] + res.ccr[1][0]) >= 0.8) || (fabs(res.ccr[0][0]) >= 0.5) || (fabs(res.ccr[1][0]) >= 0.5)) { //'If correlation exists
      f1 = atan(res.ccr[1][1] / res.ccr[0][1]) / Pi ;
      if (f1 >= 0) {
        if (res.ccr[1][1] < 0) f1 = f1 + 180;
      } else {
        if (res.ccr[1][1] > 0) f1 = f1 + 180;
      }
      PhaseRound(&f1);
      GammaTempPh[Side]=DivAndRound(f1,1.5,255);
      GammaTemp[Side] = DivAndRound(sqrt((res.ccr[1][1]*res.ccr[1][1]) + (res.ccr[0][1]*res.ccr[0][1])),0.002,255);
    }
    
    //'Trend-----------------------------------------------------------------
    
  CalcTrend:
    //  DateStart= DateToDateTime(GammaDate-av_t);
    DateStart= DateToDateTime(DateTimeToDate(DiagDate)-(av_t-1));
    
    FindMeasurements(DateStart,DiagDate,&StartNum,&EndNum,MinDaysForTrend,MesurementNum);
    if ((StartNum>0)&&(EndNum>0)&&(EndNum>=StartNum))
      cnt=EndNum-StartNum+1+UseOnLineData;
    else cnt=0;
    
    if (cnt<MaxTrendCalcMeasurements) continue;
    
    SkipInArch=0;
    for (i=StartNum;i<=EndNum;i++) {
      LoadData(Param,MeasurementNumToIndex(i),TempPage);
      SetPage(TempPage);
      if ( (Param->TrSideCount&(1<<Side)) && ( !(((int)Param->Error2)&(1<<(erSide1Critical-16+Side))) ) ) { //DE 3-1-16  exclude records with critical errors
        //'Gamma
        ar->data[0][i-StartNum-SkipInArch] = (float)Param->TrSide[Side].Gamma;//*0.01;
        //'Date
        f1=EncodeDate(Param->Year,Param->Month,Param->Day);
        f1+=(float)EncodeTime(Param->Hour,Param->Min,0)/(float)_TSecPerDay;
        ar->data[1][i-StartNum-SkipInArch] = f1;
      }  else SkipInArch++;
    }
    
    if (UseOnLineData) {
      SetPage(ParametersPage);
      f1=(float)Parameters->TrSide[Side].Gamma;//*0.01;
      f2=EncodeDate(Parameters->Year,Parameters->Month,Parameters->Day);
      f2+=(float)EncodeTime(Parameters->Hour,Parameters->Min,0)/(float)_TSecPerDay;
      SetPage(TempPage);
      //'Gamma
      ar->data[0][EndNum-StartNum+UseOnLineData-SkipInArch] = f1;
      //'Date
      ar->data[1][EndNum-StartNum+UseOnLineData-SkipInArch] = f2;
    }
    
    cnt-=SkipInArch;
    if (cnt<MaxTrendCalcMeasurements) continue;
    Cor(ar, cnt , 1, &res);
    
    //OutFloat(0,0,res.ccr[0][0],11,5);
    //OutFloat(0,8,res.ccr[0][1],11,5);
    //Redraw();
    //WaitReadKey();
    
    if (fabs(res.ccr[0][0]) >= 0.5)  // acm if correlation exists then (if positive trend store else set to zero), and actually, if no correlation trend remains at zero, so {} I added really don't care, as is else GammaTrend[Side]=0
    {if (res.ccr[0][1]>0) GammaTrend[Side] = DivAndRound(res.ccr[0][1] *0.01 * 365,1,255); else GammaTrend[Side] = 0;}                   //'Normalizing to 1 year//acm, 4-27 change from .2 to 1
    
  }
  //'' Output of diagnostics into sheet(1)----------------------------------
  //      GammaData = Array(GammaDate, GammaTemp, GammaTempPh, GammaTrend)
  return 2;
}



int DoCalcAvgZk(_TDateTime DiagDate,
                struct stZkTable *ZkTbl,
                unsigned int *ZkPercent,
                unsigned int MesurementNum,             //����� ������ ��� 0 ���� �� ��������
                char UseOnLineData)
{
  
#define MinMeasInZone MaxZkCalcMeasurements
  
  u32 Side;
  _TDateTime /*GammaDate,*/DateStart;
  unsigned int i,j,StartNum,EndNum,cnt,TstSetup=0;
#ifdef __emulator
  struct stInsulationParameters TParam;
  struct stInsulationParameters *Param=&TParam;
#else
#ifdef __arm
  struct stInsulationParameters *Param =TempParam;
#else
  struct stInsulationParameters *Param =(struct stInsulationParameters *) (0xFFFF-szInsulationParameters);
#endif
#endif
  u32 ZkPCount[MaxCorrelationSide][MaxLoads];
  unsigned int Year,Month,Day,Hour,Min,Sec;
  _TDate Date;
  _TTime Time;
  float f;
  float fAB,fBC,fCA;
  u32 tmpZkPercent[MaxCorrelationSide][MaxLoads][MaxBushingCountOnSide];
  
  
  memset((void *)&ZkPCount[0][0],0,sizeof(u32)*MaxCorrelationSide*MaxLoads);
  memset((void *)&tmpZkPercent[0][0][0],0,sizeof(u32)*MaxCorrelationSide*MaxLoads*MaxBushingCountOnSide);
  for (i=0;i<MaxLoads;i++) ZkTbl->ZkP[0][i][0]=ZkTbl->ZkP[0][i][1]=ZkTbl->ZkP[0][i][2]=0;
  for (j=0;j<MaxBushingCountOnSide;j++) ZkPercent[j]=0;
  
  if (MeasurementsInArchive+UseOnLineData<MaxZkCalcMeasurements) return 0;
  
  if ((!IsDate(Setup.InitialZkParameters.Year,Setup.InitialZkParameters.Month,Setup.InitialZkParameters.Day))||
      (!IsTime(Setup.InitialZkParameters.Hour,Setup.InitialZkParameters.Min,0))) return 0;
  
  for (Side=0;Side<MaxCorrelationSide;Side++) {
    
    LoadData(Param,MeasurementNumToIndex(1),TempPage);
    DateStart=EncodeDate(Param->Year,Param->Month,Param->Day);
    //    '' Find if data range is sufficient
    
    if (DateTimeToDate(DiagDate) - DateStart < Setup.ZkSetupInfo.ZkDays-1) continue;
    DateStart= DateToDateTime(DateTimeToDate(DiagDate)-(Setup.ZkSetupInfo.ZkDays-1));
    
    FindMeasurements(DateStart,DiagDate,&StartNum,&EndNum,MinDaysForTrend,MesurementNum);
    if ((StartNum>0)&&(EndNum>0)&&(EndNum>=StartNum))
      cnt=EndNum-StartNum+1+UseOnLineData;
    else cnt=0;
    
    if (cnt<MaxZkCalcMeasurements) continue;
    
    for (i=0;i<=EndNum-StartNum;i++) {
      LoadData(Param,MeasurementNumToIndex(StartNum+i),TempPage);
      cnt=0;
      if ((Param->SideToSideData[Side].ZkAmpl[BushingA]>0)&&(Param->SideToSideData[Side].ZkAmpl[BushingB]>0)) {  //DE 2-26-16 no change
        fAB=Param->SideToSideData[Side].ZkAmpl[BushingA]/Param->SideToSideData[Side].ZkAmpl[BushingB];
        cnt++;
      }
      if ((Param->SideToSideData[Side].ZkAmpl[BushingB]>0)&&(Param->SideToSideData[Side].ZkAmpl[BushingC]>0)) {  //DE 2-26-16 no change
        fBC=Param->SideToSideData[Side].ZkAmpl[BushingB]/Param->SideToSideData[Side].ZkAmpl[BushingC];
        cnt++;
      }
      if ((Param->SideToSideData[Side].ZkAmpl[BushingC]>0)&&(Param->SideToSideData[Side].ZkAmpl[BushingA]>0)) {  //DE 2-26-16 no change
        fCA=Param->SideToSideData[Side].ZkAmpl[BushingC]/Param->SideToSideData[Side].ZkAmpl[BushingA];
        cnt++;
      }
      if (cnt<3) continue;
      SetPage(TempPage);
      if (Param->Current[Side]>=30) {                                       //DE 2-26-16 no change
        if ((Param->Current[Side]>=0)&&(Param->Current[Side]<10)) {
          ZkTbl->ZkP[Side][0][BushingA]+=fAB;
          ZkTbl->ZkP[Side][0][BushingB]+=fBC;
          ZkTbl->ZkP[Side][0][BushingC]+=fCA;
          ZkPCount[Side][0]++;
        } else
          if ((Param->Current[Side]>=10)&&(Param->Current[Side]<20)) {
            ZkTbl->ZkP[Side][1][BushingA]+=fAB;
            ZkTbl->ZkP[Side][1][BushingB]+=fBC;
            ZkTbl->ZkP[Side][1][BushingC]+=fCA;
            ZkPCount[Side][1]++;
          } else
            if ((Param->Current[Side]>=20)&&(Param->Current[Side]<30)) {
              ZkTbl->ZkP[Side][2][BushingA]+=fAB;
              ZkTbl->ZkP[Side][2][BushingB]+=fBC;
              ZkTbl->ZkP[Side][2][BushingC]+=fCA;
              ZkPCount[Side][2]++;
            } else
              if ((Param->Current[Side]>=30)&&(Param->Current[Side]<40)) {
                ZkTbl->ZkP[Side][3][BushingA]+=fAB;
                ZkTbl->ZkP[Side][3][BushingB]+=fBC;
                ZkTbl->ZkP[Side][3][BushingC]+=fCA;
                ZkPCount[Side][3]++;
              } else
                if ((Param->Current[Side]>=40)&&(Param->Current[Side]<50)) {
                  ZkTbl->ZkP[Side][4][BushingA]+=fAB;
                  ZkTbl->ZkP[Side][4][BushingB]+=fBC;
                  ZkTbl->ZkP[Side][4][BushingC]+=fCA;
                  ZkPCount[Side][4]++;
                } else
                  if ((Param->Current[Side]>=50)&&(Param->Current[Side]<60)) {
                    ZkTbl->ZkP[Side][5][BushingA]+=fAB;
                    ZkTbl->ZkP[Side][5][BushingB]+=fBC;
                    ZkTbl->ZkP[Side][5][BushingC]+=fCA;
                    ZkPCount[Side][5]++;
                  } else
                    if ((Param->Current[Side]>=60)&&(Param->Current[Side]<70)) {
                      ZkTbl->ZkP[Side][6][BushingA]+=fAB;
                      ZkTbl->ZkP[Side][6][BushingB]+=fBC;
                      ZkTbl->ZkP[Side][6][BushingC]+=fCA;
                      ZkPCount[Side][6]++;
                    } else
                      if ((Param->Current[Side]>=70)&&(Param->Current[Side]<80)) {
                        ZkTbl->ZkP[Side][7][BushingA]+=fAB;
                        ZkTbl->ZkP[Side][7][BushingB]+=fBC;
                        ZkTbl->ZkP[Side][7][BushingC]+=fCA;
                        ZkPCount[Side][7]++;
                      } else
                        if ((Param->Current[Side]>=80)&&(Param->Current[Side]<90)) {
                          ZkTbl->ZkP[Side][8][BushingA]+=fAB;
                          ZkTbl->ZkP[Side][8][BushingB]+=fBC;
                          ZkTbl->ZkP[Side][8][BushingC]+=fCA;
                          ZkPCount[Side][8]++;
                        } else {
                          ZkTbl->ZkP[Side][9][BushingA]+=fAB;
                          ZkTbl->ZkP[Side][9][BushingB]+=fBC;
                          ZkTbl->ZkP[Side][9][BushingC]+=fCA;
                          ZkPCount[Side][9]++;
                        }
        
      }
    }
    if (UseOnLineData) {                                                            //DE 2-26-16 no change
      cnt=0;
      if ((Parameters->SideToSideData[Side].ZkAmpl[BushingA]>0)&&(Parameters->SideToSideData[Side].ZkAmpl[BushingB]>0)) {
        fAB=Parameters->SideToSideData[Side].ZkAmpl[BushingA]/Parameters->SideToSideData[Side].ZkAmpl[BushingB];
        cnt++;
      }
      if ((Parameters->SideToSideData[Side].ZkAmpl[BushingB]>0)&&(Parameters->SideToSideData[Side].ZkAmpl[BushingC]>0)) {
        fBC=Parameters->SideToSideData[Side].ZkAmpl[BushingB]/Parameters->SideToSideData[Side].ZkAmpl[BushingC];
        cnt++;
      }
      if ((Parameters->SideToSideData[Side].ZkAmpl[BushingC]>0)&&(Parameters->SideToSideData[Side].ZkAmpl[BushingA]>0)) {
        fCA=Parameters->SideToSideData[Side].ZkAmpl[BushingC]/Parameters->SideToSideData[Side].ZkAmpl[BushingA];
        cnt++;
      }
      if (cnt<3) continue;
      SetPage(TempPage);
      if (Parameters->Current[Side]>=30) {
        if ((Parameters->Current[Side]>=0)&&(Parameters->Current[Side]<10)) {
          ZkTbl->ZkP[Side][0][BushingA]+=fAB;
          ZkTbl->ZkP[Side][0][BushingB]+=fBC;
          ZkTbl->ZkP[Side][0][BushingC]+=fCA;
          ZkPCount[Side][0]++;
        } else
          if ((Parameters->Current[Side]>=10)&&(Parameters->Current[Side]<20)) {
            ZkTbl->ZkP[Side][1][BushingA]+=fAB;
            ZkTbl->ZkP[Side][1][BushingB]+=fBC;
            ZkTbl->ZkP[Side][1][BushingC]+=fCA;
            ZkPCount[Side][1]++;
          } else
            if ((Parameters->Current[Side]>=20)&&(Parameters->Current[Side]<30)) {
              ZkTbl->ZkP[Side][2][BushingA]+=fAB;
              ZkTbl->ZkP[Side][2][BushingB]+=fBC;
              ZkTbl->ZkP[Side][2][BushingC]+=fCA;
              ZkPCount[Side][2]++;
            } else
              if ((Parameters->Current[Side]>=30)&&(Parameters->Current[Side]<40)) {
                ZkTbl->ZkP[Side][3][BushingA]+=fAB;
                ZkTbl->ZkP[Side][3][BushingB]+=fBC;
                ZkTbl->ZkP[Side][3][BushingC]+=fCA;
                ZkPCount[Side][3]++;
              } else
                if ((Parameters->Current[Side]>=40)&&(Parameters->Current[Side]<50)) {
                  ZkTbl->ZkP[Side][4][BushingA]+=fAB;
                  ZkTbl->ZkP[Side][4][BushingB]+=fBC;
                  ZkTbl->ZkP[Side][4][BushingC]+=fCA;
                  ZkPCount[Side][4]++;
                } else
                  if ((Parameters->Current[Side]>=50)&&(Parameters->Current[Side]<60)) {
                    ZkTbl->ZkP[Side][5][BushingA]+=fAB;
                    ZkTbl->ZkP[Side][5][BushingB]+=fBC;
                    ZkTbl->ZkP[Side][5][BushingC]+=fCA;
                    ZkPCount[Side][5]++;
                  } else
                    if ((Parameters->Current[Side]>=60)&&(Parameters->Current[Side]<70)) {
                      ZkTbl->ZkP[Side][6][BushingA]+=fAB;
                      ZkTbl->ZkP[Side][6][BushingB]+=fBC;
                      ZkTbl->ZkP[Side][6][BushingC]+=fCA;
                      ZkPCount[Side][6]++;
                    } else
                      if ((Parameters->Current[Side]>=70)&&(Parameters->Current[Side]<80)) {
                        ZkTbl->ZkP[Side][7][BushingA]+=fAB;
                        ZkTbl->ZkP[Side][7][BushingB]+=fBC;
                        ZkTbl->ZkP[Side][7][BushingC]+=fCA;
                        ZkPCount[Side][7]++;
                      } else
                        if ((Parameters->Current[Side]>=80)&&(Parameters->Current[Side]<90)) {
                          ZkTbl->ZkP[Side][8][BushingA]+=fAB;
                          ZkTbl->ZkP[Side][8][BushingB]+=fBC;
                          ZkTbl->ZkP[Side][8][BushingC]+=fCA;
                          ZkPCount[Side][8]++;
                        } else {
                          ZkTbl->ZkP[Side][9][BushingA]+=fAB;
                          ZkTbl->ZkP[Side][9][BushingB]+=fBC;
                          ZkTbl->ZkP[Side][9][BushingC]+=fCA;
                          ZkPCount[Side][9]++;
                        }
        
      }
    }
    
    for (i=0;i<MaxLoads;i++) {
      if (ZkPCount[Side][i]>MinMeasInZone) {
        for (j=0;j<MaxBushingCountOnSide;j++) ZkTbl->ZkP[Side][i][j]/=ZkPCount[Side][i];
        
        DoUnPackDateTime(Setup.BaseLineZk.ZkPOnDate[Side][i],&Date,&Time);
        DecodeDate(Date,&Year,&Month,&Day);
        DecodeTime(Time,&Hour,&Min,&Sec);
        Year=Year%100+2000;
        if (IsDate(Year,Month,Day)&&(Hour<24)&&(Min<60)&&(Sec<60)) {
          for (j=0;j<MaxBushingCountOnSide;j++) {
            if ((Setup.BaseLineZk.ZkP[Side][i][j]>0.01)&&(Setup.BaseLineZk.ZkP[Side][i][j]<1000)) {
              f=ZkTbl->ZkP[Side][i][j]/Setup.BaseLineZk.ZkP[Side][i][j];
              if (f<1) f=1/f;
              f-=1.0;
              f*=10000;
            } else {
              f=0;
              if (ZkPCount[Side][i]>MaxZkCalcMeasurements) {
                // Save Zk Base Line
                Setup.BaseLineZk.ZkP[Side][i][j]=ZkTbl->ZkP[Side][i][j];
                Setup.BaseLineZk.ZkPOnDate[Side][i]=
                  DoPackDateTime( EncodeDate(2000+(DateTime.Year%100),DateTime.Month,DateTime.Day),
                                 EncodeTime(DateTime.Hour,DateTime.Min,0));
                Setup.BaseLineZk.ZkPCount[Side][i]=ZkPCount[Side][i];
                TstSetup=1;
                
              }
            }
            tmpZkPercent[Side][i][j]=f;
            if (f-tmpZkPercent[Side][i][j]>=0.5) tmpZkPercent[Side][i][j]++;
          }
        }
      } else {
        for (j=0;j<MaxBushingCountOnSide;j++) ZkTbl->ZkP[Side][i][j]=0;
        ZkPCount[Side][i]=0;
      }
    }
    for (j=0;j<MaxBushingCountOnSide;j++) {
      cnt=0;
      f=0;
      for (i=0;i<MaxLoads;i++) {
        if (tmpZkPercent[Side][i][j]>0) {
          f+=tmpZkPercent[Side][i][j];
          cnt++;
        }
      }
      f/=cnt;
      ZkPercent[j]=f;
      if (f-ZkPercent[j]>=0.5) ZkPercent[j]++;
    }
    
    
    
  }
  if (TstSetup) TestSetup();
  return 1;
}


















/*
#ifdef OldCalcTg


struct stvect {
float x,y;
};

struct stvect3 {
float a,b,c;
};

typedef struct stvect vect;
typedef struct stvect3 vect3;

vect Crd(vect a)
{vect tmpCrd;

tmpCrd.x = a.x * sin(a.y * Pi);
tmpCrd.y = a.x * cos(a.y * Pi);
return tmpCrd;
}

vect3 Crd3(vect a, float b)
{ vect3 tmpCrd3;
float ff,fi1,fi2,fi3;

a.y = a.y - b;
if (a.y < 0) a.y = a.y + 360;

if ((a.y>=0)&&(a.y<=120)) {
ff = a.y;
fi1 = ff * Pi;
fi2 = 60 * Pi;
fi3 = (120 - ff) * Pi;
tmpCrd3.a = a.x * sin(fi3) / sin(fi2);
tmpCrd3.b = a.x * sin(fi1) / sin(fi2);
tmpCrd3.c = 0;
} else

if ((a.y>120)&&(a.y<=240)) {
ff = a.y - 120;
fi1 = ff * Pi;
fi2 = 60 * Pi;
fi3 = (120 - ff) * Pi;
tmpCrd3.a = 0;
tmpCrd3.b = a.x * sin(fi3) / sin(fi2);
tmpCrd3.c = a.x * sin(fi1) / sin(fi2);
} else

if ((a.y>240)&&(a.y<=360)) {
ff = a.y - 240;
fi1 = ff * Pi;
fi2 = 60 * Pi;
fi3 = (120 - ff) * Pi;
tmpCrd3.a = a.x * sin(fi1) / sin(fi2);
tmpCrd3.b = 0;
tmpCrd3.c = a.x * sin(fi3) / sin(fi2);
}
return tmpCrd3;
}


vect add2(vect a, vect b)
{vect tmpadd2;

tmpadd2.x = a.x + b.x;
tmpadd2.y = a.y + b.y;
return tmpadd2;
}

vect add3(vect a,vect b,vect c)
{vect tmpadd3;

tmpadd3.x = a.x + b.x + c.x;
tmpadd3.y = a.y + b.y + c.y;
return tmpadd3;
}

vect ChSignPol(vect a)
{ vect tmpChSignPol;

tmpChSignPol.x = a.x;
tmpChSignPol.y = a.y + 180;
if (tmpChSignPol.y >= 360) tmpChSignPol.y = tmpChSignPol.y - 360;
return tmpChSignPol;
}

vect ChSignCrd(vect a)
{vect tmpChSignCrd;

tmpChSignCrd.x = -a.x;
tmpChSignCrd.y = -a.y;
return tmpChSignCrd;
}

vect Pol(vect a)
{ vect tmpPol;

tmpPol.x = sqrt(a.x * a.x + a.y * a.y);
if (fabs(a.y) > 0) {
tmpPol.y = atan(a.x / a.y) / Pi;
if (tmpPol.y < 0) tmpPol.y = tmpPol.y + 180;
if (a.x < 0)      tmpPol.y = tmpPol.y + 180;
} else {
if (a.x > 0) tmpPol.y = 90;
    else tmpPol.y = 270;
}
return tmpPol;
}


//void CalcTan(float Gamma, float Phase,float TempChange, float AAmpl, float BAmpl, float CAmpl)

//return
//0-no defect
//1-CA
//2-CA
//3-CA
//4-tgA
//5-tgA
//6-tgA

char CalcTan(struct stInsulationParameters *Param, TTgParameters *ResultParam, char StableRegime,char Side)

{vect IA0,IB0,IC0,G0,IA,IB,IC,G,I0,dTan0,I,dC,dTan,tempTan2,tempC2;
vect3 dTan3,dC3,tempTan3,tempC3;
float norm0,norm,tt,tc,TempChange;
char Result;

//Reading Initial data

IA0.x = (float)Setup.InitialParameters.TrSideParam[Side].PhaseAmplitude[BushingA]/10.0;
IA0.y = 0;
IB0.x = (float)Setup.InitialParameters.TrSideParam[Side].PhaseAmplitude[BushingB]/10.0;
IB0.y = 120;
IC0.x = (float)Setup.InitialParameters.TrSideParam[Side].PhaseAmplitude[BushingC]/10.0;
IC0.y = 240;


//    TanA0.x = InitialTgParameters.tgA/10000;
//    TanA0.y = 90;
//    TanB0.x = InitialTgParameters.tgB/10000;
//    TanB0.y = 210;
//    TanC0.x = InitialTgParameters.tgC/10000;
//    TanC0.y = 330;


//    CA0.x = InitialTgParameters.CA;
//    CA0.y = 0;
//    CB0.x = InitialTgParameters.CB;
//    CB0.y = 120;
//    CC0.x = InitialTgParameters.CC;
//    CC0.y = 240;

//Initial gamma unnormalized
G0.x = (float)Setup.InitialParameters.TrSideParam[Side].GammaAmplitude*0.01 * IA0.x / 100;
//Initial gamma phase
G0.y = (float)Setup.InitialParameters.TrSideParam[Side].GammaPhase*0.01;
//Reading temperature change
//    TempChange = Val(TextBox33.Value);
TempChange=Setup.InitialParameters.TrSideParam[Side].Temperature-(*Param).Temperature[Side];
//Normalizing
norm0 = 3 / (IA0.x + IB0.x + IC0.x);
IA0.x = IA0.x * norm0;
IB0.x = IB0.x * norm0;
IC0.x = IC0.x * norm0;
G0.x  = G0.x  * norm0;


//Reading Current data. This data must be first taken from data table by choosing the date
//Current Current
IA.x = (float)(*Param).TrSide[Side].PhaseAmplitude[BushingA]/10.0;
IA.y = 0;
IB.x = (float)(*Param).TrSide[Side].PhaseAmplitude[BushingB]/10.0;
IB.y = 120;
IC.x = (float)(*Param).TrSide[Side].PhaseAmplitude[BushingC]/10.0;
IC.y = 240;

//Current gamma unnormalized
G.x = (float)(*Param).TrSide[Side].Gamma*0.01 * IA.x / 100;
//Current gamma phase
G.y = (float)(*Param).TrSide[Side].GammaPhase*0.01;

//Normalizing
norm = 3 / (IA.x + IB.x + IC.x);
IA.x = IA.x * norm;
IB.x = IB.x * norm;
IC.x = IC.x * norm;
G.x  = G.x  * norm;


//Calc sum initial currents
IA0 = Crd(IA0);
IB0 = Crd(IB0);
IC0 = Crd(IC0);
I0 = add3(IA0, IB0, IC0);
I0 = Pol(I0);

//    TextBox24.Value = Format(I0.x, "0.00%")
//    TextBox26.Value = I0.y

I0 = Crd(I0);
//-I0 in Decart coordinates
I0 = ChSignCrd(I0);
G0 = Crd(G0);

dTan0 = add2(G0, I0);


dTan0 = Pol(dTan0);
//    TextBox25.Value = Format(dTan0.x, "0.00%")  'Format(xxx, "0.00%")
//    TextBox27.Value = dTan0.y


//Calc sum current currents
IA = Crd(IA);
IB = Crd(IB);
IC = Crd(IC);
I = add3(IA, IB, IC);

//Calc sum vector dC
dC = add2(I, I0);

//Calc sum vector tan delta from  data as G-G0-dC
dC = ChSignCrd(dC);

G0 = ChSignCrd(G0);

G = Crd(G);
dTan = add3(G, dC, G0);
dTan = Pol(dTan);
//    TextBox30.Value = Format(dTan.x, "0.00%")
//    TextBox31.Value = dTan.y

dC = ChSignCrd(dC);
dC = Pol(dC);
//    TextBox29.Value = Format(dC.x, "0.00%")
//    TextBox28.Value = dC.y


//Finding coordinates in the phase coordinates system

tt = dTan.y;
//    xt = dTan.x;

tc = dC.y;
//    xc = dC.x;

tempTan2 = dTan;
tempTan2 = ChSignPol(tempTan2);

tempC2 = dC;
tempC2 = ChSignPol(tempC2);



dTan3 = Crd3(dTan, 90);
dC3 = Crd3(dC, 0);

//Special case of negative Tan delta and Capacitance change with temperature
if (TempChange < -1) {
tempTan3 = Crd3(tempTan2, 90);

if ((tt>=255)&&(tt<=285)) {
dTan3.a = -tempTan3.a;
dTan3.b = dTan3.b - tempTan3.a;
dTan3.c = dTan3.c - tempTan3.a;
        }
if ((tt>=15)&&(tt<=45)) {
dTan3.b = -tempTan3.b;
dTan3.a = dTan3.a - tempTan3.b;
dTan3.c = dTan3.c - tempTan3.b;
        }

if ((tt>=135)&&(tt<=165)) {
dTan3.c = -tempTan3.c;
dTan3.b = dTan3.b - tempTan3.c;
dTan3.a = dTan3.a - tempTan3.c;
        }
    }

if (TempChange < -1) {
tempC3 = Crd3(tempC2, 0);

if ((tc>=165)&&(tc<=195)) {
dC3.a = -tempC3.a;
dC3.b = dC3.b - tempC3.a;
dC3.c = dC3.c - tempC3.a;
        }

if ((tc>=285)&&(tc<=315)) {
dC3.b = -tempC3.b;
dC3.a = dC3.a - tempC3.b;
dC3.c = dC3.c - tempC3.b;
        }

if ((tc>=45)&&(tc<=75)) {
dC3.c = -tempC3.c;
dC3.b = dC3.b - tempC3.c;
dC3.a = dC3.a - tempC3.c;
        }
    }


TempChange=dC3.a;
Result=1;

if (StableRegime) tt=Setup.GammaSetupInfo.GammaSideSetup[Side].C0[0];
    else tt=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[0];
ResultParam->C[0]=(int)(tt+(tt*dC3.a));

if (StableRegime) tt=Setup.GammaSetupInfo.GammaSideSetup[Side].C0[1];
    else tt=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[1];
ResultParam->C[1]=(int)(tt+(tt*dC3.b));
//    ResultParam->C[0]=(int)(dC3.a*1000);
//    ResultParam->C[1]=(int)(dC3.b*1000);
if (dC3.b>TempChange) {
TempChange=dC3.b;
Result=2;
    }
//    ResultParam->C[2]=(int)(dC3.c*1000);

if (StableRegime) tt=Setup.GammaSetupInfo.GammaSideSetup[Side].C0[2];
    else tt=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[2];
ResultParam->C[2]=(int)(tt+(tt*dC3.c));
if (dC3.c>TempChange) {
TempChange=dC3.c;
Result=3;
    }
//    ResultParam->tg[0]=(int)(dTan3.a*10000);
if (StableRegime) tt=Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[0];
    else tt=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[0];
ResultParam->tg[0]=(int)(tt+(dTan3.a*10000.0));
if (dTan3.a>TempChange) {
TempChange=dTan3.a;
Result=4;
    }
//    ResultParam->tg[1]=(int)(dTan3.b*10000);
if (StableRegime) tt=Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[1];
    else tt=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[1];
ResultParam->tg[1]=(int)(tt+(dTan3.b*10000.0));
if (dTan3.b>TempChange) {
TempChange=dTan3.b;
Result=5;
    }
//    ResultParam->tg[2]=(int)(dTan3.c*10000);
if (StableRegime) tt=Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[2];
    else tt=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[2];
ResultParam->tg[2]=(int)(tt+(dTan3.c*10000.0));
if (dTan3.c>TempChange) {
TempChange=dTan3.c;
Result=6;
    }

if (TempChange<0.001) {
Result=0;
    }

//    TextBox14.Value = Format(dTan3.a, "0.00%")
//    TextBox15.Value = Format(dTan3.b, "0.00%")
//    TextBox16.Value = Format(dTan3.c, "0.00%")
//    TextBox17.Value = Format(dC3.a, "0.00%")
//    TextBox18.Value = Format(dC3.b, "0.00%")
//    TextBox19.Value = Format(dC3.c, "0.00%")

//sss = MsgBox("amplitude delta Tan= " & dTan.x / norm2 * 100 & " % " & "  Angle= " & dTan.y)

//    'sss = MsgBox("amplitude delta C= " & dC.x / norm1 * 100 & " % " & "  Angle= " & dC.y)
return Result;
}

#endif
*/
/*
char DoDiagnosis(struct stInsulationParameters *Param,struct stDiagnosis *Diag, char StableRegime)
{ char Result=0,i,j;
unsigned int Year,Month,Day;


for (i=0;i<MaxTransformerSide;i++)
if (IsInitialParam) {
DecodeDate(Setup.GammaSetupInfo.GammaSideSetup[i].STABLEDate,&Year,&Month,&Day);
if (IsDate(Year,Month,Day)) {
Result=CalcTan(Param,&(Diag->TgParam[i]),StableRegime,i);
   } else {
Result=CalcTan(Param,&(Diag->TgParam[i]),STABLEDiagRegime,i);
   }

//   CalcNewTg(Param,&(Diag->TgParam[Side]),0,0,StableRegime);

} else {
for (j=0;j<MaxBushingCountOnSide;j++) {
(*Diag).TgParam[i].tg[j]=Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[j];
(*Diag).TgParam[i].C[j]=Setup.GammaSetupInfo.GammaSideSetup[i].C0[j];
    }

}

return Result;
}
*/

/*
//#define MaxTestMeasurement 6//20
#define MaxTestMeasurement 64
#define Avgs 3


//������ ��������� �������� � ������
#define DataBaseAddr (unsigned int)0x8000
#define DataCAddr (unsigned int)DataBaseAddr
#define DataCSize (unsigned int)(MaxTestMeasurement*MaxBushingCountOnSide*sizeof(int))
#define DatatgValueAddr (unsigned int)(DataBaseAddr+DataCSize)
#define DatatgValueSize (unsigned int)(MaxTestMeasurement*MaxBushingCountOnSide*sizeof(int))
#define DataTAddr (unsigned int)(DataBaseAddr+DataCSize+DatatgValueSize)
#define DataTSize (unsigned int)(MaxTestMeasurement*sizeof(char))
#define DataStabEndAddr (unsigned int)(DataBaseAddr+DataCSize+DatatgValueSize+DataTSize)
*/
/*
char RunStabilityTest(void)
{


signed char MsStatus;

//  int C[MaxTestMeasurement][MaxBushingCountOnSide];
static __no_init int C[MaxTestMeasurement][MaxBushingCountOnSide] @DataCAddr;

//  int tgValue[MaxTestMeasurement][MaxBushingCountOnSide];
static __no_init int tgValue[MaxTestMeasurement][MaxBushingCountOnSide] @DatatgValueAddr;

//  char T[MaxTestMeasurement];
static __no_init char T[MaxTestMeasurement] @DataTAddr;

//  float SumC[MaxBushingCountOnSide];
float SumC[MaxBushingCountOnSide];
float Sumtg[MaxBushingCountOnSide];
float DeltaC[MaxBushingCountOnSide];
float DeltaTg[MaxBushingCountOnSide];

char i,j,MsCount=0,OkMsCount=0;
TDateTime CurTime,AddTime,Time;
unsigned int ch=KB_NO,CurError;
float f;
char Cur=0,TMin,TMax,Tsr;
long l;

if (!IsInitialParam) {
ind_string[0]=chn;
ind_string[1]=cho;
ind_string[2]=chb;
ind_string[3]=chL;
ind_string[4]=chn;
ind_string[5]=chS;
Redraw();
WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
return 0;
  }

for (i=0;i<MaxTestMeasurement;i++) {
for (j=0;j<MaxBushingCountOnSide;j++) {
C[i][j]=tgValue[i][j]=0;
      }
T[i]=0;
 }

RunInpCAndTg();
ind_string[0]=chr;
ind_string[1]=chU;
ind_string[2]=chn;
ind_string[3]=chEmpty;
ind_string[4]=chEmpty;
ind_string[5]=chEmpty;
Redraw();
ch=KB_NO;
while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
ch=WaitReadKey();
if (ch==KB_ESC) return 0;
  }

CurTime=DateTimeNowNoSec()+60;
AddTime=TimeToDateTime(EncodeTime(0,5,0));

while ((MsCount<MaxTestMeasurement)&&(ch!=KB_ESC)) {

if (CurTime<DateTimeNow()) {

IndOff();
//           MsStatus=StartMeasure(svNoSave,Avgs);
MsStatus=StartMeasure(svSaveAllways,Avgs);

MsStatus=0;
MsCount++;
CurError=Error;
CurError&=~((1<<erStop)|(1<<erPause));
if ((MsStatus>=0)&&(CurError==0)) {
DoDiagnosis(&Parameters,&Diagnosis,STABLEDiagRegime);
for (j=0;j<MaxBushingCountOnSide;j++) {
C[OkMsCount][j]=Diagnosis.TgParam[HVSide].C[j];
tgValue[OkMsCount][j]=Diagnosis.TgParam[HVSide].tg[j];
              }
T[OkMsCount]=Parameters.TrSide[HVSide].Temperature;
OkMsCount++;
           }
CurTime=AddDateTime(CurTime,AddTime);
IndOn();
        }
Time=SubDateTime(CurTime,DateTimeNow());
ClearInd();
OutLong(0,MsCount,2,0);
Time=(((Time&0x0000FFFF)%43200)*2/10+1)*10;
OutLong(3,Time/60,1,0);
OutLong(4,Time%60,2,1);
SetPoint(3);
Redraw();
ch=ReadKey();
  }


if (OkMsCount>=3) {

for (j=0;j<MaxBushingCountOnSide;j++) {
SumC[j]=0;
Sumtg[j]=0;
for (i=0;i<OkMsCount;i++) {
SumC[j]+=C[i][j];
Sumtg[j]+=tgValue[i][j];
//            if (tg[i][j]<Sumtg[j]) Sumtg[j]=tg[i][j];
         }
SumC[j]/=OkMsCount;
Sumtg[j]/=OkMsCount;
DeltaC[j]=DeltaTg[j]=0;
for (i=0;i<OkMsCount;i++) {
//C
if (SumC[j]>0) {
f=fabs((float)C[i][j]/SumC[j]);
if (f<1.0) f=1.0/f;
f=(f-1)*100.0;
             } else {
f=0;
             }
if (f>DeltaC[j]) DeltaC[j]=f;
//tg
f=fabs((float)tgValue[i][j]-Sumtg[j])/100.0;
if (f>DeltaTg[j]) DeltaTg[j]=f;

         }
     }
TMin=255;
TMax=0;
f=0;
for (i=0;i<OkMsCount;i++) {
if (T[i]<TMin) TMin=T[i];
if (T[i]>TMax) TMax=T[i];
f+=T[i];
     }
f/=OkMsCount;
Tsr=f;
if (f-Tsr>=0.5) Tsr++;

ch=KB_NO;
while ((ch!=KB_ESC)&&(ch!=KB_ENTER)) {


ClearInd();
switch (Cur) {
       case 0:{
ind_string[0]=chC;
ind_string[1]=chA;
f=SumC[0]/10.0;
if (f<1000)
OutFloat(2,f,4,3,0);
            else {
l=f;
if (f-l>0.5) l++;
if (l>9999) l=9999;
OutLong(2,l,4,0);
            }
break;
       }
       case 1:{
ind_string[0]=chd;
ind_string[1]=chC;
ind_string[2]=chA;
OutFloat(3,DeltaC[0],3,1,0);
break;
       }
       case 2:{
ind_string[0]=chC;
ind_string[1]=chb;
f=SumC[1]/10.0;
if (f<1000)
OutFloat(2,f,4,3,0);
            else {
l=f;
if (f-l>0.5) l++;
if (l>9999) l=9999;
OutLong(2,l,4,0);
            }
break;
       }
       case 3:{
ind_string[0]=chd;
ind_string[1]=chC;
ind_string[2]=chb;
OutFloat(3,DeltaC[1],3,1,0);
break;
       }
       case 4:{
ind_string[0]=chC;
ind_string[1]=chC;
f=SumC[2]/10.0;
if (f<1000)
OutFloat(2,f,4,3,0);
            else {
l=f;
if (f-l>0.5) l++;
if (l>9999) l=9999;
OutLong(2,l,4,0);
            }
break;
       }
       case 5:{
ind_string[0]=chd;
ind_string[1]=chC;
ind_string[2]=chC;
OutFloat(3,DeltaC[2],3,1,0);
break;
       }
       case 6:{
ind_string[0]=cht;
ind_string[1]=chA;
OutFloat(2,Sumtg[0]/100,4,2,0);
break;
       }
       case 7:{
ind_string[0]=chd;
ind_string[1]=cht;
ind_string[2]=chA;
OutFloat(3,DeltaTg[0],3,1,0);
break;
       }
       case 8:{
ind_string[0]=cht;
ind_string[1]=chb;
OutFloat(2,Sumtg[1]/100,4,2,0);
break;
       }
       case 9:{
ind_string[0]=chd;
ind_string[1]=cht;
ind_string[2]=chb;
OutFloat(3,DeltaTg[1],3,1,0);
break;
       }
       case 10:{
ind_string[0]=cht;
ind_string[1]=chC;
OutFloat(2,Sumtg[2]/100,4,2,0);
break;
       }
       case 11:{
ind_string[0]=chd;
ind_string[1]=cht;
ind_string[2]=chC;
OutFloat(3,DeltaTg[2],3,1,0);
break;
       }
       case 12:{
ind_string[0]=cht;
ind_string[1]=ch0;
OutLong(2,-70+TMin,4,0);
break;
       }
       case 13:{
ind_string[0]=cht;
ind_string[1]=ch1;
OutLong(2,-70+Tsr,4,0);
break;
       }
       case 14:{
ind_string[0]=cht;
ind_string[1]=ch2;
OutLong(2,-70+TMax,4,0);
break;
       }
     }
Redraw();
ch=WaitReadKey();
switch (ch) {
       case (char)KB_LEFT: {
if (Cur) Cur--; else Cur=14;
break;
       }
       case (char)KB_RIGHT: {
if (Cur<14) Cur++; else Cur=0;
break;
       }
       case (char)KB_UP: {
if (Cur<14) Cur++; else Cur=0;
break;
       }
       case (char)KB_DOWN: {
if (Cur) Cur--; else Cur=14;
break;
       }

     }
     }

i=0;
ch=KB_NO;
ClearInd();
ind_string[0]=chS;
ind_string[1]=chA;
ind_string[2]=chU;
ind_string[3]=chE;
ind_string[4]=chMinus;
do {
if (i) ind_string[5]=chY;
        else  ind_string[5]=chn;
Redraw();
ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
if ((ch==KB_UP)||(ch==KB_LEFT)||(ch==KB_RIGHT))
if (i) i=0; else i=1;
     } while ((ch!=KB_ESC)&&(ch!=KB_ENTER)) ;
if ((ch==KB_ENTER)&&(i==1)) {
AddTgParameters.STABLEDate=DateTimeNowNoSec();
for (i=0;i<MaxBushingCountOnSide;i++) {
AddTgParameters.STABLEC[i]=(unsigned int)SumC[i];
AddTgParameters.STABLETg[i]=(int)Sumtg[i];

//Delta tg*100 ,%
AddTgParameters.STABLEDeltaTg[i]=(unsigned int)(DeltaTg[i]*100.0);
//Delta C*100 ,%
AddTgParameters.STABLEDeltaC[i]=(unsigned int)(DeltaC[i]*100.0);
AddTgParameters.STABLESaved=1;
        }
AddTgParameters.STABLETemperature=Tsr;
     }
  }



return 1;
}

*/
/*
//'***************************************
//'*** MulSum returns the multiplied sum o
//'     f all values in two exualy sized
//'*** one-dimensional arrays.
//'     ***************************************
float MulSum(float *dblArr1, float *dblArr2, char Count)
{   float Result;
char i;

Result = 0;
for (i=0;i<Count;i++)
Result=Result+(dblArr1[i]*dblArr2[i]);
return Result;
}

float Sum(float *dblArr, char Count)
{   float Result;
char i;

Result = 0;
for (i=0;i<Count;i++)
Result=Result + dblArr[i];
return Result;
}

void LeastSquare(float *dblX, float *dblY, float *dblK, float *dblM, char Count)
{
float dblSumX, dblSumY; // Holds sums For x and y
float dblSumXX,dblSumXY; // Holds sums For x^2 and xy
float dblCount;
float dblDelta;

dblSumX = Sum(dblX,Count);
dblSumY = Sum(dblY,Count);
dblSumXX = MulSum(dblX, dblX, Count);
dblSumXY = MulSum(dblX, dblY, Count);
dblCount = Count;
dblDelta = dblSumXX * dblCount - dblSumX * dblSumX;
*dblM = (dblSumXX * dblSumY - dblSumX * dblSumXY) / dblDelta;
*dblK = (dblSumXY * dblCount - dblSumX * dblSumY) / dblDelta;
}


void SwapF(float *f1, float *f2)
{ float f;
f=*f1;
*f1=*f2;
*f2=f;
}

char RunTemperatureTest(void)
{
#define tmpPi 0.0174532925199432957692369076848861

//������ ��������� �������� � ������
//#define DataBaseAddr (unsigned int)0x8000
#define DataFAddr    (unsigned int)DataStabEndAddr
#define DataFSize    (unsigned int)(MaxTestMeasurement*MaxBushingCountOnSide*sizeof(float))
#define DataFQAddr   (unsigned int)(DataFAddr+DataFSize)
#define DataFQSize   (unsigned int)(MaxTestMeasurement*MaxBushingCountOnSide*2*sizeof(float))
#define DataF0Addr   (unsigned int)(DataFAddr+DataFSize+DataFQSize)
#define DataF0Size   (unsigned int)(MaxBushingCountOnSide*sizeof(float))
#define DataAlfaAddr (unsigned int)(DataFAddr+DataFSize+DataFQSize+DataF0Size)
#define DataAlfaSize (unsigned int)(MaxBushingCountOnSide*sizeof(float))
#define DataY0Addr   (unsigned int)(DataFAddr+DataFSize+DataFQSize+DataF0Size+DataAlfaSize)
#define DataY0Size   (unsigned int)(MaxBushingCountOnSide*sizeof(float))
#define DataTg1Addr  (unsigned int)(DataFAddr+DataFSize+DataFQSize+DataF0Size+DataAlfaSize+DataY0Size)
#define DataTg1Size  (unsigned int)(MaxTestMeasurement*sizeof(float))
#define DataTempAddr (unsigned int)(DataFAddr+DataFSize+DataFQSize+DataF0Size+DataAlfaSize+DataY0Size+DataTg1Size)
#define DataTempSize (unsigned int)(MaxTestMeasurement*sizeof(float))
#define DataTgAddr   (unsigned int)(DataFAddr+DataFSize+DataFQSize+DataF0Size+DataAlfaSize+DataY0Size+DataTg1Size+DataTempSize)
#define DataTgSize   (unsigned int)(MaxBushingCountOnSide*MaxTestMeasurement*sizeof(float))

signed char MsStatus;
char i,j,MsCount=0,OkMsCount=0,Cur=0;
TDateTime CurTime,AddTime,Time;
unsigned int ch=KB_NO,CurError;
//  float tmpF[MaxBushingCountOnSide];


//  float F[MaxTestMeasurement][MaxBushingCountOnSide];
static __no_init float F[MaxTestMeasurement][MaxBushingCountOnSide] @DataFAddr;

//  float FQ[MaxTestMeasurement][MaxBushingCountOnSide*2];
static __no_init float FQ[MaxTestMeasurement][MaxBushingCountOnSide*2] @DataFQAddr;

//  float FF[MaxTestMeasurement][MaxBushingCountOnSide];
float FF;

//  float F0[MaxBushingCountOnSide];
static __no_init float F0[MaxBushingCountOnSide] @DataF0Addr;

//  float alfa[MaxBushingCountOnSide];
static __no_init float alfa[MaxBushingCountOnSide] @DataAlfaAddr;

//  float Y0[MaxBushingCountOnSide];
static __no_init float Y0[MaxBushingCountOnSide] @DataY0Addr;


//  float tg1[MaxTestMeasurement];
static __no_init float tg1[MaxTestMeasurement] @DataTg1Addr;

//  float temp[MaxTestMeasurement];
static __no_init float temp[MaxTestMeasurement] @DataTempAddr;

//  float tg[MaxTestMeasurement][MaxBushingCountOnSide];
static __no_init float tg[MaxTestMeasurement][MaxBushingCountOnSide] @DataTgAddr;

int  TMin,TMax,Tsr;
float f;

float Sum;
int   ind[3];       //sequense of phase based on FQ angles

SetPage(FlashBufPage);

for (i=0;i<MaxTestMeasurement;i++) {
for (j=0;j<MaxBushingCountOnSide;j++) {
tg[i][j]=0;
      }
temp[i]=0;
 }


RunInpCAndTg();
ind_string[0]=chr;
ind_string[1]=chU;
ind_string[2]=chn;
ind_string[3]=chEmpty;
ind_string[4]=chEmpty;
ind_string[5]=chEmpty;
Redraw();
ch=KB_NO;
while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
ch=WaitReadKey();
if (ch==KB_ESC) return 0;
  }


CurTime=DateTimeNowNoSec()+60;
AddTime=TimeToDateTime(EncodeTime(0,6,0));

while ((MsCount<MaxTestMeasurement)&&(ch!=KB_ESC)) {

if (CurTime<DateTimeNow()) {

IndOff();
//           MsStatus=StartMeasure(svNoSave,Avgs);
MsStatus=StartMeasure(svSaveAllways,Avgs);
SetPage(FlashBufPage);

MsStatus=0;

IndOn();
MsCount++;
CurError=Error;
CurError&=~((1<<erStop)|(1<<erPause));
if ((MsStatus>=0)&&(CurError==0)) {

temp[OkMsCount]=-70.0+Parameters.TrSide[HVSide].Temperature;

//              temp[OkMsCount]=70-(OkMsCount*2);
F[OkMsCount][0]=0;
CalcNewTg(&Parameters,&Diagnosis.TgParam[HVSide],&F[OkMsCount][1],&F[OkMsCount][2],NORMALDiagRegime);
//              DoDiagnosis(&Parameters,&Diagnosis);
for (j=0;j<MaxBushingCountOnSide;j++) {
//                 F[OkMsCount][j]=tmpF[j];
tg[OkMsCount][j]=Diagnosis.TgParam[HVSide].tg[j]/100.0;
              }


OkMsCount++;
           }
CurTime=AddDateTime(CurTime,AddTime);
        }
Time=SubDateTime(CurTime,DateTimeNow());
ClearInd();
OutLong(0,MsCount,2,0);
Time=(((Time&0x0000FFFF)%43200)*2/10+1)*10;
OutLong(3,Time/60,1,0);
OutLong(4,Time%60,2,1);
SetPoint(3);
Redraw();
ch=ReadKey();
  }

for (i=0;i<MaxBushingCountOnSide;i++)
F0[i] = atan(AddTgParameters.Tg0[i]/100.0 / 100.0) / tmpPi;

//Sort By temperature
for (i=0;i<OkMsCount-1;i++) {
for (j=i+1;j<OkMsCount;j++) {
if (temp[i]>temp[j]) {
//Swap
SwapF(&temp[i],&temp[j]);
for (MsCount=0;MsCount<MaxBushingCountOnSide;MsCount++) {
SwapF(&F[i][MsCount],&F[j][MsCount]);
SwapF(&tg[i][MsCount],&tg[j][MsCount]);
              }
          }
      }

  }


TMin=255;
TMax=-100;
f=0;
for (i=0;i<OkMsCount;i++) {
if (temp[i]<TMin) TMin=temp[i];
if (temp[i]>TMax) TMax=temp[i];
f+=temp[i];
  }
f/=OkMsCount;
Tsr=f;
if (f-Tsr>=0.5) Tsr++;

if (TMax-TMin>1) {

for (i=0;i<MaxBushingCountOnSide;i++) {
F0[i] = atan(AddTgParameters.Tg0[i]/100.0 / 100.0) / tmpPi;
     }


for (i=0;i<OkMsCount;i++) {
Sum=0;
for (j=0;j<MaxBushingCountOnSide;j++) {
Sum=Sum+F[i][j];
         }
//Quasi-Angles 0 - a, 1 - b. 2 - c
for (j=0;j<MaxBushingCountOnSide;j++){
FQ[i][j] = Sum-(3*F[i][j]);
FQ[i][j + 3] =FQ[i][j];
         }
     }


//This checks an order of phases based on FQ. ind(0) - index of the phase that has smallest TanDelta and so on.
for (j=0;j<OkMsCount;j++) {
ind[0]=0;
for (i=0;i<MaxBushingCountOnSide;i++) {
if (FQ[j][ind[0]] < FQ[j][i]) ind[0]=i;
         }
ind[1]=ind[0]+1;
if (FQ[j][ind[1]] < FQ[j][ind[1] + 1]) {
ind[2] = ind[1];
ind[1] = ind[2] + 1;
         } else {
ind[2] = ind[1] + 1;
         }
if (ind[1]>2) ind[1]=ind[1]-3;
if (ind[2]>2) ind[2]=ind[2]-3;

for (i=0;i<MaxBushingCountOnSide;i++) {
FF= F0[ind[0]] + F[j][i] - F[j][ind[0]];
if (FF < F0[i]) FF=F0[i];
tg[j][i] = tan(FF*tmpPi)*100.0;
F0[i] = FF;
         }
      }




for (i=0;i<MaxBushingCountOnSide;i++) {
for (j=0;j<OkMsCount;j++) {
tg1[j]=log(tg[j][i]);
          }

LeastSquare(temp,tg1,&alfa[i],&Y0[i],OkMsCount);
if (alfa[i]<0) alfa[i]=0;
Y0[i]=alfa[i]*20.0+Y0[i];
Y0[i]=exp(Y0[i]);

//          Y0[i] = exp(Y0[i]);
//       if (Y0[i]<0) Y0[i]=0;
      }
   } else {
for (i=0;i<MaxBushingCountOnSide;i++) {
alfa[i]=0;
Y0[i]=0;
      }
   }




ch=KB_NO;
while ((ch!=KB_ESC)&&(ch!=KB_ENTER)) {


ClearInd();
switch (Cur) {
       case 0:{
ind_string[0]=chA;
ind_string[1]=ch1;
OutFloat(2,alfa[0],4,1,0);
//            OutFloat(0,alfa[0],6,2,0);
break;
       }
       case 1:{
ind_string[0]=chA;
ind_string[1]=ch2;
OutFloat(2,Y0[0],4,2,0);
break;
       }
       case 2:{
ind_string[0]=chb;
ind_string[1]=ch1;
OutFloat(2,alfa[1],4,1,0);
//            OutFloat(0,alfa[1],6,2,0);
break;
       }
       case 3:{
ind_string[0]=chb;
ind_string[1]=ch2;
OutFloat(2,Y0[1],4,2,0);
break;
       }
       case 4:{
ind_string[0]=chC;
ind_string[1]=ch1;
OutFloat(2,alfa[2],4,1,0);
//            OutFloat(0,alfa[2],6,2,0);
break;
       }
       case 5:{
ind_string[0]=chC;
ind_string[1]=ch2;
OutFloat(2,Y0[2],4,2,0);
break;
       }
       case 6:{
ind_string[0]=cht;
ind_string[1]=ch0;
OutLong(2,TMin,4,0);
break;
       }
       case 7:{
ind_string[0]=cht;
ind_string[1]=ch1;
OutLong(2,Tsr,4,0);
break;
       }
       case 8:{
ind_string[0]=cht;
ind_string[1]=ch2;
OutLong(2,TMax,4,0);
break;
       }
     }
Redraw();
ch=WaitReadKey();
switch (ch) {
       case (char)KB_LEFT: {
if (Cur) Cur--; else Cur=8;
break;
       }
       case (char)KB_RIGHT: {
if (Cur<8) Cur++; else Cur=0;
break;
       }
       case (char)KB_UP: {
if (Cur<8) Cur++; else Cur=0;
break;
       }
       case (char)KB_DOWN: {
if (Cur) Cur--; else Cur=8;
break;
       }

     }
     }

i=0;
ch=KB_NO;
ClearInd();
ind_string[0]=chS;
ind_string[1]=chA;
ind_string[2]=chU;
ind_string[3]=chE;
ind_string[4]=chMinus;
do {
if (i) ind_string[5]=chY;
        else  ind_string[5]=chn;
Redraw();
ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
if ((ch==KB_UP)||(ch==KB_LEFT)||(ch==KB_RIGHT))
if (i) i=0; else i=1;
     } while ((ch!=KB_ESC)&&(ch!=KB_ENTER)) ;
if ((ch==KB_ENTER)&&(i==1)) {
AddTgParameters.HeatDate=DateTimeNowNoSec();
for (i=0;i<MaxBushingCountOnSide;i++) {
AddTgParameters.K[i]=alfa[i];
AddTgParameters.B[i]=Y0[i];
        }
AddTgParameters.MinTemperature1=(char)(70+TMin);
AddTgParameters.AvgTemperature1=(char)(70+Tsr);
AddTgParameters.MaxTemperature1=(char)(70+TMax);
     }


return 1;
}
*/

