#include "flash.h"
#include "defs.h"
#include "Graph.h"
#include "SysUtil.h"
#include "keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "ram.h"
#include "rtc.h"
#include "LCD.h"

#include "drvflash.h"
#include "defkb.h"

#include "flashdef.h"

#ifdef __emulator

#include <fcntl.h>
#include <io.h>
#include "debug.h"

extern int FlashFile;

#endif

char FatBuf[2048]; //DE new 1024// was 2048
char FlashBuf[528]; //DE new 264// was 528
char FlashBufDop[16]; //DE new 8// was 16

u16  FatPageSize = 512;         // Size of Fat Page

long  FlashPage           =-1; //����� ����� � ����

u16  FlashID             = 0;                      //DE new ??
u16  FatPageInBlock      = 1;                      //DE new 8 was 2 //����� ������� � ����� Fat The number of pages in the block Fat
u16  FatBlockLen = 512;
u16  FatBlocksInClaster  = 1;
u32 FatBlockCount = 8192;
//---
u32 FlashPageLen = 512;         // The size of the data for the page flash
u32 FlashPageDopLen = 16;      // For more information on the page flash
u32 FlashPagesInBlock = 1;    // Pages in block flash
u32 FlashAllBlocksCount = 8192;  // Total blocks in flash
u32 FlashBlockLen = 512;        // Block length in flash
u32 FlashFullPageLen = 528;     //
//---
u16	cfShift;              //DE new = 1, old = 2
u16	rfShift;              //DE new = 1, old = 2
u16	rfMask;               //DE new = 1, old = 3
u16	wfcShift;             //DE new = 1, old = 2
u16	wfcShiftMask;         //DE new = 0xFFFE, old = 0xFFFC
u16	wfcMask;              //DE new = 1, old = 3
u16	wfncShift;             //DE new = 1, old = 2
u16	wfncShiftMask;        //DE new = 0xFFFE, old = 0xFFFC
u16	wfncMask;             //DE new = 1, old = 3
//---
//	�������
#define	CMD_SERIAL_DATA_INPUT       (unsigned char)0x80
#define	CMD_READ_MODE_1             (unsigned char)0x00
#define	CMD_READ_MODE_2             (unsigned char)0x01
#define	CMD_READ_MODE_3             (unsigned char)0x50
#define	CMD_RESET                   (unsigned char)0xFF
#define CMD_AUTO_PROGRAM            (unsigned char)0x10
#define CMD_AUTO_BLOCK_ERASE_FIRST  (unsigned char)0x60
#define CMD_AUTO_BLOCK_ERASE_SECOND (unsigned char)0xD0
#define	CMD_STATUS_READ             (unsigned char)0x70
#define	CMD_ID_READ                 (unsigned char)0x90

//	��������� ������� � ����������
#define	DATA_get()	R_NAND_DATA		// ������ ������
#define	DATA_put(d)	R_NAND_DATA = d		// ������ ������
#define	ADDR_put(a)	R_NAND_AD = a		// ������ ������
#define	CMD_put(c)	R_NAND_CM = c		// �������� �������
#define	ALE_set()	R_SET_ALE = reg		// ��������� ������� ALE - ������ �������� ������
#define	ALE_clr()	R_CLR_ALE = reg		// ����� ������� ALE - ����� �������� ������
#define	SE_set()	R_SET_SE = reg		// ��������� ������� SE - ������ �������� �������
#define	SE_clr()	R_CLR_SE = reg		// ����� ������� SE - ���������� �������� �������
#define	WP_set()	R_SET_WP = reg		// ��������� ������� WP - ���������� ������
#define	WP_clr()	R_CLR_WP = reg		// ����� ������� WP - ������ ������
#define	CE_set()	R_SET_CE = 1		// ��������� ������� CE
#define	CE_clr()	R_CLR_CE = 0		// ����� ������� CE

/*
    ����� ����� ��������
      1 = Ok
*/

#define	FLASH_SPI_REGIME 3
#define	FLASH_SPI_SPEED  SPIFREQ_16000

void FlashCheckSetPageSizeLarge(void)
{
u8 i;
SPI_SetCS(SPI_CS_FLASH);
// SPI_WriteRead(0x57);             Deleted 2014-11-14 GM 0x57 old legacy code
SPI_WriteRead(0x57);              // Added 2014-11-14 GM   Replacement code
while (((i=SPI_WriteRead(0)) & 0x80)==0);
SPI_ClearCS();
if(i&0x01) //currently 256 bytes
{
  SPI_SetCS(SPI_CS_FLASH);
  SPI_WriteRead(0x3D);
  SPI_WriteRead(0x2A);
  SPI_WriteRead(0x80);
  SPI_WriteRead(0xA7); //send cmd sequence to switch to 264 bytes 
}
}

void STATUS_FLASH(void)
{
#ifndef __emulator
SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x57);
while ((SPI_WriteRead(0) & 0x80)==0);
SPI_ClearCS();
#endif
}

/*
    Read the device manufacturer and ID.
*/
//#pragma optimize=s 2  //DE 12-9-15 turn off optimization so Coeff are resolved
u32 FlashIDInline(void)
{
#ifndef __emulator
u8 t, type;
//u8 seq[64];
//u8 cnt;
//DisableLCD(0xFF);
SPI_ClearCS();
SPI_Prepare(FLASH_SPI_SPEED,FLASH_SPI_REGIME);
SPI_SetCS(SPI_CS_FLASH);

SPI_WriteRead(0x9F);          // 2014-11-14 GM Read the manufacture code to setup flash
type=SPI_WriteRead(0);                 //DE 12-9-14 Read the EDI string Device ID byte 1 == 0x1F is not used
type=SPI_WriteRead(0);                 // DE 12-9-14 Read the EDI string Device ID byte 2 == 0x28 is not used
type=SPI_WriteRead(0);                 // DE 12-9-14 Read the EDI string Device ID byte 3 == 0x00 is not used
type=SPI_WriteRead(0);                 // DE 12-9-14 Read the EDI string (Byte 4,bit 0) for new chip 0 = old chip, 1 = new chip
/*for(cnt=5;cnt<=64;cnt++)
{
  seq[cnt]=SPI_WriteRead(0);
}*/
SPI_ClearCS();

//SPI_WriteRead(0x57);          // 2014-11-14 Changed from 0x57 to new status read command 0xD7
//t=SPI_WriteRead(0);
//SPI_ClearCS();
//EnableLCD();
if(type == 0)  //old chip
{
  SPI_SetCS(SPI_CS_FLASH);
  SPI_WriteRead(0x57);          // 2014-11-14 Changed from 0x57 to new status read command 0xD7
  t=SPI_WriteRead(0);
  SPI_ClearCS();  
  t&=0x38;     // 0x38 - 00111000
  t=t >> 3;    // same as t == 7
}
else   //new chip
{
  t=2;
  FlashCheckSetPageSizeLarge();
}
return t;
#else
return 4;
#endif
}


char COMPARE_FLASH(u32 page)
{
#ifndef __emulator
char i;
  page <<= cfShift;
  DisableLCD(0xFF);
  SPI_Prepare(FLASH_SPI_SPEED,FLASH_SPI_REGIME);

  STATUS_FLASH();

  SPI_WriteRead(0x60);
  SPI_WriteRead((char)(page>>8));
  SPI_WriteRead((char)page);
  SPI_WriteRead(0x0);
  SPI_ClearCS();

  STATUS_FLASH();

  SPI_SetCS(SPI_CS_FLASH);
  SPI_WriteRead(0x57);
  while ((SPI_WriteRead(0x0)&0x80)==0)
      i=(SPI_WriteRead(0x0)>>6)&0x01;
  SPI_ClearCS();
  EnableLCD();

  return i;
#else
  return 1;
#endif
}


/*
    ������ ��������
      1 - Ok
*/
int FlashReadPageInline(u32 page,char* Buf,u16 off,u16 len)
{
#ifndef __emulator

unsigned int i;
u16 pg;

pg=(page<<rfShift);

DisableLCD(0xFF);
SPI_Prepare(FLASH_SPI_SPEED,FLASH_SPI_REGIME);

STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x53);
//i=page;
SPI_WriteRead((char)(pg>>8));   //AH
SPI_WriteRead((char)pg);   //AL
SPI_WriteRead(0);
SPI_ClearCS();

STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x54);
//SPI_WriteRead(0xD4);
SPI_WriteRead(0);
SPI_WriteRead((char)((off>>8) & rfMask));
SPI_WriteRead((char)off);
SPI_WriteRead(0);
for (i=0;i<len;i++)
  {
  Buf[i]=SPI_WriteRead(0);
  }
SPI_ClearCS();
EnableLCD();

return 1;
#else

if (FlashFile==0) return 0;
lseek(FlashFile,page*FlashFullPageLen+off,0);
return read(FlashFile, Buf, len)==len;

#endif
}

/*
 Write buffer to page with verify
    ������ �������� Entry page
      1 - Ok
steps
1. Copy page of flash to buffer 1
2. Write data to buffer 1
3. Write buffer 1 to flash without built-in erase
4. Read page to buffer 1
5. Read buffer 1 and verify all bytes match buffer 
*/
int FlashWritePageInline(u32 page,char* Buf,u16 off,u16 len)
{
#ifndef __emulator

u16 pg;
int res=1;

pg=(page << wfncShift); // << 2 = 4*page
DisableLCD(0xFF);
SPI_Prepare(FLASH_SPI_SPEED,FLASH_SPI_REGIME);

STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x53);  // 1. Main memory page to buffer 1 transfer
SPI_WriteRead((char)(pg>>8));   //AH MSB
SPI_WriteRead((char)pg);   //AL LSB
SPI_WriteRead(0);
SPI_ClearCS();

pg = (page << wfncShift) & wfcShiftMask; //4*page and clear lowest 2 bits
STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x84); // 2. Write to buffer 1
//i=page;
SPI_WriteRead((char)(pg>>8));   //AH
SPI_WriteRead( ((char)(pg)) | ((char)( (off>>8) & wfcMask)) );      //AL
SPI_WriteRead((char)off);
for (pg=0;pg<len;pg++) {SPI_WriteRead(Buf[pg]);}
SPI_ClearCS();

STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x88);  // 3. Buffer 1 to main memory page program without built-in erase
pg=(page << rfShift); // << 2 = divide by 4
SPI_WriteRead((char)(pg>>8));   //AH
SPI_WriteRead((char)pg);           //AL
SPI_WriteRead(0);
SPI_ClearCS();


//return COMPARE_FLASH(page);
//////////////////////
STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x53);  // 4. Main memory page to buffer 1 transfer
//i=page;
SPI_WriteRead((char)(pg>>8));   //AH
SPI_WriteRead((char)pg);   //AL
SPI_WriteRead(0);
SPI_ClearCS();

STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x54);  // 5. Buffer 1 read
//SPI_WriteRead(0xD4);
SPI_WriteRead(0);
SPI_WriteRead((char)((off>>8) & rfMask)); // AH
SPI_WriteRead((char)off);                 // AL
SPI_WriteRead(0);
for (pg=0;pg<len;pg++) {
    if (SPI_WriteRead(0)!=Buf[pg]) {res=0; break;} // Verify each byte
  }
SPI_ClearCS();
EnableLCD();

return res;
//return COMPARE_FLASH(page);

#else

if (FlashFile==0) return 0;
lseek(FlashFile,page*FlashFullPageLen+off,0);
return write(FlashFile, Buf, len)==len;

#endif
}

/*
    ������ ������� ��������
*/
char FlashReadStatusPage(u32 NStr)
{
char i;
if (FlashReadPageInline(NStr,&i,FlashPageLen+5,1)==1) {return i;}
else                                                  {return 0;}
}

/*
    ������� ����
      1 - Ok
*/
int FlashClearBlockInLine(u32 NStr)
{
#ifndef __emulator
u16 pg;

pg=(NStr << wfcShift);

DisableLCD(0xFF);
SPI_Prepare(FLASH_SPI_SPEED,FLASH_SPI_REGIME);

STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x81);  //DE 1-8-15 Page Erase cmd
SPI_WriteRead((char)(pg>>8));   //AH
SPI_WriteRead((char)pg);   //AL
SPI_WriteRead(0);
SPI_ClearCS();
EnableLCD();

FlashPage=-1;

return 1;

/*
unsigned int i;

i = NStr << rfShift;
DisableLCD(0xFF);
SPI_Prepare(FLASH_SPI_SPEED,FLASH_SPI_REGIME);

STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x53);
SPI_WriteRead((char)(i>>8));   //AH
SPI_WriteRead((char)i);   //AL
SPI_WriteRead(0);
SPI_ClearCS();

i = (NStr << wfcShift) & wfcShiftMask;
STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x84);
//i=page;
SPI_WriteRead((char)(i>>8));   //AH
SPI_WriteRead( ((char)(i)) | ((char)( (0>>8) & wfcMask)) );      //AL
SPI_WriteRead((char)0);
for (i=0;i<FlashPageLen;i++) {SPI_WriteRead(0xFF);}
SPI_ClearCS();

STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x88);
//i=page;
SPI_WriteRead((char)(NStr>>8));   //AH
SPI_WriteRead((char)NStr);           //AL
SPI_WriteRead(0);
SPI_ClearCS();

EnableLCD();
*/

/*
unsigned int i;

i = NStr << rfShift;
DisableLCD(0xFF);
SPI_Prepare(FLASH_SPI_SPEED,FLASH_SPI_REGIME);

STATUS_FLASH();


SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x53);
SPI_WriteRead((char)(i>>8));   //AH
SPI_WriteRead((char)i);   //AL
SPI_WriteRead(0);
SPI_ClearCS();

i = (NStr << wfcShift) & wfcShiftMask;
STATUS_FLASH();

SPI_SetCS(SPI_CS_FLASH);
//SPI_WriteRead(0x82);
SPI_WriteRead(0x84);
//i=page;
SPI_WriteRead((char)(i>>8));   //AH
SPI_WriteRead( ((char)(i)) | ((char)( (0>>8) & wfcMask)) );      //AL
SPI_WriteRead((char)0);
for (i=0;i<FlashPageLen;i++) {SPI_WriteRead(0xFF);}
SPI_ClearCS();

SPI_SetCS(SPI_CS_FLASH);
SPI_WriteRead(0x83);

SPI_WriteRead((char)(i>>8));   //AH
SPI_WriteRead((char)i);           //AL
SPI_WriteRead(0);
SPI_ClearCS();
EnableLCD();
*/

//return COMPARE_FLASH(NStr);

#endif
}


/***********************************************************************



***********************************************************************/

#define FlashTries 4	// ����� ������� ������

/*
    ������������� ������� ��� ������/������ 	
Universal function for the read / write
*/
int flash_write_read_block(f_type_write_read func,u32 NStr,char* Buf,u16 off,u16 len)
{
char j;
u16 i,k,ReadBytes,SeekStr,Seek;

if (FlashReadStatusPage(FlashPagesInBlock*((NStr*FatPageInBlock)/FlashPagesInBlock))!=0xFF) return -1;

NStr = NStr*FatPageInBlock+off/FlashPageLen;
SeekStr = off%FlashPageLen;

for (j=0;j<FlashTries;j++)
    {
    Seek=0;

    if ( SeekStr )
       {
       if (len<(FlashPageLen-SeekStr)) {ReadBytes=len;}
       else                            {ReadBytes=FlashPageLen-SeekStr;}
       if (func(NStr,&Buf[Seek],SeekStr,ReadBytes)!=1) continue;
       Seek+=ReadBytes;
       SeekStr=0;
       NStr++;
       }
    if (Seek>=len) return Seek;
    ReadBytes=FlashPageLen;
    k=(len-Seek)/FlashPageLen;
    for (i=0;i<k;i++)
       {
       if (func(NStr,&Buf[Seek],0,ReadBytes)!=1) continue;
       Seek+=ReadBytes;
       NStr++;
       }

    if (Seek>=len)
       {
       return Seek;
       }
    else
       {
       SeekStr=len-Seek;
       if (func(NStr,&Buf[Seek],0,SeekStr)!=1) continue;
       Seek+=SeekStr;
       }
    return Seek;
    }
return 0;
}

/*
    Initializing features of Flash memory device
*/
s32 InitFlash(int ForceScan,u16 aFatBlockLen)
{
   FlashID=FlashIDInline(); //FlashID=2; ////ag 3-14-16//FlashID=FlashIDInline();
   
   
   //DE 1-16-15 Setup for new flash with 264 byte page length
    if (FlashID==2)                                         // AT45DB641E New Flash 8 Mbytes
      {
      // Each record uses 1984 bytes of 8126216 bytes for max 4095 records
      cfShift = 1;                //Compare file address shift
      rfShift = 1;                //Read file address shift
      rfMask = 0x0001;            //Read file address mask
      wfcShift = 1;               //Write file with compare address shift
      wfcShiftMask = 0xFFFE;      //Write file with compare address mask
      wfcMask = 0x0001;           //Write file with compare address mask
      wfncShift = 1;              //Write file no compare address shift
      wfncShiftMask = 0xFFFE;     //Write file no compare address mask
      wfncMask = 0x0001;          //Write file no compare address mask
      FlashPageLen        = 248;  //#Bytes in one Flash page = 264 bytes - 16 bytes = 248 bytes
      FlashPageDopLen     = 16;   //#Bytes stored in File index Must be >= 16 bytes for 16 bytes of record cluster index data (TIndexClaster)
      FlashPagesInBlock   = 1;    ////ag 3-14-16//#Pages in a block of Flash
      FlashAllBlocksCount = 8191; //#FATBlocks in Flash chip Number of blocks in flash = 8650752 /(8*264) = 4096 blocks
      FatPageSize = 248;          //#Bytes in one page of FAT      
      }
   
   /*if (FlashID==4) // at45db081 1 Mbytes Legacy not tested
      {
      cfShift = 1;                //Compare file address shift
      rfShift = 1;                //Read file address shift
      rfMask = 0x0001;            //Read file address mask
      wfcShift = 1;               //Write file with compare address shift
      wfcShiftMask = 0xFFFE;      //Write file with compare address mask
      wfcMask = 0x0001;           //Write file with compare address mask
      wfncShift = 1;              //Write file no compare address shift
      wfncShiftMask = 0xFFFE;     //Write file no compare address mask
      wfncMask = 0x0001;          //Write file no compare address mask
      FlashPageLen        = 248;    //#Bytes in one Flash page
      FlashPageDopLen     = 16;     //#Bytes stored in File index
      FlashPagesInBlock   = 2;      //#Pages in a block of Flash
      FlashAllBlocksCount = 8192;   //#FATBlocks in Flash chip
      FatPageSize = 248;            //#Bytes in one page of FAT
      }*/ ////ag 3-14-16

   if (FlashID==6) // at45db321 4 Mbytes
      {
      // Each record uses 1024 bytes of 4194304 bytes for max 4096 records
      cfShift = 2;                //Compare file address shift
      rfShift = 2;                //Read file address shift
      rfMask = 0x0003;            //Read file address mask
      wfcShift = 2;               //Write file with compare address shift
      wfcShiftMask = 0xFFFC;      //Write file with compare address mask
      wfcMask = 0x0003;           //Write file with compare address mask
      wfncShift = 2;              //Write file no compare address shift
      wfncShiftMask = 0xFFFC;     //Write file no compare address mask
      wfncMask = 0x0003;          //Write file no compare address mask
      FlashPageLen        = 512;    //#Bytes in one Flash page
      FlashPageDopLen     = 16;     //#Bytes stored in File index Must be >= 16 bytes for 16 bytes of record cluster index data (TIndexClaster)
      FlashPagesInBlock   = 1;      //#Pages in a block of Flash
      FlashAllBlocksCount = 8192;   //#FATBlocks in Flash chip
      FatPageSize = 512;            //#Bytes in one page of FAT
      }

   if (FlashID==7) // at45db642 8 Mbytes not tested
      {
      cfShift = 3;                //Compare file address shift
      rfShift = 3;                //Read file address shift
      rfMask = 0x0007;            //Read file address mask
      wfcShift = 3;               //Write file with compare address shift
      wfcShiftMask = 0xFFF8;      //Write file with compare address mask
      wfcMask = 0x0007;           //Write file with compare address mask
      wfncShift = 3;              //Write file no compare address shift
      wfncShiftMask = 0xFFF8;     //Write file no compare address mask
      wfncMask = 0x0007;          //Write file no compare address mask
      FlashPageLen        = 1024;   //#Bytes in one Flash page
      FlashPageDopLen     = 32;     //#Bytes stored in File index Must be >= 16 bytes for 16 bytes of record cluster index data (TIndexClaster)
      FlashPagesInBlock   = 1;      //#Pages in a block of Flash
      FlashAllBlocksCount = 8192;   //#FATBlocks in Flash chip
      FatPageSize = 1024;           //#Bytes in one page of FAT
      }

    /*if(FlashID==2)
   {
     aFatBlockLen = 496; //DE 3-11-16 for new flash chip
   }*/
   if ((FlashID==4)||(FlashID==6)||(FlashID==7) || (FlashID==2))    //2014-11-15 GM Added ID 2 for new flash
      {
      FatPageInBlock      = FatPageSize*FlashPagesInBlock/FlashPageLen; ////ag 3-14-16//aFatBlockLen/FlashPageLen;
      FatBlockLen         = FatPageSize*FlashPagesInBlock; ////ag 3-14-16//aFatBlockLen;
      FatBlocksInClaster  = FlashPagesInBlock/FatPageInBlock;
      FatBlockCount       = FlashPagesInBlock/FatPageInBlock;
      FatBlockCount      *= FlashAllBlocksCount;      
      FlashFullPageLen = FlashPageLen+FlashPageDopLen;
      return 1;
      } else
      return 0;
}

/*
    �������� ���� ��� �������
*/
int FlashMarkBadBlock(u32 NStr)
{
char j;
char i=0xF0;

NStr = FlashPagesInBlock*((NStr*FatPageInBlock)/FlashPagesInBlock);
for (j=0;j<FlashTries;j++)
    {
    if (FlashWritePageInline(NStr,&i,FlashPageLen+5,1)!=1) continue;
    return 1;
    }
return 0;
}

/*
    ������ ����
*/
char FlashBadBlock(u32 NStr)
{
NStr=FlashPagesInBlock*((NStr*FatPageInBlock)/FlashPagesInBlock);
return (FlashReadStatusPage(NStr)!=0xFF);
}

/*
    FlashClearBlock - ������� ����

      ���������� ������ �����
      ���� �� ������� �������, �� �������� ��� ������
*/
int FlashClearBlock(u32 NStr,char test)
{
char i=0;
int res;
NStr *= FlashPagesInBlock;
switch (test)
  {
  case 1:
           res=FlashClearBlockInLine(NStr);
           break;
  case 2:
           FlashClearBlockInLine(NStr);
           res=FlashWritePageInline(NStr,&i,FlashPageLen+5,1);
           break;
  default:
           if (FlashReadStatusPage(NStr)!=0xFF) return -1;
           res=FlashClearBlockInLine(NStr);
           if (res!=1) FlashMarkBadBlock(NStr);

           break;
  }
return res;
}

/*
    ������� �������������� ������� �����
*/
int FlashReadBlockDop(u32 NStr,char* Buf,u16 off,u16 len)
{
char j;
if (FlashReadStatusPage(FlashPagesInBlock*((NStr*FatPageInBlock)/FlashPagesInBlock))!=0xFF) return -1;
NStr *= FatPageInBlock;
for (j=0;j<FlashTries;j++)
    {
    if (FlashReadPageInline(NStr,Buf,FlashPageLen+off,len)!=1) continue;
    return len;
    }
return 0;

}

/*
    ������� ���� ������
*/
int FlashReadBlock(u32 NStr,char* Buf,u16 off,u16 len)
{
return flash_write_read_block(FlashReadPageInline,NStr,Buf,off,len);
}

/*
    �������� ���� ������ (��� ����������� �����������)
*/
int FlashWriteBlockInline(u32 NStr,char* Buf,u16 off,u16 len)
{
return flash_write_read_block(FlashWritePageInline,NStr,Buf,off,len);
}

/*
    �������� ��������������� ������� ����� (��� ����������� �����������)
*/
int FlashWriteBlockDopInline(u32 NStr,char* Buf,u16 off,u16 len)
{
char j;

if (FlashReadStatusPage(FlashPagesInBlock*((NStr*FatPageInBlock)/FlashPagesInBlock))!=0xFF) return -1;
NStr *= FatPageInBlock;
for (j=0;j<FlashTries;j++)
    {
    if (FlashWritePageInline(NStr,Buf,FlashPageLen+off,len)!=1) continue;
    return len;
    }
return 0;
}

/*
    ���������� ���� ������ (��� ����������� �����������)
*/
int FlashWriteBlockRewrite(u32 NClaster,char* dest, char *source, u16 len)
{
u16 j;
int res=0;
char *addr,
#ifndef __arm
page,
#endif
c;
u32 NBlock;

res=FlashClearBlock(NClaster,0);
if (res<=0) return res;

#ifndef __arm
page=GetPage();
#endif
for (j=0;j<len;j++)
    {
#ifndef __arm
    SetPage(page);
#endif
    c=*(source++);
#ifndef __arm
    SetPage(FlashMemoryPage);
#endif
    *(dest++)=c;
    }

NBlock=FatBlocksInClaster*NClaster; //����� ������� ����� � ��������

res=FlashWriteBlockInline(NBlock,FlashBuf,0,FlashPageLen*FlashPagesInBlock); //�������� ������
if (res<=0) return res;
//�������� ������ �� ������
addr=FlashBufDop;
for (j=0;j<FatBlocksInClaster;j++)
    {
    res=FlashWriteBlockDopInline(NBlock+j,addr,0,FlashPageDopLen);
    if (res<=0) return res;
    addr += FlashPageDopLen;
    }
return res;
}

/*
    �������� ��������������� ������� ����� � ��������� (������������� �������)
*/
int FlashWriteBlockDop(u32 NStr,char* Buf,u16 off,u16 len)
{
return FlashWriteBlock(NStr,Buf,FatBlockLen+off,len);
}

/*
    Write block of data with verification (iniversal function)
*/
int FlashWriteBlock(u32 NStr,char* Buf,u16 off,u16 len)
{
#ifndef __arm
char page;
#endif
int res=0;
unsigned int i,seek;
unsigned long NBlock,NClaster;
char *addr,c;

#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif

if ((off+len)>(FatBlockLen+FlashPageDopLen)) goto FlashWriteBlockEnd; //check size of block to be written

NClaster=NStr/FatBlocksInClaster;   //claster number to be deleted
NBlock=FatBlocksInClaster*NClaster; //first block number in the claster

if (FlashReadStatusPage(FlashPagesInBlock*((NStr*FatPageInBlock)/FlashPagesInBlock))!=0xFF) goto FlashWriteBlockEnd;

if (FlashPage!=NBlock) // check cash
   {
   res=FlashReadBlock(NBlock,FlashBuf,0,FlashPageLen*FlashPagesInBlock); //read data
   if (res<=0) goto FlashWriteBlockEnd;
   FlashPage=NBlock;
   }
   //read information about blocks
   addr=FlashBufDop;
   for (i=0;i<FatBlocksInClaster;i++)
      {
      res=FlashReadBlockDop(NBlock+i,addr,0,FlashPageDopLen);
      if (res<=0) goto FlashWriteBlockEnd;
      addr += FlashPageDopLen;
      }

if (off>=FatBlockLen) //write information about blocks
   {
   addr=&(FlashBufDop[(NStr-NBlock)*FlashPageDopLen]);
   for (i=0;i<FlashPageDopLen;i++)
     {
     SetPage(page); c=Buf[i];
     SetPage(FlashMemoryPage);
     if ( (~(*(addr++))) & c )
        {
        SetPage(page);
        res=FlashWriteBlockRewrite(NClaster,&(FlashBufDop[(NStr-NBlock)*FlashPageDopLen]), Buf, len);
        if (res>0) res = len;
        else FlashMarkBadBlock(NClaster);
        goto FlashWriteBlockEnd;
        }
     }

   SetPage(page);
   res=FlashWriteBlockDopInline(NStr,Buf,off-FatBlockLen,len);
   }
else //write data in the block
   {
   seek=(NStr-NBlock)*FatBlockLen+off;
   addr=&(FlashBuf[seek]);
   for (i=seek;i<seek+len;i++)
     {
     if (*(addr++)!=0xFF)
        {
        SetPage(page);
        res=FlashWriteBlockRewrite(NClaster,&(FlashBuf[seek]), Buf, len);
        if (res>0) res = len;
        else FlashMarkBadBlock(NClaster);
        goto FlashWriteBlockEnd;
        }
     }
   addr=&(FlashBuf[seek]);
   for (i=0;i<len;i++)
       {
       SetPage(page); c=Buf[i];
       SetPage(FlashMemoryPage); *(addr++)=c;
       }
   SetPage(page);
   res=FlashWriteBlockInline(NStr,Buf,off,len);
   if (res<=0) FlashMarkBadBlock(NStr);
   }
FlashWriteBlockEnd:
SetPage(page);
return res;
}


/*
    ������������� ���� ������
*/
int FlashRewriteBlock(u32 NClaster)
{
#ifndef __arm
char page;
#endif
int res=0;
u16 i,j;
u32 NBlock;
char *addr,*buf,flag=0;

#ifndef __arm
page=GetPage();
SetPage(FlashMemoryPage);
#endif

if (NClaster>=FlashAllBlocksCount) {goto FlashRewriteBlockEnd;}

NBlock=FatBlocksInClaster*NClaster; //����� ������� ����� � ��������

if (FlashBadBlock(NBlock)) {goto FlashRewriteBlockEnd;}


if (FlashPage!=NBlock) // ��������� ���
   {
   res=FlashReadBlock(NBlock,FlashBuf,0,FlashPageLen*FlashPagesInBlock); //������� ������
   if (res<=0) {goto FlashRewriteBlockEnd;}
   FlashPage=NBlock;
   }

//������� ���������� � ������
buf=FlashBuf;
addr=FlashBufDop;
for (i=0;i<FatBlocksInClaster;i++)
    {
    res=FlashReadBlockDop(NBlock+i,addr,0,FlashPageDopLen);
    if (res<=0) {goto FlashRewriteBlockEnd;}
    if (addr[0]==0x00)
       {
       for (j=0;j<FlashPageDopLen;j++)
           { *(addr++)=0xFF;}
       for (j=0;j<FlashPageLen;j++)
           { *(buf++)=0xFF;}
       flag=1;
       }
    else
       {
       addr += FlashPageDopLen;
       buf  += FlashPageLen;
       }
    }
if (flag)
   {

   res=FlashClearBlock(NClaster,0); //������� ������
   if (res<=0) {goto FlashRewriteBlockEnd;}

   res=FlashWriteBlockInline(NBlock,FlashBuf,0,FlashPageLen*FlashPagesInBlock); //�������� ������
   if (res<=0) {goto FlashRewriteBlockEnd;}

   //�������� ������ �� ������
   addr=FlashBufDop;
   for (j=0;j<FatBlocksInClaster;j++)
       {
       res=FlashWriteBlockDopInline(NBlock+j,addr,0,FlashPageDopLen);
       if (res<=0) return res;
       addr += FlashPageDopLen;
       }

   res=1;
   }

FlashRewriteBlockEnd:
SetPage(page);
return res;
}

/*
    ����� ������� ������
*/
u16 FlashBadBlockCount(void){
u16 n=0,i;

for (i=0;i<FlashAllBlocksCount;i++)
    if (FlashReadStatusPage(i*FlashPagesInBlock)!=0xFF)n++;

return n;
}


/*********************************************************************************


    ������ ��� ������


*********************************************************************************/
ROM char TestFlashLowInit[] = "Init Flash -";
ROM char TestFlashLowName[] = "TC58256";
ROM char TestFlashLowNo[]   = "no flash";

void TestFlashLow(void)
{
long i,j,n,m,k,count;
char c;
_TDateTime Time1,Time2;

SetPage(0);
SetFont(Font8x6);
ClearLCD();
OutString(0,0,TestFlashLowInit);
Redraw();

InitFlash(0,(unsigned int)1024);

if (FlashID==0x9875) OutString(6*13,0,TestFlashLowName);
else                 OutString(6*13,0,TestFlashLowNo);
Redraw();

WaitReadKey();

InitFlash(0,(unsigned int)1024);

ClearLCD();
OutInt(0,56,FatPageInBlock,2);
OutInt(3*6,56,FatBlockLen,5);
OutFloat(9*6,56,FatBlockCount,5,0);
OutFloat(15*6,56,FatBlocksInClaster,5,0);

//AutoRepeatEnable();
OutString1(0,8,"Bad block"); Redraw();
n=0;
for (i=0;i<FlashAllBlocksCount;i++)
    {
    if (FlashReadStatusPage(i*FlashPagesInBlock)!=0xFF)
       {
       OutInt(127-6*5,n*8,i,5);
       Redraw();
       n++;
       }
    /*
    if (i==0)
       {
       FlashClearBlock(i,1);
       }
    */
    /*
    if ((i==1525)||(i==1725)||(i==1726))
       {
       c=0;
       FlashWritePageInline(i*FlashPagesInBlock,&c,FlashPageLen+5,1);
       }
    */
    }
OutInt(10*6,8,n,5);
Redraw();
WaitReadKey();

ClearLCD();
OutInt(0,56,FatPageInBlock,2);
OutInt(3*6,56,FatBlockLen,5);
OutFloat(9*6,56,FatBlockCount,5,0);
OutFloat(15*6,56,FatBlocksInClaster,5,0);

count=FatBlockCount;

OutString1(0,0,"Clear"); Redraw();
DateTimeNow(&Time1);
n=0;
for (i=0;i<FatBlockCount/FatBlocksInClaster;i++)
  {
  if (FlashClearBlock(i,0)!=1)
     {
     n++;
     }
  }
DateTimeNow(&Time2);
Time2 -= Time1;
TimeToStr(Time2, OutStr);
OutString1(7*6,0,OutStr);
OutInt(16*6,0,n,5);
Redraw();

OutString1(0,8,"Verify");
Redraw();
DateTimeNow(&Time1);
k=FatBlockCount/64;
n=0; m=0;
for (i=0;i<count;i++)
    {
    c=1;
    if (FlashReadBlockDop(i,(char *)0x8000,0,FlashPageDopLen)!=FlashPageDopLen)
       {c=0; n++;}
    else
       for (j=0;j<FlashPageDopLen;j++)
           if (*((char *)0x8000+j)!=0xFF) {c=0; m++; break;}

    if (c)
       {
       if (FlashReadBlock(i,(char *)0x8000,0,FatBlockLen)!=FatBlockLen)
          {n++;}
       else
          for (j=0;j<FatBlockLen;j++)
              if (*((char *)0x8000+j)!=0xFF)
                 {m++; break;}
       }

    if ((i+1)%k==0)
       {
       OutInt(7*6,8,i+1,5);
       Redraw();
       }
    }
DateTimeNow(&Time2);
Time2 -= Time1;
TimeToStr(Time2, OutStr);
OutString1(7*6,8,OutStr);
OutFloat(16*6,8,n,5,0);
Redraw();

OutString1(0,16,"WriteD");
Redraw();
DateTimeNow(&Time1);
k=FatBlockCount/64;
n=0;
for (i=0;i<count;i++)
    {
    for (j=0;j<FlashPageDopLen;j++)
        {*((char *)0x8000+j)=(char)(i+j);}
    *((char *)0x8000+5)=0xFF;
    if (FlashWriteBlockDop(i,(char *)0x8000,0,FlashPageDopLen)!=FlashPageDopLen)
       {n++;}
    if ((i+1)%k==0)
       {OutInt(7*6,16,i+1,5); Redraw();}
    }
DateTimeNow(&Time2);
Time2 -= Time1;
TimeToStr(Time2, OutStr);
OutString1(7*6,16,OutStr);
OutFloat(16*6,16,n,5,0);
Redraw();

OutString1(0,24,"Write");
Redraw();
DateTimeNow(&Time1);
k=FatBlockCount/64;
n=0;

for (i=0;i<count;i++)
    {
    for (j=0;j<FatBlockLen;j++)
        {*((char *)0x8000+j)=(char)(i+j+1);}

    if (FlashWriteBlock(i,(char *)0x8000,0,FatBlockLen)!=FatBlockLen)
       {n++;}

    if ((i+1)%k==0)
       {
       OutFloat(7*6,24,i+1,5,0);
       Redraw();
       }
    }

DateTimeNow(&Time2);
Time2 -= Time1;
TimeToStr(Time2, OutStr);
OutString1(7*6,24,OutStr);
OutFloat(16*6,24,n,5,0);
Redraw();



OutString1(0,32,"Read");
Redraw();
DateTimeNow(&Time1);
k=FatBlockCount/64;
m=0; n=0;
for (i=0;i<count;i++)
    {
    c=1;
    if (FlashReadBlockDop(i,(char *)0x8000,0,FlashPageDopLen)!=FlashPageDopLen)
       {c=0; n++;}
    else
    for (j=0;j<FlashPageDopLen;j++)
        {
        if (*((char *)0x8000+j)!=(char)(i+j))
           {if (j==5) continue; c=0; m++; break;}
        }

    if (c)
       {
       if (FlashReadBlock(i,(char *)0x8000,0,FatBlockLen)!=FatBlockLen)
          {n++;}
       else
       for (j=0;j<FatBlockLen;j++)
           {
           if (*((char *)0x8000+j)!=(char)(i+j+1))
              {m++; break;}
           }
       }


    if ((i+1)%k==0)
       {OutFloat(7*6,32,i+1,5,0); Redraw();}
    }

DateTimeNow(&Time2);
Time2 -= Time1;
TimeToStr(Time2, OutStr);
OutString1(7*6,32,OutStr);
OutFloat(16*6,32,n+m,5,0);
Redraw();
WaitReadKey();
}

