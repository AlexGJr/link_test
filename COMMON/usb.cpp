
#include "BoardAdd.h"
#include "TypesDef.h"
#include "USB.h"
#include "Keyboard.h"
#include "WatchDog.h"
#include "SysUtil.h"
#include <stdlib.h>
#include "defs.h"
#include "LCD.h"
#include "UART.h"

#ifndef AT91RM9200
#include "EFC.h"
#endif


#define AT91C_EP_CTRL 0
#define AT91C_EP_IN  1
#define AT91C_EP_OUT 2

#define AT91C_EP_IN_OUT_SIZE 0x40

#define FW_DS_RX 0x01
#define FW_DS_TX 0x02

// acm, 4-12-10, see if this cause of problem with large record download, change from 4000 to ffff -> no, but
// discovered by unplugging/plugging in USB cable transfer continued.  Boot time maybe increased by incrementing this timeout, will research later.
#define USB_TIMEOUT 0x4000 // 4mS for RAM

__no_init volatile u16 USBCurRead @ "USBDATA";
__no_init volatile u8* USBReadAddr @ "USBDATA";
__no_init u8 USBReadData[USBDataBlock] @ "USBDATA";

__no_init volatile u16 USBWriteCount @ "USBDATA";
__no_init volatile u16 USBCurWrite @ "USBDATA";
__no_init volatile u8* USBWriteAddr @ "USBDATA";

//0 - stop, 1 - start
__no_init volatile u8 USBStarted @ "USBDATA";




#define MIN(a, b) (((a) < (b)) ? (a) : (b))


/*Descriptor Types*/
#define DEVICE                      0x01
#define CONFIGURATION               0x02
#define XSTRING                     0x03
#define INTERFACE                   0x04
#define ENDPOINT                    0x05
#define HID                         0x21
#define HIDREPORT                   0x22
#define HIDPHYSICAL                 0x23

/*Class codes*/
#define USB_DEVICE_CLASS_COMMUNICATIONS     0x02
#define USB_DEVICE_CLASS_STORAGE            0x08
#define USB_DEVICE_CLASS_VENDOR_SPECIFIC    0xFF
/*SubClass codes*/
#define NOSUBCLASS                  0x00

// interface protocol
#define USB_PROTOCOL_CODE_CBI0								0x00
#define USB_PROTOCOL_CODE_CBI1								0x01
#define USB_PROTOCOL_CODE_BULK								0x50

#define USB_ENDPOINT_TYPE_BULK                    0x02


const char devDescriptor[] @ "USBCONST" = {
  18,                /*length of this desc.    */
  DEVICE,            /*DEVICE descriptor       */
  0x10,0x01,         /*spec rev level (BCD)    */
  0x02,              /*device class            */
  0x00,              /*device subclass         */
  0x00,              /*device protocol         */
  0x08,              /*max packet size         */
  0x41,0x04,         /*vendor ID    */
  0xC9,0x51,         /*National's product ID   */
  0x00,0x01,         /*National's revision ID  */
  0,
  0,
  0,                 /*index of ser. # string  */
  0x01               /*number of configs.      */
};


/* Definitions for bits in the bmAttributes field of  */
/*   (Table 9-10) Standard Configuration Descriptor   */
/* Reserved (reset to zero) */                    /* D4...D0 */
#define USB_CONFIG_BUS_NOWAKEUP           0x80 /* D5=0 + D6=0 + D7=1 */
#define USB_CONFIG_SELF_NOWAKEUP          0xC0 /* D5=0 + D6=1 + D7=1 */
#define USB_CONFIG_BUS_WAKEUP             0xA0 /* D5=1 + D6=0 + D7=1 */
#define USB_CONFIG_SELF_WAKEUP            0xE0 /* D5=1 + D6=1 + D7=1 */
/* Reserved (set to one) */                       /*   D7    */


// 9.6.2 Configuration
const char cfgDescriptor[] @ "USBCONST" = {
  9,                 /*length of this desc.    */
  CONFIGURATION,     /*CONFIGURATION descriptor*/
  37,0x00,           /*total length returned   */
  0x01,              /*number of interfaces    */
  0x01,              /*# of this config   */
  0,
  USB_CONFIG_SELF_NOWAKEUP,        /*attr.: self powered     */
  50,                /*max power (100 mA)      */
  // 9.6.3 Interface
  9,        /*length of this desc.    */
  INTERFACE,         /*INTERFACE descriptor    */
  0x00,              /*interface number        */
  0x00,              /*alternate setting       */
  0x02,              /*# of (non 0) endpoints  */
  USB_DEVICE_CLASS_VENDOR_SPECIFIC,          /*interface class         */
  NOSUBCLASS,        /*interface subclass      */
  USB_PROTOCOL_CODE_CBI0,              /*interface protocol      */
  0,
  
  /* Header Functional Descriptor */
  0x05, // bFunction Length
  0x24, // bDescriptor type: CS_INTERFACE
  0x00, // bDescriptor subtype: Header Func Desc
  0x10, // bcdCDC:1.1
  0x01,
  
  // 9.6.4 Endpoint
  /*The WDM driver for this board references the endpoints by 'pipe'    */
  /*number: 0, 1, 2 et al in order below:                               */
  
  /*Pipe 0 (endpoint 1)                          */
  7,        /*length of this desc.    */
  ENDPOINT,          /*ENDPOINT descriptor     */
  0x81,              /*address (IN to host TX)            */
  USB_ENDPOINT_TYPE_BULK,              /*attributes  (BULK) */
  AT91C_EP_IN_OUT_SIZE,0x00,         /*max packet size (64)    */
  0x00              /*interval (ms)           */
    ,
    /*Pipe 1 (endpoint 2)                          */
    7,        /*length of this desc.    */
    ENDPOINT,          /*ENDPOINT descriptor     */
    0x02,              /*address (OUT from host RX)            */
    USB_ENDPOINT_TYPE_BULK,              /*attributes  (BULK) */
    AT91C_EP_IN_OUT_SIZE,0x00,         /*max packet size (64)    */
    0x00             /*interval (ms)           */
};

/* USB standard request code */
#define STD_GET_STATUS_ZERO           0x0080
#define STD_GET_STATUS_INTERFACE      0x0081
#define STD_GET_STATUS_ENDPOINT       0x0082

#define STD_CLEAR_FEATURE_ZERO        0x0100
#define STD_CLEAR_FEATURE_INTERFACE   0x0101
#define STD_CLEAR_FEATURE_ENDPOINT    0x0102

#define STD_SET_FEATURE_ZERO          0x0300
#define STD_SET_FEATURE_INTERFACE     0x0301
#define STD_SET_FEATURE_ENDPOINT      0x0302

#define STD_SET_ADDRESS               0x0500
#define STD_GET_DESCRIPTOR            0x0680
#define STD_SET_DESCRIPTOR            0x0700
#define STD_GET_CONFIGURATION         0x0880
#define STD_SET_CONFIGURATION         0x0900
#define STD_GET_INTERFACE             0x0A81
#define STD_SET_INTERFACE             0x0B01
#define STD_SYNCH_FRAME               0x0C82


#define DISABLE_ALL_IT 0x00002F0F
#define STATUS_ALL_IT  0x00003F0F
#define CLEAR_ALL_IT   0x00003F00



__no_init u32 currentReceiveBank @ "USBDATA";
__no_init volatile u8 currentConfiguration @ "USBDATA";
__no_init volatile u8 NeedTX @ "USBDATA";
__no_init volatile u32 LastRXTime @ "USBDATA";



#pragma diag_suppress=Pa082

static void RxTxControl(void) @ "USBCODE";

//__interwork void __USB_segment_init(void) @ "USBCODE";  // acm, roll our own segment init, link all in USBCODE, defined at bottom
//ag commented out to align with PDM
//__interwork void __USB_segment_init(void) @ "USBCODE";  // acm, roll our own segment init, link all in USBCODE, defined at bottom /2.05ag

void fw_busReset(void) @ "USBCODE"
{
  // empty the endpoint buffers
  AT91F_UDP_EpClear( AT91C_BASE_UDP, AT91C_EP_IN, AT91C_UDP_EPEDS );
  AT91F_UDP_EpClear( AT91C_BASE_UDP, AT91C_EP_OUT, AT91C_UDP_EPEDS );
  
  USBWriteCount=0;
  USBCurWrite=0;
  USBCurRead=0;
  
}


// when something is received on main EP, put the data
void fw_mainRxDone(void) @ "USBCODE"
{
  s32 _len;
  
  if (USBReadAddr==NULL) {
    /* disable interrupt if we can't receive next time */
    AT91F_UDP_DisableIt( AT91C_BASE_UDP, AT91C_UDP_EPINT2 ); // AT91C_EP_OUT
    return;
  }
  
  /* Read the bytes received */
  _len = ( ( AT91F_UDP_EpStatus( AT91C_BASE_UDP, AT91C_EP_OUT ) >> 16) &0x7F );
  
#ifdef USB_RX_TIMEOUT
  if (_len) {
    if ((USBCurRead)&&(LastRXTime)&&(GetTic32()-LastRXTime>USB_RX_TIMEOUT)) {
      USBCurRead=0;
    }
    LastRXTime=GetTic32();
  }
#endif
  
  if ((USBCurRead+_len)>USBDataBlock)
    _len=USBDataBlock-USBCurRead;
  
  if (_len) {
    
    while(_len-- > 0) {
      USBReadAddr[USBCurRead] = AT91F_UDP_EpRead( AT91C_BASE_UDP, AT91C_EP_OUT );
      USBCurRead++;
    }
  }
  
  // Read data bank
  AT91F_UDP_EpClear( AT91C_BASE_UDP, AT91C_EP_OUT, currentReceiveBank);
  if (currentReceiveBank == AT91C_UDP_RX_DATA_BK0) currentReceiveBank = AT91C_UDP_RX_DATA_BK1;
  else currentReceiveBank = AT91C_UDP_RX_DATA_BK0;
  
  if (USBCurRead>=USBDataBlock) {
    /* disable interrupt if we can't receive next time */
    AT91F_UDP_DisableIt( AT91C_BASE_UDP, AT91C_UDP_EPINT2 );
  } else {
    /* enable interrupt we can receive next time */
    AT91F_UDP_EnableIt( AT91C_BASE_UDP, AT91C_UDP_EPINT2 );
  }
}


static void FillTXBank(s32 Len) @ "USBCODE"
{
  s32 cpt;
#ifdef USB_ALIGN64
  s32 cpt1;
#endif
  // Fill the second bank
  cpt = MIN(Len, AT91C_EP_IN_OUT_SIZE);
#ifdef USB_ALIGN64
  cpt1=AT91C_EP_IN_OUT_SIZE-cpt;
#endif
  while (cpt--) {
    AT91F_UDP_EpWrite( AT91C_BASE_UDP, AT91C_EP_IN, USBWriteAddr[USBCurWrite]);
    USBCurWrite++;
  }
#ifdef USB_ALIGN64
  while (cpt1--)
    AT91F_UDP_EpWrite( AT91C_BASE_UDP, AT91C_EP_IN, 0);
#endif
}

//        called when something is transmitted by main EP,
//        pick data in the IN FIFO and transmit it
void fw_mainTxDone(void) @ "USBCODE"
{
  u32 i;
  
  // Wait for the the first bank to be sent
  if ( !(AT91C_BASE_UDP->UDP_CSR[AT91C_EP_IN] & AT91C_UDP_TXCOMP) )
    return;
  
  AT91C_BASE_UDP->UDP_CSR[AT91C_EP_IN] &= ~(AT91C_UDP_TXCOMP);
  i=USB_TIMEOUT;
  while (AT91C_BASE_UDP->UDP_CSR[AT91C_EP_IN] & AT91C_UDP_TXCOMP)
    if (--i==0) break;
  
  /* a transfer will begin */
  if (NeedTX) {
    AT91C_BASE_UDP->UDP_CSR[AT91C_EP_IN] |= AT91C_UDP_TXPKTRDY;
    NeedTX=0;
  }
  
  if (USBWriteCount>USBCurWrite) {
    FillTXBank(USBWriteCount-USBCurWrite);
    NeedTX=1;
  } else {
    USBWriteCount=USBCurWrite=0;
  }
}

void USB_Usart_Init(void) @ "USBCODE"
{
  //*** Replaces AT91F_PIO_ClearOutput call
  AT91C_BASE_PIOA->PIO_CODR=AT91C_PIO_PA8;
  //*** Replaces AT91F_PIO_CfgOutput call
  AT91C_BASE_PIOA->PIO_PER = AT91C_PIO_PA8; // Set in PIO mode
  AT91C_BASE_PIOA->PIO_OER = AT91C_PIO_PA8; // Configure in Output
  
  AT91PS_USART COM0 = AT91C_BASE_US0;
  
  //*** repalces AT91F_PIO_CfgPeriph
  AT91C_BASE_PIOA->PIO_ASR = (AT91C_PA5_RXD0 | AT91C_PA6_TXD0 | AT91C_PA7_RTS0);
  AT91C_BASE_PIOA->PIO_BSR = 0;
  AT91C_BASE_PIOA->PIO_PDR = (AT91C_PA5_RXD0 | AT91C_PA6_TXD0 | AT91C_PA7_RTS0 | 0); // Set in Periph mode
  
  //*** AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_US0 ) ;
  AT91C_BASE_PMC->PMC_PCER = 1 << AT91C_ID_US0;
  
  //*** AT91F_US_Configure (COM0, MCK, AT91C_US_USMODE_RS485 | AT91C_US_NBSTOP_1_BIT | AT91C_US_PAR_NONE | AT91C_US_CHRL_8_BITS, AT91_BAUD_RATE, 0);
  COM0->US_IDR = (unsigned int) -1;
  
  //* Reset receiver and transmitter
  COM0->US_CR = AT91C_US_RSTRX | AT91C_US_RSTTX | AT91C_US_RXDIS | AT91C_US_TXDIS ;
  
  //* Define the baud rate divisor register
  //***AT91F_US_SetBaudrate(pUSART, mainClock, baudRate);
  unsigned int baud_value = ((MCK*10)/(115200 * 16));
  if ((baud_value % 10) >= 5)
    baud_value = (baud_value / 10) + 1;
  else
    baud_value /= 10;
  COM0->US_BRGR = baud_value;	//AT91F_US_Baudrate(MCK, 115200);
  
  //***AT91F_US_SetTimeguard(pUSART, timeguard);
  COM0->US_TTGR = 0;
  
  //* Clear Transmit and Receive Counters
  //***AT91F_PDC_Open((AT91PS_PDC) &(pUSART->US_RPR));
  AT91PS_PDC pPDC = (AT91PS_PDC)&(COM0->US_RPR);	//my pointer
  //AT91F_PDC_DisableRx(pPDC);
  pPDC->PDC_PTCR = AT91C_PDC_RXTDIS;
  //AT91F_PDC_DisableTx(pPDC);
  pPDC->PDC_PTCR = AT91C_PDC_TXTDIS;
  
  //* Reset all Counter register Next buffer first
  //AT91F_PDC_SetNextTx(pPDC, (char *) 0, 0);
  pPDC->PDC_TNPR = 0;
  pPDC->PDC_TNCR = 0;
  //AT91F_PDC_SetNextRx(pPDC, (char *) 0, 0);
  pPDC->PDC_RNPR = 0;
  pPDC->PDC_RNCR = 0;
  //AT91F_PDC_SetTx(pPDC, (char *) 0, 0);
  pPDC->PDC_TPR = 0;
  pPDC->PDC_TCR = 0;
  //AT91F_PDC_SetRx(pPDC, (char *) 0, 0);
  pPDC->PDC_RPR = 0;
  pPDC->PDC_RCR = 0;
  
  //AT91F_PDC_EnableRx(pPDC);
  pPDC->PDC_PTCR = AT91C_PDC_RXTEN;
  //AT91F_PDC_EnableTx(pPDC);
  pPDC->PDC_PTCR = AT91C_PDC_TXTEN;
  
  //* Define the USART mode
  COM0->US_MR = AT91C_US_USMODE_RS485 | AT91C_US_NBSTOP_1_BIT | AT91C_US_PAR_NONE | AT91C_US_CHRL_8_BITS;
  
  // Enable usart
  COM0->US_CR = AT91C_US_RXEN | AT91C_US_TXEN ;
  
  //* Enable USART IT error and RXRDY
  //***AT91F_US_EnableIt(COM0,AT91C_US_RXRDY);
  COM0->US_IER = AT91C_US_RXRDY;
  
  AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_US0, USART_INTERRUPT_LEVEL,AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, Usart_c_irq_handler);
  AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_US0);
  
  UartStarted[0]=1;
  
}



void fw_isr(void) @ "USBCODE"// acm, 2-19-12, sometimes hung up here during debug/reboot, had to unplug cable to continue
{
  
  u32 isr = AT91F_UDP_InterruptStatusRegister(AT91C_BASE_UDP);
  
  if (isr) {
    
    /**********************************/
    /* USB End Of Bus Reset Interrupt */
    /**********************************/
    //    PrintDebugHex(isr);
    
    if(isr & AT91C_UDP_ENDBUSRES)
    {
      
      AT91F_UDP_DisableIt( AT91C_BASE_UDP, DISABLE_ALL_IT );
      AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, CLEAR_ALL_IT );//clear interrupt
      AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, CLEAR_ALL_IT );// twice like Atmel example
      AT91F_UDP_EnableIt( AT91C_BASE_UDP,  AT91C_UDP_EPINT0
                         | AT91C_UDP_RXSUSP | AT91C_UDP_RXRSM );
      
      fw_busReset();
      currentConfiguration=0;
      
      // Reset all endpoint
      AT91F_UDP_ResetEp( AT91C_BASE_UDP, 0x0000FFFF );
      AT91F_UDP_EpSet( AT91C_BASE_UDP, AT91C_EP_CTRL, AT91C_UDP_EPTYPE_CTRL );
      AT91F_UDP_EpSet( AT91C_BASE_UDP, AT91C_EP_IN, AT91C_UDP_EPTYPE_BULK_IN );
      AT91F_UDP_EpSet( AT91C_BASE_UDP, AT91C_EP_OUT, AT91C_UDP_EPTYPE_BULK_OUT );
      
      // Enable endpoint CTRL
      AT91F_UDP_EnableEp( AT91C_BASE_UDP, AT91C_EP_CTRL );
    }
    if(isr & AT91C_UDP_EPINT0) { 
      
      AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, AT91C_UDP_EPINT0);
      AT91F_UDP_EnableIt( AT91C_BASE_UDP,  AT91C_UDP_EPINT1 | AT91C_UDP_EPINT2);
      RxTxControl();
    }
    
    if(isr & AT91C_UDP_EPINT1) {
      /* we sent something */
      /* transfer is already in progress */
      /* don't mark it again, this INT can be due to a stall */
      if( AT91F_UDP_EpStatus( AT91C_BASE_UDP, AT91C_EP_IN ) & AT91C_UDP_TXCOMP ) {
        AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, AT91C_UDP_EPINT1);
        fw_mainTxDone();
      }
    }
    
    /********************************************/
    /* (UDP) Endpoint 2 Interrupt : EP BULK OUT */
    /********************************************/
    if(isr & AT91C_UDP_EPINT2) {
      AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, AT91C_UDP_EPINT2);
      fw_mainRxDone();
    }
    
    if(isr & AT91C_UDP_RXRSM)
    {
      AT91F_UDP_DisableIt( AT91C_BASE_UDP, DISABLE_ALL_IT );
      AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, CLEAR_ALL_IT );
      AT91F_UDP_EnableIt( AT91C_BASE_UDP,     AT91C_UDP_EPINT0
                         | AT91C_UDP_EPINT1
                           | AT91C_UDP_EPINT2
                             | AT91C_UDP_RXSUSP);
      
      AT91F_PMC_CfgSysClkEnableReg( AT91C_BASE_PMC, AT91C_PMC_UDP );
      AT91F_UDP_EnableTransceiver(AT91C_BASE_UDP);
    } else
      // (UDP) USB Suspend Interrupt
      if(isr & AT91C_UDP_RXSUSP)
      {
        AT91F_UDP_DisableIt( AT91C_BASE_UDP, DISABLE_ALL_IT );
        AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, CLEAR_ALL_IT );
        AT91F_UDP_EnableIt( AT91C_BASE_UDP,  AT91C_UDP_RXRSM );
        
        AT91F_UDP_DisableTransceiver(AT91C_BASE_UDP);
        AT91F_PMC_CfgSysClkDisableReg( AT91C_BASE_PMC, AT91C_PMC_UDP );
        
        fw_busReset();
      }
    
  }
  
  /* clear interrupt */
  AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, CLEAR_ALL_IT );
  
}




//*----------------------------------------------------------------------------
//* \fn    AT91F_CDC_Write
//* \brief Send through endpoint 2
//*----------------------------------------------------------------------------
static void AT91F_UDP_Write(u8 *pData, u32 length) @ "USBCODE"
{
  USBWriteCount=length;
  USBWriteAddr=(volatile u8 *)pData;
  USBCurWrite=0;
  
  // Send the first packet
  FillTXBank(length);
  AT91C_BASE_UDP->UDP_CSR[AT91C_EP_IN] |= AT91C_UDP_TXPKTRDY;
  
  if (USBWriteCount>USBCurWrite) {
    // Fill the second bank
    FillTXBank(USBWriteCount-USBCurWrite);
    NeedTX=1;
  }
}


//*----------------------------------------------------------------------------
//* \fn    AT91F_USB_SendData
//* \brief Send Data through the control endpoint
//*----------------------------------------------------------------------------
static void AT91F_USB_SendData(AT91PS_UDP pUdp, const char *pData, u32 length) @ "USBCODE"
{
  u32 cpt = 0;
  AT91_REG csr;
  u32 i;
  
  do {
    cpt = MIN(length, 8);
    length -= cpt;
    
    while (cpt--)
      pUdp->UDP_FDR[0] = *pData++;
    
    if (pUdp->UDP_CSR[0] & AT91C_UDP_TXCOMP) {
      pUdp->UDP_CSR[0] &= ~(AT91C_UDP_TXCOMP);
      i=USB_TIMEOUT;
      while (pUdp->UDP_CSR[0] & AT91C_UDP_TXCOMP)
        if (--i==0) break;
    }
    
    i=USB_TIMEOUT;
    pUdp->UDP_CSR[0] |= AT91C_UDP_TXPKTRDY;
    do {
      csr = pUdp->UDP_CSR[0];
      
      // Data IN stage has been stopped by a status OUT
      if (csr & AT91C_UDP_RX_DATA_BK0) {
        pUdp->UDP_CSR[0] &= ~(AT91C_UDP_RX_DATA_BK0);
        return;
      }
      if (--i==0) break;
    } while ( !(csr & AT91C_UDP_TXCOMP) );
    
  } while (length);
  
  if (pUdp->UDP_CSR[0] & AT91C_UDP_TXCOMP) {
    pUdp->UDP_CSR[0] &= ~(AT91C_UDP_TXCOMP);
    i=USB_TIMEOUT;
    while (pUdp->UDP_CSR[0] & AT91C_UDP_TXCOMP)
      if (--i==0) break;
  }
}

//*----------------------------------------------------------------------------
//* \fn    AT91F_USB_SendZlp
//* \brief Send zero length packet through the control endpoint
//*----------------------------------------------------------------------------
static void AT91F_USB_SendZlp(AT91PS_UDP pUdp) @ "USBCODE"
{
  u32 i;
  
  pUdp->UDP_CSR[0] |= AT91C_UDP_TXPKTRDY;
  i=USB_TIMEOUT;
  while ( !(pUdp->UDP_CSR[0] & AT91C_UDP_TXCOMP) )
    if (--i==0) break;
  pUdp->UDP_CSR[0] &= ~(AT91C_UDP_TXCOMP);
  i=USB_TIMEOUT;
  while (pUdp->UDP_CSR[0] & AT91C_UDP_TXCOMP)
    if (--i==0) break;
}

//*----------------------------------------------------------------------------
//* \fn    AT91F_USB_SendStall
//* \brief Stall the control endpoint
//*----------------------------------------------------------------------------
static void AT91F_USB_SendStall(AT91PS_UDP pUdp) @ "USBCODE"
{
  u32 i;
  pUdp->UDP_CSR[0] |= AT91C_UDP_FORCESTALL;
  i=USB_TIMEOUT;
  while ( !(pUdp->UDP_CSR[0] & AT91C_UDP_ISOERROR) )
    if (--i==0) break;
  pUdp->UDP_CSR[0] &= ~(AT91C_UDP_FORCESTALL | AT91C_UDP_ISOERROR);
  i=USB_TIMEOUT;
  while (pUdp->UDP_CSR[0] & (AT91C_UDP_FORCESTALL | AT91C_UDP_ISOERROR))
    if (--i==0) break;
}

//*----------------------------------------------------------------------------
//* \brief This function is a callback invoked when a SETUP packet is received
//*----------------------------------------------------------------------------
static void RxTxControl(void) @ "USBCODE"
{
  AT91PS_UDP pUDP = AT91C_BASE_UDP;
  u8 bmRequestType, bRequest;
  u16 wValue, wIndex, wLength, wStatus;
  u32 i;
  
  if ( !(pUDP->UDP_CSR[0] & AT91C_UDP_RXSETUP) )
    return;
  
  bmRequestType = pUDP->UDP_FDR[0];
  bRequest      = pUDP->UDP_FDR[0];
  wValue        = (pUDP->UDP_FDR[0] & 0xFF);
  wValue       |= (pUDP->UDP_FDR[0] << 8);
  wIndex        = (pUDP->UDP_FDR[0] & 0xFF);
  wIndex       |= (pUDP->UDP_FDR[0] << 8);
  wLength       = (pUDP->UDP_FDR[0] & 0xFF);
  wLength      |= (pUDP->UDP_FDR[0] << 8);
  
  if (bmRequestType & 0x80) {
    pUDP->UDP_CSR[0] |= AT91C_UDP_DIR;
    i=USB_TIMEOUT;
    while ( !(pUDP->UDP_CSR[0] & AT91C_UDP_DIR) ){
      if (--i==0) break;
    }
  }
  pUDP->UDP_CSR[0] &= ~AT91C_UDP_RXSETUP;
  i=USB_TIMEOUT;
  while ( (pUDP->UDP_CSR[0] & AT91C_UDP_RXSETUP)  ){
    if (--i==0) break;
  }
  
  // Handle supported standard device request Cf Table 9-3 in USB specification Rev 1.1
  switch ((bRequest << 8) | bmRequestType) {
  case STD_GET_DESCRIPTOR:
    if (wValue == 0x100)       // Return Device Descriptor
      AT91F_USB_SendData(pUDP, devDescriptor, MIN(sizeof(devDescriptor), wLength));
    else if (wValue == 0x200)  // Return Configuration Descriptor
      AT91F_USB_SendData(pUDP, cfgDescriptor, MIN(sizeof(cfgDescriptor), wLength));
    else
      AT91F_USB_SendStall(pUDP);
    break;
  case STD_SET_ADDRESS:
    AT91F_USB_SendZlp(pUDP);
    pUDP->UDP_FADDR = (AT91C_UDP_FEN | wValue);
    pUDP->UDP_GLBSTATE  = (wValue) ? AT91C_UDP_FADDEN : 0;
    break;
  case STD_SET_CONFIGURATION:
    currentConfiguration = wValue;
    AT91F_USB_SendZlp(pUDP);
    pUDP->UDP_GLBSTATE  = (wValue) ? AT91C_UDP_CONFG : AT91C_UDP_FADDEN;
    pUDP->UDP_CSR[1] = (wValue) ? (AT91C_UDP_EPEDS | AT91C_UDP_EPTYPE_BULK_IN) : 0;
    pUDP->UDP_CSR[2] = (wValue) ? (AT91C_UDP_EPEDS | AT91C_UDP_EPTYPE_BULK_OUT)  : 0;
    pUDP->UDP_CSR[3] = 0;
    break;
  case STD_GET_CONFIGURATION:
    AT91F_USB_SendData(pUDP, (char *) &(currentConfiguration), sizeof(currentConfiguration));
    break;
  case STD_GET_STATUS_ZERO:
    wStatus = 0;
    AT91F_USB_SendData(pUDP, (char *) &wStatus, sizeof(wStatus));
    break;
  case STD_GET_STATUS_INTERFACE:
    wStatus = 0;
    AT91F_USB_SendData(pUDP, (char *) &wStatus, sizeof(wStatus));
    break;
  case STD_GET_STATUS_ENDPOINT:
    wStatus = 0;
    wIndex &= 0x0F;
    if ((pUDP->UDP_GLBSTATE & AT91C_UDP_CONFG) && (wIndex <= 2)) {
      wStatus = (pUDP->UDP_CSR[wIndex] & AT91C_UDP_EPEDS) ? 0 : 1;
      AT91F_USB_SendData(pUDP, (char *) &wStatus, sizeof(wStatus));
    }
    else if ((pUDP->UDP_GLBSTATE & AT91C_UDP_FADDEN) && (wIndex == 0)) {
      wStatus = (pUDP->UDP_CSR[wIndex] & AT91C_UDP_EPEDS) ? 0 : 1;
      AT91F_USB_SendData(pUDP, (char *) &wStatus, sizeof(wStatus));
    }
    else
      AT91F_USB_SendStall(pUDP);
    break;
  case STD_SET_FEATURE_ZERO:
    AT91F_USB_SendStall(pUDP);
    break;
  case STD_SET_FEATURE_INTERFACE:
    AT91F_USB_SendZlp(pUDP);
    break;
  case STD_SET_FEATURE_ENDPOINT:
    wIndex &= 0x0F;
    if ((wValue == 0) && wIndex && (wIndex <= 2)) {
      pUDP->UDP_CSR[wIndex] = 0;
      AT91F_USB_SendZlp(pUDP);
    }
    else
      AT91F_USB_SendStall(pUDP);
    break;
  case STD_CLEAR_FEATURE_ZERO:
    AT91F_USB_SendStall(pUDP);
    break;
  case STD_CLEAR_FEATURE_INTERFACE:
    AT91F_USB_SendZlp(pUDP);
    break;
  case STD_CLEAR_FEATURE_ENDPOINT:
    wIndex &= 0x0F;
    if ((wValue == 0) && wIndex && (wIndex <= 2)) {
      if (wIndex == 1)
        pUDP->UDP_CSR[1] = (AT91C_UDP_EPEDS | AT91C_UDP_EPTYPE_BULK_IN);
      else if (wIndex == 2)
        pUDP->UDP_CSR[2] = (AT91C_UDP_EPEDS | AT91C_UDP_EPTYPE_BULK_OUT);
      AT91F_USB_SendZlp(pUDP);
    }
    else
      AT91F_USB_SendStall(pUDP);
    break;
  default:
    AT91F_USB_SendStall(pUDP);
    break;
  }
}


// 1 -  USB present
u32 IsUSBPresent(void) @ "USBCODE"
{
  u32 fl;
  
  if (!USBStarted)
    return 0;
  
  if (currentConfiguration) fl=1;
  else fl=0;
  
#if defined(USB_DETECT_LINE)
#if defined(__KIT9)
  if (!AT91F_PIO_IsInputSet(AT91C_BASE_PIOC,USB_DETECT_LINE)) fl=0;
#else
  if (!AT91F_PIO_IsInputSet(AT91C_BASE_PIOA,USB_DETECT_LINE)) fl=0;
#endif
#endif
  
  return fl;
}


void USBDisable(void) @ "USBCODE"
{
  AT91F_AIC_DisableIt( AT91C_BASE_AIC, AT91C_ID_UDP );
}


void USBEnable(void) @ "USBCODE"
{
  AT91F_AIC_EnableIt( AT91C_BASE_AIC, AT91C_ID_UDP );
}


void USBStartWrite(u8* Buf, u32 Len) @ "USBCODE"
{
  if (!USBStarted)
    return;
  USBDisable();
  AT91F_UDP_Write(Buf, Len);
  USBEnable();
}



u32 USBWrited(void) @ "USBCODE"
{
  if (!USBStarted)
    return 1;
  return (USBCurWrite>=USBWriteCount);
}


void USBMemMove(u8 *Dst, u8 *Src, u32 Len) @ "USBCODE"
{
  u32 i;
  if (Len==0)
    return;
  for(i=0;i<Len;i++)
    Dst[i]=Src[i];
}

void USBMemSet(void *s, int c, u32 n) @ "USBCODE"/* Copied from memset.c */
{       /* store c throughout unsigned char s[n] */
  const unsigned char uc = c;
  unsigned char *su = (unsigned char *)s;
  
  for (; 0 < n; ++su, --n)
    *su = uc;
}

u32 USBRead(u8* Buf, u32 Len) @ "USBCODE"
{
  if(!UartStarted[0])
  {
    if (!USBStarted)
      return 0;
    if ((USBCurRead>=Len) && (USBReadAddr!=NULL)) 
    {
      USBDisable();
      USBMemMove(Buf,(u8 *)USBReadAddr,Len);
      if (USBCurRead-Len>0) 
      {
        USBMemMove((u8 *)USBReadAddr,(u8 *)(&(USBReadAddr[Len])),USBCurRead-Len);
        USBCurRead-=Len;
      } 
      else
        USBCurRead=0;
      // reset interrupt for reading
      AT91F_UDP_EnableIt( AT91C_BASE_UDP, AT91C_UDP_EPINT2 ); // AT91C_EP_OUT
      
      USBEnable();
      return Len;
    } 
    else 
    {
      // reset interrupt for reading
      AT91F_UDP_EnableIt( AT91C_BASE_UDP, AT91C_UDP_EPINT2 ); // AT91C_EP_OUT
      return 0;
    }
  }
  else
  {
    if (UartCurRead[0]>=Len) 
    {
      USBMemMove(Buf,(u8 *)UartReadBuf[0],Len);
      UartCurRead[0]=0;//reset the buffer index to 0
      return Len;
    } 
    else 
    {
      return 0;
    }  
  }
}


u32 USBReaded(void) @ "USBCODE"
{
  if (!USBStarted)
    return 0;
  return USBCurRead;
}


void USBFlushRX(void) @ "USBCODE"
{
  USBCurRead=0;
}


u32 USBWrite(u8* Buf, u32 Len) @ "USBCODE"
{u32 i=USB_TIMEOUT;

USBStartWrite(Buf, Len);
while (!USBWrited()){
  if (!IsUSBPresent())
    return 0;
  if ((--i)==0) {
    //fw_busReset();
    return 0;
  }
}
return Len;
}



void USBShift(u32 Len) @ "USBCODE"
{
  
#ifdef USB_ALIGN64
  char USBDataSubBlock=64;
#else
  char USBDataSubBlock=1;
#endif
  
  if (Len>0) {
    //align
    if(Len%USBDataSubBlock) Len=(Len/USBDataSubBlock+1)*USBDataSubBlock;
    
    if (USBCurRead<=Len) {
      USBCurRead=0;
    } else {
      USBDisable();
      USBMemMove((u8 *)USBReadAddr,(u8 *)(&(USBReadAddr[Len])),(USBCurRead-=Len));  //acm, verified 3rd expression returns USBCurRead-Len
      USBEnable();
    }
  }
}


typedef char SAM7ZaprosSignature[8];
#define szSAM7ZaprosSignature sizeof(SAM7ZaprosSignature)

// Separated, to prevent apparance of such a string in a program
const SAM7ZaprosSignature CurrentSAM7ZaprosSignature @ "USBCONST" ="VC     ";
const SAM7ZaprosSignature CurrentSAM7ZaprosSignatureVer @ "USBCONST" =

#ifdef __KIT
"  7Sk01";
#endif
#ifdef __Korsar
"  7SK01";
#endif
#ifdef __TTMon
"  7ST01";
#endif
#ifdef __PDMon
"  7SP01";
#endif
#ifdef __R1500
"  7S401";
#endif
#ifdef __R1500_6
"  7S501";
#endif
#ifdef __VVTester
"  7SV01";
#endif
#ifdef __KIT9
"  9Sk01";
#endif
#ifdef __R505
"  7SR01";
#endif
#ifdef __LocMon
"  7SL01";
#endif
#ifdef __AR200
"  7SA01";
#endif
#ifdef __TIM3
"  7S301";
#endif
#ifdef __Diana2M
"  7S201";
#endif
#ifdef __TDM0
"  7SM01";
#endif



const u8 OkSignature[64] @ "USBCONST" =

// Vibro-Center xxx Ok Signature 01
#ifdef __KIT
"VC7Ok01";
#endif
#ifdef __Korsar
"VC7OK01";
#endif
#ifdef __TTMon
"VC7OT01";
#endif
#ifdef __PDMon
"VC7OP01";
#endif
#ifdef __R1500
"VC7O401";
#endif
#ifdef __R1500_6
"VC7O501";
#endif
#ifdef __VVTester
"VC7OV01";
#endif
#ifdef __KIT9
"VC90k01";
#endif
#ifdef __R505
"VC7OR01";
#endif
#ifdef __LocMon
"VC7OL01";
#endif
#ifdef __AR200
"VC7OA01";
#endif
#ifdef __TIM3
"VC7O301";
#endif
#ifdef __Diana2M
"VC7O201";
#endif
#ifdef __TDM0
"VC7OM01";
#endif


#ifndef AT91RM9200
static int SignatureOk(u8* Buf) @ "USBCODE"
{
  return ((Buf[0]==CurrentSAM7ZaprosSignature[0])&&
          (Buf[1]==CurrentSAM7ZaprosSignature[1])&&
            (Buf[2]==CurrentSAM7ZaprosSignatureVer[2])&&
              (Buf[3]==CurrentSAM7ZaprosSignatureVer[3])&&
                (Buf[4]==CurrentSAM7ZaprosSignatureVer[4])&&
                  (Buf[5]==CurrentSAM7ZaprosSignatureVer[5])&&
                    (Buf[6]==CurrentSAM7ZaprosSignatureVer[6])&&
                      (Buf[7]==CurrentSAM7ZaprosSignatureVer[7]));
}

typedef struct {
  SAM7ZaprosSignature Signature;
  s32 BlockAddr,
  BlockLen,
  FullLen,
  Option;
  TCRC CRCProg,
  CRCHdr;
  u8 Reserv[64-28];
} TProgCommand;

#define szTProgCommand 64
__no_init TProgCommand Command @ "USBDATA";
#define USBProgPacket 8192


static u32 USBReadCommand(TProgCommand* Command) @ "USBCODE"
{
  u32 length;
  u32 previous = 0;
  u32 retry = 10;
  length = USBRead((u8*)Command, szTProgCommand);
  // SOE only/ retry
  if((UartStarted[0] != 0) && (length != szTProgCommand))
  {
    previous = UartCurRead[0];
    while((retry>0) && (length!=szTProgCommand))
    {
      Delay(10);
      if(UartCurRead[0] > previous)// Check if any new bytes were received
      {
        length = USBRead((u8*)Command, szTProgCommand);
        previous = UartCurRead[0];
      }
      else
        retry--;
    }
    if(retry == 0)//No additional bytes received after 5 ms, clear the buffer - will not work with significant fragmentation
      UartCurRead[0] = 0;      
  }
  //if(retry == 0)//No additional bytes received after 5 ms, clear the buffer - will not work with significant fragmentation
  //UartCurRead[XPortPort] = 0;
  if (length==szTProgCommand) 
  {
    if (CheckCRC((u8*)Command, szTProgCommand, &(Command->CRCHdr)))
      if (SignatureOk((u8*)Command))
        return 1;
  }
  return 0;
}



static void USBWriteOk(u8 value) @ "USBCODE"
{
  if(UartStarted[0] == 0)
  {
    USBFlushRX();
    USBStartWrite((u8*)OkSignature, sizeof(OkSignature));
    while (!USBWrited())
      if (!IsUSBPresent())
        break;
  }
  else
  {
    Command.Signature[3] = 'O';
    Command.Reserv[0] = value;//Set reserved[0] in the signature to value
    UartWriteBlock(0,&Command,sizeof(Command));
  }
}


//__no_init u8 data[USBProgPacket] @ "USBDATA";  // acm, 3-3-09, explicty define array segment.  Otherwise it is an "auto variable", which gets placed into the stack.  Doing this fixed a fw upload hang (prefetch abort) unsure why, but suspect stack related.

static void USBDownloadProgramInline(void) @ "USBCODE"
{
  u8 data[USBProgPacket]; // acm, 3-4-09, define here but grow stack size definition to avoid stack overflow.  This wastes less memory.
  //   u8 *data = (u8*)malloc(USBProgPacket);  // acm, heap experiment.  Did this to verify if anybody is using HEAP, e.g. stdio.h.  Turns out nobody is.  Enabling this code here will result in a HEAP segment showing up in MAP file.
  u32 length, Len, tickPacket;
  u32 l;
  char flash_write_ok=1; // acm, v2.00, flag passed up from EFC_Write(), if write to flash failed, redo FW upgrade
  
  
  // Read BlockLen byte
  l=0;
  tickPacket = AT91C_BASE_RTTC->RTTC_RTVR+32*5;//Give the device 5 seconds to download a full 8K chunk
  while (l<Command.BlockLen && tickPacket>AT91C_BASE_RTTC->RTTC_RTVR) 
  {
    if (l+USBDataBlock<Command.BlockLen) 
      Len=USBDataBlock;
    else 
      Len=Command.BlockLen-l;
    
    length = USBRead(&(data[l]), Len);  // acm, read 1k blocks in at a time, fill up 8k array
    
    if (UartStarted[0] == 0)
      if(!IsUSBPresent())
        break;
    if (length>0)
    {
      tickPacket = AT91C_BASE_RTTC->RTTC_RTVR+32*5;                   
      if(((l+length)<Command.BlockLen) && (UartStarted[0] == 1))
      {
        USBWriteOk((l/USBDataBlock)+1);
      }                  
      l+=length;
    }
  }
  if (CheckCRC((u8*)data, Command.BlockLen, &(Command.CRCProg))) {
    
    // New !!!
    if (Command.BlockAddr==0) 
    {
      USBWriteOk(8);
    }
    
    // Program
    ResetWatchDog();
    
    // something has changed, write
#ifdef __KIT
    AT91F_PIO_ClearOutput( AT91C_BASE_PIOA, LED4 ) ;
#endif
    AT91F_disable_interrupt();
    flash_write_ok=EFCWrite(0x00100000 | Command.BlockAddr,data,Command.BlockLen,(Command.BlockAddr==0));
    AT91F_enable_interrupt();
    //ResetWatchDog();  //acm, 3-12 reset watchdog more often
    
#ifdef __KIT
    AT91F_PIO_ClearOutput( AT91C_BASE_PIOA, LED3 ) ;
#endif
    if (flash_write_ok && Command.BlockAddr!=0) 
    {  // acm, v2.00 skip "ok" if Flash write error, redo FW upgrade
      USBWriteOk(8);
#ifdef __KIT
      AT91F_PIO_ClearOutput( AT91C_BASE_PIOA, LED2 ) ;
#endif
    } 
  }
  
}

static void USBDownloadProgram(void) @ "USBCODE"
{
  u32 retry;
  
  retry=100000;
  while (IsUSBPresent() || UartStarted[0]) {
    
    ResetWatchDog();
    if ((retry--)==0) break;
    if (USBReadCommand(&Command)) 
    { // acm, read next block to program from DM Loader, maybe a retry
      retry=1e9;
      if(Command.BlockLen == 0)
      {
        USBWriteOk(33);// Handshake signature
      }        
      else
      {
        USBWriteOk(44);// Setup packet ACK signature
        USBDownloadProgramInline();            
      }
    }
  }
}
#endif


void USBWaitInit(void) @ "USBCODE"
{
#if defined(USB_DETECT_LINE)
  u32 tick;
  AT91C_BASE_RTTC->RTTC_RTMR=AT91C_RTTC_RTTRST | 0x400; // 32 Hz
  Delay(1);
  
  ResetWatchDog();// Need this to make sure in case we go into the SOE portion and never detect the signature
  
  //Poll USB and ethernet data for device signature received
  
  //Init uart
  AT91_BAUD_RATE = 115200;
  //Usart_init();
  USB_Usart_Init();
  tick = AT91C_BASE_RTTC->RTTC_RTVR+32*5;
  // Scan for signature received, ping-ponging between USB and UART ports
  while(tick>AT91C_BASE_RTTC->RTTC_RTVR)
  {
    if(USBCurRead >= szTProgCommand)// Check if signature packet was received on USB
    {
      UartStarted[0] = 0;//Disable uart
      USBDownloadProgram();
    }
    else if(UartCurRead[0] >= szTProgCommand)//Check if signature packet was received on UART
    {
      if (SignatureOk((u8*)UartReadBuf))
      {
        if(UartReadBuf[0][28] == 33)
          USBDownloadProgram();
      }
      else
        UartCurRead[0] = 0;
    }
  }
#endif
}


//*----------------------------------------------------------------------------
//* \brief This function Open the USB device
//*----------------------------------------------------------------------------
void USBInit(void) @ "USBCODE"
{
   // acm, run segment init here, needs to be run before accessing initialized variables.
  // Necessary because moved segment_init call in Cstartup to after USB call, this change eliminates a potential hang
  // if there is a bad FW load.  Segment init will be run again before Main(), initializing the new FW variables correctly.
  
  //__USB_segment_init(); ag commented to align with PDM
  
  //  u32 isr;
  
#ifdef __KIT
  // Disable UDP PullUp (USB_DP_PUP) : enable & Clear of the corresponding PIO
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA16);
  // Set in PIO mode and Configure in Output
  AT91F_PIO_CfgOutput(AT91C_BASE_PIOA,AT91C_PIO_PA16);
  Delay(10);
#endif
#ifdef SAM7SE
  AT91C_BASE_UDP->UDP_TXVC&=~(AT91C_UDP_PUON);
#endif    
#ifndef AT91RM9200
  // Set the PLL USB Divider
  AT91C_BASE_CKGR->CKGR_PLLR |= AT91C_CKGR_USBDIV_1 ;
#endif
  
  // Specific Chip USB Initialisation
  // Enables the 48MHz USB clock UDPCK and System Peripheral USB Clock
  AT91F_PMC_CfgSysClkEnableReg( AT91C_BASE_PMC, AT91C_PMC_UDP );
  // Enables the corresponding peripheral clock.
  AT91F_PMC_EnablePeriphClock( AT91C_BASE_PMC,(1 << AT91C_ID_UDP) );
  
  
  
#if defined(USB_DETECT_LINE)
#if defined(__KIT9)
  AT91F_PIO_CfgInput(AT91C_BASE_PIOC,USB_DETECT_LINE);
#else
  AT91F_PIO_CfgInput(AT91C_BASE_PIOA,USB_DETECT_LINE);
#endif
#endif
  
  AT91F_UDP_DisableIt( AT91C_BASE_UDP, DISABLE_ALL_IT );
  //  AT91F_UDP_InterruptClearRegister( AT91C_BASE_UDP, CLEAR_ALL_IT );
  //  isr = AT91F_UDP_InterruptStatusRegister(AT91C_BASE_UDP);
  //  isr = isr;
  
  fw_busReset();
  
  currentReceiveBank       = AT91C_UDP_RX_DATA_BK0;
  currentConfiguration=0;
  NeedTX=0;
  LastRXTime=0;
  
  USBCurRead=0;
  USBReadAddr=USBReadData;
  
  USBWriteCount=USBCurWrite=0;
  USBWriteAddr=NULL;
  
  USBStarted=1;
  
  // Acquire an interrupt vector and assign an ISR routine.
  AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_UDP, USB_PRIORITY, AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, fw_isr );
  // Enable corresponding IT number
  AT91F_AIC_EnableIt( AT91C_BASE_AIC, AT91C_ID_UDP );
  
  
#ifdef __KIT
  // Enable UDP PullUp (USB_DP_PUP) : enable & Clear of the corresponding PIO
  // Clear for set the Pul up resistor
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA16);
  
  AT91F_PIO_CfgOutput( AT91C_BASE_PIOA, LED_MASK);
  AT91F_PIO_SetOutput( AT91C_BASE_PIOA, LED_MASK) ;
#endif
  
  
#ifdef __KIT9
  
  // RESET UDP
  AT91F_UDP_ResetEp( AT91C_BASE_UDP, 0);
  AT91C_BASE_UDP->UDP_GLBSTATE = 0;
  
  // Enable UDP PullUp (USB_DP_PUP): Set the Pul up resistor
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOC,AT91C_PIO_PC8);
  // Set in PIO mode and Configure in Output
  AT91F_PIO_CfgOutput(AT91C_BASE_PIOC,AT91C_PIO_PC8);
  Delay(10);
  AT91F_PIO_SetOutput(AT91C_BASE_PIOC,AT91C_PIO_PC8);
#endif
  
#ifdef SAM7SE    
  DelayMks(100);
  AT91C_BASE_UDP->UDP_TXVC|=AT91C_UDP_PUON;
#endif  
  USBWaitInit();
  
}

void USBClose(void) @ "USBCODE"
{
  USBStarted=0;
  
  USBDisable();
  
#ifdef __KIT
  AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PIO_PA16);
#endif
  
#ifdef __KIT9
  AT91F_PIO_ClearOutput(AT91C_BASE_PIOC,AT91C_PIO_PC8);
#endif
  
  AT91F_UDP_DisableTransceiver(AT91C_BASE_UDP);
  fw_busReset();
  
  AT91F_PMC_CfgSysClkDisableReg( AT91C_BASE_PMC, AT91C_PMC_UDP );
  AT91F_PMC_DisablePeriphClock( AT91C_BASE_PMC,(1 << AT91C_ID_UDP) );
}

//ag commented out to align with PDM
//void TestUSB(void) @ "USBCODE"

