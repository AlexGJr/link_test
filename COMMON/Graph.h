#ifndef _graph_h
#define _graph_h

#include "defs.h"

#define         Font8x8         1
#define         Font6x5         2
#define         Font8x6         3


extern char CurFontWidth;       //������ �������� �����
extern char CurFontHeight;      //������  �������� �����
extern char CurFontPtrNumer;    //��������� �������� �����

extern char OutStr[LEN];


char CurFontPtr(unsigned int i1,unsigned int i2);
void SetFont(unsigned int FontType);


void DrawLine(unsigned int X1,unsigned int Y1, unsigned int X2,unsigned int Y2);
void OutString(unsigned int X,unsigned int Y,char const *String);
void OutStringInv(unsigned int X,unsigned int Y,char const *String);
void OutString1(unsigned int X,unsigned int Y,char String[]);
void OutString1Inv(unsigned int X,unsigned int Y,char String[]);
void OutInt(unsigned int X,unsigned int Y,int IntValue,unsigned char DigitCount);
void OutDate(unsigned int X, unsigned int Y);
void OutTime(unsigned int X, unsigned int Y);
void OutTimeNoSec(unsigned int X, unsigned int Y);
char *FloatToStr(char *str, float f, char c1, char c2);
char* IntToStr(int IntValue,unsigned int DigitCount);

void OutFloat(unsigned int x, unsigned int y, float f, char d1, char d2);
void OutFloatLeftZero(unsigned int x, unsigned int y, float f, char d1, char d2);

void ClearRect2(unsigned int X1,unsigned int Y1,unsigned int X2,unsigned int Y2);
void DrawRect(unsigned int x1,unsigned int y1,unsigned int x2,unsigned int y2);


//Add

void InputTimeNoSec(u32 X,u32 Y,u32* Hour,u32* Min);
int InputInt(u32 X,u32 Y,s32 IntValue,u32 DigitCount);
float InputFloat(u32 X,u32 Y,float Value,u32 DigitCount,u32 d2);
void InputDate(u32 X,u32 Y,u32* Day,u32* Month,u32* Year);
void InputTime(u32 X,u32 Y,u32* Hour,u32* Min,u32* Sec);
void OutStringCenter(u32 x,u32 y,u32 len,char ROM *str);
char* IntToStrFormat(s32 IntValue,u32 DigitCount);
char fStrLen(char ROM *s);
char* LongToStr(s32 l);
char* FloatToStrSpec(float f, u32 Dgts);
long StrToLong(char* str);
int StrToInt(char* str);
float StrToFloat(char* str);


#endif

