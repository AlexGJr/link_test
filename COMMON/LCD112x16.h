#ifndef LCD112x16_h
#define LCD112x16_h

#define ScreenWidth     112
#define ScreenHeight    16
#define ScreenWidthDiv8 14

#define VideoRamLen     224
#define VideoRamLenDiv4 56

extern unsigned char Video_Ram[VideoRamLen];

extern volatile char RedrawEnabled;
extern char LCDConnected;

// �� ����� ������ � ������� SPI ���� ��������� ���������� ������
void DisableLCD(char Symbol);
// ��������� ���������� ������
void EnableLCD();


//extern const char Fnt6x5[256][5];
//extern const char Fnt8x6[256][6];
//extern const char Fnt8x8[256][8];

#endif

