#include "BoardAdd.h"
#include "Fram.h"
#include "keyboard.h"
#include "DefKB.h"
#ifdef SAM7SE
#include "spi_se.h"
#else
#include "spi.h"
#endif


//#define FramSize 8192
#define FramSize 32768
int k=0;
//short FRAMBuf[512];

void InitFram(void)
{
}


#ifndef __emulator

static void SetupSPI(void)
{
// Настройка SPI
SPI_Prepare(SPIFREQ_8000,0);
}



unsigned int WriteFram(unsigned int addr, void *Buf, unsigned int Count)
{
//unsigned short v;
unsigned int i;

SetupSPI();

SPI_SetCS(SPI_CS_FRAM);
SPI_WriteRead(0x06); // Write Enable
SPI_ClearCS();

SPI_SetCS(SPI_CS_FRAM);
SPI_WriteRead(0x02); // Write

SPI_WriteRead(addr >> 8); // Addr Hi
SPI_WriteRead(addr & 0xFF); // Addr Lo

for (i=0;i<Count;i++) {
  SPI_WriteRead(((char*)Buf)[i]); // Data[i]
}
SPI_ClearCS();

//while (1){
SPI_SetCS(SPI_CS_FRAM);
SPI_WriteRead(0x04);// Write Disable
SPI_ClearCS();
//}

//v=v;
return Count;
}



unsigned int ReadFram(unsigned int addr, void *Buf, unsigned int Count)
{
//unsigned short v;
unsigned int i;

SetupSPI();

SPI_SetCS(SPI_CS_FRAM);
SPI_WriteRead(0x03); // Read

SPI_WriteRead(addr >> 8); // Addr Hi
SPI_WriteRead(addr & 0xFF); // Addr Lo

for (i=0;i<Count;i++) {
  ((char*)Buf)[i]=SPI_WriteRead(0); // Data[i]
}
SPI_ClearCS();

SPI_SetCS(SPI_CS_FRAM);
SPI_WriteRead(0x04);// Write Disable
SPI_ClearCS();

//v=v;
return Count;
}

#else

#include <vcl.h>
#include <graphics.hpp>
#include <sys\stat.h>
#include <fcntl.h>
#include <io.h>
#pragma hdrstop

static int OpenFramFile(void)
{
int FlashFile;
char TMP[1024];

FlashFile=open("fram.dat",O_CREAT | O_BINARY | O_RDWR, S_IWRITE | S_IREAD);
if (filelength(FlashFile)<FramSize) {
    memset(TMP,255,1024);
    lseek(FlashFile,0,SEEK_END);
    while (filelength(FlashFile)<FramSize)
        write(FlashFile, TMP, 1024);
}

return FlashFile;
}


unsigned int WriteFram(unsigned int addr, void *Buf, unsigned int Count)
{
int FlashFile;

FlashFile=OpenFramFile();

lseek(FlashFile,addr,0);
write(FlashFile, Buf, Count);

close(FlashFile);
FlashFile=0;

return Count;
}



unsigned int ReadFram(unsigned int addr, void *Buf, unsigned int Count)
{
int FlashFile;

FlashFile=OpenFramFile();

lseek(FlashFile,addr,0);
read(FlashFile, Buf, Count);

close(FlashFile);
FlashFile=0;

return Count;
}

#endif

/*
short Bf[512];

static void TST(int Addr)
{
 int i;

 memset((void *)Bf[0], 0, 1024);
 ReadFram(Addr,Bf,1024);
 for( i=0;i<512;i++)
    if (Bf[i]!=i+2){
     EnableLCD();
     PrintDebugHex(Addr+i);
     k++;
     Redraw();
    }
}

void TestFram(void)
{
 int i,j;

 DisableLCD(0xFF);

 Delay(1000);
 for(j=0;j<32;j++){
   for(i=0;i<512;i++)
     Bf[i]=i+2;
   WriteFram(j*1024,Bf,1024);
   TST(j*1024);
 }

  EnableLCD();
  if(k==0){
    OutString(0,8,"Test Fram - Ok");
  }
  else {
    OutString(0,8,"Test Fram - Error");
  }
  Redraw();

  WaitReadKey();
}

*/
