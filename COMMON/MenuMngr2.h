#ifndef _MenuMngr_h
#define _MenuMngr_h

#define MaxCursors 12
#define MaxMenuHeight 4

struct MenuMngr{
           char NumberCurCursor,NumberTopCursor,NumberCursors;
           char MessageFont;
           char* Cursors[MaxCursors];
           char Param[MaxCursors][6];
};

void InitMenuMngr(struct MenuMngr *Menu, unsigned int aFontType);
void AddCursor(struct MenuMngr *Menu, char *Text);
void SetCursorText(struct MenuMngr *Menu, int aCursor, char *Text);
void SetCursorParam(struct MenuMngr *Menu, int aCursor, char *Text);
void RedrawMenu(struct MenuMngr *Menu);
char ScanMenu(struct MenuMngr *Menu);
char ScanMenuNoWait(struct MenuMngr *Menu);

#endif
