#ifndef _rtc_h
#define _rtc_h

#include "TypesDef.h"

#ifdef SAM7SE
#define AT91C_TWI_TXRDY AT91C_TWI_TXRDY_MASTER
#define AT91C_TWI_TXCOMP AT91C_TWI_TXCOMP_MASTER
#endif

char GET_TIME(u32 *year,u32 *month,u32 *day,u32 *hour,u32 *min,u32 *sec);
char SET_TIME(u32  year,u32  month,u32  day,u32  hour,u32  min,u32  sec);
void InitRTC(void);

void TestRTC(void);

int ReadTWI(int Addr, char* Data, int Len);
void clr_RTC_STATUS();

#endif


