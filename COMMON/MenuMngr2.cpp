#include "Graph.h"
#include "Defs.h"
#include "LCD.h"
#include "Picture.h"
#include "keyboard.h"
#include "DefKB.h"
#include "MenuMngr.h"
#include <string.h>


void InitMenuMngr(struct MenuMngr *Menu, unsigned int aFontType)
{
Menu->NumberCurCursor=Menu->NumberTopCursor=Menu->NumberCursors=0;
Menu->MessageFont=aFontType;
}


void AddCursor(struct MenuMngr *Menu, char *Text)
{
if (Menu->NumberCursors>=MaxCursors)
   return;
Menu->Cursors[Menu->NumberCursors]=Text;
Menu->Param[Menu->NumberCursors][0]=0;
Menu->NumberCursors++;
return;
}


void SetCursorText(struct MenuMngr *Menu, int aCursor, char *Text)
{
if (aCursor<Menu->NumberCursors) {
   Menu->Cursors[aCursor]=Text;
   RedrawMenu(Menu);
}
}


void SetCursorParam(struct MenuMngr *Menu, int aCursor, char *Text)
{
if (aCursor<Menu->NumberCursors) {
   strcpy(Menu->Param[aCursor],Text);
   RedrawMenu(Menu);
}
}


void RedrawMenu(struct MenuMngr *Menu)
{
int i,j;
char s[22];

SetFont(Menu->MessageFont);
i=Menu->NumberTopCursor;
while (i<Menu->NumberTopCursor+MaxMenuHeight) {
      if (i>=Menu->NumberCursors)
         break;
      s[0]=' ';
      strcpy(&(s[1]),Menu->Cursors[i]);

      if (Menu->Param[i][0]!=0) {
         j=strlen(s);
         while (j<13)
             s[j++]=' ';
         s[j]=0;
         strcat(s,Menu->Param[i]);
      }

      j=strlen(s);
      while (j<19)
          s[j++]=' ';
      s[j]=0;
      if (i==Menu->NumberCurCursor) OutStringInv(0,(i-Menu->NumberTopCursor)*8,s);
         else OutString(0,(i-Menu->NumberTopCursor)*8,s);

      i++;
}
s[1]=0;
if (Menu->NumberTopCursor>0) s[0]=24;
   else s[0]=' ';
OutString(116,0,s);
if (Menu->NumberTopCursor<Menu->NumberCursors-MaxMenuHeight) s[0]=25;
                                                        else s[0]=' ';
OutString(116,24,s);

NeedRedraw();
}


void DoDownKey(struct MenuMngr *Menu)
{
if (Menu->NumberCursors) {
    if (Menu->NumberCurCursor<Menu->NumberCursors-1) {
       Menu->NumberCurCursor++;
       if (Menu->NumberCurCursor-Menu->NumberTopCursor>=MaxMenuHeight-1) {
          if (Menu->NumberCurCursor<Menu->NumberCursors-1)
             Menu->NumberTopCursor=Menu->NumberCurCursor-(MaxMenuHeight-2);
       }
    } else {
       Menu->NumberCurCursor=Menu->NumberTopCursor=0;
    }
    RedrawMenu(Menu);
}
}


char ScanMenu(struct MenuMngr *Menu)
{
KEY ch;

while (1) {
   RedrawMenu(Menu);
   ch=WaitReadKey()&KB_KEY;
   if (ch==KB_SET) {DoDownKey(Menu);}
   if (ch==KB_HOLD) {return Menu->NumberCurCursor+1;}
}
}


char ScanMenuNoWait(struct MenuMngr *Menu)
{
KEY ch;

RedrawMenu(Menu);
ch=ReadKey()&KB_KEY;
if (ch==KB_SET) {DoDownKey(Menu);}
if (ch==KB_HOLD) {return Menu->NumberCurCursor+1;}
return 0;
}



