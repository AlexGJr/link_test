#include "GraphInput.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "DefKB.h"
#include "Graph.h"
#include "Keyboard.h"
#include "LCD.h"
#include "rtc.h"
#include "strutil.h"


/*

*/
char GetCurSymbol(char EndInp)
{
char CurSymbol;
if (EndInp==0) EndInp='0';
if (EndInp==' ') EndInp='0';
if (EndInp=='-') CurSymbol=10;
   else { CurSymbol=(char)EndInp-(char)'0'; }
return CurSymbol;
}
/*
int InputInt(unsigned int X,unsigned int Y,int IntValue,unsigned char DigitCount)
{
   unsigned char CurPos;
   char EndInp;
   int ReturnValue;
   KEY ch;

   int2bcd(IntValue);
   CurPos=0;
   EndInp=0;
   while (1){
       BCDToStr(BCDValue,DigitCount);
       OutString1(X,Y,OutStr);
       DrawLine(X+(DigitCount-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(DigitCount-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
#ifndef __arm
       Redraw();
#endif
       ch=WaitReadKey();
       switch (ch) {
       case KB_ESC   : { EndInp=1; ReturnValue=IntValue; break;}
       case KB_ENTER : { EndInp=1; ReturnValue=BCDToInt(BCDValue); break;}
       case KB_LEFT  : {if (CurPos<DigitCount-1) CurPos++; break;}
       case KB_RIGHT : {if (CurPos>0) CurPos--; break;}
       case KB_UP : {if (BCDValue[5-CurPos]<9) BCDValue[5-CurPos]++;
                        else BCDValue[5-CurPos]=0; break;}
       case KB_DOWN : {if (BCDValue[5-CurPos]>0) BCDValue[5-CurPos]--;
                        else BCDValue[5-CurPos]=9; break;}
#ifdef BigKB
       case  KB_1    : {BCDValue[5-CurPos]=1; break;}
       case  KB_2    : {BCDValue[5-CurPos]=2; break;}
       case  KB_3    : {BCDValue[5-CurPos]=3; break;}
       case  KB_4    : {BCDValue[5-CurPos]=4; break;}
       case  KB_5    : {BCDValue[5-CurPos]=5; break;}
       case  KB_6    : {BCDValue[5-CurPos]=6; break;}
       case  KB_7    : {BCDValue[5-CurPos]=7; break;}
       case  KB_8    : {BCDValue[5-CurPos]=8; break;}
       case  KB_9    : {BCDValue[5-CurPos]=9; break;}
       case  KB_0    : {BCDValue[5-CurPos]=0; break;}
       case  KB_MINUS : {if (BCDValue[0]) BCDValue[0]=0; else BCDValue[0]=1; break;}
#endif
       }
       if ((CurPos==DigitCount-1)&(BCDValue[5-CurPos])) BCDValue[0]=0;
       if (EndInp) break;
   }
   OutInt(X,Y,ReturnValue,DigitCount);
   return ReturnValue;
}
*/

void InputTime(unsigned int X,unsigned int Y,char* Hour,char* Min,char* Sec)
{
   unsigned char CurPos;
   char Value[9], ss1[3], ss2[3], ss3[3];
   int i1,i2,i3;
   char EndInp;
   KEY ch;

   CurPos=0;
   EndInp=0;

   i1=*Hour;
   i2=*Min;
   i3=*Sec;
   IntToStr(i1,2);
   Value[0]=OutStr[0];
   Value[1]=OutStr[1];
   if (Value[0]==' ') Value[0]='0';
   IntToStr(i2,2);
   Value[3]=OutStr[0];
   Value[4]=OutStr[1];
   if (Value[3]==' ') Value[3]='0';
   IntToStr(i3,2);
   Value[6]=OutStr[0];
   Value[7]=OutStr[1];
   if (Value[6]==' ') Value[6]='0';
   Value[2]=':';
   Value[5]=':';
   Value[8]=0;

   while (1){
       OutString1(X,Y,Value);
       DrawLine(X+(8-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(8-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
#ifndef __arm
       Redraw();
#endif
       ch=WaitReadKey();
       switch (ch) {
       case KB_ESC   : { EndInp=1;  break;}
       case KB_ENTER : { EndInp=1;
                        ss1[0]=Value[0]; ss1[1]=Value[1]; ss1[2]=0;
                        *Hour=StrToInt(ss1);
                        ss2[0]=Value[3]; ss2[1]=Value[4]; ss2[2]=0;
                        *Min=StrToInt(ss2);
                        ss3[0]=Value[6]; ss3[1]=Value[7]; ss3[2]=0;
                        *Sec=StrToInt(ss3);
                        break;}
       case KB_LEFT  : {if (CurPos<8-1) CurPos++;
                        if ((CurPos==2)|(CurPos==5)) CurPos++;
                        break;}
       case KB_RIGHT : {if (CurPos>0) CurPos--;
                        if ((CurPos==2)|(CurPos==5)) CurPos--;
                        break;}
       }
       if (EndInp) break;
   }
}


char InputTimeUpDown(unsigned int X,unsigned int Y,u16 *Hour,u16 *Min,u16 *Sec)
{
   unsigned char CurPos;
   char Value[9], ss1[5], tmp;
   int i1,i2,i3;
   char EndInp;
   KEY ch;
   UNI_INT t;
   char res=0;

#ifdef angl
   char pm;

extern ROM char TimeAM;
extern ROM char TimePM;
#endif

   CurPos=0;
   EndInp=0;

#ifdef angl
 if (*Hour>=12) {
    i1=*Hour-12;
    pm=1;
 } else {
    i1=*Hour;
    pm=0;
 }
#else
   i1=*Hour;
#endif


   i2=*Min;
   i3=*Sec;
   IntToStr(i1,2);
   Value[0]=OutStr[0];
   Value[1]=OutStr[1];
   if (Value[0]==' ') Value[0]='0';
   IntToStr(i2,2);
   Value[3]=OutStr[0];
   Value[4]=OutStr[1];
   if (Value[3]==' ') Value[3]='0';
   IntToStr(i3,2);
   Value[6]=OutStr[0];
   Value[7]=OutStr[1];
   if (Value[6]==' ') Value[6]='0';
   Value[2]=':';
   Value[5]=':';
   Value[8]=0;

   while (1){
       OutString1(X,Y,Value);
       DrawLine(X+(8-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(8-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
#ifdef angl
       if (pm)
          OutString(X+CurFontWidth*8,Y,&TimePM);
       else
          OutString(X+CurFontWidth*8,Y,&TimeAM);
#endif
#ifndef __arm
       Redraw();
#endif
       ch=WaitReadKey();
       switch (ch) {
       case KB_ESC   : { EndInp=1;  break;}
       case KB_ENTER : { EndInp=1;
			ss1[0]='0';
			ss1[1]='0';
                        ss1[2]=Value[0]; ss1[3]=Value[1]; ss1[4]=0;
                        t=StrToInt(ss1);
                        *Hour=t;
                        ss1[2]=Value[3]; ss1[3]=Value[4]; ss1[4]=0;
                        t=StrToInt(ss1);
                        *Min=t;
                        ss1[2]=Value[6]; ss1[3]=Value[7]; ss1[4]=0;
                        t=StrToInt(ss1);
                        *Sec=t;
                        res=1;
                        break;}
       case KB_LEFT  : {if (CurPos<8-1) CurPos++;
                        if ((CurPos==2)|(CurPos==5)) CurPos++;
                        break;}
       case KB_RIGHT : {if (CurPos>0) CurPos--;
                        if ((CurPos==2)|(CurPos==5)) CurPos--;
                        break;}
       case  KB_UP   : { tmp=Value[7-CurPos]-'0';
                         if (tmp<9) tmp++;
                         switch (tmp){
                            case  0: {
                                     Value[7-CurPos]='0';
#ifdef angl
                                     if (CurPos==7) pm=0;
#endif
                                     break;
                                     }
                            case  1: {
//                                     if ((CurPos==4)&&(Value[3]=='0')&&(Value[4]>'2')) break;
#ifdef angl
                                     if (CurPos==7)
                                        if (Value[1]>'1') {pm=0;break;}
#endif
                                     Value[7-CurPos]='1'; break;}
                            case  2: {
#ifdef angl
                                     if (CurPos==7) {pm=1;break;}
                                     if ((CurPos==6)&&(Value[0]>'0')) break;
#endif
                                     if ((CurPos==7)&&(Value[0]>'0')&&(Value[1]>'3')) break;
                                     Value[7-CurPos]='2'; break;}
                            case  3: {
                                     if (CurPos==7) break;
                                     Value[7-CurPos]='3'; break;
                                     }
                            case  4: {
                                     if (CurPos==7) break;
                                     if ((CurPos==6)&(Value[0]>'1')) break;
                                     Value[7-CurPos]='4'; break;}
                            case  5: {
                                     if (CurPos==7) break;
                                     if ((CurPos==6)&(Value[0]>'1')) break;
                                     Value[7-CurPos]='5'; break;}
                            case  6: {
                                     if (CurPos==7) break;
                                     if ((CurPos==6)&(Value[0]>'1')) break;
                                     if ((CurPos==4)|(CurPos==1)) break;
                                     Value[7-CurPos]='6'; break;}
                            case  7: {
                                      if (CurPos==7) break;
                                      if ((CurPos==6)&(Value[0]>'1')) break;
                                      if ((CurPos==4)|(CurPos==1)) break;
                                      Value[7-CurPos]='7'; break;}
                            case  8: {
                                      if (CurPos==7) break;
                                      if ((CurPos==6)&(Value[0]>'1')) break;
                                      if ((CurPos==4)|(CurPos==1)) break;
                                      Value[7-CurPos]='8'; break;}
                            case  9: {
                                      if (CurPos==7) break;
                                      if ((CurPos==6)&(Value[0]>'1')) break;
                                      if ((CurPos==4)|(CurPos==1)) break;
                                      Value[7-CurPos]='9'; break;}
                         }
                         break;
                      }
       case  KB_DOWN  : { tmp=Value[7-CurPos]-'0';
                         if (tmp>0) tmp--;
                         switch (tmp){
                            case  0: {
                                     Value[7-CurPos]='0';
#ifdef angl
                                     if (CurPos==7) {pm=0;}
#endif
                                     break;
                                     }
                            case  1: {
                                      Value[7-CurPos]='1';
                                      break;
                                      }
                            case  2: {Value[7-CurPos]='2'; break;}
                            case  3: {
                                     if (CurPos==7) break;
                                     Value[7-CurPos]='3'; break;
                                     }
                            case  4: {
                                     if (CurPos==7) break;
                                     if ((CurPos==6)&(Value[0]>'1')) break;
                                     Value[7-CurPos]='4'; break;}
                            case  5: {
                                     if (CurPos==7) break;
                                     if ((CurPos==6)&(Value[0]>'1')) break;
                                     Value[7-CurPos]='5'; break;}
                            case  6: {
                                     if (CurPos==7) break;
                                     if ((CurPos==6)&(Value[0]>'1')) break;
                                     if ((CurPos==4)|(CurPos==1)) break;
                                     Value[7-CurPos]='6'; break;}
                            case  7: {
                                      if (CurPos==7) break;
                                      if ((CurPos==6)&(Value[0]>'1')) break;
                                      if ((CurPos==4)|(CurPos==1)) break;
                                      Value[7-CurPos]='7'; break;}
                            case  8: {
                                      if (CurPos==7) break;
                                      if ((CurPos==6)&(Value[0]>'1')) break;
                                      if ((CurPos==4)|(CurPos==1)) break;
                                      Value[7-CurPos]='8'; break;}
                            case  9: {
                                      if (CurPos==7) break;
                                      if ((CurPos==6)&(Value[0]>'1')) break;
                                      if ((CurPos==4)|(CurPos==1)) break;
                                      Value[7-CurPos]='9'; break;}
                         }
                         break;
                      }
       }
       if (EndInp) break;
   }
return res;
}


char InputDateUpDown(unsigned int X,unsigned int Y,u16 *Day,u16 *Month,u16 *Year)
{
   unsigned char CurPos;//, i;
   char Value[9], ss1[3], ss2[3], ss3[3];
   int i1,i2,i3;
   char EndInp,Pos;
   unsigned int ch;
   char res=0;

   CurPos=0;
   EndInp=0;

   i1=*Day;
   i2=*Month;
   i3=*Year%100;
   IntToStr(i1,2);
   Value[0]=OutStr[0];
   Value[1]=OutStr[1];
   if (Value[0]==' ') Value[0]='0';
   IntToStr(i2,2);
   Value[3]=OutStr[0];
   Value[4]=OutStr[1];
   if (Value[3]==' ') Value[3]='0';
   IntToStr(i3,2);
   Value[6]=OutStr[0];
   Value[7]=OutStr[1];
   if (Value[6]==' ') Value[6]='0';
   Value[2]='.';
   Value[5]='.';
   Value[8]=0;

   while (1>0){
       OutString1(X,Y,Value);
       DrawLine(X+(8-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(8-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKey();
       Pos=7-CurPos;
       switch (ch) {
       case KB_ESC   : { EndInp=1;  break;}
       case KB_ENTER : {
                       ss1[0]=Value[0]; ss1[1]=Value[1]; ss1[2]=0;
                       //i1=atoi(ss1);
                       i1=StrToInt(ss1);
                       ss2[0]=Value[3]; ss2[1]=Value[4]; ss2[2]=0;
                       //i2=atoi(ss2);
                       i2=StrToInt(ss2);
                       ss3[0]=Value[6]; ss3[1]=Value[7]; ss3[2]=0;
                       //i3=atoi(ss3);
                       i3=StrToInt(ss3);
                       if (IsDate(i3,i2,i1)==1)
                         {EndInp=1; *Day=i1; *Month=i2; *Year=2000+i3;}
                       res=1;
                       break;
                       }
       case KB_LEFT  : {if (CurPos<8-1) CurPos++;
                        if ((CurPos==2)||(CurPos==5)) CurPos++;
                        break;
                        }
       case KB_RIGHT : {if (CurPos>0) CurPos--;
                        if ((CurPos==2)||(CurPos==5)) CurPos--;
                        break;
                        }
#ifdef BigKB
       case  KB_0    : {Value[Pos]='0'; break;}
       case  KB_1    : {Value[Pos]='1'; break;}
       case  KB_2    : {Value[Pos]='2'; break;}
       case  KB_3    : {Value[Pos]='3'; break;}
       case  KB_4    : {Value[Pos]='4'; break;}
       case  KB_5    : {Value[Pos]='5'; break;}
       case  KB_6    : {Value[Pos]='6'; break;}
       case  KB_7    : {Value[Pos]='7'; break;}
       case  KB_8    : {Value[Pos]='8'; break;}
       case  KB_9    : {Value[Pos]='9'; break;}
#endif
       case  KB_UP   : {i1=Value[Pos]-'0';
                        if (i1<9) Value[Pos]++;
                        else Value[Pos]='0';
                        break;}
       case  KB_DOWN : {i1=Value[7-CurPos]-'0';
                        if (i1>0) Value[7-CurPos]--;
                        else Value[7-CurPos]='9';
                        break;}
       }
       if (EndInp) break;
   }
return res;
}

char InputString(unsigned int X,unsigned int Y,char *Str,char StrLength,char Font,char FullTable)
{
#define MaxStrInp 30
char CurPos,EndInp=0,Length,TmpStr[MaxStrInp],i,n;
unsigned int ch;

   if (StrLength>MaxStrInp) Length=MaxStrInp;
   else Length=StrLength;
   for (i=0;i<Length;i++) TmpStr[i]=32;
   for (i=0;i<Length;i++)
       if (Str[i]==0) break;
       else TmpStr[i]=Str[i];
   TmpStr[Length]=0;

   CurPos=Length-1;
   SetFont(Font);
   //AutoRepeatEnable();
   //while (ReadKey()!=KB_NO);
   n=TmpStr[0];
   while (1>0){
       OutString1(X,Y,TmpStr);
       DrawLine(X+(Length-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(Length-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKey();

       switch (ch) {
       case KB_ESC   : { EndInp=1;  break;}
       case KB_ENTER : { EndInp=1;
                         for (i=Length;i>0;i--)
                             if (TmpStr[i]==32) TmpStr[i]=0;
                             else break;
                         for (i=0;i<Length;i++)
                             Str[i]=TmpStr[i];
                         Str[Length]=0;
                         break;
                       }
       case KB_LEFT  : {CurPos++;
                        if (CurPos==Length) CurPos=0;
                        n=TmpStr[Length-1-CurPos];
                        break;}
       case KB_RIGHT : {
                        if (CurPos==0) {CurPos=Length-1;} else CurPos--;
                        n=TmpStr[Length-1-CurPos];
                        break;}
       case  KB_UP    : {
                         if (FullTable) {
                            if (n==255) n=1;
                            else n++;
                         } else {
                            if (n==255) {n=32; }
                            else {
                                n++;
                                if (n==33) n=48;
                                if (n==58) n=65;
                                if (n==91) n=192;
                            }
                         }
                         TmpStr[Length-1-CurPos]=n;
                         break;
                        }
       case  KB_DOWN  : {
                         if (FullTable) {
                            if (n==1) n=255;
                            else n--;
                         } else {
                             n--;
                             if (n==31) n=255;
                             if (n==47) n=32;
                             if (n==191) n=90;
                             if (n==64) n=57;
                           }
                         TmpStr[Length-1-CurPos]=n;
                         break;
                        }
      }

       if (EndInp) break;
   }
   //AutoRepeatDisable();
ClearRect(X,Y,X+StrLength*CurFontWidth,Y+CurFontHeight);
OutString1(X,Y,Str);
Redraw();
return ch==KB_ENTER;
}


float InputFloat(unsigned int X,unsigned int Y,float Num,int r,int I)
{
unsigned char CurPos;
char Value[20], ss1[10];
int i,j;
KEY ch;
float a1,a2;
char Symbols[11]={'0','1','2','3','4','5','6','7','8','9','-'};
char CurSymbol;
int Znak;


CurPos=0;
strcpy(Value,FloatToStr(Num*1.000001,r,I));
zerointstr(Value);
CurSymbol=GetCurSymbol(Value[CurPos]);
while (1)
  {
  OutString1(X,Y,Value);
  DrawLine(X+CurPos*CurFontWidth,Y+CurFontHeight-1,
           X+(CurPos+1)*CurFontWidth-1,Y+CurFontHeight-1);
  Redraw();
  ch=WaitReadKey();
  do switch (ch)
     {
     case KB_ESC   : {goto End;}
     case KB_LEFT  : {if (CurPos>0) CurPos--;
                      else          CurPos=r-1;
                      CurSymbol=GetCurSymbol(Value[CurPos]);
                      break;}
     case KB_RIGHT : {if (CurPos<r-1) CurPos++;
                      else            CurPos=0;
                      CurSymbol=GetCurSymbol(Value[CurPos]);
                      break;}
     case KB_UP    : {do
                        {
                        if (CurSymbol<10) CurSymbol++;
                        else CurSymbol=0;
                        Value[CurPos]=Symbols[CurSymbol];
                        }
                      while ( (CurPos!=0) && (Value[CurPos]=='-'));
                      break;}
     case KB_DOWN  : {do
                        {
                        if (CurSymbol>0) CurSymbol--;
                        else CurSymbol=10;
                        Value[CurPos]=Symbols[CurSymbol];
                        }
                      while ( (CurPos!=0) && (Value[CurPos]=='-'));
                      break;}
     case KB_ENTER : {j=0;
                      if (Value[0]=='-') Znak=-1;
                      else               Znak=1;
                      for (i=0;i<r;i++)
                          {
                          if (((Value[i]>='0') && (Value[i]<='9')) || (Value[i]=='-'))
                            ss1[j++]=Value[i];
                          if (Value[i]=='.') break;
                          }
                      if (j==0)
			 {ss1[0]='0';j=1;}
                      ss1[j]=0;
                      j=StrToInt(ss1);
                      a1=j;
                      a2=1;
                      for (j=0;i<r;i++)
                           if ((Value[i]>='0') && (Value[i]<='9'))
                              {
                              ss1[j++]=Value[i];
                              a2 /= 10;
                              }
                      if (j==0)
			 {ss1[0]='0'; j=1;}
                      ss1[j]=0;
                      j=StrToInt(ss1);
                      a2 *=j;
                      Num=(a1>=0)?a1+a2:a1-a2;
                      Num=Znak*fabs(Num);
                      goto End;
                      }
     }
  while (Value[CurPos]=='.');
  }

End:
ClearRect(X,Y,X+r*CurFontWidth,Y+CurFontHeight);
strcpy(Value,FloatToStr(Num*1.000001,r,I));
OutString1(X,Y,Value);
Redraw();
return Num;
}

