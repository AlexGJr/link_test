#include "ind.h"
#include "LCD.h"
#include "Defs.h"
#include "DateTime.h"
#include "DefKB.h"
#include "Keyboard.h"
#include "RTC.h"
#include "SysUtil.h"
#include "GraphMini.h"

char BCDValue[6];
char OutStr[LEN];

void int2bcd(int IntValue)
{
int i;
if (IntValue<0)
{
BCDValue[0]=1;
IntValue=-IntValue;
} else {
BCDValue[0]=0;
}
for (i=5;i>0;i--)
{
    BCDValue[i]=IntValue%10;
    IntValue=IntValue/10;
}
}


char GetDigit(int c)
{
 switch (c) {
    case 1:return ch1;
    case 2:return ch2;
    case 3:return ch3;
    case 4:return ch4;
    case 5:return ch5;
    case 6:return ch6;
    case 7:return ch7;
    case 8:return ch8;
    case 9:return ch9;
   default:
    case 0:return ch0;
 }
}

void OutUInt(char Pos,unsigned int Value, char Digits,char LeftZero)
{
 char i,MaxDigits,FirstD=0,c,CurPos=0;
 unsigned int Div,CurV;

CurPos=Pos;
if (Digits>5) {MaxDigits=5;CurPos=Digits-5+Pos;}
else MaxDigits=Digits;

if (Value>0) {
   CurV=Value;
   for (i=MaxDigits;i>0;i--){
       Div=1;
       for (c=0;c<i-1;c++) Div*=10;
       c=CurV/Div;
       if (c) FirstD=1;
       if (FirstD) {
           IndData[CurPos]=GetDigit(c);
       } else
          if (LeftZero)
             IndData[CurPos]=ch0;
          else
             IndData[CurPos]=chEmpty;
       CurV-=c*Div;
       CurPos++;
       if (CurPos>=IndCount) break;
   }

} else {
   if (LeftZero)
      for (i=0;i<MaxDigits;i++)
          IndData[Pos+i]=GetDigit(0);
   else
      IndData[Pos+Digits-1]=GetDigit(0);
//   IndData[Pos+Digits-1]=ind_const[0];
}
}

void OutLong(char Pos,long Value, char Digits,char LeftZero)
{
 char i,MaxDigits,FirstD=0,c,CurPos=0;
 long Div,CurV;

CurPos=Pos;
if (Digits>8) {MaxDigits=8;CurPos=Digits-8+Pos;}
else MaxDigits=Digits;

if (Value!=0) {
   if (Value<0) {IndData[CurPos]=chMinus; CurPos++; MaxDigits--; CurV=0-Value;}
   else CurV=Value;
   for (i=MaxDigits;i>0;i--){
       Div=1;
       for (c=0;c<i-1;c++) Div*=10;
       c=CurV/Div;
       if (c) FirstD=1;
       if (FirstD)
          IndData[CurPos]=GetDigit(c);
       else
          if (LeftZero)
             IndData[CurPos]=GetDigit(0);
          else
             IndData[CurPos]=chEmpty;
       CurV-=c*Div;
       CurPos++;
       if (CurPos>=IndCount) break;
   }

} else {
   if (LeftZero)
      for (i=0;i<MaxDigits;i++)
          IndData[Pos+i]=GetDigit(0);
   else
      IndData[Pos+Digits-1]=GetDigit(0);
}
}


void OutFloat(char Pos,float Value, char Digits, char DigitsInt,char LeftZero)
{
 char i,minus=0;
 long l1,l2,M=1;
 float f;

if (Value<0) {l1=0-Value; minus=1;f=0-Value;}
else {l1=Value; f=Value;}

for (i=0;i<(Digits-DigitsInt);i++) M*=10;
f=(f-(float)l1)*(float)M;
l2=f;
if (f-(float)l2>=0.5) {
   l2++;
   if (l2>=M) {l1++; l2=0;}
}
if (minus) OutLong(Pos,0-l1,DigitsInt,LeftZero);
else OutLong(Pos,l1,DigitsInt,LeftZero);
SetPoint(Pos+DigitsInt-1);
OutLong(Pos+DigitsInt,l2,Digits-DigitsInt,1);
}


long InpLong(char Pos,long Value, char Digits,char LeftZero)
{
 char CurPos=0,i;
 KEY ch=KB_NO;
 long l1;
 long M;

l1=Value;

while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
      OutLong(Pos,l1,Digits,LeftZero);
      Redraw();
      SetBlinking1((Pos+Digits)-CurPos-1);
      ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
      switch (ch) {
         case KB_LEFT:{
             if (CurPos<Digits-1) CurPos++;
             break;
         }
         case KB_RIGHT:{
             if (CurPos) CurPos--;
             break;
         }
         case KB_UP:{
              M=1;
              for (i=0;i<CurPos+1;i++) M*=10;
              if (l1%M<9*(M/10)) l1+=M/10;
#ifdef gamma_m
              else {
                   if (l1%M>=9*(M/10)) l1-=9*M/10;
                   }
#endif
              break;
         }
         case KB_DOWN:{
                M=1;
                for (i=0;i<CurPos+1;i++) M*=10;
                if (l1%M>=1*(M/10)) l1-=M/10;
             break;
         }
      }



}
SetBlinking((Pos+Digits)-CurPos-1,0);
if (ch==KB_ENTER) return l1;
else return Value;
}


long InpLongSequrity(char Pos,long Value, char Digits,char LeftZero)
{
 char CurPos=0,i;
 KEY ch=KB_NO;
 long l1;
 long M;

l1=Value;

while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
      OutLong(Pos,l1,Digits,LeftZero);
      for (i=Pos;i<Pos+Digits;i++)
          if (i!=(Pos+Digits)-CurPos-1) IndData[i]=~0x10;
      Redraw();
      SetBlinking1((Pos+Digits)-CurPos-1);
      ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
      switch (ch) {
         case KB_LEFT:{
             if (CurPos<Digits-1) CurPos++;
             break;
         }
         case KB_RIGHT:{
             if (CurPos) CurPos--;
             break;
         }
         case KB_UP:{
              M=1;
              for (i=0;i<CurPos+1;i++) M*=10;
              if (l1%M<9*(M/10)) l1+=M/10;
#ifdef gamma_m
              else {
                   if (l1%M>=9*(M/10)) l1-=9*M/10;
                   }
#endif
              break;
         }
         case KB_DOWN:{
                M=1;
                for (i=0;i<CurPos+1;i++) M*=10;
                if (l1%M>=1*(M/10)) l1-=M/10;
             break;
         }
      }
}
SetBlinking((Pos+Digits)-CurPos-1,0);
if (ch==KB_ENTER) return l1;
else return Value;
}

float InpFloat(char Pos,float Value, char Digits, char DigitsInt,char LeftZero)
{
 char CurPos=0,i;
 KEY ch=KB_NO;
 float f;
 long l1,l2;
 long M,MM;

f=Value;
l1=f;
MM=1;
for (i=0;i<(Digits-DigitsInt);i++) MM*=10;
l2=(f-(float)l1)*MM*10.0;
if (l2%10==9) l2++;
l2/=10;

while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
      OutLong(Pos,l1,DigitsInt,LeftZero);
      SetPoint(Pos+DigitsInt-1);
      OutLong(Pos+DigitsInt,l2,Digits-DigitsInt,1);
      Redraw();
//      OutFloat(Pos,f,Digits,DigitsInt,LeftZero);
      SetBlinking1((Pos+Digits)-CurPos-1);
      ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
      switch (ch) {
         case KB_LEFT:{
             if (CurPos<Digits-1) CurPos++;
             break;
         }
         case KB_RIGHT:{
             if (CurPos) CurPos--;
             break;
         }
         case KB_UP:{
              if (CurPos<(Digits-DigitsInt)) {
                M=1;
                for (i=0;i<CurPos;i++) M*=10;

//                if (M==1) {
//                   if (l2<9) l2+=M; else l2-=9;
//                } else {

                  if ((l2%(M*10))<(9*M)) l2+=M;
//                l2+=M;
#ifdef gamma_m
                   else l2=l2-(9*M);

#endif
//               }
              } else {
                 //�����
                M=1;
                for (i=0;i<(CurPos-(Digits-DigitsInt)+1);i++) M*=10;
                if ((l1%M)<(9*(M/10))) l1+=(M/10);
#ifdef gamma_m
                else {
                     if ((l1%M)>=(9*(M/10))) l1-=9*M/10;
                     }
#endif

              }
              break;
         }
         case KB_DOWN:{
              if (CurPos<(Digits-DigitsInt)) {
                M=1;
                for (i=0;i<(Digits-DigitsInt+CurPos-1);i++) M*=10;
                if (l2%M>=1*(M/10)) l2-=(M/10);
              } else {
                 //�����
                M=1;
                for (i=0;i<(CurPos-(Digits-DigitsInt)+1);i++) M*=10;
                if (l1%M>=1*(M/10)) l1-=(M/10);
              }
             break;
         }
      }



}
SetBlinking((Pos+Digits)-CurPos-1,0);
f=(float)((((float)l2)/((float)MM))+(float)l1)*1.000001;
//f=(float)(((float)l2)/((float)MM));
//f=f+l1;
if (ch==KB_ENTER) return f;
else return Value;
}

void OutTime(void)
{

 int2bcd(DateTime.Hour);
 IndData[0]=GetDigit(BCDValue[4]);
 IndData[1]=GetDigit(BCDValue[5]);

 int2bcd(DateTime.Min);
 IndData[2]=GetDigit(BCDValue[4]);
 IndData[3]=GetDigit(BCDValue[5]);

 int2bcd(DateTime.Sec);
 IndData[4]=GetDigit(BCDValue[4]);
 IndData[5]=GetDigit(BCDValue[5]);

 SetPoint(1);
 SetPoint(3);
}

void OutDate(void)
{

 int2bcd(DateTime.Month);
 IndData[0]=GetDigit(BCDValue[4]);
 IndData[1]=GetDigit(BCDValue[5]);

 int2bcd(DateTime.Day);
 IndData[2]=GetDigit(BCDValue[4]);
 IndData[3]=GetDigit(BCDValue[5]);

 int2bcd(DateTime.Year);
 IndData[4]=GetDigit(BCDValue[4]);
 IndData[5]=GetDigit(BCDValue[5]);

 SetPoint(1);
 SetPoint(3);
}

/*
void SetBlinking2(char Pos)
{
  SetBlinking(0,0);
  SetBlinking(1,0);
  SetBlinking(2,0);
  SetBlinking(3,0);
  SetBlinking(4,0);
  SetBlinking(5,0);
  switch (Pos) {
//     case 0:{ SetBlinking(6,1); SetBlinking(7,1); break;}
     case 0:{ SetBlinking(4,1); SetBlinking(5,1); break;}
     case 1:{ SetBlinking(2,1); SetBlinking(3,1); break;}
     case 2:{ SetBlinking(0,1); SetBlinking(1,1); break;}
  }
}
*/

void InpTime(void)
{ char CurPos=0;
  KEY ch=KB_NO;

  OutTime();
  SetBlinking2(CurPos);
  Redraw();
  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
     ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
     switch (ch) {
       case (char)KB_LEFT: {
          if (CurPos<2) {
             CurPos++;
             SetBlinking2(CurPos);
          }
          break;
       }
       case (char)KB_RIGHT: {
          if (CurPos>0) {
             CurPos--;
             SetBlinking2(CurPos);
          }
          break;
       }
       case (char)KB_UP: {
          if ((CurPos==0)&&(DateTime.Sec<59)) DateTime.Sec++;
#ifdef gamma_m
          else if ((CurPos==0)&&(DateTime.Sec==59)) DateTime.Sec=0;
#endif
          if ((CurPos==1)&&(DateTime.Min<59)) DateTime.Min++;
#ifdef gamma_m
          else if ((CurPos==1)&&(DateTime.Min==59)) DateTime.Min=0;
#endif
          if ((CurPos==2)&&(DateTime.Hour<23)) DateTime.Hour++;
#ifdef gamma_m
          else if ((CurPos==2)&&(DateTime.Hour==23)) DateTime.Hour=0;
#endif
          OutTime();
          Redraw();
          break;
       }
       case (char)KB_DOWN: {
          if ((CurPos==0)&&(DateTime.Sec>0)) DateTime.Sec--;
          if ((CurPos==1)&&(DateTime.Min>0)) DateTime.Min--;
          if ((CurPos==2)&&(DateTime.Hour>0)) DateTime.Hour--;
          OutTime();
          Redraw();
          break;
       }
       case (char)KB_ENTER: {
          SET_TIME_FROMBUF();
       }
     }
  }
  SetBlinking2(10);
}

void InpTimeHM(char *H,char *M)
{
#define Step 1

 char CurPos=0,HH,MM;
 KEY ch=KB_NO;


  HH=*H;
  MM=*M;
  SetBlinking2(CurPos);
  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
     int2bcd(HH);
     IndData[2]=GetDigit(BCDValue[4]);
     IndData[3]=GetDigit(BCDValue[5]);
     int2bcd(MM);
     IndData[4]=GetDigit(BCDValue[4]);
     IndData[5]=GetDigit(BCDValue[5]);
     SetPoint(3);

     Redraw();
     ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
     switch (ch) {
       case (char)KB_LEFT: {
          if (!CurPos) {
             CurPos++;
             SetBlinking2(CurPos);
          }
          break;
       }
       case (char)KB_RIGHT: {
          if (CurPos) {
             CurPos--;
             SetBlinking2(CurPos);
          }
          break;
       }
       case (char)KB_UP: {
          if (CurPos==0)
             if (MM<60-Step) MM+=Step;
#ifdef gamma_m
             else MM=0;
#endif
          if (CurPos==1)
             if (HH<23) HH++;
#ifdef gamma_m
             else HH=0;
#endif
          break;
       }
       case (char)KB_DOWN: {
          if ((CurPos==0)&&(MM>0)) MM-=Step;
          if ((CurPos==1)&&(HH>0)) HH--;
          break;
       }
       case (char)KB_ENTER: {
          *H=HH;
          *M=MM;
       }
     }
  }
  SetBlinking2(10);
}


char InpDateDM(char *D,char *M)
{ char CurPos=0,DD,MM;
  KEY ch=KB_NO;

  DD=*D;
  MM=*M;
  SetBlinking2(CurPos);
  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
     int2bcd(MM);
     IndData[2]=GetDigit(BCDValue[4]);
     IndData[3]=GetDigit(BCDValue[5]);
     int2bcd(DD);
     IndData[4]=GetDigit(BCDValue[4]);
     IndData[5]=GetDigit(BCDValue[5]);
     SetPoint(3);

     Redraw();
     ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
     switch (ch) {
       case (char)KB_LEFT: {
          if (!CurPos) {
             CurPos++;
             SetBlinking2(CurPos);
          }
          break;
       }
       case (char)KB_RIGHT: {
          if (CurPos) {
             CurPos--;
             SetBlinking2(CurPos);
          }
          break;
       }
       case (char)KB_UP: {
          if (CurPos==0) {
             if (((MM==1)||
                  (MM==3)||
                  (MM==5)||
                  (MM==7)||
                  (MM==8)||
                  (MM==10)||
                  (MM==12))&&(DD<31)) DD++;
             if (((MM==4)||
                  (MM==6)||
                  (MM==9)||
                  (MM==11))&&(DD<30)) DD++;
             if ((MM==2)&&(DD<28)) DD++;
          }

          if ((CurPos==1)&&(MM<12)) MM++;

          break;
       }
       case (char)KB_DOWN: {
          if ((CurPos==0)&&(DD>1)) DD--;
          if ((CurPos==1)&&(MM>1)) MM--;
          break;
       }
       case (char)KB_ENTER: {
          *D=DD;
          *M=MM;
       }
     }
  }
  SetBlinking2(10);
  if (ch==KB_ENTER) return 1;
  else return 0;
}


void InpDate(void)
{ char CurPos=0;
  struct StDateTime tmpDateTime;
  KEY ch=KB_NO;


  tmpDateTime.Year=DateTime.Year;
  tmpDateTime.Month=DateTime.Month;
  tmpDateTime.Day=DateTime.Day;
  OutDate();
  SetBlinking2(CurPos);
  Redraw();
  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
     ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
     switch (ch) {
       case (char)KB_LEFT: {
          if (CurPos<2) {
             CurPos++;
             SetBlinking2(CurPos);
          }
          break;
       }
       case (char)KB_RIGHT: {
          if (CurPos>0) {
             CurPos--;
             SetBlinking2(CurPos);
          }
          break;
       }
       case (char)KB_UP: {
          if ((CurPos==0)&&(tmpDateTime.Year<99)) {
             tmpDateTime.Year++;
          }
#ifdef gamma_m
         else if (tmpDateTime.Year==99) tmpDateTime.Year=0;
#endif
          if (CurPos==1) {
             if  ((tmpDateTime.Month==1)||
                  (tmpDateTime.Month==3)||
                  (tmpDateTime.Month==5)||
                  (tmpDateTime.Month==7)||
                  (tmpDateTime.Month==8)||
                  (tmpDateTime.Month==10)||
                  (tmpDateTime.Month==12))
                 if (tmpDateTime.Day<31) tmpDateTime.Day++;

#ifdef gamma_m
                 else tmpDateTime.Day=1;
#endif

             if ((tmpDateTime.Month==4)||
                 (tmpDateTime.Month==6)||
                 (tmpDateTime.Month==9)||
                 (tmpDateTime.Month==11))
                 if (tmpDateTime.Day<30) tmpDateTime.Day++;
#ifdef gamma_m
                 else tmpDateTime.Day=1;
#endif

             if ((tmpDateTime.Month==2)&&(tmpDateTime.Year%4==0))
                if (tmpDateTime.Day<29) tmpDateTime.Day++;
#ifdef gamma_m
               else tmpDateTime.Day=1;
#endif

             if ((tmpDateTime.Month==2)&&(tmpDateTime.Year%4!=0))
                if (tmpDateTime.Day<28) tmpDateTime.Day++;
#ifdef gamma_m
                else tmpDateTime.Day=1;
#endif
          }
          if ((CurPos==2)&&(tmpDateTime.Month<12)) tmpDateTime.Month++;
#ifdef gamma_m
          else if (tmpDateTime.Month==12) tmpDateTime.Month=1;
#endif
          DateTime.Year=tmpDateTime.Year;
          DateTime.Month=tmpDateTime.Month;
          DateTime.Day=tmpDateTime.Day;
          OutDate();
          Redraw();
          break;
       }
       case (char)KB_DOWN: {
          if ((CurPos==0)&&(tmpDateTime.Year)) tmpDateTime.Year--;
          if ((CurPos==1)&&(tmpDateTime.Day>1))  tmpDateTime.Day--;
          if ((CurPos==2)&&(tmpDateTime.Month>1)) tmpDateTime.Month--;
          DateTime.Year=tmpDateTime.Year;
          DateTime.Month=tmpDateTime.Month;
          DateTime.Day=tmpDateTime.Day;
          OutDate();
          Redraw();
          break;
       }
       case (char)KB_ENTER: {
          GET_TIME_INBUF();
          DateTime.Year=tmpDateTime.Year;
          DateTime.Month=tmpDateTime.Month;
          DateTime.Day=tmpDateTime.Day;
          SET_TIME_FROMBUF();
          break;
       }

     }

  }
  SetBlinking2(10);
}


long InpLongWithMinus(char Pos,long Value, char Digits,char LeftZero)
{
 char CurPos=0,i;
 KEY ch=KB_NO;
 long l1,l;
 long M;

l1=Value;

while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
      OutLong(Pos,l1,Digits,LeftZero);
//      if (l1<0) OutLong(Pos,l1,Digits,LeftZero);
//      else OutLong(Pos+1,l1,Digits-1,LeftZero);
      Redraw();
      SetBlinking1((Pos+Digits)-CurPos-1);
      ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
      switch (ch) {
         case KB_LEFT:{
             if (CurPos<Digits-1) CurPos++;
             break;
         }
         case KB_RIGHT:{
             if (CurPos) CurPos--;
             break;
         }
         case KB_UP:{
              if (CurPos==(Digits-1)) {
                 l1=0-l1;
              } else {
                 M=1;
                 for (i=0;i<CurPos+1;i++) M*=10;
                 l=l1;
                 if (l<0) l=0-l;
                 if (l%M<9*(M/10)) l+=M/10;
#ifdef gamma_m
                 else {
                    if (l%M>=9*(M/10)) l-=9*M/10;
                 }
#endif
                 if (l1<0) l1=-l; else l1=l;
              }
              break;
         }
         case KB_DOWN:{
              if (CurPos==(Digits-1)) {
                 l1=0-l1;
              } else {
                  M=1;
                  for (i=0;i<CurPos+1;i++) M*=10;
                  l=l1;
                  if (l<0) l=0-l;
                  if (l%M>=1*(M/10)) l-=M/10;
                  if (l1<0) l1=-l; else l1=l;
              }
             break;
         }
      }



}
SetBlinking((Pos+Digits)-CurPos-1,0);
if (ch==KB_ENTER) return l1;
else return Value;
}

char* LongToStr(s32 l)
{long Div=1000000000,tmp;
 int i,Value,j=0,IsDigit=0;

 if (l!=0) {
    if (l<0) {OutStr[0]='-'; j++; l=0-l;}
    tmp=l;
    for (i=0;i<10;i++) {
        Value=tmp/Div;
        tmp-=Value*Div;
        Div/=10;
        if (Value) IsDigit=1;
        if (IsDigit) {
           OutStr[j++]=48+Value;
        }
    }
 }else {OutStr[0]='0'; j++;}
 OutStr[j]=0;
 return OutStr;
}

char* IntToStr(int i)
{
return LongToStr((long)i);
}

char* FloatToStr(float f) // XXX.X
{ long l,l1;
  char tmpOutStr[tmpStrLen],Minus=0;
  int i,j;
  float f1;


  if (f<0) {
     Minus=1;
     l=f;
     f1=(-f+l);
  } else {
     l=f;
     f1=(f-l);
  }

  l1=(f1*1000.0);

  if ((f1*1000.0)-(float)l1>=0.5) {
     l1++;
     if (l1>=1000.0) {l++; l1=0;}
  }

  LongToStr(l1);
  j=0;
  if (l1) {
     if (l1<10) j=2;
        else if (l1<100) j=1;
  }
  for (i=0;i<j;i++) tmpOutStr[i]='0';

  for (i=j;i<tmpStrLen;i++) {
      tmpOutStr[i]=OutStr[i-j];
      if (OutStr[i-j]==0) break;
  }
  if ((l==0)&&(Minus)) {
     OutStr[0]='-';
     OutStr[1]='0';
     OutStr[2]=0;
  }  else LongToStr(l);

  for (i=0;i<tmpStrLen;i++) {
      if (OutStr[i]==0) break;
  }
  OutStr[i]='.';
  for (j=i+1;j<tmpStrLen;j++) {
      OutStr[j]=tmpOutStr[j-(i+1)];
      if (tmpOutStr[j-(i+1)]==0) break;
  }
  OutStr[tmpStrLen-1]=0;
return OutStr;
}


char* FloatToStrSpec(float f, char Digits) // XXX.X
{ long l,l1;
  char tmpOutStr[tmpStrLen],Minus=0;
  int i,j;
  float f1;


  if (f<0) {
     Minus=1;
     l=f;
     f1=(-f+l);
  } else {
     l=f;
     f1=(f-l);
  }

  l1=(f1*10000.0);

  if ((f1*10000.0)-(float)l1>=0.5) {
     l1++;
     if (l1>=10000.0) {l++; l1=0;}
  }

  LongToStr(l1);
  j=0;
  if (l1) {
     if (l1<10) j=3;
        else if (l1<100) j=2;
             else if (l1<1000) j=1;
  }
  for (i=0;i<j;i++) tmpOutStr[i]='0';

  for (i=j;i<tmpStrLen;i++) {
      tmpOutStr[i]=OutStr[i-j];
      if (OutStr[i-j]==0) break;
  }
  if ((l==0)&&(Minus)) {
     OutStr[0]='-';
     OutStr[1]='0';
     OutStr[2]=0;
  }  else LongToStr(l);

  for (i=0;i<tmpStrLen;i++) {
      if (OutStr[i]==0) break;
  }
  OutStr[i]='.';
  for (j=i+1;j<tmpStrLen;j++) {
      OutStr[j]=tmpOutStr[j-(i+1)];
      if (tmpOutStr[j-(i+1)]==0) break;
  }
  OutStr[tmpStrLen-1]=0;
return OutStr;
}

char rStrLen(char *s)
{
char i;
if (s[0]==0) return 0;
i=1;
while (s[i]) i++;
return i;
}

float StrToFloat(char* str)
{ int i=0,j=0;
  long Value1=0,Value2=0,Mul1,Mul2;
  float f;
  char Minus=0;

  Mul1=1;
  for (i=rStrLen(str)-1;i>=0;i--) {
      if ((str[i]>='0')&&(str[i]<='9')) Value1+=(str[i]-'0')*Mul1;
      else if (str[i]=='-') Minus=1;
           else if (str[i]=='.') break;
      Mul1*=10;
  }
  if (i>0) {
     j=i;
     Mul2=1;
     for (i=j-1;i>=0;i--) {
         if ((str[i]>='0')&&(str[i]<='9')) Value2+=(str[i]-'0')*Mul2;
         else if (str[i]=='-') {Minus=1; /*Value2=0-Value2;*/ break;}
              else {break;}
         Mul2*=10;
     }
  } else {
    Value2=Value1;
    Value1=0;
  }
  f=(float)Value2+(float)Value1/(float)Mul1;
  if (Minus==1) return (0.0-f);
  else return f;
}
