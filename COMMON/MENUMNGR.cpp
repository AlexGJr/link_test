#include "MenuMngr.h"
#include "protocol.h"

#include "defkb.h"
#include "defs.h"
#include "Graph.h"
//#include "Graph2.h"
#include "LCD.h"
#include "Keyboard.h"
#include "picture.h"
//#include "picres.h"
//#include "setup.h"
//#include "utils.h"
#include "ScrlMngr.h"
//#include "StrUtil.h"

#include <string.h>


void InitMenuMngr(struct MenuMngr *Menu,KEY LefKEY,KEY RighKEY,KEY ActionKey )
{
 (*Menu).NumberCurCursor=1;
 (*Menu).NumberCursors=0;
 (*Menu).LeftKey=LefKEY;
 (*Menu).RightKey=RighKEY;
 (*Menu).ActionKey=ActionKey;
 (*Menu).MessageX=0;
 (*Menu).MessageY=0;
 (*Menu).MessageFont=0;
 (*Menu).DrawLine=0;
 (*Menu).LineX1=0;
 (*Menu).LineY1=0;
 (*Menu).LineX2=0;
 (*Menu).LineY2=0;
}

void AddCursor(struct MenuMngr *Menu,u16 X, u16 Y, char *Cursor, char *Msg,char EInfo, void *El,KEY FKey)
{
struct CursorInfo *Cursors;

if ((*Menu).NumberCursors<MaxCursors) {
   (*Menu).NumberCursors++;
   Cursors=&((*Menu).Cursors[(*Menu).NumberCursors-1]);
   Cursors->X=X;
   Cursors->Y=Y;
   Cursors->Cursor=Cursor;
   Cursors->Message=Msg;
   Cursors->ElementInfo=EInfo;
   Cursors->Element=El;
   Cursors->FKey=FKey;
}
}

void AddCursorMenu(struct MenuMngr *Menu,u16 X, char *Msg,KEY FKey)
{
//AddCursor(Menu, X,206-20,(char *)UpArrowMenu,Msg,NilMenuElement,0,FKey);
//AddCursor(Menu, X,181-20,UpArrowMenu,Msg,NilMenuElement,0,FKey);
AddCursor(Menu, X,44,(char *)UpArrowMenu,Msg,NilMenuElement,0,FKey);
}


void ClearCursor(struct MenuMngr *Menu)
{  char ch;
   struct CursorInfo *Cursors;

   ch=(*Menu).NumberCurCursor-1;

   Cursors=&((*Menu).Cursors[ch]);

   if (Cursors->Cursor!=0)
     {
     //LoadImage(Cursors->Cursor);
     //ClearRect(Cursors->X,Cursors->Y,ImageHeight,ImageWidth);
     ClearPicture(Cursors->X,Cursors->Y,(char *)Cursors->Cursor);
     }
   if ((*Menu).DrawLine){
             DrawLine(Cursors->X+(*Menu).LineX1,
                      Cursors->Y+(*Menu).LineY1,
                      Cursors->X+(*Menu).LineX2,
                      Cursors->Y+(*Menu).LineY2);
   }
}

void RedrawCursor(struct MenuMngr *Menu)
{
struct CursorInfo *Cursors;
Cursors=&((*Menu).Cursors[(*Menu).NumberCurCursor-1]);
if (Cursors->Cursor!=0)
  OutPicture(Cursors->X,Cursors->Y,(char *)Cursors->Cursor);
if ((*Menu).MessageFont)
  {
  SetFont((*Menu).MessageFont);
  OutString((*Menu).MessageX,(*Menu).MessageY,(char *)Cursors->Message);
  }
Redraw();
}

char DoLefKEY(struct MenuMngr *Menu)
{
if ((*Menu).NumberCursors)
   {
   ClearCursor(Menu);
   if ((*Menu).NumberCurCursor>1) (*Menu).NumberCurCursor--;
   else                           (*Menu).NumberCurCursor=(*Menu).NumberCursors;
   return smrRedrawCursor;
   }
else
   return smrNone;
}

char DoRighKEY(struct MenuMngr *Menu)
{
if ((*Menu).NumberCursors)
   {
   ClearCursor(Menu);
   if ((*Menu).NumberCurCursor<(*Menu).NumberCursors) (*Menu).NumberCurCursor++;
   else                                               (*Menu).NumberCurCursor=1;
   return smrRedrawCursor;
   }
else
   return smrNone;
}

void SetMessage(struct MenuMngr *Menu,u16 X,u16 Y,char Font)
{
   (*Menu).MessageX=X;
   (*Menu).MessageY=Y;
   (*Menu).MessageFont=Font;
}

void LineAfterCursor(struct MenuMngr *Menu,u16 X1,u16 Y1,u16 X2,u16 Y2)
{
 (*Menu).DrawLine=1;
 (*Menu).LineX1=X1;
 (*Menu).LineY1=Y1;
 (*Menu).LineX2=X2;
 (*Menu).LineY2=Y2;
}

u16 ScanMenu(struct MenuMngr *Menu)
{
  KEY key;
  u16 ch;
  do
    {
    ch=ScanMenuNoWait(Menu,&key);
    switch (ch)
      {
      case smrRedrawCursor:
           RedrawCursor(Menu); break;
      case smrRedrawScrl:
           Redraw(); break;
      }
    }
  while (ch>MaxCursors);
  return ch;
}

u16 ScanMenuNoWait(struct MenuMngr *Menu,KEY *Key)
{
KEY ch;
char i;

ch=ReadKey();
(*Key)=ch;
if (Menu->NumberCursors==0)
   {
   if ((*Key)==KB_ESC) ch=0;
   else ch=smrNone;
   }
else
   {

   if ((*Key)==KB_ESC) return 0;
   if ((*Key)==KB_NO)  return smrNone;


   switch ((*Menu).Cursors[(*Menu).NumberCurCursor-1].ElementInfo)
      {
      case ScrlMenuElement :
           if (DoScrlNoWait((struct ScrollerMngr *)((*Menu).Cursors[(*Menu).NumberCurCursor-1].Element),ch)==1)
               return smrRedrawScrl;
           break;
      }

   if (ch==(*Menu).LeftKey)   {return DoLefKEY(Menu);}
   if (ch==(*Menu).RightKey)  {return DoRighKEY(Menu);}

   for (i=0;i<(*Menu).NumberCursors;i++)
       {
       if (((*Menu).Cursors[i].FKey!=KB_NO)&&((*Menu).Cursors[i].FKey==ch))
          {
          return i+1;
          }
       }

   if (ch==(*Menu).ActionKey){return Menu->NumberCurCursor;}

   ch=smrNone;
   }
return ch;
}

void DrawLUCorner(int x,int y)
{
OutPoint(x+1,y+4,1);
OutPoint(x+1,y+3,1);
OutPoint(x+2,y+2,1);
OutPoint(x+3,y+1,1);
OutPoint(x+4,y+1,1);
}

void DrawLDCorner(int x,int y)
{
OutPoint(x+1,y-4,1);
OutPoint(x+1,y-3,1);
OutPoint(x+2,y-2,1);
OutPoint(x+3,y-1,1);
OutPoint(x+4,y-1,1);
}


void DrawRUCorner(int x,int y)
{
OutPoint(x-1,y+4,1);
OutPoint(x-1,y+3,1);
OutPoint(x-2,y+2,1);
OutPoint(x-3,y+1,1);
OutPoint(x-4,y+1,1);
}


void DrawRDCorner(int x,int y)
{
OutPoint(x-1,y-4,1);
OutPoint(x-1,y-3,1);
OutPoint(x-2,y-2,1);
OutPoint(x-3,y-1,1);
OutPoint(x-4,y-1,1);
}

void DrawFrame(int x1,int y1,int x2,int y2)
{
DrawLine(x1+5,y1,x2-5,y1);
DrawLine(x1+5,y2,x2-5,y2);
DrawLine(x1,y1+5,x1,y2-5);
DrawLine(x2,y1+5,x2,y2-5);
DrawLUCorner(x1,y1);
DrawRUCorner(x2,y1);
DrawLDCorner(x1,y2);
DrawRDCorner(x2,y2);
}


void DrawUpB(int x,int y,int len)
{
DrawLine(x-3,y+3,x-3,y+15);
DrawLUCorner(x-3,y-2);
DrawLine(x+2,y-2,x+len,y-2);
}

void DrawDnB(int x,int y,int len)
{
DrawLine(x-3,y+15,x+len-5,y+15);
DrawLine(x+len,y-2,x+len,y+10);
DrawRDCorner(x+len,y+15);
}


void DrawButton(int x,int y, int len,char *Text)
{
SetFont(Font8x16);
ClearRect(x,y,x+len,y+CurFontHeight+6);
DrawFrame(x,y,x+len,y+CurFontHeight+6);
if (Text) {OutString(x+(len-strlen(Text)*CurFontWidth)/2,y+3,Text);}
return;
}



void DrawPageHeader(char *Title)
{
ClearLCD();
DrawFrame(0,0,ScreenWidth-1,20);
DrawFrame(0,0,319,215);
SetFont(Font12x16);
if (Title) OutStringCenter(0,3,ScreenWidth,Title);



}

void DrawPageTemplate(char *Title, char *Bottom)
{
DrawPageHeader(Title);
DrawFrame(0,ScreenHeight-21,ScreenWidth-1,ScreenHeight-1);
SetFont(Font8x16);
if (Bottom) OutStringCenter(0,ScreenHeight-1-20+3,ScreenWidth,Bottom);
}

void SetMenuItem(struct MenuMngr *Menu, int aItem)
{
ClearCursor(Menu);
Menu->NumberCurCursor=aItem;
RedrawCursor(Menu);
}

void DrawStr(char *Str)
{
SetFont(Font8x6);
OutStringCenter(0,2,128,Str);
DrawRect(0,0,127,11);
}

void DrawDown(char *Str)
{
SetFont(Font8x6);
OutStringCenter(0,54,128,Str);
DrawRect(0,52,127,63);
}

