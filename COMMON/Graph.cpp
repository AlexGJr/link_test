
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "defkb.h"
#include "defs.h"
#include "DateTime.h"
#include "keyboard.h"
#include "lcd.h"
#include "Graph.h"
#include "debug.h"
#include "Font8x8w.h"
#include "Font8x6w.h"
#include "Font6x5w.h"
#include "Debug.h"
#include "SysUtil.h"

extern struct StDateTime DateTime;
char OutStr[LEN];
char BCDValue[11];



void DrawLine(unsigned int X1,unsigned int Y1,
              unsigned int X2,unsigned int Y2)
{
     int T, E, denom;
     int dX, dY, aux;
     int XX2,YY2;
     int X, Y;
     signed int vertlonger=0;
     signed int Xinc=1, Yinc=1;

     if (X2>ScreenWidth-1) XX2=ScreenWidth-1;
        else XX2=X2;
     if (Y2>ScreenHeight-1) YY2=ScreenHeight-1;
        else YY2=Y2;
     dX=XX2-X1;
     dY=YY2-Y1;
     if (dX<0) {Xinc=-1; dX=-dX;}
     if (dY<0) {Yinc=-1; dY=-dY;}
     if (dY>dX) {aux=dX; dX=dY; dY=aux; vertlonger=1;}
     denom=dX<<1;
     T=dY<<1;
     E=-dX;
     X=X1;
     Y=Y1;
     while (dX>=0) {

       PutPixel(X,Y);

       if ((E+=T)>0){
          if (vertlonger) X+=Xinc; else Y+=Yinc;
          E-=denom;
       }
       if (vertlonger) Y+=Yinc; else X+=Xinc;
       dX-=1;
     }
}



void OutString(unsigned int X,unsigned int Y,char const *String)
{
OutStringMask(X,Y,String,0);
}

void OutStringInv(unsigned int X,unsigned int Y,char const *String)
{
OutStringMask(X,Y,String,0xFF);
}

void OutString1(unsigned int X,unsigned int Y,char String[])
{
OutStringMask(X,Y,String,0);
}

void OutString1Inv(unsigned int X,unsigned int Y,char String[])
{
OutStringMask(X,Y,String,0xFF);
}

int BCDToInt(char Value[6])
{ register int i;
  i=Value[1]*10000+Value[2]*1000+Value[3]*100+Value[4]*10+Value[5];
  if (Value[0]) i=0-i;
  return i;
  }


char* IntToStr(s32 IntValue,u32 DigitCount)
{
int i,Sign,MinDigit;

if (IntValue<0) {
   Sign=1;
   IntValue=-IntValue;
} else {
   Sign=0;
}
for (i=10;i>=0;i--)
{
    BCDValue[i]=IntValue%10;
    IntValue=IntValue/10;
}

MinDigit=0;
for (i=0; i<=10; i++)
    {if (BCDValue[i]) {MinDigit=i; break;}}

for (i=0; i<DigitCount; i++)
    OutStr[i]=' ';
if (MinDigit) {
   if (11-MinDigit+Sign>DigitCount) {
//      PrintDebug("DigitCount overflow");
      DigitCount=11-MinDigit+Sign;
   }

   i=DigitCount-(11-MinDigit)-Sign;
   if (Sign) {
      OutStr[i]='-';
      i++;
   }
   while (i<DigitCount) {
       OutStr[i]=BCDValue[MinDigit]+'0';
       MinDigit++;
       if (MinDigit>10)
          break;
       i++;
   }
} else {
    OutStr[DigitCount-1]='0';
}
OutStr[DigitCount]=0;
return OutStr;
}


char* BCDToStr(char Value[6],unsigned char DigitCount)
{
int i, j, MaxDigit;
MaxDigit=0;
for (i=1; i<6; i++){if (Value[i]) {MaxDigit=i; break;}}
if (MaxDigit) {
   i=0;
   for (j=5-DigitCount+1; j<6; j++)
   {
   if ((Value[j]!=0))
       {OutStr[i]=Value[j]+48;}
    else {
    if (j>MaxDigit)
      {OutStr[i]=48;}
    else
     {OutStr[i]=32;} }
    i++;
  }
  if ((Value[0])&(OutStr[0]<49))
  {OutStr[0]=45;}
  OutStr[i]=0;
} else
{
    for (i=0; i<DigitCount-1; i++) OutStr[i]=32;
    OutStr[DigitCount-1]=48; OutStr[DigitCount]=0;
}
return OutStr;
}






void OutDate(unsigned int X, unsigned int Y)
{
// extern struct StDateTime DateTime;
extern ROM char DateSeparator[];

// GET_TIME();
#ifdef engl
 IntToStr(DateTime.Month,2);
#else
 IntToStr(DateTime.Day,2);
#endif
 if (OutStr[0]==' ') OutStr[0]='0';
 OutStr[2]=0;
 OutString1(X,Y,OutStr);
 OutString(X+CurFontWidth*2,Y,DateSeparator);
#ifdef engl
 IntToStr(DateTime.Day,2);
#else
 IntToStr(DateTime.Month,2);
#endif
 if (OutStr[0]==' ') OutStr[0]='0';
 OutStr[2]=0;
 OutString1(X+CurFontWidth*3,Y,OutStr);
 OutString(X+CurFontWidth*5,Y,DateSeparator);
 IntToStr(DateTime.Year%100,2);
 if (OutStr[0]==' ') OutStr[0]='0';
 OutStr[2]=0;
 OutString1(X+CurFontWidth*6,Y,OutStr);
}


void OutTime(unsigned int X, unsigned int Y)
{
// extern struct StDateTime DateTime;
 extern  char const TimeSeparator[];

// GET_TIME();
 IntToStr(DateTime.Hour,2);
 if (OutStr[0]==' ') OutStr[0]='0';
 OutStr[2]=0;
 OutString1(X,Y,OutStr);
 OutString(X+CurFontWidth*2,Y,TimeSeparator);
 IntToStr(DateTime.Min,2);
 if (OutStr[0]==' ') OutStr[0]='0';
 OutStr[2]=0;
 OutString1(X+CurFontWidth*3,Y,OutStr);
 OutString(X+CurFontWidth*5,Y,TimeSeparator);
 IntToStr(DateTime.Sec,2);
 if (OutStr[0]==' ') OutStr[0]='0';
 OutStr[2]=0;
 OutString1(X+CurFontWidth*6,Y,OutStr);
 }


void OutTimeNoSec(unsigned int X, unsigned int Y)
{
// extern struct StDateTime DateTime;
 extern  char TimeSeparator[];

// GET_TIME();
 IntToStr(DateTime.Hour,2);
 if (OutStr[0]==' ') OutStr[0]='0';
 OutStr[2]=0;
 OutString1(X,Y,OutStr);
 OutString(X+CurFontWidth*2,Y,TimeSeparator);
 IntToStr(DateTime.Min,2);
 if (OutStr[0]==' ') OutStr[0]='0';
 OutStr[2]=0;
 OutString1(X+CurFontWidth*3,Y,OutStr);
 }


/*
char *FloatToStr(char *str, float f, char c1, char c2)
{
  int l,l1;
  char tmp[LEN];
  char tmp1[LEN];
  int i,ii,neg;

  f*=1.000001;
  if (f<0) {neg=1; f=0-f;} else neg=0;
  l=f;
  sprintf(tmp1,"%ld",l);
  if (neg) {strcpy(tmp,"-"); strcat(tmp,tmp1); }
  else {strcpy(tmp,tmp1);}
  if (c2>0) {
         l1=10;
         for (i=1; i<c2; i++)  l1*=10;
         f=(f*l1)-(float)l*l1;
         l=f;
         if (l>0) {
                tmp1[0]=0;
                sprintf(tmp1,"%ld",l);
                strcat(tmp,".");
                for (ii=strlen(tmp1); ii<c2; ii++) strcat(tmp,"0");
                strcat(tmp,tmp1);
         } else
            {
            strcat(tmp,".");
            for (i=0;i<c2;i++) strcat(tmp,"0");
            }
  }
  i=strlen(tmp);
  if (i<c1) {
         strcpy(str," ");
         for (ii=1;ii<(c1-i);ii++) strcat(str," ");
         strcat(str,tmp);
  }  else {
         for (ii=0;ii<c1;ii++) str[ii]=tmp[ii];
         str[c1]=0;
  }


if (str[c1-1]=='.')
  {
  str[c1-1]=0;
  strcpy(tmp," ");
  strcat(tmp,str);
  strcpy(str,tmp);
  }
else {str[c1]=0;}


return str;
}
*/

/*
char *FloatToStr(char *str, float f, char c1, char c2)
{
int i,ic,neg;
char tmp[LEN],tmp1[LEN];
long i1,i2,mn;

if (f<0.0) { f=0.0-f; strcpy(tmp1,"-"); neg=1; c1--; }
   else { tmp1[0]=0; neg=0; }

mn=1;
ic=0;
while ((long)(f/(float)mn)>0) {
    mn*=10;
    ic++;
}
if (ic==0)
   ic=1;
if (c1<ic) {
   for (i=0;i<c1;i++)
       strcat(tmp1,"*");
   strcpy(str,tmp1);
   return str;
}

if (ic+1+c2>c1) {
   if (c1<ic+2) c2=0;
      else c2=c1-ic-1;
}

mn=1;
for (i=0;i<c2;i++)
    mn*=10;

f*=(float)mn;
i1=(long)f;
if ((f-(float)i1)>0.5)
   i1++;
i2=i1%mn;
i1/=mn;

strcat(tmp1,IntToStr(i1,ic));
if (c2) {
   strcat(tmp1,".");
   strcpy(tmp,IntToStr(i2,c2));
   i=0;
   while (tmp[i]) {
       if (tmp[i]==' ')
          tmp[i]='0';
       i++;
   }
   strcat(tmp1,tmp);
}

// �������� ������
if (neg)
   c1++;
i1=strlen(tmp1);
if (i1<c1) {
   for (i=i1;i>=0;i--)
       tmp1[i+c1-i1]=tmp1[i];
   for (i=0;i<c1-i1;i++)
       tmp1[i]=' ';
}

strcpy(str,tmp1);
return str;
}
*/

//------------------------------------------------------------------------------
char *FloatToStr(char *str, float f, char c1, char c2)
{
int i,ic,ic2,neg;
char tmp[LEN],tmp1[LEN];
long i1,i2,mn;

if (f<0.0) { f=0.0-f; strcpy(tmp1,"-"); neg=1; c1--; }
   else { tmp1[0]=0; neg=0; }
mn=1;
ic=0;
while ((long)(f/(float)mn)>0) {
    mn*=10;
    ic++;
}
if (ic==0)
   ic=1;

if (c1<ic)
   {
   FloatToStrOverflow:
   for (i=0;i<c1;i++)
       strcat(tmp1,"*");
   strcpy(OutStr,tmp1);
   return OutStr;
   }

if (ic+1+c2>c1) {
   if (c1<ic+2) c2=0;
      else c2=c1-ic-1;
}

mn=1;
for (i=0;i<c2;i++)
    mn*=10;

f*=(float)mn;
i1=(long)f;
if ((f-(float)i1)>0.5)
   {
   i1++;
   }

i2=i1%mn;
i1/=mn;

//--- ���� ����� ���� �������� ��� ���������� ������������ ��� ���������� � ���������� �������� �����
mn=1;
ic2=0;
while ((long)(i1/(float)mn)>0) {
    mn*=10;
    ic2++;
}
if (ic2==0) ic2=1;
if (c1<ic2) goto FloatToStrOverflow;
if (ic!=ic2)
   {
   if (ic2+1+c2>c1)
      {
      if (c1<ic2+2) c2=0;
      else c2=c1-ic2-1;
      }
   ic=ic2;
   }
//------------------------------------------------------------------------------

strcat(tmp1,IntToStr(i1,ic));
if (c2) {
   strcat(tmp1,".");
   strcpy(tmp,IntToStr(i2,c2));
   i=0;
   while (tmp[i]) {
       if (tmp[i]==' ')
          tmp[i]='0';
       i++;
   }
   strcat(tmp1,tmp);
}

// �������� ������
if (neg)
   c1++;
i1=strlen(tmp1);
if (i1<c1) {
   for (i=i1;i>=0;i--)
       tmp1[i+c1-i1]=tmp1[i];
   for (i=0;i<c1-i1;i++)
       tmp1[i]=' ';
}

strcpy(str,tmp1);
//strcpy(OutStr,tmp1);
return str;
}



//������� ����� ���� float
void OutFloat(unsigned int x, unsigned int y, float f, char d1, char d2)
{
OutString1(x,y,FloatToStr(OutStr,f,d1,d2));
}


//������� ����� ���� float
void OutFloatLeftZero(unsigned int x, unsigned int y, float f, char d1, char d2)
{
int i;
FloatToStr(OutStr,f,d1,d2);
for (i=0; i<d1; i++)
    if (OutStr[i]==' ') OutStr[i]='0';
    else break;
OutString1(x,y,OutStr);
}


char CurFontWidth=8;             //������ �������� �����
char CurFontHeight=8;            //������  �������� �����
char CurFontPtrNumer=Font8x8;    //��������� �������� �����


char CurFontPtr(unsigned int i1,unsigned int i2)
{
//#ifdef __emulator
char b;
  switch (CurFontPtrNumer){
    case Font6x5: { b=Fnt6x5[i1][i2]; return b;}
    case Font8x6: { b=Fnt8x6[i1][i2]; return b;}
    default:{ b=Fnt8x8[i1][i2]; return b;}
  }
//#else
//  return Fnt6x5[i1][i2];
//#endif
}


void SetFont(unsigned int FontType)
{
  switch (FontType){
    case Font6x5: {CurFontWidth=5; CurFontHeight=6; CurFontPtrNumer=Font6x5; /*Fnt6x5[0];*/ break; }
//#ifdef __emulator
    case Font8x6: {CurFontWidth=6; CurFontHeight=8; CurFontPtrNumer=Font8x6; /*Fnt8x6[0];*/ break; }
    default:{CurFontWidth=8; CurFontHeight=8; CurFontPtrNumer=Font8x8; /*&Fnt8x8[0];*/ break; }
//#endif
  }
}






void OutInt(unsigned int X,unsigned int Y,int IntValue,unsigned char DigitCount)
{ OutString(X,Y,IntToStr(IntValue,DigitCount)); }



void ClearRect2(unsigned int X1,unsigned int Y1,unsigned int X2,unsigned int Y2)
{
ClearRect(X1,Y1,Y2-Y1+1,X2-X1+1);
}



void DrawRect(unsigned int x1,unsigned int y1,
              unsigned int x2,unsigned int y2)
{
DrawLine(x1,y1,x2,y1);
DrawLine(x1,y2,x2,y2);
DrawLine(x1,y1,x1,y2);
DrawLine(x2,y1,x2,y2);
}

void int2bcd(int IntValue)
{
int i;
if (IntValue<0)
{
BCDValue[0]=1;
IntValue=-IntValue;
} else {
BCDValue[0]=0;
}
for (i=5;i>0;i--)
{
    BCDValue[i]=IntValue%10;
    IntValue=IntValue/10;
}
}

char* IntToStrFormat(s32 IntValue,u32 DigitCount)
{
char i, j, MaxDigit;
int2bcd(IntValue);
MaxDigit=0;
for (i=1; i<6; i++){if (BCDValue[i]) {MaxDigit=i; break;}}
if (MaxDigit) {
   i=0;
   for (j=5-DigitCount+1; j<6; j++)
   {
   if ((BCDValue[j]!=0))
       {OutStr[i]=BCDValue[j]+48;}
    else {
       if (j>MaxDigit)
        {OutStr[i]=48;}
     else
       {OutStr[i]=32;} }
     i++;
     }
   if ((BCDValue[0])&(OutStr[0]<49))
   {OutStr[0]=45;}
   OutStr[i]=0;
} else {
    for (i=0; i<DigitCount-1; i++) OutStr[i]=32;
    OutStr[DigitCount-1]=48; OutStr[DigitCount]=0;
}
return OutStr;
}

//---------- ----------
void InputTimeNoSec(u32 X,u32 Y,u32* Hour,u32* Min)
{
   unsigned char CurPos;
   char Value[6], ss1[3], ss2[3];
   unsigned int i1,i2;
   char EndInp,Pos;
   unsigned int ch;

   CurPos=0;
   EndInp=0;

   i1=*Hour;
   i2=*Min;
   IntToStrFormat(i1,2);
   Value[0]=OutStr[0];
   Value[1]=OutStr[1];
   if (Value[0]==' ') Value[0]='0';
   IntToStrFormat(i2,2);
   Value[3]=OutStr[0];
   Value[4]=OutStr[1];
   if (Value[3]==' ') Value[3]='0';
   Value[2]=':';
   Value[5]=0;

   while (1){
       OutString1(X,Y,Value);
       DrawLine(X+(5-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(5-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKey();
       Pos=4-CurPos;
       switch (ch) {
       case KB_ESC   : { EndInp=1;  break;}
       case KB_ENTER : {
                        ss1[0]=Value[0]; ss1[1]=Value[1]; ss1[2]=0;
                        i1=atoi(ss1);
                        ss2[0]=Value[3]; ss2[1]=Value[4]; ss2[2]=0;
                        i2=atoi(ss2);
                        if (IsTime(i1,i2,0)==1)
                          {EndInp=1; *Hour=i1; *Min=i2;;}
                        break;
                        }
       case KB_LEFT  : {if (CurPos<5-1) CurPos++;
                        if (CurPos==2) CurPos++;
                        break;}
       case KB_RIGHT : {if (CurPos>0) CurPos--;
                        if (CurPos==2) CurPos--;
                        break;}
#ifdef BigKB
       case  KB_0    : {Value[Pos]='0'; break;}
       case  KB_1    : {Value[Pos]='1'; break;}
       case  KB_2    : {Value[Pos]='2'; break;}
       case  KB_3    : {Value[Pos]='3'; break;}
       case  KB_4    : {Value[Pos]='4'; break;}
       case  KB_5    : {Value[Pos]='5'; break;}
       case  KB_6    : {Value[Pos]='6'; break;}
       case  KB_7    : {Value[Pos]='7'; break;}
       case  KB_8    : {Value[Pos]='8'; break;}
       case  KB_9    : {Value[Pos]='9'; break;}
#endif
       case  KB_UP   : {i1=Value[Pos]-'0';
                        if (i1<9) Value[Pos]++;
                        else Value[Pos]='0';
                        break;}
       case  KB_DOWN : {i1=Value[4-CurPos]-'0';
                        if (i1>0) Value[4-CurPos]--;
                        else Value[4-CurPos]='9';
                        break;}
       }
       if (EndInp) break;
   }
}


//---------- InputInt ----------
int InputInt(u32 X,u32 Y,s32 IntValue,u32 DigitCount)
{
   unsigned char CurPos,Pos;
   char EndInp;
   int ReturnValue;
   unsigned int ch;
   int2bcd(IntValue);
   CurPos=0;
   EndInp=0;
   while (1>0)
     {
       OutString1(X,Y,BCDToStr(BCDValue,DigitCount));
       DrawLine(X+(DigitCount-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(DigitCount-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKey();
       Pos=5-CurPos;
       switch (ch) {
       case KB_ESC   : { EndInp=1; ReturnValue=IntValue; break;}
       case KB_ENTER : { EndInp=1; ReturnValue=BCDToInt(BCDValue); break;}
       case KB_LEFT  : {if (CurPos<DigitCount-1) CurPos++; break;}
       case KB_RIGHT : {if (CurPos>0) CurPos--; break;}
#ifdef BigKB
       case KB_1     : {BCDValue[Pos]=1; break;}
       case KB_2     : {BCDValue[Pos]=2; break;}
       case KB_3     : {BCDValue[Pos]=3; break;}
       case KB_4     : {BCDValue[Pos]=4; break;}
       case KB_5     : {BCDValue[Pos]=5; break;}
       case KB_6     : {BCDValue[Pos]=6; break;}
       case KB_7     : {BCDValue[Pos]=7; break;}
       case KB_8     : {BCDValue[Pos]=8; break;}
       case KB_9     : {BCDValue[Pos]=9; break;}
       case KB_0     : {BCDValue[Pos]=0; break;}
#endif
           /*case  KB_UP    : {
                             if (BCDValue[Pos]<='-') BCDValue[Pos]=0;
                             else if (BCDValue[Pos]>=9) BCDValue[Pos]='-';
                             else BCDValue[Pos]++;
                             break;
                            }
           case  KB_DOWN  : {
                             if (BCDValue[Pos]<='-') BCDValue[Pos]=9;
                             else if (BCDValue[Pos]<=0) BCDValue[Pos]='-';
                             else BCDValue[Pos]--;
                             break;
                            }*/
       case KB_UP    : {if (BCDValue[Pos]<9) BCDValue[Pos]++;
                        else {
                          BCDValue[Pos]=0;
                          if (BCDValue[0]) BCDValue[0]=0; else BCDValue[0]=1;
                          }
                        break;}
       case KB_DOWN  : {if (BCDValue[Pos]>0) BCDValue[Pos]--;
                        else {
                          BCDValue[Pos]=9;
                          if (BCDValue[0]) BCDValue[0]=0; else BCDValue[0]=1;
                          }
                        break;}
       case KB_MOD : {if (BCDValue[0]) BCDValue[0]=0; else BCDValue[0]=1; break;}
#ifdef BigKB
       case KB_MINUS : {if (BCDValue[0]) BCDValue[0]=0; else BCDValue[0]=1; break;}
#endif
       }
       if ((CurPos==DigitCount-1)&(BCDValue[5-CurPos])) BCDValue[0]=0;
       if (EndInp) break;
   }
   OutInt(X,Y,ReturnValue,DigitCount);
   return ReturnValue;
}

//---------- ----------
void pFloatToStr(char *str, float f, char c1, char c2)
{ long l,l1;
  char tmp[LEN];
  char tmp1[LEN];
  char i,ii,neg;

  if (f<0) {neg=1; f=0-f;} else neg=0;
  l=f;
  sprintf(tmp1,"%ld",l);
  if (neg) {strcpy(tmp,"-"); strcat(tmp,tmp1); }
  else {strcpy(tmp,tmp1);}
  if (c2>0) {
         l1=10;
         for (i=1; i<c2; i++)  l1*=10;
         f=(f*l1)-(float)l*l1;
         l=f;
         if (l>0) {
                tmp1[0]=0;
                sprintf(tmp1,"%ld",l);
                strcat(tmp,".");
                for (ii=strlen(tmp1); ii<c2; ii++) strcat(tmp,"0");
                strcat(tmp,tmp1);
         } else
            {
            strcat(tmp,".");
            for (i=0;i<c2;i++) strcat(tmp,"0");
            }
  }
  i=strlen(tmp);
  if (i<c1) {
           strcpy(str," ");
           for (ii=1;ii<(c1-i);ii++) strcat(str," ");
         strcat(str,tmp);
  }  else {
             for (ii=0;ii<c1;ii++) str[ii]=tmp[ii];
           str[c1]=0;
  }
}

float StrToFloat(char* str)
{ int i,j;
  long Value1=0,Value2=0,Mul1,Mul2;
  float f;
  char Minus=0;

  Mul1=1;
  for (i=strlen(str)-1;i>=0;i--) {
      if ((str[i]>='0')&&(str[i]<='9')) Value1+=(str[i]-'0')*Mul1;
      else if (str[i]=='-') Minus=1;
           else if (str[i]=='.') break;
      Mul1*=10;
  }
  if (i>0) {
     j=i;
     Mul2=1;
     for (i=j-1;i>=0;i--) {
         if ((str[i]>='0')&&(str[i]<='9')) Value2+=(str[i]-'0')*Mul2;
         else if (str[i]=='-') {Minus=1; /*Value2=0-Value2;*/ break;}
              else {break;}
         Mul2*=10;
     }
  } else {
    Value2=Value1;
    Value1=0;
  }
  f=(float)Value2+(float)Value1/(float)Mul1;
  if (Minus==1) return (0.0-f);
  else return f;
}

#define MaxStrInp 30

float InputFloat(u32 X,u32 Y,float Value,u32 DigitCount,u32 d2)
{
char i,CurPos,Length,Str[MaxStrInp],OldStr[MaxStrInp];//,Curs[2];
unsigned int ch;

if (d2==0)
   return InputInt(X,Y,floor(Value),DigitCount);

//SetFont(Font);

Length=DigitCount;

if (Length>MaxStrInp-1)
   Length=MaxStrInp-1;

pFloatToStr(Str,Value,Length,d2);
for (i=Length;i<DigitCount;i++)
    Str[i]=' ';
Str[DigitCount]=0;
strcpy(OldStr,Str);

CurPos=0;
for (i=0;i<DigitCount-1;i++)
  if (Str[i]==' ')
    CurPos++;
//Curs[1]=0;

while (1){
       OutString1(X,Y,Str);
//       Curs[0]=Str[CurPos];
       //OutString1Inv(X+CurPos*CurFontWidth,Y,Curs);
       DrawLine(X+(CurPos)*CurFontWidth,Y+CurFontHeight-1,X+(CurPos+1)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKey();

       switch (ch) {
           case KB_ESC   : {
                             OutString1(X,Y,OldStr);
                             return Value;
                           }
           case KB_ENTER : { Value=StrToFloat(Str);
                             pFloatToStr(Str,Value,DigitCount,d2);
                             OutString1(X,Y,Str);
                             return Value;
                           }
#define DoRight() if (CurPos<Length-1) CurPos++; \
                  if (CurPos==DigitCount-d2-1) CurPos++; \
                  break;

#ifdef BigKB
           case KB_0     : { Str[CurPos]='0'; DoRight(); }
           case KB_1     : { Str[CurPos]='1'; DoRight(); }
           case KB_2     : { Str[CurPos]='2'; DoRight(); }
           case KB_3     : { Str[CurPos]='3'; DoRight(); }
           case KB_4     : { Str[CurPos]='4'; DoRight(); }
           case KB_5     : { Str[CurPos]='5'; DoRight(); }
           case KB_6     : { Str[CurPos]='6'; DoRight(); }
           case KB_7     : { Str[CurPos]='7'; DoRight(); }
           case KB_8     : { Str[CurPos]='8'; DoRight(); }
           case KB_9     : { Str[CurPos]='9'; DoRight(); }
           case KB_PLUS  : { if (Str[CurPos]=='-') Str[CurPos]=' ';
                                else Str[CurPos]='-';
                             DoRight();
                           }
           case KB_COMMA : { CurPos=DigitCount-d2;
                             break;
                           }
#endif
           case KB_RIGHT : {
                             DoRight();
                           }
           case KB_LEFT  : {
                             if (CurPos>0) {
                                CurPos--;
                             }
                             if (CurPos==DigitCount-d2-1) CurPos--;
                             break;
                           }
           case  KB_UP    : {
                             if (Str[CurPos]<='-') Str[CurPos]='0';
                             else if (Str[CurPos]>='9') Str[CurPos]='-';
                             else Str[CurPos]++;
                             break;
                            }
           case  KB_DOWN  : {
                             if (Str[CurPos]<='-') Str[CurPos]='9';
                             else if (Str[CurPos]<='0') Str[CurPos]='-';
                             else Str[CurPos]--;
                             break;
                            }
#undef DoRight
       }
   }
}

long StrToLong(char* str)
{ int i;
  long Value=0,Mul;

  Mul=1;
  for (i=strlen(str)-1;i>=0;i--) {

      if ((str[i]>='0')&&(str[i]<='9')) Value+=(str[i]-'0')*Mul;
      else if (str[i]!='-') return Value;
           else {return 0-Value; }
      Mul*=10;
  }
  return Value;
}

int StrToInt(char* str)
{ return StrToLong(str); }


//---------- ----------
void InputDate(u32 X,u32 Y,u32* Day,u32* Month,u32 * Year)
//void InpDateUpDown(unsigned int X,unsigned int Y,char* Day,char* Month,unsigned int* Year)
{
extern ROM char DateSeparator[];

#define Digits 7
   unsigned char CurPos;//, i;
   char Value[11], /*ss1[3], ss2[3],*/ ss3[5],tmp;
   int i1,i2,i3;
   char EndInp;
   unsigned int ch;
   u16 t;

   CurPos=0;
   EndInp=0;

   i1=*Day;
   i2=*Month;
   i3=(*Year%100);

#ifdef engl
   IntToStr(i2,2);
   Value[0]=OutStr[0];
   Value[1]=OutStr[1];
   if (Value[0]==' ') Value[0]='0';
   IntToStr(i1,2);
   Value[3]=OutStr[0];
   Value[4]=OutStr[1];
   if (Value[3]==' ') Value[3]='0';
#else
   IntToStrFormat(i1,2);
   Value[0]=OutStr[0];
   Value[1]=OutStr[1];
   if (Value[0]==' ') Value[0]='0';
   IntToStrFormat(i2,2);
   Value[3]=OutStr[0];
   Value[4]=OutStr[1];
   if (Value[3]==' ') Value[3]='0';
#endif

   IntToStrFormat(i3,2);
   Value[6]=OutStr[0];
   Value[7]=OutStr[1];
   if (Value[6]==' ') Value[6]='0';
   if (Value[7]==' ') Value[7]='0';
   Value[2]=(char)DateSeparator[0];
   Value[5]=(char)DateSeparator[0];
   Value[8]=0;

   while (1){
       OutString1(X,Y,Value);
       DrawLine(X+(Digits-CurPos)*CurFontWidth+1,Y+CurFontHeight-1,X+(Digits-CurPos+1)*CurFontWidth-1,Y+CurFontHeight-1);
#ifndef __arm9
       Redraw();
#endif
       ch=WaitReadKey();
       switch (ch) {
       case KB_ESC   : { EndInp=1;  break;}
       case KB_ENTER : { EndInp=1;
#ifdef engl
                         ss3[0]='0'; ss3[1]='0'; ss3[4]=0;
                         ss3[2]=Value[0]; ss3[3]=Value[1];
                         t=StrToInt(ss3);
                         *Month=t;
                         ss3[2]=Value[3]; ss3[3]=Value[4];
                         t=StrToInt(ss3);
                         *Day=t;
#else
                         ss3[0]='0'; ss3[1]='0'; ss3[4]=0;
                         ss3[2]=Value[0]; ss3[3]=Value[1];
                         t=StrToInt(ss3);
                         *Day=t;
                         ss3[2]=Value[3]; ss3[3]=Value[4];
                         t=StrToInt(ss3);
                         *Month=t;
#endif
                         ss3[0]=Value[6]; ss3[1]=Value[7]; ss3[2]=Value[8]; ss3[3]=Value[9]; ss3[4]=0;
                         t=StrToInt(ss3);
                         *Year=(t%100);
                         if (((*Month==4)||(*Month==6)||(*Month==9)||(*Month==11))&&(*Day>30))
                            *Day=30;
                         if (*Year%4) {
                            if ((*Month==2)&&(*Day>28)) *Day=28;
                         } else
                            if ((*Month==2)&&(*Day>29)) *Day=29;
                         break;}
       case KB_LEFT  : {if (CurPos<Digits) CurPos++;
                        if ((CurPos==5)|(CurPos==2)) CurPos++;
                         break;}
       case KB_RIGHT : {if (CurPos>0) CurPos--;
                        if ((CurPos==5)|(CurPos==2)) CurPos--;
                        break;}
       case  KB_UP   : { tmp=Value[Digits-CurPos]-'0';
                         if (tmp<9) tmp++;
                         switch (tmp){
#ifdef engl
                               case  0: {if ((CurPos==5)&(Value[3]=='0')) break;
                                         if ((CurPos==8)&(Value[0]=='0')) break;
                                         Value[Digits-CurPos]='0';
                                          break;}
                               case  1: {
                                         if ((CurPos==9)&(Value[1]>'2')) break;
                                         Value[Digits-CurPos]='1';
                                         break;}
                               case  2: {if (CurPos==9) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='2'; break;}
                               case  3: {if (CurPos==9) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         if ((CurPos==6)&(Value[4]>'1')) break;
                                         Value[Digits-CurPos]='3'; break;}
                               case  4: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='4'; break;}
                               case  5: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='5'; break;}
                               case  6: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='6'; break;}
                               case  7: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='7'; break;}
                               case  8: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='8'; break;}
                               case  9: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='9'; break;}
#else
                               case  0: {if ((CurPos==8)&(Value[0]=='0')) break;
                                         if ((CurPos==5)&(Value[3]=='0')) break;
                                         Value[Digits-CurPos]='0';
                                          break;}
                               case  1: {
                                         if ((CurPos==6)&(Value[4]>'2')) break;
                                         Value[Digits-CurPos]='1';
                                         break;}
                               case  2: {if (CurPos==6) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='2'; break;}
                               case  3: {if (CurPos==6) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         if ((CurPos==9)&(Value[1]>'1')) break;
                                         Value[Digits-CurPos]='3'; break;}
                               case  4: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='4'; break;}
                               case  5: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='5'; break;}
                               case  6: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='6'; break;}
                               case  7: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='7'; break;}
                               case  8: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='8'; break;}
                               case  9: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='9'; break;}
#endif
                         }
                         break;
                       }
       case  KB_DOWN   : { tmp=Value[Digits-CurPos]-'0';
                         if (tmp>0) tmp--;
                         switch (tmp){
#ifdef engl
                               case  0: {if ((CurPos==5)&(Value[3]=='0')) break;
                                         if ((CurPos==8)&(Value[0]=='0')) break;
                                         Value[Digits-CurPos]='0';
                                          break;}
                               case  1: {if ((CurPos==9)&(Value[1]>'2')) break;
                                         Value[Digits-CurPos]='1';
                                         break;}
                               case  2: {if (CurPos==9) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='2'; break;}
                               case  3: {if (CurPos==9) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         if ((CurPos==6)&(Value[4]>'1')) break;
                                         Value[Digits-CurPos]='3'; break;}
                               case  4: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='4'; break;}
                               case  5: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='5'; break;}
                               case  6: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='6'; break;}
                               case  7: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='7'; break;}
                               case  8: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='8'; break;}
                               case  9: {if ((CurPos==6)|(CurPos==4)) break;
                                         if ((CurPos==5)&(Value[3]>'2')) break;
                                         Value[Digits-CurPos]='9'; break;}
#else
                               case  0: {if ((CurPos==8)&(Value[0]=='0')) break;
                                         if ((CurPos==5)&(Value[3]=='0')) break;
                                         Value[Digits-CurPos]='0';
                                          break;}
                               case  1: {if ((CurPos==6)&(Value[4]>'2')) break;
                                         Value[Digits-CurPos]='1';
                                         break;}
                               case  2: {if (CurPos==6) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='2'; break;}
                               case  3: {if (CurPos==6) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         if ((CurPos==9)&(Value[1]>'1')) break;
                                         Value[Digits-CurPos]='3'; break;}
                               case  4: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='4'; break;}
                               case  5: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='5'; break;}
                               case  6: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='6'; break;}
                               case  7: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='7'; break;}
                               case  8: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='8'; break;}
                               case  9: {if ((CurPos==9)|(CurPos==4)) break;
                                         if ((CurPos==8)&(Value[0]>'2')) break;
                                         Value[Digits-CurPos]='9'; break;}
#endif
                         }
                         break;
                       }

       }
       if (EndInp) break;
   }
}

//---------- ----------
void InputTime(u32 X,u32 Y,u32 * Hour,u32 * Min,u32 * Sec)
{
   unsigned char CurPos;
   char Value[9], ss1[3], ss2[3], ss3[3];
   unsigned int i1,i2,i3;
   char EndInp,Pos;
   unsigned int ch;

   CurPos=0;
   EndInp=0;

   i1=*Hour;
   i2=*Min;
   i3=*Sec;
   IntToStrFormat(i1,2);
   Value[0]=OutStr[0];
   Value[1]=OutStr[1];
   if (Value[0]==' ') Value[0]='0';
   IntToStrFormat(i2,2);
   Value[3]=OutStr[0];
   Value[4]=OutStr[1];
   if (Value[3]==' ') Value[3]='0';
   IntToStrFormat(i3,2);
   Value[6]=OutStr[0];
   Value[7]=OutStr[1];
   if (Value[6]==' ') Value[6]='0';
   Value[2]=':';
   Value[5]=':';
   Value[8]=0;

   while (1>0){
       OutString1(X,Y,Value);
       DrawLine(X+(8-CurPos-1)*CurFontWidth,Y+CurFontHeight-1,X+(8-CurPos)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKey();
       Pos=7-CurPos;
       switch (ch) {
       case KB_ESC   : { EndInp=1;  break;}
       case KB_ENTER : {
                        ss1[0]=Value[0]; ss1[1]=Value[1]; ss1[2]=0;
                        i1=atoi(ss1);
                        ss2[0]=Value[3]; ss2[1]=Value[4]; ss2[2]=0;
                        i2=atoi(ss2);
                        ss3[0]=Value[6]; ss3[1]=Value[7]; ss3[2]=0;
                        i3=atoi(ss3);
                        if (IsTime(i1,i2,i3)==1)
                          {EndInp=1; *Hour=i1; *Min=i2; *Sec=i3;}
                        else
                         {/*Beep();*/}
                        break;
                        }
       case KB_LEFT  : {if (CurPos<8-1) CurPos++;
                        if ((CurPos==2)|(CurPos==5)) CurPos++;
                        break;}
       case KB_RIGHT : {if (CurPos>0) CurPos--;
                        if ((CurPos==2)|(CurPos==5)) CurPos--;
                        break;}
#ifdef BigKB
       case  KB_0    : {Value[Pos]='0'; break;}
       case  KB_1    : {Value[Pos]='1'; break;}
       case  KB_2    : {Value[Pos]='2'; break;}
       case  KB_3    : {Value[Pos]='3'; break;}
       case  KB_4    : {Value[Pos]='4'; break;}
       case  KB_5    : {Value[Pos]='5'; break;}
       case  KB_6    : {Value[Pos]='6'; break;}
       case  KB_7    : {Value[Pos]='7'; break;}
       case  KB_8    : {Value[Pos]='8'; break;}
       case  KB_9    : {Value[Pos]='9'; break;}
#endif
       case  KB_UP   : {i1=Value[Pos]-'0';
                        if (i1<9) Value[Pos]++;
                        else Value[Pos]='0';
                        break;}
       case  KB_DOWN : {i1=Value[7-CurPos]-'0';
                        if (i1>0) Value[7-CurPos]--;
                        else Value[7-CurPos]='9';
                        break;}
       }
       if (EndInp) break;
   }
}

void OutStringCenter(u32 x,u32 y,u32 len,char ROM *str)
{
char i=0;
while (str[i]!=0) {i++;}
OutString(x+(len-(CurFontWidth*i))/2,y,str);
//OutString(0,y,str);
}


char fStrLen(char ROM *s)
{
char i;
if (s[0]==0) return 0;
i=1;
while (s[i]) i++;
return i;
}

char* LongToStr(s32 l)
{long Div=1000000000,tmp;
 int i,Value,j=0,IsDigit=0;

 if (l!=0) {
    if (l<0) {OutStr[0]='-'; j++; l=0-l;}
    tmp=l;
    for (i=0;i<10;i++) {
        Value=tmp/Div;
        tmp-=Value*Div;
        Div/=10;
        if (Value) IsDigit=1;
        if (IsDigit) {
           OutStr[j++]=48+Value;
        }
    }
 }else {OutStr[0]='0'; j++;}
 OutStr[j]=0;
 return OutStr;
}

char* FloatToStrSpec(float f, u32 Dgts)
{
 long l,l1;
  char tmpOutStr[LEN],Minus=0;
  int i,j;
  float f1;

  if (f<0) {
     Minus=1;
     l=f;
     f1=(-f+l);
  } else {
     l=f;
     f1=(f-l);
  }

  l1=(f1*10000.0);

  if ((f1*10000.0)-(float)l1>=0.5) {
     l1++;
     if (l1>=10000.0) {l++; l1=0;}
  }

  LongToStr(l1);
  j=0;
  if (l1) {
     if (l1<10) j=3;
        else if (l1<100) j=2;
             else if (l1<1000) j=1;
  }
  for (i=0;i<j;i++) tmpOutStr[i]='0';

  for (i=j;i<LEN;i++) {
      tmpOutStr[i]=OutStr[i-j];
      if (OutStr[i-j]==0) break;
  }
  if ((l==0)&&(Minus)) {
     OutStr[0]='-';
     OutStr[1]='0';
     OutStr[2]=0;
  }  else LongToStr(l);

  for (i=0;i<LEN;i++) {
      if (OutStr[i]==0) break;
  }
  OutStr[i]='.';
  for (j=i+1;j<LEN;j++) {
      OutStr[j]=tmpOutStr[j-(i+1)];
      if (tmpOutStr[j-(i+1)]==0) break;
  }
  OutStr[LEN-1]=0;
return OutStr;
}


